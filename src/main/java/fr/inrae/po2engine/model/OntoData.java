/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import com.github.sardine.DavResource;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.application.Platform;
import javafx.beans.property.*;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.json.JSONArray;
import org.json.JSONException;
import org.yaml.snakeyaml.util.UriEncoder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

public abstract class OntoData {

    protected StringProperty name;
    protected Date creationDate;
    protected Date modifiedDate;
    protected Boolean publiq;
    protected String downloadLink;
    protected String davPath;
    protected BooleanProperty publicloaded;
    protected BooleanProperty privateLoaded;
    protected BooleanProperty localLoaded;
    protected BooleanProperty loaded;
    protected BooleanProperty synchroCloud;
    protected static Logger logger = LogManager.getLogger(OntoData.class);
    protected String group;
    protected IntegerProperty majorVersion;
    protected IntegerProperty minorVersion;

    protected Boolean local;
    //    private File ontologyFile;
    protected Boolean lock;
    protected BooleanProperty canRead;
    protected BooleanProperty canWrite;
    protected String description;
    protected JSONArray log;
    protected String semDescription;
    protected String semURIRepository;
    protected String semStatementSize;
    protected String currentVersionLoaded;
    protected String davFileID;
    protected BooleanProperty modified;
    protected Boolean inError;
    protected Boolean isInit;

    public OntoData(String name) {
        this.isInit = false;
        this.group = null;
        this.downloadLink = null;
        this.creationDate = null;
        this.modifiedDate = null;
        this.localLoaded = new SimpleBooleanProperty(false);
        this.publicloaded = new SimpleBooleanProperty(false);
        this.privateLoaded = new SimpleBooleanProperty(false);
        this.synchroCloud = new SimpleBooleanProperty(false);
        this.modified = new SimpleBooleanProperty(false);
        this.inError = false;

        this.loaded = new SimpleBooleanProperty(false);
        this.loaded.bind(localLoaded.or(publicloaded.or(privateLoaded)));
        this.name = new SimpleStringProperty(name);
        this.publiq = null;
        this.local = true;
//        this.ontologyFile = null;
        this.lock = false;
        this.canRead = new SimpleBooleanProperty(false);
        this.canWrite = new SimpleBooleanProperty(false);
        this.majorVersion = new SimpleIntegerProperty(0);
        this.minorVersion = new SimpleIntegerProperty(0);
        this.description = null;
        this.log = new JSONArray();
        this.semDescription = "";
        this.semURIRepository = "";
        this.semStatementSize = "";
        this.currentVersionLoaded = "";
        this.davFileID = "";
    }

    public void initResource() {
        if(!local && !isInit) {
            Tools.addProgress(ProgressPO2.OPEN, "retrieving informations");
            try {
                CloudConnector.searchDavFileId(this);
                CloudConnector.searchProperties(this);
                RDF4JTools.initRepositotyInfos(this);
                isInit = true;
            } catch (IOException | RepositoryException e) {
                logger.error(e.getClass() + ":" + e.getMessage(), e);
            }
            Tools.delProgress(ProgressPO2.OPEN);
        }
    }

    public boolean isInit() {
        return this.isInit;
    }

    public IntegerProperty getMajorVersion() {
        return this.majorVersion;
    }

    public IntegerProperty getMinorVersion() {
        return this.minorVersion;
    }


    public void setMajorVersion(String s) {
        try {
            this.majorVersion.set(Integer.parseInt(s));
        } catch (NumberFormatException e) {
            this.majorVersion.set(0);
        }
    }

    public void setMinorVersion(String s) {
        try {
            this.minorVersion.set(Integer.parseInt(s));
        } catch (NumberFormatException e) {
            this.minorVersion.set(0);
        }
    }

    public void incMajorVersion() {
        this.majorVersion.set(this.majorVersion.get()+1);
    }

    public void incMinorVersion() {
        this.minorVersion.set(this.minorVersion.get()+1);
    }

    public boolean isModified() {
        return modified.get();
    }

    public void setModified(boolean modified) {
        this.modified.set(modified);
    }

    public BooleanProperty modifiedProperty() {
        return modified;
    }

    public abstract String getType();

    public String getDavFileID() {
        return davFileID;
    }

    public Boolean isSynchro() {
        Boolean sync = false;
        if(currentVersionLoaded != null) {
            try {
                MutablePair<String, Date> onlineVersion = CloudConnector.getDavVersion(this);
                if(onlineVersion.getLeft() != null) {
                    sync = onlineVersion.getLeft().equals(currentVersionLoaded);
                } else {
                    sync = currentVersionLoaded.isEmpty();
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        synchroCloud.setValue(sync);
        return sync;
    }

    public void setDavFileID(String davFileID) {
        this.davFileID = davFileID;
    }

    public String getCurrentVersionLoaded() {
        return currentVersionLoaded;
    }

    public void setCurrentVersionLoaded(String currentVersionLoaded) {
        logger.debug("setting version for " + getName().get() + " - " + currentVersionLoaded);
        this.currentVersionLoaded = currentVersionLoaded;
    }

    public String getSemDescription() {
        return semDescription;
    }

    public void setSemDescription(String semDescription) {
        this.semDescription = semDescription;
    }

    public String getSemURIRepository() {
        return semURIRepository;
    }

    public void setSemURIRepository(String semURIRepository) {
        this.semURIRepository = semURIRepository;
    }

    public String getSemStatementSize() {
        return semStatementSize;
    }

    public void setSemStatementSize(String semStatementSize) {
        this.semStatementSize = semStatementSize;
    }

    public BooleanProperty isPublicLoaded() {
        return this.publicloaded;
    }

    public BooleanProperty isPrivateLoaded() {
        return this.privateLoaded;
    }

    public BooleanProperty isSynchroCloud() {
        return this.synchroCloud;
    }

    public BooleanProperty isLocalLoaded() {
        return this.localLoaded;
    }

    public BooleanProperty isLoaded() {
        return loaded;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public Boolean isPublic() {
        return publiq;
    }

    public void setPublic(Boolean publiq) {
        this.publiq = publiq;
    }

    public StringProperty getName() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setLocal(Boolean local) {
        this.local = local;
    }

    public Boolean isLocal() {
        return local;
    }

    public String getDavPath() {
        return this.davPath;
    }

    public void setDavPath(String path) {
        this.davPath = path;
    }


    public Boolean isLocked() {
        if (lock != null) {
            return lock;
        }
        return false;
    }

    public void setRead(Boolean canRead) {
        this.canRead.set(canRead);
    }

    public void setWrite(Boolean canWrite) {
        this.canWrite.set(canWrite);
    }

    public BooleanProperty canWrite() {
        return canWrite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getModifiedDate() {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modified) {
        this.modifiedDate = modified;
    }

    public void addLog(String newLog) {
        this.log.put(newLog);
    }

    public String getOntoDataLogString() {
        StringBuilder builder = new StringBuilder();
        for (int i = this.log.length() - 1; i >= 0; i--) {
            builder.append(" - " + this.log.getString(i) + "\n");
        }
        return builder.toString();
    }

    public String getOntoDataLog() {
        return this.log.toString();
    }

    public void setOntoDataLog(String log) {
        try {
            JSONArray o = new JSONArray(log);
            this.log = o;
        } catch (JSONException e) {
            System.err.println("arg + " + log);
        }
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String groupName) {
        this.group = groupName;
    }

    public void setInError(boolean b) {
        this.inError = b;
    }

}
