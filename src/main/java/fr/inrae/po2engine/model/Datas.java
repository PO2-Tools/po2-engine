/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import java.util.Map;
import java.util.TreeMap;

public class Datas {

    private static TreeMap<String, OntoData> listDatas = new TreeMap<>(String::compareToIgnoreCase);

    private Datas() {
    }

    public static Data addData(String dataName) {
        Data d = null;
        if (!listDatas.containsKey(dataName)) {
            d = new Data(dataName);
            listDatas.put(dataName, d);
        } else {
            if (listDatas.get(dataName).isLocal()) {
                d = (Data)listDatas.get(dataName);
            }
        }
        return d;
    }

    public static Data getData(String dataName) {
        return (Data)listDatas.get(dataName);
    }

    public static Map<String, OntoData> getDatas() {
        return listDatas;
    }


}
