/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.exception.CantWriteException;
import fr.inrae.po2engine.exception.LocalLockException;
import fr.inrae.po2engine.exception.NotLoginException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFWriter;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Ontology extends OntoData{

//    private StringProperty name;
//    private Date creationDate;
//    private Date modifiedDate;
//    private Boolean publiq;
//    private String downloadLink;
//    private String davPath;
//    private Boolean publicloaded;
//    private Boolean privateLoaded;
//    private Boolean localLoaded;
//    private Boolean local;

    public static String Attribut = JenaTools.SSN+"Property";
    public static String Process = "http://opendata.inrae.fr/PO2/core/Transformation_Process";
    public static String Component = "http://opendata.inrae.fr/PO2/core/component";
    public static String Material = JenaTools.SOSA + "Actuator";
    public static String Method = JenaTools.SOSA + "Procedure";
    public static String Step = "http://opendata.inrae.fr/PO2/core/step";
    public static String Scale = "http://opendata.inrae.fr/PO2/core/observationScale";

    private File ontologyFile;

    private static ObservableList<VocabConcept> listProcess = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listMethod = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listScale = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listMaterial = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listStep = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listAttribute = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listComponent = FXCollections.observableArrayList();
    private static ObservableList<String> listUnit = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listObservation = FXCollections.observableArrayList();
    private static ObservableList<VocabConcept> listObject = FXCollections.observableArrayList();

    private ObservableList<SkosScheme> listSkosScheme = FXCollections.observableArrayList();
    private SkosScheme mainScheme;
    private SkosScheme currentScheme;
    private SimpleBooleanProperty showDeprecated = new SimpleBooleanProperty(false);

    private Set<String> listAuthor = new HashSet<>();
    private StringProperty listKeyword = new SimpleStringProperty();
    private VocabConcept rootNode;

    private OntModel domainModel;
    private String DOMAIN_NS;
    private OntModel coreModel;
    private String CORE_NS;

    private String baseURI;
    private static Logger logger = LogManager.getLogger(Ontology.class);



//    private Boolean lock;
//    private BooleanProperty canRead;
//    private BooleanProperty canWrite;
//    private String description;

    public Ontology(String name) {
        super(name);
        this.ontologyFile = null;
        coreModel = JenaTools.ReadCoreModel();
        CORE_NS = coreModel.getNsPrefixURI("PO2");
    }

    public String getListKeyword() {
        return listKeyword.get();
    }

    public StringProperty listKeywordProperty() {
        return listKeyword;
    }

    public void setListKeyword(String listKeyword) {
        this.listKeyword.set(listKeyword);
    }

    public String generateURI(String prefix) {
        String uriTemp = "";
        while (uriTemp.isEmpty()) {
            String temp = Tools.normalize(this.getBaseURI()  + prefix+"_"+Tools.shortUUID());
            if(isUniqueURI(temp)) {
                uriTemp = temp;
            }
        }
        return uriTemp;
    }

    public void setDomainModel(OntModel domainModel) {
        this.domainModel = domainModel;
        DOMAIN_NS = domainModel.getNsPrefixURI("domain");
    }

    public String getDOMAIN_NS() {
        return this.DOMAIN_NS;
    }

    public OntModel getDomainModel() {
        return this.domainModel;
    }

    public OntModel getCoreModel() {
        return this.coreModel;
    }

    public String getCORE_NS() {
        return this.CORE_NS;
    }

    private Boolean isUniqueURI(String uri) {
        return isUniqueURI(uri, rootNode);
    }

    private Boolean isUniqueURI(String uri, VocabConcept node) {
        if(node.getURI().equalsIgnoreCase(uri)) {
            return false;
        }
        for(VocabConcept son : node.getSubNode()) {
            if(!isUniqueURI(uri, son)) {
                return false;
            }
        }
        return true;
    }

    public String getBaseURI() {
        return baseURI;
    }

    public void setBaseURI(String baseURI) {
        this.baseURI = baseURI;
    }

    public VocabConcept getNodeByURI(String uri) {
        if(rootNode != null) {
            return rootNode.getNodeByURI(uri);
        }
        return null;
    }

    public VocabConcept getRootNode() {
        return this.rootNode;
    }

    public void setRootNode(VocabConcept root) {
        this.rootNode = root;
    }

    private void clear() {
        this.mainScheme = null;
        this.currentScheme = null;
        this.listSkosScheme.clear();
        listProcess.clear();
        listMethod.clear();
        listScale.clear();
        listMaterial.clear();
        listStep.clear();
        listAttribute.clear();
        listComponent.clear();
        listUnit.clear();
        listObservation.clear();
        listObject.clear();
        this.listAuthor.clear();
        this.privateLoaded.set(false);
        this.publicloaded.set(false);
        this.localLoaded.set(false);
    }

    public void addAuthor(String author) {
        listAuthor.add(author);
    }
    public Set<String> getListAuthor() {
        return listAuthor;
    }

    @Override
    public String getType() {
        return "ONTOLOGY";
    }

    private void loadDav() {
        File ontoFile = CloudConnector.getDavOntology(this);
        if (ontoFile != null) {
            ontologyFile = ontoFile;
            privateLoaded.setValue(true);
        }
    }

    public SkosScheme createSkosScheme(String name) {
        SkosScheme s = new SkosScheme(this.generateURI("cs"));
        s.setName(name);
        addSkosScheme(s);
        return s;
    }

    public SkosScheme createSkosSchemeByURI(String schemeURI) {
        if(!schemeURI.isEmpty()) {
            SkosScheme sc = this.listSkosScheme.filtered(skosScheme -> skosScheme.getUri().equals(schemeURI)).stream().findFirst().orElse(null);
            if(sc == null) {
                sc = new SkosScheme(schemeURI);
                addSkosScheme(sc);
                return sc;
            }
        }
        return null;
    }

    public SkosScheme getSkosSchemeByURI(String uri) {
        return this.listSkosScheme.filtered(skosScheme -> skosScheme.getUri().equals(uri)).stream().findFirst().orElse(null);
    }

    public SkosScheme getSkosSchemeByName(String name) {
        return this.listSkosScheme.filtered(skosScheme -> skosScheme.getName().toLowerCase().equals(name.toLowerCase())).stream().findFirst().orElse(null);
    }

    public void addSkosScheme(SkosScheme scheme) {
        listSkosScheme.add(scheme);
    }

    public void setMainSkosScheme(SkosScheme main) {
        this.mainScheme = main;
        this.currentScheme = this.mainScheme;
    }

    public void setCurrentSkosScheme(SkosScheme current) {
        this.currentScheme = current;
    }

    public void setCurrentSkosScheme(String name) {
        SkosScheme s = listSkosScheme.stream().filter(skosScheme -> skosScheme.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
        if(s != null) {
            setCurrentSkosScheme(s);
        }
    }

    public SkosScheme getCurrentScheme() {
        return currentScheme;
    }
    
    public SkosScheme getSkosScheme(String uri) {
        return listSkosScheme.stream().filter(skosScheme -> skosScheme.getUri().equals(uri)).findFirst().orElse(null);
    }

    public ObservableList<SkosScheme> getlistConceptScheme() {
        return this.listSkosScheme;
    }

    public SkosScheme getMainScheme() {
        return this.mainScheme;
    }


    public JSONArray getVocabJSON() {
        JSONArray vocab = new JSONArray();
        // list Process
        JSONArray processArray = new JSONArray();
        processArray.put("process");
        processArray.put("process@en");
        vocab.put(processArray);
        for(VocabConcept n : listProcess) {
            JSONArray arr = new JSONArray();
            arr.put(n.getName());
            if(!n.getLabel("en").getValue().isEmpty()) {
                arr.put(n.getLabel("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            if(!n.getDefinition("en").getValue().isEmpty()) {
                arr.put(n.getDefinition("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            vocab.put(arr);
        }

//        listAttribute
        JSONArray attArray = new JSONArray();
        attArray.put("attribute");
        attArray.put("attribute@en");
        vocab.put(attArray);
        for(VocabConcept n : listAttribute) {
            JSONArray arr = new JSONArray();
            arr.put(n.getName());
            if(!n.getLabel("en").getValue().isEmpty()) {
                arr.put(n.getLabel("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            if(!n.getDefinition("en").getValue().isEmpty()) {
                arr.put(n.getDefinition("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            vocab.put(arr);
        }
//        listComponent
        JSONArray compoAtt = new JSONArray();
        compoAtt.put("component");
        compoAtt.put("component@en");
        vocab.put(compoAtt);
        for(VocabConcept n : listComponent) {
            JSONArray arr = new JSONArray();
            arr.put(n.getName());
            if(!n.getLabel("en").getValue().isEmpty()) {
                arr.put(n.getLabel("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            if(!n.getDefinition("en").getValue().isEmpty()) {
                arr.put(n.getDefinition("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            vocab.put(arr);
        }
//        listMaterial
        JSONArray matAtt = new JSONArray();
        matAtt.put("material");
        matAtt.put("material@en");
        vocab.put(matAtt);
        for(VocabConcept n : listMaterial) {
            JSONArray arr = new JSONArray();
            arr.put(n.getName());
            if(!n.getLabel("en").getValue().isEmpty()) {
                arr.put(n.getLabel("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            if(!n.getDefinition("en").getValue().isEmpty()) {
                arr.put(n.getDefinition("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            vocab.put(arr);
        }
//        listStep
        JSONArray stepAtt = new JSONArray();
        stepAtt.put("step");
        stepAtt.put("step@en");
        vocab.put(stepAtt);
        for(VocabConcept n : listStep) {
            JSONArray arr = new JSONArray();
            arr.put(n.getName());
            if(!n.getLabel("en").getValue().isEmpty()) {
                arr.put(n.getLabel("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            if(!n.getDefinition("en").getValue().isEmpty()) {
                arr.put(n.getDefinition("en").getValue() + "@en");
            } else {
                arr.put("");
            }
            vocab.put(arr);
        }
        return vocab;
    }

    public Boolean load() throws UnsupportedEncodingException, FileNotFoundException {
        logger.info("Loading ontology " + getName().get());
        Tools.addProgress(ProgressPO2.OPEN, "Retrieving ontology");
        this.initResource();
        this.clear();
        if (local) {
            File f = new File(Tools.ontoPath+getName().get());
            File[] listFile = f.listFiles();

            for (int i = 0; i < listFile.length; i++) {
                if (FilenameUtils.getExtension(listFile[i].getPath()).equals("rdf")
                        || FilenameUtils.getExtension(listFile[i].getPath()).equals("owl") || FilenameUtils.getExtension(listFile[i].getPath()).equals("ttl")) {
                    this.ontologyFile = listFile[i];
                    localLoaded.setValue(true);
                    canRead.setValue(true);
                    setCurrentVersionLoaded("");
                }
            }
        } else {
            if (publiq) {
                if (downloadLink != null) {
                    ZipFile in = CloudConnector.getZipFile(this);
                    Tools.updateProgress(ProgressPO2.OPEN, 0.5);
                    for (Enumeration<ZipArchiveEntry> ite = in.getEntriesInPhysicalOrder(); ite.hasMoreElements(); ) {
                        ZipArchiveEntry entry = ite.nextElement();
                        try {
                            File curfile = new File(Tools.ontoPath, entry.getName());
                            if (!entry.isDirectory()) {

                                File parent = curfile.getParentFile();
                                if (!parent.exists()) {
                                    parent.mkdirs();
                                }

                                try {
                                    OutputStream out = new FileOutputStream(curfile);
                                    IOUtils.copy(in.getInputStream(entry), out);
                                    out.close();
                                    if (FilenameUtils.getExtension(curfile.getPath()).equals("rdf")
                                            || FilenameUtils.getExtension(curfile.getPath()).equals("owl") || FilenameUtils.getExtension(curfile.getPath()).equals("ttl")) {
                                        ontologyFile = curfile;
                                        publicloaded.setValue(true);
                                        canRead.setValue(true);
                                    }
                                } catch (FileNotFoundException fnf) {
                                    logger.error("Failed unzip file " + curfile.getAbsolutePath());
                                }
                            } else {
                                curfile.mkdirs();
                            }
                        } catch (IOException e) {
                            logger.error("impossible de décompresser " + downloadLink, e);
                        }
                    }
                    try {
                        in.close();
                    } catch (IOException e) {
                        logger.error("impossible de fermer le zip ", e);

                    }
                }
            } else {
                // l'ontologie est privé. On utilise webDav . L'utilisateur est connecté sinon il ne pourrait pas etre ici
                loadDav();
            }
        }

        Tools.updateProgress(ProgressPO2.OPEN, 1.0);
        Tools.delProgress(ProgressPO2.OPEN);

        if(this.isLoaded().get()) {
            JenaTools.loadModel(this);
            setCurrentSkosScheme(this.getMainScheme());
            rebuildConstraints();
        }
        return publicloaded.getValue() || privateLoaded.getValue() || localLoaded.getValue();
    }


    public Boolean lockMode() throws AlreayLockException, LocalLockException, IOException, URISyntaxException, CantWriteException, NotLoginException {
        Tools.addProgress(ProgressPO2.LOCK, "lock aquisition");
        if (!local) {
            if(!isSynchro()) {
                load();
//                MainApp.getOntologyControler().startOpenFile(this.getName().get());
            }
            if (!privateLoaded.getValue()) {
                if (!CloudConnector.isInitUser()) {
                    Tools.delProgress(ProgressPO2.LOCK);
                    throw new NotLoginException();
                }
                this.loadDav();
            }
            if (privateLoaded.getValue() && canWrite.getValue()) {
                try {
                    lock = CloudConnector.lock(this);
                } catch (AlreayLockException e) {
                    Tools.updateProgress(ProgressPO2.LOCK, 1.0);
                    Tools.delProgress(ProgressPO2.LOCK);
                    throw e;
                }
            } else {
                lock = false;
                Tools.updateProgress(ProgressPO2.LOCK, 1.0);
                Tools.delProgress(ProgressPO2.LOCK);
                throw new CantWriteException();
            }
            if (lock == null) {
                lock = false;
            }
        } else {
            Tools.updateProgress(ProgressPO2.LOCK, 1.0);
            Tools.delProgress(ProgressPO2.LOCK);
            throw new LocalLockException();
        }

        Tools.updateProgress(ProgressPO2.LOCK, 1.0);
        Tools.delProgress(ProgressPO2.LOCK);
        return lock;
    }

    public Boolean unlockMode() {
        if (privateLoaded.getValue() && lock) {
            Boolean res = CloudConnector.unlock(this);
            if (res) {
                lock = false;
            }

            return res;
        }
        return false;
    }

    public File getOntologyFile() {
        return ontologyFile;
    }




    public String getLocalPath() {
        return ontologyFile.getPath();
    }

    public File getLocalFile() {
        return ontologyFile;
    }

    public static ObservableList<VocabConcept> listProcessProperty() {
        return listProcess;
    }

    public static ObservableList<VocabConcept> listMaterialProperty() {
        return listMaterial;
    }

    public static ObservableList<VocabConcept> listStepProperty() {
        return listStep;
    }

    public static ObservableList<VocabConcept> listAttributeProperty() {
        return listAttribute;
    }
    public static ObservableList<VocabConcept> listObservationProperty() {
        return listObservation;
    }
    public static ObservableList<VocabConcept> listObjectProperty() {
        return listObject;
    }

    public static ObservableList<VocabConcept> listComponentProperty() {
        return listComponent;
    }

    public static ObservableList<String> listUnitProperty() {
        return listUnit;
    }
    public static ObservableList<VocabConcept> listMethodProperty() {
        return listMethod;
    }
    public static ObservableList<VocabConcept> listScaleProperty() {
        return listScale;
    }

    public OntClass searchProcess(String name) {
        return listProcess.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchScale(String name) {
        return listScale.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchStep(String name) {
        return listStep.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchCompo(String name) {
        return listComponent.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchObj(String name) {
        return listObject.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchMaterial(String name) {
        return listMaterial.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchMethod(String name) {
        return listMethod.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public OntClass searchAttribut(String name) {
        return listAttribute.stream().filter(vn -> vn.matchLabel(name)).findFirst().map(vn -> domainModel.getOntClass(vn.getURI())).orElse(null);
    }

    public Resource searchOrCreateObj(String name, JSONObject json, OntModel dataModel) {
        OntClass newClass = searchObj(name);
        if(newClass == null) {
            newClass = dataModel.createClass(getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(name));
            newClass.addProperty(SKOS.prefLabel,name, "");
            json.getJSONArray("warning").put("Object "+ name + " not found in ontology");
        }
        return newClass;
    }

    public OntClass getObsClass() {
        return getCoreModel().getOntClass(getCORE_NS()+"TransformationProcessObservation");
    }

    public void rebuildConstraints() {
        Tools.addProgress(ProgressPO2.CONSTAINT, "Building Constraints");
        Tools.updateProgress(ProgressPO2.CONSTAINT, 0.0);
        Ontology.listProcessProperty().clear();
        Ontology.listMethodProperty().clear();
        Ontology.listScaleProperty().clear();
        Ontology.listObjectProperty().clear();
        Ontology.listStepProperty().clear();
        Ontology.listComponentProperty().clear();
        Ontology.listAttributeProperty().clear();
        Ontology.listMaterialProperty().clear();
        Ontology.listObservationProperty().clear();

        Double pas = 1.0/(getRootNode().getSubNodeInScheme().size() + 1);
        for (VocabConcept sonNode : getRootNode().getSubNodeInScheme()) {
            List<VocabConcept> temp = new ArrayList<>(sonNode.flattened().collect(Collectors.toMap(VocabConcept::getURI, Function.identity(), (a, b) -> a)).values());
            temp.removeIf(val -> val.getName().startsWith("PO2 /"));
            Collections.sort(temp);
            if (sonNode.isPO2Node().toLowerCase().contains("process")) {
                Ontology.listProcessProperty().setAll(temp);
                Ontology.listObjectProperty().addAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("step")) {
                Ontology.listStepProperty().setAll(temp);
                Ontology.listObjectProperty().addAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("component")) {
                Ontology.listComponentProperty().setAll(temp);
                Ontology.listObjectProperty().addAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("attribute")) {
                Ontology.listAttributeProperty().setAll(temp);
                Ontology.listObjectProperty().addAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("material")) {
                Ontology.listMaterialProperty().setAll(temp);
                Ontology.listObjectProperty().addAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("observation")) {
                Ontology.listObservationProperty().setAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("scale")) {
                Ontology.listScaleProperty().setAll(temp);
            }
            if (sonNode.isPO2Node().toLowerCase().contains("method")) {
                Ontology.listMethodProperty().setAll(temp);
                Ontology.listObjectProperty().addAll(temp);
            }
            Tools.updateProgress(ProgressPO2.CONSTAINT, Tools.getProgress(ProgressPO2.CONSTAINT).getProgress() + pas);
        }
        Tools.delProgress(ProgressPO2.CONSTAINT);
    }

    public boolean isLabelExist(String label, String lang) {
        return isLabelExist(rootNode, label, lang);
    }

    public boolean isLabelExist(VocabConcept topConcept, String label, String lang) {
        if(!label.isEmpty()) {
            String rootLabel = topConcept.getLabel(lang).get();
            if (rootLabel != null && rootLabel.equalsIgnoreCase(label)) {
                return true;
            } else {
                return topConcept.getSubNode().stream().map(sonNode -> isLabelExist(sonNode, label, lang)).reduce(Boolean::logicalOr).orElse(false);
            }
        }
        return true;
    }

    public Boolean publish(JSONObject listPath) {
        if(!listPath.keySet().contains("file_onto")) {
            return false;
        }
        String  repoLabel = "repository for ontology " + this.getName().get();
        String projectID = Tools.normalize(this.getName().get());

        try {
            RDF4JTools.createOrClearRepository(repoLabel, projectID);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        try {

            RDF4JTools.upload(projectID, "http://opendata.inrae.fr/PO2/ontology/" + projectID, new FileInputStream(listPath.getString("file_onto")), RDFFormat.TURTLE);
            RDF4JTools.setFreeReadRepository(projectID, isPublic());
        } catch (AuthenticationException | IOException e) {
            e.printStackTrace();
            return false;
        }

        CloudConnector.addLog(this, "published");
        RDF4JTools.initRepositotyInfos(this);
        return true;
    }

    public JSONObject semantize() throws IOException {
        Tools.addProgress(ProgressPO2.PUBLISH, "Loading core ontology");

        JSONObject mess = new JSONObject();
        JSONArray error = new JSONArray();
        mess.put("error", error);
        JSONArray listMessages = new JSONArray();
        mess.put("listMessages", listMessages);
        if(!this.isLocked()) {
            error.put("ontology not locked");
        }
        if(!this.isLoaded().get()) {
            error.put("ontology not loaded");
        }
        if(this.isModified()) {
            error.put("ontology not saved");
        }

        if(error.length() > 0) {
            Tools.delProgress(ProgressPO2.PUBLISH);
            return mess;
        }

        OntModel coreModel = JenaTools.ReadCoreModel();
        // on refait une sauvegarde pour changer le num de version majeur
        Boolean newSave = JenaTools.saveModel(this, true);
        if(!newSave) {
            Tools.delProgress(ProgressPO2.PUBLISH);
            error.put("unable to save ontology");
            return mess;
        } else {
            Boolean upload = CloudConnector.update(this);
            if(!upload ){
                Tools.delProgress(ProgressPO2.PUBLISH);
                error.put("unable to upload ontology");
                return mess;
            }
        }


        // on supprime les imports
       coreModel.listSubModels(true).toList().forEach(m -> coreModel.removeSubModel(m));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Tools.updateProgress(ProgressPO2.PUBLISH, "Loading domain ontology");

        OntModel domainModel = JenaTools.ReadModel(ontologyFile.getAbsolutePath());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Tools.updateProgress(ProgressPO2.PUBLISH, "Settings NameSpaces");

        Model unionModel = coreModel.union(domainModel);
        File tempDomain = File.createTempFile("pref", "ttl");
        FileOutputStream output = new FileOutputStream(tempDomain);
        RDFWriter ontologyWriter = RDFWriter.create().source(unionModel).lang(Lang.TTL).build();
        ontologyWriter.output(output);
        output.flush();
        mess.put("file_onto", tempDomain.getAbsolutePath());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Tools.delProgress(ProgressPO2.PUBLISH);

        return mess;
    }

    public boolean isShowDeprecated() {
        return showDeprecated.get();
    }

    public SimpleBooleanProperty showDeprecatedProperty() {
        return showDeprecated;
    }

    public void setShowDeprecated(boolean showDeprecated) {
        this.showDeprecated.set(showDeprecated);
    }
}
