/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TablePart extends GenericPart {

    private static final KeyWords caractK = new KeyWords(true, true,"attribute@en","attribut@fr","caracteristic@en","caractéristique@fr", "caractéristiques@fr", "caracteristique@fr");
    private static final List<KeyWords> listKeyWord = Arrays.asList(caractK);

    private Boolean isACompo = null;

    GenericTable table;


    public TablePart() {
        this.type = DataPartType.UNDEFINED;
        // only used for detection
    }
    public TablePart(Data data) {
        this();
        this.data = data;
        this.isACompo = false;
    }

    public void setIsACompo(Boolean isACompo) {
        this.isACompo = isACompo;
    }
    @Override
    public Boolean checkPart(DataPart part) {

        // on regarde s'il y a une entete
        List<String> firstLine = part.getLine(0);
        if(firstLine != null && !firstLine.isEmpty()) {
            String firstCell = firstLine.get(0);
            if(DataPartType.RAW_DATA.map(firstCell)) { // entete OK
                this.type = DataPartType.RAW_DATA;
                part.removeLine(0);
                part.setLineTable(true);
                return true;
            }
            if(DataPartType.CALC_DATA.map(firstCell)) { // entete OK
                this.type = DataPartType.CALC_DATA;
                part.removeLine(0);
                part.setLineTable(true);
                return true;
            }
        }
        return false;
    }

    public void setType(KeyWords type) {
        this.type = type;
    }

    public void initTablePart(Boolean simple) {
        if(simple) {
            table = new SimpleTable(data, isACompo);
        } else {
            table = new ComplexTable(data);
        }
    }

    @Override
    public void mapPart(DataPart part) {
        mapPart(part, -1);
    }

    public void mapPart(DataPart part, int replicateID) {

        // on regarde si le tableau est simple (caract, objet, value, unit, comment)
        // ou s'il est complexe
        if(table == null) {
            if (checkKeyword(part.getHeader())) {
                table = new SimpleTable(data, this.isACompo);
                // tableau simple
            } else {
                table = new ComplexTable(data);
            }
        }
        if(table != null) {
            table.mapPart(part, replicateID);
        }
    }

    @Override
    public void checkValue() {
        table.checkValue();
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }


    public void validateData() {

    }

    public void unbind() {
        if(table != null) {
            table.unbind();
        }
    }

    public void cleanReplicatesValue(List<Integer> list) {
        if(table instanceof ComplexTable) {
            ((ComplexTable)table).cleanReplicatesValue(list);
        }
        if(table instanceof SimpleTable) {
            ((SimpleTable)table).cleanReplicatesValue(list);
        }
    }

    public LinkedList<LinkedList<String>> getDataForExcel(int replicateID) {
        return table.getDataForExcel(replicateID);
    }
    public LinkedList<LinkedList<String>> getDataForExcel() {
        return table.getDataForExcel(-1);
    }

    public GenericTable getTable() {
        return this.table;
    }

    public void addMap(DataPart p, int replicateID) {
        table.addMap(p, replicateID);
    }

    public void cloneFrom(TablePart data) {
        this.type = data.getPartType();
        try {
            this.table.cloneFrom(data.getTable());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
