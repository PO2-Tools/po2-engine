/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.*;
import org.apache.commons.collections4.map.LinkedMap;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class StepMetaPart extends GenericPart {
    private static final KeyWords stepIdK = new KeyWords(false, true, "id@en", "id@fr");
    private static final KeyWords stepTypeK = new KeyWords(false, true, "type@en", "type@fr");
    private static final KeyWords dateK = new KeyWords(true, true,"date@en","date@fr");
    private static final KeyWords heureK = new KeyWords(true, true,"hour@en","heure@fr");
    private static final KeyWords descriptionK = new KeyWords(false, false, "description@en", "description@fr");
    private static final KeyWords dureeK = new KeyWords(false, false,"handling time@en","durée de la manipulation@fr", "duree de la manipulation@fr");
    private static final KeyWords parentK = new KeyWords(false, false, "parent step@en", "étape parente^fr");
    private static final KeyWords UUIDK = new KeyWords(false, true,"uuid@en","uuid@fr", "uuid@fr");
    private static final KeyWords scaleK = new KeyWords(false, true,"scale@en","échelle@fr", "echelle@fr");
    public static final KeyWords agentK = new KeyWords(false, true,"agent@en","agent@fr");

    private static final List<KeyWords> listKeyWord = Arrays.asList(dateK, heureK);

    private LinkedMap<KeyWords, ComplexField> map = null;
    private ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> agentsMap;

    ChangeListener l = (observableValue, o, t1) -> {
        data.setModified(true);
    };

    public StepMetaPart() {
        this.type = DataPartType.GENERAL_INFO;
        // only used for detection !!
        dateK.setUsedInReplicate(true);
        heureK.setUsedInReplicate(true);
        dureeK.setUsedInReplicate(true);
        agentK.setUsedInReplicate(true);
    }
    public StepMetaPart(Data data) {
        this();
        this.data = data;
        map = new LinkedMap<>();
        map.put(stepIdK, new ComplexField());
        map.put(stepTypeK, new ComplexField());
        map.put(dateK, new ComplexField());
        map.put(heureK, new ComplexField());
        map.put(dureeK, new ComplexField());
        map.put(descriptionK, new ComplexField());
        map.put(parentK, new ComplexField());
        map.put(scaleK, new ComplexField());
        map.put(UUIDK, new ComplexField());


        map.get(stepTypeK).setMendatory(true);
        map.get(stepIdK).setMendatory(true);
        map.get(stepTypeK).setListConstraint(Ontology.listStepProperty());
        map.get(scaleK).setMendatory(true);
        map.get(scaleK).setListConstraint(Ontology.listScaleProperty());



        map.get(stepIdK).setValuesListener(l);
        map.get(stepTypeK).setValuesListener(l);
        map.get(dateK).setValuesListener(l);
        map.get(heureK).setValuesListener(l);
        map.get(dureeK).setValuesListener(l);
        map.get(descriptionK).setValuesListener(l);
        map.get(parentK).setValuesListener(l);
        map.get(scaleK).setValuesListener(l);
        map.get(UUIDK).setValuesListener(l);

        agentsMap = FXCollections.observableHashMap();
    }


    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void unbind() {
        for(ComplexField c : map.values()) {
            c.unbindReplicatesValues();
        }
        agentsMap.forEach((k, v) -> v.unbind());
    }

    public ComplexField getCType() {
        return map.get(stepTypeK);
    }

    public ComplexField getCId() {
        return map.get(stepIdK);
    }

    public ComplexField getCDate() {
        return map.get(dateK);
    }

    public ComplexField getCTime() {
        return map.get(heureK);
    }

    public ComplexField getCDuration() {
        return map.get(dureeK);
    }

    public ComplexField getCDescription() {
        return map.get(descriptionK);
    }

    public ComplexField getCParent() {
        return map.get(parentK);
    }

    public ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> getAgentsMap() {
        return agentsMap;
    }

    public void setAgents(String agents) {
        HashMap<Integer, String> listRep = Tools.convertReplicate(agents);
        listRep.forEach((k, v) -> {
            if(!agentsMap.containsKey(k)) {
                agentsMap.put(k, new SimpleObjectProperty<>());
                agentsMap.get(k).addListener(l);
            }
            HashMap<KeyWords, ComplexField> la = this.data.getProjectFile().getAgent(v);
            if(la == null) {
                la = Tools.createFakeAgentFromString(v);
            }
            agentsMap.get(k).setValue(la);
        });
     }

    public String getAgents() {
        if(agentsMap.size() == 1 && agentsMap.containsKey(0)) {
            return Tools.agentToString(agentsMap.get(0).get());
        }
        JSONArray listAgent = new JSONArray();
        agentsMap.forEach((k, v) -> {
            JSONObject o = new JSONObject();
            o.put("id", k);
            o.put("value", Tools.agentToString(v.get()));
            listAgent.put(o);
        });
        return listAgent.toString();
    }

    public String getAgent(int replicateID) {
        if(agentsMap.containsKey(replicateID)) {
            return Tools.agentToString(agentsMap.get(replicateID).get());
        }
        return "";
    }

    public ComplexField getCUuid() { return map.get(UUIDK);}
    public String getScale() {
        return map.get(scaleK).getValue().get();
    }
    public ComplexField getCScale() {
        return map.get(scaleK);
    }
    public void setScale(String scale) {
        map.get(scaleK).getValue().set(scale);
    }


    public void mapPart(DataPart part, int replicateID) {
        for(Map.Entry<KeyWords, ComplexField> entry : map.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                if(!k.isUsedInReplicate() || replicateID == -1) {
                    map.get(k).setValue(val.get(0));
                } else {
                    map.get(k).setValueWithReplicate(replicateID, val.get(0));
                }
            }
        }
        List<String> val = getLineValues(agentK, part);
        if(val != null  && val.size() > 0) {
            if(replicateID == -1) {
                setAgents(val.get(0));
            } else {
                HashMap<KeyWords, ComplexField> findAgent = this.data.getProjectFile().getAgent(val.get(0));
                if(!agentsMap.containsKey(replicateID)) {
                    agentsMap.put(replicateID, new SimpleObjectProperty<>());
                    agentsMap.get(replicateID).addListener(l);
                }
                if(findAgent != null) {
                    agentsMap.get(replicateID).setValue(findAgent);
                } else {
                    agentsMap.get(replicateID).setValue(Tools.createFakeAgentFromString(val.get(0)));
                }
            }

        }
    }
    @Override
    public void mapPart(DataPart part) {
        mapPart(part, -1);
    }

    @Override
    public void checkValue() {
        map.get(stepTypeK).checkValue();
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public LinkedMap<KeyWords, ComplexField> getContent() {
        return map;
    }


    public void validateData() {
    }

    public void generateUUID() {
        if(map.get(UUIDK).getValue().isEmpty().get()) {
            map.get(UUIDK).getValue().set(UUID.randomUUID().toString());
        }
    }

    public String getUuid(boolean force) {
        if (force) generateUUID();
        return map.get(UUIDK).getValue().get();
    }

    public void cleanAgentReplicateValues(List<Integer> list) {
        // != 0 to prevent removing value for default replicate
        agentsMap.entrySet().removeIf(repIDValue -> repIDValue.getKey() != 0 && !list.contains(repIDValue.getKey()));
    }

}
