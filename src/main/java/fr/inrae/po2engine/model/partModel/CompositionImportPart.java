/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import org.apache.commons.collections4.map.LinkedMap;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/******** USED ONLY FOR IMPORT ***********/

public class CompositionImportPart extends GenericPart {
    private static final KeyWords compoIDK = new KeyWords(true, true,"composition id@en");
    private static final KeyWords compoTypeK = new KeyWords(true, true,"composition type@en");
    private static final KeyWords compoNameK = new KeyWords(true, true,"composition name@en");
    private static final KeyWords compoIOK = new KeyWords(true, true,"composition input/output@en");
    private static final List<KeyWords> listKeyWord = Arrays.asList(compoIDK, compoTypeK,compoNameK, compoIOK);


    LinkedMap<KeyWords, ComplexField> map = null;

    public CompositionImportPart() {
        this.type = DataPartType.COMPOSITION_IMPORT;
        // only used for detection !!
    }
    public CompositionImportPart(Data data) {
        this();
        this.data = data;
        map = new LinkedMap<>();
        map.put(compoIDK, new ComplexField());
        map.put(compoTypeK, new ComplexField());
        map.put(compoNameK, new ComplexField());
        map.put(compoIOK, new ComplexField());
    }



    public String getCompositionID() {
        return map.get(compoIDK).getValue().get();
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void unbind() {

    }



    @Override
    public void mapPart(DataPart part) {
        for(Map.Entry<KeyWords, ComplexField> entry : map.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                map.get(k).setValue(val.get(0));
            }
        }
    }

    @Override
    public void checkValue() {
        // nothing to do here
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public LinkedMap<KeyWords, ComplexField> getContent() {
        return map;
    }


    public void validateData() {
    }

    public Boolean isInputComposition() {
        return map.get(compoIOK).getValue().get().equalsIgnoreCase("input");
    }

    public String getCompositionName() {
        return map.get(compoNameK).getValue().get();
    }

    public String getCompositionType() {
        return map.get(compoTypeK).getValue().get();
    }
}
