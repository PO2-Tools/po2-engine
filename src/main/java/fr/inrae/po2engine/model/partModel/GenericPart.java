/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class GenericPart {

    protected KeyWords type ;
//    protected DataNode node;
    private Boolean selectedInTree = false;
    protected Data data;



    public abstract Boolean checkPart(DataPart part);
    public abstract void unbind();
    public abstract void mapPart(DataPart part);
    public abstract void checkValue();

    public Boolean isSelectedInTree() {
        return selectedInTree;
    }
    public void setSelectedInTree(Boolean selected) {
        this.selectedInTree = selected;
    }

    abstract List<KeyWords> getListKeywords();

    public KeyWords getPartType() {
        return  type;
    }
    public Boolean checkKeyword(List<String> line) {
        Boolean result = false;
        Integer compt = 0;
        Integer nbForDetection = 0;
        List<KeyWords> listKeyWord = getListKeywords();
        for(String value : line) {
            Iterator<KeyWords> iteK = listKeyWord.iterator();
            boolean done = false;
            while(!done && iteK.hasNext()) {
                KeyWords k = iteK.next();
                if(k.isUsedForDetection()) {
                    nbForDetection++;
                    if (k.map(value)) {
                        compt++;
                        done = true;
                    }
                }
            }
        }

        try {
            if (((double) compt / listKeyWord.size() * 100.0) >= 50) {
                result = true;
            }
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return result;
    }
    protected KeyWords getKeyword(String s) {
        List<KeyWords> listKeyWord = getListKeywords();
        Iterator<KeyWords> iteK = listKeyWord.iterator();
        while(iteK.hasNext()) {
            KeyWords k = iteK.next();
            if (k.map(s)) {
                return k;
            }
        }
        return null;
    }

    protected List<String> getLineValues(KeyWords k, DataPart part) {
        for (List<String> l : part.getListLine()) {
            if(l.size() > 0 && k.map(l.get(0))) {
                l.remove(0);
                return l;
            }
        }
        return null;
    }

    protected Integer getOccurence(KeyWords objectK, DataPart part) {
        Integer occ = 0;
        for(String l : part.getHeader()) {
            if (objectK.map(l)) {
                occ++;
            }
        }
        return occ;
    }
    protected List<String> getColumnValues(Integer colNum, DataPart part) {
        List<String> list = new LinkedList<>();
        if(colNum >= 0 && part.getListLine().size() > 0 && part.getHeader().size() > colNum) {
            for(List<String> li : part.getListLine()) {
                list.add(li.get(colNum));
            }
            list.remove(0);

        }
        return completeLine(list, part.getNBLine()-1);
    }
    public List<String> getColumnValues(KeyWords k, DataPart part) {
        return getColumnValues(k, part, 1);
    }

        protected List<String> getColumnValues(KeyWords k, DataPart part, Integer occ) {
        Integer curOcc = 0;
        List<String> list = new LinkedList<>();
        if(part.getListLine().size() > 0) {
            Integer rowCount = 0;
            for(String l : part.getListLine().get(0)) {
                if(k.map(l)) {
                    curOcc++;
                    if(curOcc == occ) {
                        for (List<String> li : part.getListLine()) {
                            list.add(li.get(rowCount));
                        }
                        list.remove(0);
                        return list;
                    }
                }
                rowCount++;
            }
        }

        return completeLine(list, part.getNBLine()-1);
    }

    protected void detectTableType(DataPart part) {
        part.setLineTable(checkKeyword(part.getLine(0)));
    }

    private List<String> completeLine(List<String> list, int size) {
        while(list.size() < size) {
            list.add("");
        }
        return list;
    }

    public void setType(KeyWords type) {
        this.type = type;
    }

//    public DataNode getDataNode() {
//        return this.node;
//    }
}
