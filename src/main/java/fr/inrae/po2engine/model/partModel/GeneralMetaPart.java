/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class GeneralMetaPart extends GenericPart {
    private static Logger logger = LogManager.getLogger(GeneralMetaPart.class);

    LinkedMap<KeyWords, ComplexField> content = null;
    private String oldSampleType = "";
    private static final KeyWords PO2EngineK = new KeyWords(true, true,"PO2 Engine@en","PO2 Engine@fr");
    private static final KeyWords startDateK = new KeyWords(true, true,"date@en","date@fr");
    private static final KeyWords endDateK = new KeyWords(false, true,"end date@en","date de fin@fr");
    private static final KeyWords processK = new KeyWords(true, true,"process@en","procédé@fr");
    private static final KeyWords sampleNK = new KeyWords(true, true,"sample name@en","nom de l'échantillon@fr", "nom échantillon@fr");
    private static final KeyWords sampleCK = new KeyWords(true, true,"sample code@en","code de l'échantillon@fr", "code échantillon@fr");
    private static final KeyWords UUIDK = new KeyWords(false, true,"uuid@en","uuid@fr", "uuid@fr");
    private static final KeyWords processTypeK = new KeyWords(false, true,"process type@en","type de procédé@fr");
    private static final KeyWords descriptionK = new KeyWords(true, true,"description@en","description@fr");
    private static final KeyWords externalLinksK = new KeyWords(true, true,"external links@en","liens externe@fr");
    public static final KeyWords replicateK = new KeyWords(false, false, "replicate@en");

    private static final List<KeyWords> listKeyWord = Arrays.asList(startDateK, processK);

    private ObservableList<Replicate> listReplicate = null;

    public GeneralMetaPart() {
        this.type = DataPartType.GENERAL_INFO;
        // only used for detection !!
    }
    public GeneralMetaPart(GeneralFile generalFile) {
        this();
        this.data = generalFile.getData();
        ChangeListener<String> l = (observableValue, o, t1) -> {
            data.setModified(true);
        };

        content = new LinkedMap<>();

        content.put(PO2EngineK, new ComplexField());
        content.get(PO2EngineK).setValue(Tools.getEngineVersion());

        content.put(startDateK, new ComplexField());
        content.get(startDateK).setValuesListener(l);

        content.put(endDateK, new ComplexField());
        content.get(endDateK).setValuesListener(l);

        content.put(processK, new ComplexField());
        content.get(processK).setValuesListener(l);

        content.put(processTypeK, new ComplexField());
        content.get(processTypeK).setValuesListener(l);
        content.get(processTypeK).setMendatory(true);
        content.get(processTypeK).setListConstraint(Ontology.listProcessProperty());

        content.put(descriptionK, new ComplexField());
        content.get(descriptionK).setValuesListener(l);

        content.put(UUIDK, new ComplexField());
        content.get(UUIDK).setValuesListener(l);

        content.get(processK).setMendatory(true);
        content.get(startDateK).setMendatory(true);
        content.get(endDateK).setMendatory(true);

        content.put(externalLinksK, new ComplexField());
        content.get(externalLinksK).setValuesListener(l);

        content.get(processK).setValue("Process");

        listReplicate = FXCollections.observableArrayList();
    }

    public String getOldSampleType() {
        return this.oldSampleType;
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void mapPart(DataPart part) {
        for(Map.Entry<KeyWords, ComplexField> entry : content.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                content.get(k).setValue(val.get(0));
            }
        }
        // if processType is empty we use process
        if(getCType().getValue().isEmpty().get()) {
            setProcessType("Process");
        }
        // check for old sample type
        List<String> valSampleType = getLineValues(sampleCK, part);
        List<String> valSampleName = getLineValues(sampleNK, part);
        if(valSampleName != null && valSampleName.size() > 0) {
            String st = valSampleName.get(0).trim();
            if(!st.isEmpty()) {
                oldSampleType = st;
                content.get(processK).getValue().setValue(content.get(processK).getValue().get().concat(" - "+oldSampleType));
                String stt = valSampleType.get(0).trim();
                if(!stt.isEmpty()) {
                    content.get(processK).getValue().setValue(content.get(processK).getValue().get().concat(" ("+stt+")"));
                }
            }
        }
        List<String> replicate = getLineValues(replicateK, part);
        if(replicate != null && !replicate.isEmpty() && !replicate.get(0).isEmpty()) {
            try {
                JSONArray arr = new JSONArray(replicate.get(0));
                arr.forEach(o -> {
                    JSONObject re = (JSONObject) o;
                    listReplicate.add(new Replicate(re.getInt("id"), re.optString("name"), re.optString("description")));
                });
            } catch (JSONException je) {
                logger.error("can't parse json : ", je);
            }
        }
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public Property<String> processNameProperty() {
        return content.get(processK).getValue();
    }

    public StringProperty titleProperty() {
        return content.get(processK).getValue();
    }

    public ComplexField getCType() {
        return content.get(processTypeK);
    }

    public void setProcessType(String type) {
        content.get(processTypeK).setValue(type);
    }

    public ComplexField getCTitle() {
        return content.get(processK);
    }

    public ComplexField getCStartDate() {
        return content.get(startDateK);
    }

    public ComplexField getCEndDate() {
        return content.get(endDateK);
    }

    public ComplexField getCExternalLinks() { return content.get(externalLinksK);}


    public void unbind() {
        for(KeyWords k : content.keySet()) {
            content.get(k).unbindReplicatesValues();
        }
    }

    public ObservableList<Replicate> getListReplicate()  {
        return this.listReplicate;
    }

    public Replicate getReplicate(int id) {
        return this.listReplicate.stream().filter(r -> r.getId() == id).findAny().orElse(null);
    }

    public void addReplicate(Replicate replicate) {
        this.listReplicate.add(replicate);
        this.data.setModified(true);
    }
    public Replicate addReplicate(String name) {
        return addReplicate(name, "");
    }
    public Replicate addReplicate(String name, String description) {
        if(listReplicate.isEmpty()) {
            // on ajoute l'itinéraire comme un replicat
            this.listReplicate.add(new Replicate(0, titleProperty().get(), "Default replicate"));
        }
        // get the max id for the replicate
        Integer id = this.listReplicate.stream().mapToInt(re -> re.getId()).max().orElse(-1) + 1;
        Replicate temp = new Replicate(id, name, description);
        this.listReplicate.add(temp);
        this.data.setModified(true);
        return temp;
    }

    public boolean removeReplicate(Replicate replicate) {
        if(replicate != null && replicate.getId() != 0 && this.listReplicate.contains(replicate)) {
            this.listReplicate.remove(replicate);
            this.data.setModified(true);
            return true;
        }
        return false;
    }


    public ComplexField getCDescription() { return content.get(descriptionK);}

    public LinkedMap<KeyWords, ComplexField> getContent() {
        return content;
    }

    public StringProperty startDateProperty() { return content.get(startDateK).getValue();}

    public StringProperty endDateProperty() { return content.get(endDateK).getValue();}

    public void generateUUID() {
        if(content.get(UUIDK).getValue().isEmpty().get()) {
            content.get(UUIDK).getValue().set(UUID.randomUUID().toString());
        }
    }

    public String getUuid(boolean force) {
        if (force) generateUUID();
        return content.get(UUIDK).getValue().get();
    }

    public void checkValue() {
        content.get(processTypeK).checkValue();
    }

    public String getReplicateAsString() {
        JSONArray arr = new JSONArray();
        getListReplicate().forEach(re -> {
            JSONObject obj = re.toJson();
            arr.put(obj);
        });
        return arr.toString();
    }
}
