/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import org.apache.commons.collections4.map.LinkedMap;

import java.util.*;

public class ConduiteMetaPart extends GenericPart {
    private static final KeyWords dateK = new KeyWords(true, true,"date@en","date@fr");
    private static final KeyWords heureK = new KeyWords(true, true,"hour@en","heure@fr");
    private static final KeyWords dureeK = new KeyWords(true, true,"handling time@en","durée de la manipulation@fr", "duree de la manipulation@fr");
    private static final KeyWords UUIDK = new KeyWords(false, true,"uuid@en","uuid@fr", "uuid@fr");
    private static final List<KeyWords> listKeyWord = Arrays.asList(dateK, heureK,dureeK);

    LinkedMap<KeyWords, ComplexField> map = null;


    public ConduiteMetaPart() {
        this.type = DataPartType.GENERAL_INFO;
        // only used for detection !!
    }
    public ConduiteMetaPart(Data data) {
        this();
        this.data = data;
        map = new LinkedMap<>();
        map.put(dateK, new ComplexField());
        map.put(heureK, new ComplexField());
        map.put(dureeK, new ComplexField());
        map.put(UUIDK, new ComplexField());
    }

    @Override
    public Boolean checkPart(DataPart part) {

        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void unbind() {

    }

    @Override
    public void mapPart(DataPart part) {
        for(Map.Entry<KeyWords, ComplexField> entry : map.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                map.get(k).setValue(val.get(0));
            }
        }

    }

    @Override
    public void checkValue() {
        // nothing to do here
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }


    public void validateData() {
        //Global check.
//        for(Map.Entry<KeyWords, ComplexField> entry : map.entrySet()) {
//            if(entry.getKey().isMandatory()) {
//                if(entry.getValue().getValue().isEmpty().get()) {
//                    entry.getValue().setState(FieldState.ERROR, "Field is mendatory");
//                }
//            }
//        }
    }


    public LinkedMap<KeyWords, ComplexField> getContent() {
        return map;
    }

    public void generateUUID() {
        if(map.get(UUIDK).getValue().isEmpty().get()) {
            map.get(UUIDK).getValue().set(UUID.randomUUID().toString());
        }
    }

    /**
     * get the uuid of the file
     * @param force if the uuid is empty, a new ione is create
     * @return the uuid of the file
     */
    public String getUuid(boolean force) {
        if (force) generateUUID();
        return map.get(UUIDK).getValue().get();
    }
}
