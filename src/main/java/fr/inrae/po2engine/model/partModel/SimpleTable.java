/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.FieldState;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.BooleanExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

public class SimpleTable extends GenericTable{


    SimpleObjectProperty<FieldState> state = new SimpleObjectProperty<>();
    ObservableList<ComplexField> listValidation = FXCollections.observableArrayList();
    public static final KeyWords caractK = new KeyWords(true, true,"attribute@en","attribut@fr","caracteristic@en","caractéristique@fr", "caractéristiques@fr", "caracteristique@fr");
    public static final KeyWords ObjK = new KeyWords(false, false,"object@en","objet@fr");
    public static final KeyWords valK = new KeyWords(true, true,"value@en","valeur@fr");
    static {
        valK.setUsedInReplicate(true);
    }
    public static final KeyWords unitK = new KeyWords(false, false,"unit@en","unité@fr","unite@fr", "unités@fr");
    public static final KeyWords commentK = new KeyWords(false, false,"comment@en","commentaire@fr", "description@fr");
    public static final KeyWords numK = new KeyWords(false, false,"num@en","num@fr");
    public static final KeyWords actionK = new KeyWords(false, false,"action@en","action@fr");


    //
    public static final KeyWords minK = new KeyWords(false, false,"minimum@en","min@en","minimum@fr","min@fr");
    public static final KeyWords maxK = new KeyWords(false, false,"maximum@en","max@en","maximum@fr","max@fr");
    public static final KeyWords meanK = new KeyWords(false, false,"mean@en","moyenne@fr");
    public static final KeyWords sdK = new KeyWords(false, false,"sd@en","standard deviation@en","ecart-type@fr");

    private static Logger logger = LogManager.getLogger(SimpleTable.class);
    private static LinkedList<KeyWords> listKeyWord = new LinkedList<>(List.of(caractK, ObjK, valK, unitK, commentK ));


    private ObservableList<Map<KeyWords, ComplexField>> content = FXCollections.observableArrayList();

    private Boolean isACompo;

    private Individual myObsCollection = null;

    public SimpleTable(Data data, Boolean isACompo) {
        ObservableList<ObjectProperty<FieldState>> listAllProp = FXCollections.observableArrayList();

        listValidation.addListener((ListChangeListener<? super ComplexField>) change -> {
            BooleanBinding bError = Bindings.createBooleanBinding(()-> listValidation.stream().anyMatch(s->s.stateProperty().isEqualTo(FieldState.ERROR).getValue()), listValidation.stream().map(s->s.stateProperty()).toArray(Observable[]::new));
            this.state.bind(Bindings.when(bError).then(FieldState.ERROR).otherwise(FieldState.VALID));
        });
        BooleanBinding bWarning = Bindings.createBooleanBinding(()-> listValidation.stream().anyMatch(s->s.stateProperty().isEqualTo(FieldState.WARNING).get()),listValidation);

        this.data = data;
        this.isACompo = isACompo;
    }

    public static LinkedList<KeyWords> getListKeyWord() {
        return listKeyWord;
    }

    public Boolean getIsACompo() {
        return this.isACompo;
    }

    public ObservableList<Map<KeyWords, ComplexField>> getContent() {return content;}

    public Integer getSize() {
        return content.size();
    }

    @Override
    public void addMap(DataPart p, int replicateID) {

    }

    @Override
    public void checkValue() {
        for(Map<KeyWords, ComplexField> line : content) {
            line.get(caractK).checkValue();
            line.get(ObjK).checkValue();
            line.get(valK).checkValue();
        }
    }

    public SimpleObjectProperty<FieldState> stateProperty() {
        return state;
    }

    private void standardizeValue(DataPart part) {
        if(part != null) {
            for (Integer i = 0; i < part.getHeader().size(); i++) {
                if (minK.map(part.getHeader().get(i))) {
                    // on remplace le header
                    part.getHeader().set(i, valK.getPrefLabel("en"));
                    List<String> l = getColumnValues(i, part);
                    for (Integer j = 0; j < l.size(); j++) {
                        if(!l.get(j).isEmpty()) {
                            part.setValue(j + 1, i, "[" + l.get(j));
                        }
                    }
                }
                if (maxK.map(part.getHeader().get(i))) {
                    // on remplace le header
                    part.getHeader().set(i, valK.getPrefLabel("en"));
                    List<String> l = getColumnValues(i, part);
                    for (Integer j = 0; j < l.size(); j++) {
                        if(!l.get(j).isEmpty()) {
                            part.setValue(j + 1, i, l.get(j) + "]"); // on decale pour ne pas prendre l'entête
                        }
                    }
                }
                if (meanK.map(part.getHeader().get(i))) {
                    // on remplace le header
                    part.getHeader().set(i, valK.getPrefLabel("en"));
                    List<String> l = getColumnValues(i, part);
                    for (Integer j = 0; j < l.size(); j++) {
                        if(!l.get(j).isEmpty()) {
                            part.setValue(j + 1, i, "[[" + l.get(j));
                        }
                    }
                }
                if (sdK.map(part.getHeader().get(i))) {
                    // on remplace le header
                    part.getHeader().set(i, valK.getPrefLabel("en"));
                    List<String> l = getColumnValues(i, part);
                    for (Integer j = 0; j < l.size(); j++) {
                        if(!l.get(j).isEmpty()) {
                            part.setValue(j + 1, i, l.get(j) + "]]");
                        }
                    }
                }
            }
        }
    }

    public void mapPart(DataPart part, int replicateID) {
        if(replicateID == -1) {
            content.clear();
        }
        listValidation.clear();
        standardizeValue(part);
        Map<KeyWords, List<String>> temp = new LinkedHashMap<>();
        String join = "::";

        for(KeyWords k : listKeyWord) {
            if(k.equals(valK)) {
                join = ";";
            }
            if(part != null) {
                Integer occ = 0;
                temp.put(k, getColumnValues(-1, part));
                for (Integer i = 0; i < part.getHeader().size(); i++) {
                    if (k.map(part.getHeader().get(i))) {

                        // on fusionne les valeurs
                        List<String> old = temp.get(k);
                        List<String> newV = getColumnValues(i, part);
                        List<String> res = new LinkedList<>();
                        for (int co = 0; co < old.size(); co++) {
                            if (!old.get(co).isEmpty() && !newV.get(co).isEmpty()) {
                                res.add(co, String.join(join, old.get(co), newV.get(co)));
                            } else {
                                res.add(co, old.get(co) + newV.get(co));
                            }
                        }
                        temp.put(k, res);
                    }
                }
            } else {
                temp.put(k, new LinkedList<>());
            }
        }

        if(!temp.isEmpty()) {
            KeyWords one = temp.keySet().iterator().next();
            for (int i = 0; i < temp.get(one).size(); i++) {
                Map<KeyWords, ComplexField> newLine = null;
                if(replicateID != -1) {
                    if(content.size() > i) {
                        newLine = content.get(i);
                    }
                }
                if(newLine == null) {
                    newLine  = new LinkedHashMap<>();
                    content.add(newLine);
                }
                for (Map.Entry<KeyWords, List<String>> entry : temp.entrySet()) {
                    ComplexField c = null;
                    if(newLine.containsKey(entry.getKey())) {
                        c = newLine.get(entry.getKey());
                        c.unbindReplicatesValues();
                    }
                    else {
                        c = new ComplexField();
                    }
                    if(!entry.getKey().isUsedInReplicate() || replicateID == -1) {
                            c.setValue(entry.getValue().get(i));
                    } else {
                            c.setValueWithReplicate(replicateID, entry.getValue().get(i));
                    }
                    if(!entry.getKey().equals(numK) && !entry.getKey().equals(actionK)) {
                        listValidation.add(c);
                    }
                    if(entry.getKey().equals(valK) || entry.getKey().equals(caractK)) {
                        c.setMendatory(true);
                        if(entry.getKey().equals(caractK)) {
                            c.setListConstraint(Ontology.listAttributeProperty());
                        }
                    }
                    if(entry.getKey().equals(ObjK)) {
                        if(isACompo) {
                            c.setMendatory(true);
                            c.setListConstraint(Ontology.listComponentProperty());
                        } else {
                            c.setListConstraint(Ontology.listObjectProperty());
                        }
                    }
                    newLine.put(entry.getKey(), c);
                }
                newLine.put(numK, new ComplexField("num"));
                newLine.put(actionK, new ComplexField("action"));
            }
        }


    }

    private List<String> getColumnValues(Integer colNum, DataPart part) {
        List<String> list = new LinkedList<>();
        if(colNum >= 0 && part.getListLine().size() > 0 && part.getHeader().size() > colNum) {
            for(List<String> li : part.getListLine()) {
                list.add(li.get(colNum));
            }
            list.remove(0);

        }
        return completeLine(list, part.getNBLine()-1);
    }

    private List<String> completeLine(List<String> list, int size) {
        while(list.size() < size) {
            list.add("");
        }
        return list;
    }


    @Override
    public void unbind() {
    }

    public void addLine(ComplexField c) {
        Map<KeyWords, ComplexField> newLine = new HashMap<>();
        newLine.put(numK, new ComplexField("num"));
        newLine.put(actionK, new ComplexField("action"));
        newLine.put(caractK, c);
        c.setMendatory(true);
        c.setListConstraint(Ontology.listAttributeProperty());
        listValidation.add(c);

        ComplexField v = new ComplexField();
        v.setMendatory(true);
        newLine.put(valK, v);
        listValidation.add(v);

        ComplexField oc = new ComplexField();
        if(isACompo) {
            oc.setListConstraint(Ontology.listComponentProperty());
            oc.setMendatory(true);
        } else {
            oc.setListConstraint(Ontology.listObjectProperty());
        }
        newLine.put(ObjK, oc);
        ComplexField uc = new ComplexField();
//        uc.setListConstraint(data.listUnitProperty());
        newLine.put(unitK, uc);
        newLine.put(commentK, new ComplexField());
        content.add(newLine);


    }

    public Integer getNbLine() {
        return content.size();
    }

    public Map<KeyWords, ComplexField> addLine() {
        return addLine(-1);
    }

    public void addObservationLine(String attribut, String object, String value, String unit, String comment) {
        Map<KeyWords, ComplexField> ligne = addLine();
        ligne.get(SimpleTable.caractK).setValue(attribut);
        ligne.get(SimpleTable.ObjK).setValue(object);
        ligne.get(SimpleTable.valK).setValue(value);
        ligne.get(SimpleTable.unitK).setValue(unit);
        ligne.get(SimpleTable.commentK).setValue(comment);
    }

    public Map<KeyWords, ComplexField> addLine(Integer index) {
        Map<KeyWords, ComplexField> newLine = new HashMap<>();
        newLine.put(numK, new ComplexField("num"));
        newLine.put(actionK, new ComplexField("action"));
        ComplexField cc = new ComplexField();
        newLine.put(caractK, cc);
        cc.setListConstraint(Ontology.listAttributeProperty());
        cc.setMendatory(true);
        listValidation.add(cc);
        ComplexField v = new ComplexField();
        v.setMendatory(true);
        newLine.put(valK, v);
        listValidation.add(v);

        ComplexField oc = new ComplexField();
        if(isACompo) {
            oc.setListConstraint(Ontology.listComponentProperty());
            oc.setMendatory(true);
        } else {
            oc.setListConstraint(Ontology.listObjectProperty());
        }
        newLine.put(ObjK, oc);

        ComplexField uc = new ComplexField();
//        uc.setListConstraint(data.listUnitProperty());
        newLine.put(unitK, uc);

        newLine.put(commentK, new ComplexField());


        if(index != null && index >= 0) {
            content.add(index, newLine);
        } else {
            content.add(newLine);
        }

        data.setModified(true);
        checkValue();
        return newLine;
    }

    public void removeLine(Map<KeyWords,ComplexField> line) {
        listValidation.remove(line.get(caractK));
        listValidation.remove(line.get(valK));
        this.content.remove(line);
        data.setModified(true);
    }

    @Override // never used
    public ComplexField addColumn(Integer index) {
        return null;
    }

    @Override // never used
    public ComplexField addColumn(String value) {
        return null;
    }

    public ComplexField getComplexeValue(String value) {
        for(Map<KeyWords, ComplexField> line : content) {
            if(value.equalsIgnoreCase(line.get(caractK).getValue().get())) {
                    return line.get(valK);
            }
        }
        return null;
    }

    public ComplexField getComplexeCaract(String value) {
        for(Map<KeyWords, ComplexField> line : content) {
            if(value.equalsIgnoreCase(line.get(caractK).getValue().get())) {
                return line.get(caractK);
            }
        }
        return null;
    }

    public ComplexField getComplexeField(String value, KeyWords key) {
        for(Map<KeyWords, ComplexField> line : content) {
            if(value.equalsIgnoreCase(line.get(caractK).getValue().get())) {
                return line.get(key);
            }
        }
        return null;
    }


    public void renameComplexeCaract(String oldValue, String newValue) {
        for(Map<KeyWords, ComplexField> line : content) {
            if(oldValue.equalsIgnoreCase(line.get(caractK).getValue().get())) {
               line.get(caractK).setValue(newValue);
            }
        }
    }

//    public <T> TableColumn<T, ?> getTableColumnByName(TableView<T> tableView, String name) {
//        for (TableColumn<T, ?> col : tableView.getColumns())
//            if (col.getText().equals(name)) return col ;
//        return null ;
//    }

    public void cleanReplicatesValue(List<Integer> list) {
        for(Map<KeyWords, ComplexField> line : content) {
            line.get(valK).cleanReplicateValues(list);
        }
    }

    public LinkedList<LinkedList<String>> getDataForExcel(int replicateID) {
        LinkedList<LinkedList<String>> result = new LinkedList<>();
        Boolean firstDone = false;

          // on ajoute un header
        LinkedList<String> header = new LinkedList<>();
        header.add(caractK.getPrefLabel("en"));
        header.add(ObjK.getPrefLabel("en"));
        header.add(valK.getPrefLabel("en"));
        header.add(unitK.getPrefLabel("en"));
        header.add(commentK.getPrefLabel("en"));
        result.add(header);

        for(Map<KeyWords, ComplexField> line : content) {
            LinkedList<String> currentLine = new LinkedList<>();
            currentLine.add(line.get(caractK).getValue().get());
            currentLine.add(line.get(ObjK).getValue().get());
            if(replicateID == -1) {
                currentLine.add(line.get(valK).getValuesAsString());
            } else {
                currentLine.add(line.get(valK).getValueWithReplicate(replicateID).getValue());
            }
            currentLine.add(line.get(unitK).getValue().get());
            currentLine.add(line.get(commentK).getValue().get());
            result.add(currentLine);
        }
        return result;
    }

    public void publishAsSettings(Ontology ontology, Resource r, JSONObject json, String uuid) {
        Property hasSettingMeasure = ontology.getCoreModel().getProperty(ontology.getCORE_NS()+"hasSettingMeasure");

        Integer lineCompteur = 1;
        for(Map<KeyWords, ComplexField> line : content) {
            String attr = line.get(caractK).getValue().get();


                String unit = line.get(unitK).getValue().get();
                String value = line.get(valK).getValue().get();
            String comment = line.get(commentK).getValue().get();

            String[] listObj = line.get(ObjK).getValue().get().split("::");
                ArrayList<Resource> listResource = new ArrayList<>();
                for (String o : listObj) {
                    if(!o.isEmpty()) {
                        listResource.add(ontology.searchOrCreateObj(o, json, data.getDataModel()));
                    }
                }
                Individual iresult = JenaTools.addQuantityQuality(ontology, data, null, attr, value, unit, uuid + "_"+lineCompteur, json);
                r.addProperty(hasSettingMeasure, iresult);
                lineCompteur++;
            }

    }

    public void publishAsCharacteristics(Ontology ontology, Resource r, JSONObject json, String uuid) {
        Property hasProperty = ontology.getCoreModel().getProperty(JenaTools.SSN+"hasProperty");
        Property hasObjectOfInterest = ontology.getCoreModel().getOntProperty(JenaTools.PO2CORE + "hasObjectOfInterest");

        Integer lineCompteur = 1;
        for(Map<KeyWords, ComplexField> line : content) {
            String attr = line.get(caractK).getValue().get();
            if(!attr.equalsIgnoreCase("material type") &&
                !attr.equalsIgnoreCase("measuring instrument") &&
                !attr.equalsIgnoreCase("processing equipment") &&
                !attr.equalsIgnoreCase("identifier") &&
                !attr.equalsIgnoreCase("method type") &&
                !attr.equalsIgnoreCase("name")) {

                String unit = line.get(unitK).getValue().get();
                String value = line.get(valK).getValue().get();
                String[] listObj = line.get(ObjK).getValue().get().split("::");
                String comment = line.get(commentK).getValue().get();

                // characProperty
                OntClass classAttr = ontology.searchAttribut(attr);
                if(classAttr == null) {
                    classAttr = data.getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+Tools.shortUUIDFromString(attr));
                    classAttr.addProperty(SKOS.prefLabel,attr, "");
                    classAttr.addSuperClass(ontology.getCoreModel().getOntClass(JenaTools.SSN+"Property"));
                    json.getJSONArray("warning").put("Attribut " + attr + " not found in ontology");
                }
                Individual iAttr = data.getDataModel().createIndividual(data.getDATA_NS()+"-"+uuid+"_attr_"+Tools.normalize(attr)+"_"+ lineCompteur, classAttr);

                if(comment != null && !comment.isEmpty()) {
                    iAttr.addComment(comment, "");
                }

                for (String o : listObj) {
                    if(!o.isEmpty()) {
                        iAttr.addProperty(hasObjectOfInterest, ontology.searchOrCreateObj(o, json, data.getDataModel()));
                    }
                }

                JenaTools.addQuantityQuality(ontology, data, iAttr, attr, value, unit, uuid + "_" + lineCompteur, json);
                r.addProperty(hasProperty, iAttr);
                lineCompteur++;
            }
        }
    }

    @Deprecated
    public void publishAsComposition(Ontology ontology, Resource r, JSONObject json, String uuidWithId) {
        Property isComposedOf = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"isComposedOf");
        Property hasProperty = ontology.getCoreModel().getOntProperty(JenaTools.SSN+"hasProperty");

        Integer lineCompteur = 1;
        for(Map<KeyWords, ComplexField> line : content) {
            String compo = line.get(ObjK).getValue().get();
            if(compo != null ) {
                OntClass classCompo = ontology.searchCompo(compo);
                if(classCompo == null) {
                    if(compo.isEmpty()) {
                        classCompo = ontology.getCoreModel().getOntClass(ontology.getCORE_NS()+"Component");
                        json.getJSONArray("error").put("missing component");
                    } else {
                        logger.debug("création class compo : " + compo);
                        classCompo = data.getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+ Tools.shortUUIDFromString(compo));
                        classCompo.addProperty(SKOS.prefLabel,compo, "");
                        classCompo.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS()+"Component"));
                        json.getJSONArray("error").put("component "+ compo + " not found in ontology");
                    }

                }
                Individual iCompo = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_component_"+Tools.normalize(compo)+"_"+uuidWithId+"_"+lineCompteur , classCompo);
                String labelCompo = compo;
                String unit = line.get(unitK).getValue().get();
                String value = line.get(valK).getValue().get();
                String attr = line.get(caractK).getValue().get();
                String comment = line.get(commentK).getValue().get();

                if( !value.isEmpty()) {
                    labelCompo+=" "+value;
                }
                if( !unit.isEmpty()) {
                    labelCompo+=" "+unit;
                }

                iCompo.addProperty(SKOS.prefLabel, labelCompo, "");


                r.addProperty(isComposedOf, iCompo);


                // observedProperty
                OntClass classAttr = ontology.searchAttribut(attr);
                if(classAttr == null) {
                    classAttr = data.getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+Tools.shortUUIDFromString(attr));
                    classAttr.addProperty(SKOS.prefLabel,attr, "");
                    classAttr.addSuperClass(ontology.getCoreModel().getOntClass(JenaTools.SSN+"Property"));
                    json.getJSONArray("warning").put("Attribut " + attr + " not found in ontology");
                }
                Individual iAttr = data.getDataModel().createIndividual(data.getDATA_NS()+"-"+uuidWithId+"_attr_"+Tools.normalize(attr)+"_"+ lineCompteur, classAttr);
                iAttr.addProperty(SKOS.prefLabel, attr, "");
                JenaTools.addQuantityQuality(ontology, data, iAttr, attr, value, unit, uuidWithId + "_" + lineCompteur,  json);


                iCompo.addProperty(hasProperty, iAttr);
                if(comment != null && !comment.isEmpty()) {
                    iAttr.addComment(comment, "");
                }
            } else {
                json.getJSONArray("error").put("no component in composition table");
            }
            lineCompteur++;
        }
    }
    @Override
    public Individual publish(Ontology ontology, Resource r, Boolean raw, Integer tableIndice, JSONObject json, String obsUuid, int replicateID) {
        // 1 table = 1 observationCOllection
        myObsCollection = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_"+ obsUuid + "_"+tableIndice, ontology.getCoreModel().getOntClass(JenaTools.SOSA + "ObservationCollection"));


        OntClass sosaObs = ontology.getCoreModel().getOntClass(JenaTools.SOSA+"Observation");
        Property sosaHasMember = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"hasMember");

        Property rawResult = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"hasRawResult");
        Property calcResult = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"hasCalcResult");

        Property observedProperty = ontology.getCoreModel().getOntProperty(JenaTools.SOSA + "observedProperty");
        Property hasObjectOfInterest = ontology.getCoreModel().getOntProperty(JenaTools.PO2CORE + "hasObjectOfInterest");

        Integer lineCompteur = 0;
        for(Map<KeyWords, ComplexField> line : content) {
            // each line is a sosa observation
            Individual obsLine = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_"+ obsUuid + "_"+tableIndice + "_"+lineCompteur, sosaObs);
            myObsCollection.addProperty(sosaHasMember, obsLine); // each line (1 sosa obs) is a member of the collection sosa
            String attr = line.get(caractK).getValue().get();
            String value = line.get(valK).getValueWithReplicate(replicateID).get();
            String unit = line.get(unitK).getValue().get();
            String[] listObj = line.get(ObjK).getValue().get().split("::");
            String comment = line.get(commentK).getValue().get();

            // Result
            Individual iResult = JenaTools.addQuantityQuality(ontology, data, null, attr, value, unit, obsUuid+"_"+tableIndice + "_" + lineCompteur, json);
            obsLine.addProperty(raw ? rawResult : calcResult, iResult);
            // Sensor TODO

            // procedure TODO

            // FOI TODO

            // observedProperty
            OntClass classAttr = ontology.searchAttribut(attr);
            if(classAttr == null) {
                classAttr = data.getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+Tools.shortUUIDFromString(attr));
                classAttr.addProperty(SKOS.prefLabel,attr, "");
                classAttr.addSuperClass(ontology.getCoreModel().getOntClass(JenaTools.SSN+"Property"));
                json.getJSONArray("warning").put("Attribut " + attr + " not found in ontology");
            }
            Individual iAttr = data.getDataModel().createIndividual(data.getDATA_NS()+"-"+obsUuid+"_attr_"+Tools.normalize(attr)+"_"+tableIndice+"_"+ lineCompteur, classAttr);
            iAttr.addProperty(SKOS.prefLabel, attr, "");
            obsLine.addProperty(observedProperty, iAttr);
            if(comment != null && !comment.isEmpty()) {
                iAttr.addComment(comment, "");
            }

            for (String o : listObj) {
                if(!o.isEmpty()) {
                    iAttr.addProperty(hasObjectOfInterest, ontology.searchOrCreateObj(o, json, data.getDataModel()));
                }
            }

            lineCompteur++;
        }
        return myObsCollection;
    }

    public Map<KeyWords, ComplexField> getLine(Integer lineNB) {
        if(lineNB >= 0 && content.size() > lineNB) {
            return content.get(lineNB);
        }
        return null;
    }

    @Override
    public void cloneFrom(GenericTable table) throws CloneNotSupportedException {
        if(!(table instanceof SimpleTable)){
            throw new CloneNotSupportedException();
        }

        //clone du content
        for(Map<KeyWords, ComplexField> line : ((SimpleTable) table).content) {
            addLine(-1);
            for(Map.Entry<KeyWords, ComplexField> cell : content.get(content.size()-1).entrySet()) {
                cell.getValue().cloneFrom(line.get(cell.getKey()));
            }
        }
    }
}
