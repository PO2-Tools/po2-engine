/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.VocabConcept;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.FieldState;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.vocabulary.SKOS;
import org.json.JSONObject;

import java.util.*;

public class MaterialMethodPart extends GenericPart {

    private static final KeyWords nameK = new KeyWords(true, true,"name@en","nom@fr");
    private static final KeyWords caractK = new KeyWords(true, true,"attribute@en","attribut@fr","caracteristic@en","caractéristique@fr", "caractéristiques@fr", "caracteristique@fr");
    private static final KeyWords ObjK = new KeyWords(false, false,"object@en","objet@fr");
    private static final KeyWords valK = new KeyWords(true, true,"value@en","valeur@fr");
    private static final KeyWords unitK = new KeyWords(false, false,"unit@en","unité@fr","unite@fr", "unités@fr");
    private static final KeyWords commentK = new KeyWords(false, false,"comment@en","commentaire@fr", "description@fr");
    private static final List<KeyWords> listKeyWord = Arrays.asList(caractK, ObjK, valK, unitK, commentK);

    private ObservableList<Map<KeyWords, ComplexField>> content = FXCollections.observableArrayList();
    private ComplexField id;
    private SimpleTable dataTable;

    private String originalID;
    private ArrayList<String> originalFolder;

    private Individual myMatMeth = null;

    public MaterialMethodPart() {
        this.type = DataPartType.UNDEFINED;
        // use only for detection !!
    }
    public MaterialMethodPart(ProjectFile projectFile, KeyWords type) {
        this(projectFile, type, true);
    }

    public MaterialMethodPart(ProjectFile projectFile, KeyWords type, Boolean addToData) {
        this();
        this.type = type;
        this.data = projectFile.getData();
        dataTable = new SimpleTable(data, false);
        this.originalID = "";
        this.originalFolder = new ArrayList<>();
        addConstraint();
        if(addToData) {
            data.addNewMaterialMethod(this);
        }
    }

    public void addOriginalFolder(String folder) {
        if(!this.originalFolder.contains(folder)) {
            this.originalFolder.add(folder);
        }
    }

    public String getOriginalID() {
        return this.originalID;
    }

    public ArrayList<String> getOriginalFolder() {
        return this.originalFolder;
    }

    public void changeID() {
        this.id.setValue(this.originalID+"-"+getOntoType().getValue().get());
    }
    @Override
    public Boolean checkPart(DataPart part) {
        // on regarde s'il y a une entete
        List<String> firstLine = part.getLine(0);
        if(firstLine != null && !firstLine.isEmpty()) {
            String firstCell = firstLine.get(0);
            if(DataPartType.METHOD_RAW.map(firstCell)) { // entete OK
                this.type = DataPartType.METHOD_RAW;
                part.removeLine(0);
                part.setLineTable(true);
                return true;
            }
            if(DataPartType.MATERIAL_RAW.map(firstCell)) { // entete OK
                this.type = DataPartType.MATERIAL_RAW;
                part.removeLine(0);
                part.setLineTable(true);
                return true;
            }
        }
        return false;
    }

    @Override
    public void unbind() {

    }


    public void addConstraint() {
        dataTable.renameComplexeCaract("identifiant", nameK.getPrefLabel("en"));
        dataTable.renameComplexeCaract("identifier", nameK.getPrefLabel("en"));
        dataTable.renameComplexeCaract("appareil utilisé", "material type");
        dataTable.renameComplexeCaract("nom de la méthode","method type");


        if(dataTable.getComplexeCaract(nameK.getPrefLabel("en")) == null) {
            dataTable.addLine(new ComplexField(nameK.getPrefLabel("en")));
        }
        id =  dataTable.getComplexeValue(nameK.getPrefLabel("en"));
        id.setMendatory(true);
        originalID = id.getValue().get();
        ComplexField temp = dataTable.getComplexeCaract(nameK.getPrefLabel("en"));
        temp.setEditable(false);
        temp.setCheckValue(false);


        if(this.type != null && this.type.equals(DataPartType.MATERIAL_RAW)) {
            if(dataTable.getComplexeCaract("material type") == null) {
                dataTable.addLine(new ComplexField("material type"));
            }
            dataTable.getComplexeValue("material type").setMendatory(true);
            temp = dataTable.getComplexeCaract("material type");
            temp.setEditable(false);
            temp.setCheckValue(false);

        }
        if(this.type != null && this.type.equals(DataPartType.METHOD_RAW)) {
            if(dataTable.getComplexeCaract("method type") == null) {
                dataTable.addLine(new ComplexField("method type"));
            }
            dataTable.getComplexeValue("method type").setMendatory(true);
            temp = dataTable.getComplexeCaract("method type");
            temp.setEditable(false);
            temp.setCheckValue(false);
        }

        ObservableList<VocabConcept> t = FXCollections.observableArrayList();

        ComplexField caractApp = dataTable.getComplexeValue("material type");
        if(caractApp != null) {
            caractApp.setConceptConstraint(Ontology.Material);
            caractApp.setListConstraint(Ontology.listMaterialProperty());
            caractApp.setIsConstrained(true);
        }

        ComplexField caractMeth = dataTable.getComplexeValue("method type");
        if(caractMeth != null) {
            caractMeth.setConceptConstraint(Ontology.Method);
            caractMeth.setListConstraint(Ontology.listMethodProperty());
            caractMeth.setIsConstrained(true);
        }

        List<Map<KeyWords, ComplexField>> lineToRemove = new ArrayList<>();
        for(Map<KeyWords, ComplexField> line : dataTable.getContent()) {
            if("measuring instrument".equalsIgnoreCase(line.get(SimpleTable.caractK).getValue().get())) {
                lineToRemove.add(line);
            }
            if("processing equipment".equalsIgnoreCase(line.get(SimpleTable.caractK).getValue().get())) {
                lineToRemove.add(line);
            }
        }
        lineToRemove.forEach(l -> dataTable.removeLine(l));
    }

    public boolean isMaterial() {
        return this.type.equals(DataPartType.MATERIAL_RAW);
    }

    public boolean isMethod() {
        return this.type.equals(DataPartType.METHOD_RAW);
    }


    @Override
    public void mapPart(DataPart part) {

            dataTable.mapPart(part);

            addConstraint();
    }

    @Override
    public void checkValue() {
        dataTable.checkValue();
    }

    @Override
    public List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public void addLine(Integer index) {
        dataTable.addLine(index);
    }

    public ComplexField getID() {
        if(id == null) {
            id = new ComplexField("unknown");
        }
        return id;
    }

    public void setId(String idName) {
        if(id == null) {
            id = new ComplexField("");
        }
        id.setValue(idName);
    }

    public void addCharacteristic(String attribut, String object, String value, String unit, String comment) {
        Map<KeyWords, ComplexField> ligne = this.dataTable.addLine();
        ligne.get(SimpleTable.caractK).setValue(attribut);
        ligne.get(SimpleTable.ObjK).setValue(object);
        ligne.get(SimpleTable.valK).setValue(value);
        ligne.get(SimpleTable.unitK).setValue(unit);
        ligne.get(SimpleTable.commentK).setValue(comment);
        checkValue();
    }

    public void setComment(String comment) {
        ComplexField f = this.dataTable.getComplexeField(nameK.getPrefLabel("en"), SimpleTable.commentK);
        if(f != null) {
            f.setValue(comment);
        }
    }

    public String getComment() {
        ComplexField f = this.dataTable.getComplexeField(nameK.getPrefLabel("en"), SimpleTable.commentK);
        if(f != null) {
            return f.getValue().get();
        }
        return "";
    }

    public ComplexField getIDCaract() {
        return dataTable.getComplexeCaract(nameK.getPrefLabel("en"));
    }

    public ComplexField getOntoType() {
        if(this.type.equals(DataPartType.MATERIAL_RAW)) {
            return dataTable.getComplexeValue("material type");
        }
        if(this.type.equals(DataPartType.METHOD_RAW)) {
            return dataTable.getComplexeValue("method type");
        }
        return null;
    }

    public void setOntoType(String type) {
        getOntoType().setValue(type);
    }

    public SimpleTable getDataTable() {
        return dataTable;
    }


    public void validateData() {

    }

    public SimpleObjectProperty<FieldState> stateProperty() {
        return dataTable.stateProperty();
    }


    @Override
    public String toString() {
        return this.getID().getValue().get();
    }

    public LinkedList<LinkedList<String>> getDataForExcel() {
        return dataTable.getDataForExcel();
    }

    public Data getData() {
        return data;
    }

    public Individual getMatMethIndividual() {
        return myMatMeth;
    }
    public Individual publish(Ontology ontology, String projectUuid, JSONObject json, Integer matMetCompteur) {
        myMatMeth = null;

        String matmet = getOntoType().getValue().get();
        if(this.type != null && this.type.equals(DataPartType.MATERIAL_RAW)) {
            OntClass classMat = ontology.searchMaterial(matmet);
            if(classMat == null) {
                classMat = data.getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+ Tools.shortUUIDFromString(matmet));
                classMat.addProperty(SKOS.prefLabel, matmet,"");
                classMat.addSuperClass(ontology.getCoreModel().getOntClass(JenaTools.SOSA+"System"));
                if(matmet.isEmpty()) {
                    json.getJSONArray("error").put("Material with empty name");
                }
                json.getJSONArray("warning").put("Material "+ matmet + " not found in ontology");
            }
            myMatMeth = data.getDataModel().createIndividual(data.getDATA_NS()+"material_"+projectUuid+"_"+matMetCompteur, classMat);
            myMatMeth.addProperty(SKOS.prefLabel, getID().getValue().get(), "");
            // ajout des characteristics
            dataTable.publishAsCharacteristics(ontology, myMatMeth, json, projectUuid+"_"+matMetCompteur);

        }
        if(this.type != null && this.type.equals(DataPartType.METHOD_RAW)) {
            OntClass classMethod = ontology.searchMethod(matmet);
            if(classMethod == null) {
                classMethod = data.getDataModel().createClass(ontology.getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(matmet));
                classMethod.addProperty(SKOS.prefLabel, matmet, "");
                classMethod.addSuperClass(ontology.getCoreModel().getOntClass(JenaTools.SOSA + "Procedure"));
                if(matmet.isEmpty()) {
                    json.getJSONArray("error").put("Method with empty name");
                }
                json.getJSONArray("warning").put("Method "+ matmet + " not found in ontology");
            }
            myMatMeth = data.getDataModel().createIndividual(data.getDATA_NS()+"method_"+projectUuid+"_"+matMetCompteur, classMethod);
            myMatMeth.addProperty(SKOS.prefLabel, getID().getValue().get(), "");
            // ajout des characteristics
            dataTable.publishAsCharacteristics(ontology, myMatMeth, json,projectUuid+"_"+matMetCompteur );
        }

        return myMatMeth;
    }

    public void initPublish() {
        myMatMeth = null;
    }
}
