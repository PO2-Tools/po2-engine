/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import org.apache.jena.ontology.Individual;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ConsignPart extends GenericPart {

    private SimpleTable dataTable;
    private static final List<KeyWords> listKeyWord = List.of();


    public ConsignPart() {
        this.type = DataPartType.CONSIGN;
        // only used for detection !!
    }
    public ConsignPart( Data data) {
        this();
        this.data = data;
        dataTable = new SimpleTable(data, false);
    }

    public Integer getSize() {
        if(dataTable != null) {
            return dataTable.getSize();
        }
        return 0;
    }


    @Override
    public Boolean checkPart(DataPart part) {
        // on regarde s'il y a une entete
        List<String> firstLine = part.getLine(0);
        if(firstLine != null && !firstLine.isEmpty()) {
            String firstCell = firstLine.get(0);
            if(this.type.map(firstCell)) { // entete OK
                part.removeLine(0);
                part.setLineTable(true);
                return true;
            }
        }
        return false;
    }

    @Override
    public void unbind() {
        // not implemented.
    }

    @Override
    public void mapPart(DataPart part) {
        dataTable.mapPart(part);
    }

    @Override
    public void checkValue() {
        dataTable.checkValue();
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public SimpleTable getDataTable() {
        return this.dataTable;
    }
//    public void bindData(TableView tableView) {
//        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
//        UITools.bindSimpleTable(dataTable, tableView);
//    }

    public void addLine(Integer index) {
        dataTable.addLine(index);
    }

    public void addCharacteristic(String attribut, String object, String value, String unit, String comment) {
        Map<KeyWords, ComplexField> ligne = this.dataTable.addLine();
        ligne.get(SimpleTable.caractK).setValue(attribut);
        ligne.get(SimpleTable.ObjK).setValue(object);
        ligne.get(SimpleTable.valK).setValue(value);
        ligne.get(SimpleTable.unitK).setValue(unit);
        ligne.get(SimpleTable.commentK).setValue(comment);
    }

    public void validateData() {
        //todo
    }

    public LinkedList<LinkedList<String>> getDataForExcel() {
        return dataTable.getDataForExcel();
    }

    public void publish(Ontology ontology, Individual iActuator, JSONObject json, String uuid) {
        dataTable.publishAsCharacteristics(ontology, iActuator, json, uuid);
    }

    public void cloneFrom(ConsignPart part) {
        try {
            dataTable.cloneFrom(part.dataTable);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
