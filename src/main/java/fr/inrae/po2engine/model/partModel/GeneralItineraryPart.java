/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.FieldState;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.model.vocabulary.PROV;
import org.eclipse.rdf4j.model.vocabulary.TIME;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

public class GeneralItineraryPart extends GenericPart {

    public static final KeyWords fatherK = new KeyWords(true, true, "father step@en", "etape parent@fr", "étape parent@fr");
    public static final KeyWords sonK = new KeyWords(true, true, "son step@en", "etape enfant@fr", "étape enfant@fr");
    private static final KeyWords itin = new KeyWords(false, false, "itinerary@en", "itinéraire@fr");
    private static final KeyWords observationItiK = new KeyWords(true, false, "observation itinerary@en");
    public static final KeyWords prodOfInterestK = new KeyWords(false, false, "products of interest type@en");
    public static final KeyWords graphLayoutK = new KeyWords(false, false, "graph layout@en");
    private static final List<KeyWords> listKeyWord = Arrays.asList(fatherK, sonK, prodOfInterestK, observationItiK);

    private static Logger logger = LogManager.getLogger(GeneralItineraryPart.class);

    private ItineraryFile itiFile;
    private GeneralFile processFile;
    private SimpleStringProperty itiName = null;
    private SimpleStringProperty itiNumber = null;

    private ObservableList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> itinerary = FXCollections.observableArrayList();

    private ObservableList<StepFile> listStep;
    private ObservableList<ObservationFile> listObservation ;
    private ComplexField prodOfInterestC;
    private HashMap<String, String> listNodePosition;

    private Map<Integer, Individual> myInterval = new HashMap<>();
    private Map<Integer, Individual> myItinerary = new HashMap<>();

    public GeneralItineraryPart() {
        this.type = DataPartType.ITINERARY;
        // only used for detection !!
    }
    public GeneralItineraryPart(GeneralFile processFile, ItineraryFile file) {
        this();
        ChangeListener<String> l = (observableValue, o, t1) -> {
            data.setModified(true);
        };

        this.listStep = FXCollections.observableArrayList();
        this.listObservation = FXCollections.observableArrayList();
        this.itiFile = file;
        this.data = processFile.getData();
        this.processFile = processFile;
        this.itiName = new SimpleStringProperty("");
        this.itiName.addListener(l);
        this.itiNumber = new SimpleStringProperty("Itinerary " + findAvailableItiNumber());
        this.prodOfInterestC = new ComplexField();
        this.prodOfInterestC.getValue().addListener(l);
        this.listNodePosition = new HashMap<>();
    }

    @Override
    public List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    private Integer findAvailableItiNumber() {
        final Integer[] n = new Integer[1];
        n[0] = this.processFile.getItinerary().size()+1; // the default value.

        while(this.processFile.getItinerary().stream().anyMatch(itineraryFile -> itineraryFile.getItineraryNumber().equalsIgnoreCase("Itinerary "+ n[0]))) {
            n[0] = n[0]+1;
        }
        return n[0];
    }

    public void clear() {
        for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : itinerary) {

            pair.getKey().getValue().setValue(null);
            pair.getValue().getValue().setValue(null);
        }
//        generateStepDataNode();
        ObservableList<ObservationFile> tempList = FXCollections.observableArrayList(listObservation);
        tempList.forEach(ObservationFile::removeFile);
        tempList.clear();
        this.clearNodePosition();
        this.data.setModified(true);
    }


    @Override
    public void unbind() {
        processFile = null;

    }

    @Override
    public Boolean checkPart(DataPart part) {
        Boolean find = checkKeyword(part.getLine(0));
        if (!find) {
            // comm sur la premiere ligne ?
            find = checkKeyword(part.getLine(1));
            if (find) {
                // comm sur la premiere ligne
                String itiHeader = part.getLine(0).stream().collect(Collectors.joining());
                part.removeLine(0);
                part.getLine(0).add("itinerary");
                part.getLine(1).add(itiHeader);
                part.removeEmptyCol();
            } else {
                // iti avec observation ?
                if(observationItiK.map(part.getLine(1).get(0))) {
                    find = true;
                    String itiHeader = part.getLine(0).stream().collect(Collectors.joining());
                    part.removeLine(0);
                    ArrayList<String> obsFile = new ArrayList<>();
                    while(observationItiK.map(part.getLine(0).get(0))) {
                        obsFile.add(part.getLine(0).get(1));
                        part.removeLine(0);
                    }
                    String prodOfInterest = "";
                    if(prodOfInterestK.map(part.getLine(0).get(0))) {
                        prodOfInterest = part.getLine(0).get(1);
                        part.removeLine(0);
                    }
                    String graphLayout = "";
                    if(graphLayoutK.map(part.getLine(0).get(0))) {
                        graphLayout = part.getLine(0).get(1);
                        part.removeLine(0);
                    }
                    part.getLine(0).add("itinerary");
                    part.getLine(0).add("observation itinerary");
                    part.getLine(0).add(prodOfInterestK.getPrefLabel("en"));
                    part.getLine(0).add(graphLayoutK.getPrefLabel("en"));
                    Integer ind = 1;
                    for(String obs : obsFile) {
                        if(part.getNBLine() <= ind) {
                            // on doit ajouter une ligne avec deux colones : father et son!
                            part.addLine(new ArrayList<>(Arrays.asList("", "")));
                        }
                        if(ind == 1) {
                            part.getLine(ind).add(itiHeader);
                        } else {
                            part.getLine(ind).add("");
                        }
                        part.getLine(ind).add(obs);
                        if(ind == 1) {
                            part.getLine(ind).add(prodOfInterest);
                        } else {
                            part.getLine(ind).add("");
                        }
                        if(ind == 1) {
                            part.getLine(ind).add(graphLayout);
                        } else {
                            part.getLine(ind).add("");
                        }
                        ind++;
                    }

                    part.removeEmptyCol();
                }
            }

        } else { // iti sans header
            part.getLine(0).add("itinerary");
            part.removeEmptyCol();
        }

        return find;
    }

    public GeneralFile getProcessFile() {
        return this.processFile;
    }

    public SimpleStringProperty getItiName() {
        return this.itiName;
    }
    public SimpleStringProperty getItiNumber() {
        return this.itiNumber;
    }

    public ObservableList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> getItinerary() {
        return itinerary;
    }

    public void removeLinkItinerary(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link) {
        getItinerary().remove(link);
        listStep.clear();
        getItinerary().forEach(pairPairPair -> {
            StepFile father = pairPairPair.getKey().getValue().get();
            StepFile son = pairPairPair.getValue().getRight().get();
            addStep(father);
            addStep(son);
        });
        data.setModified(true);
    }

    public void removeStepFromItinerary(StepFile step) {
        if(step != null) {
            ArrayList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> listLinkToRemove = new ArrayList<>();
            for(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link : itinerary) {
                if(link.getKey().getValue().get() == step || link.getValue().getValue().get() == step) {
                    listLinkToRemove.add(link);
                }
            }
            for(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link : listLinkToRemove) {
                removeLinkItinerary(link);
            }
        }
    }

    public void removeLinkItinerary(StepFile father, StepFile son) {
        if(father != null || son != null) {
            ArrayList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> listLinkToRemove = new ArrayList<>();
            for(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link : itinerary) {
                if(link.getKey().getValue().get() == father && link.getValue().getValue().get() == son) {
                    listLinkToRemove.add(link);
                }
            }

            for(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link : listLinkToRemove) {
                removeLinkItinerary(link);
            }
        }
    }

    public Boolean directLinkExist(StepFile father, StepFile son) {
        if(father != null || son != null) {
            return this.itinerary.stream().anyMatch(pairPairPair -> pairPairPair.getKey().getValue().get() == father && pairPairPair.getValue().getValue().get() == son);
        }
        return false;
    }

    public void addLinkItinerary(StepFile father, StepFile son) {
        if(!directLinkExist(father, son)) {
            if (father != null || son != null) {
                ComplexField cFather = new ComplexField("");
                if (father != null) {
                    cFather = new ComplexField(father.getNameProperty().getValue());
                    addStep(father);
                }
                ComplexField cSon = new ComplexField("");
                if (son != null) {
                    cSon = new ComplexField(son.getNameProperty().getValue());
                    addStep(son);
                }

                Pair<ComplexField, ObjectProperty<StepFile>> p1 = new MutablePair<>(cFather, new SimpleObjectProperty<>(father));
                Pair<ComplexField, ObjectProperty<StepFile>> p2 = new MutablePair<>(cSon, new SimpleObjectProperty<>(son));
                Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> p3 = new MutablePair<>(p1, p2);
                this.itinerary.add(p3);
                this.data.setModified(true);
            }
        }
    }

    public void addStep(StepFile s) {
        if(s != null) {
            if (!this.listStep.contains(s)) {
                this.listStep.add(s);
            }
            this.data.setModified(true);
        }
    }

    public ObservableList<StepFile> getListStep() {
        return this.listStep;
    }

    public ObservableList<ObservationFile> getListObservation() {
        return this.listObservation;
    }

    public ComplexField getCProdOfInterest() {
        return prodOfInterestC;
    }

    public void setNodePosition(String uuid, double xCoord, double yCoord) {
        this.listNodePosition.put(uuid, xCoord +","+ yCoord +"!");
//        "!" at the end is to force graphviz layout to not move the node
    }

    public String getNodePosition(String uuid) {
        return this.listNodePosition.getOrDefault(uuid, null);
    }

    public void clearNodePosition() {
        this.listNodePosition.clear();
    }

    public Individual getMyItinerary(int replicateID) {
        if(this.myItinerary.containsKey(replicateID)) return this.myItinerary.get(replicateID);
        return null;
    }
    public Individual getMyInterval(int replicateID) {
        if(this.myInterval.containsKey(replicateID)) return this.myInterval.get(replicateID);
        return null;
    }

    @Override
    public void mapPart(DataPart part) {

        List<String> valF = getColumnValues(fatherK, part);
        List<String> valS = getColumnValues(sonK, part);
        for (int i = 0; i < valF.size(); i++) {
            if(!valF.get(i).isEmpty() || !valS.get(i).isEmpty()) {
                ComplexField p = new ComplexField(valF.get(i));
                StepFile fs = null;
                ComplexField s = new ComplexField(valS.get(i));
                StepFile ss = null;

                Optional<StepFile> fatherStep = null;
                if(!p.getUuidProperty().isEmpty().get()) {
                    fatherStep = this.processFile.getListStep().stream().filter(st -> st.getUuid(true).equalsIgnoreCase(p.getUuidProperty().get())).findAny();
                } else {
                    fatherStep = this.processFile.getListStep().stream().filter(st -> st.getOntoType().equalsIgnoreCase(p.getValue().get()) && st.getId().equalsIgnoreCase(p.getID().get())).findAny();
                }
                if(fatherStep.isPresent()) {
                    fs = fatherStep.get();
                    addLinkItinerary(fs, null);
                }
                Optional<StepFile> sonStep = null;
                if(!s.getUuidProperty().isEmpty().get()) {
                    sonStep = this.processFile.getListStep().stream().filter(st -> st.getUuid(true).equalsIgnoreCase(s.getUuidProperty().get())).findAny();
                } else {
                    sonStep = this.processFile.getListStep().stream().filter(st -> st.getOntoType().equalsIgnoreCase(s.getValue().get()) && st.getId().equalsIgnoreCase(s.getID().get())).findAny();
                }
                if(sonStep.isPresent()) {
                    ss = sonStep.get();
                    addLinkItinerary(ss, null);
                }
                addLinkItinerary(fs, ss);
            }
        }
        List<String> valName = getColumnValues(itin, part);
        if(valName.size() > 0) {
            itiName.set(valName.get(0));
        }

        List<String> obsFileName = getColumnValues(observationItiK, part);
        for(String obs : obsFileName) {
            obs = obs.trim();
            if(obs != null && !obs.isEmpty()) {
//                            String filePath = this.data.getFilePath(obs); // on cherche dans le projet
                String folderPath = this.processFile.searchFolderPath(obs); // on cherche dans le process

                if (folderPath != null && !folderPath.isEmpty()) {
                    new ObservationFile(folderPath, obs, this.itiFile);
                } else {
                    logger.error("the obs file can't be found : " + obs);
                }
            }
        }

        List<String> prodList = getColumnValues(prodOfInterestK, part);
        if(prodList.size() > 0) {
            prodOfInterestC.setValue(prodList.get(0));
        }
        if(prodOfInterestC.getValue().get().isEmpty()) {
            // check for old sample type style
            String old = this.processFile.getOldSampleType();
            if(!old.isEmpty()) {
                prodOfInterestC.setValue(old);
            }
        }
        List<String> layout = getColumnValues(graphLayoutK, part);
        if(!layout.isEmpty() && !layout.get(0).isEmpty()) {
            try {
                JSONObject obj = new JSONObject(layout.get(0));
                obj.keySet().forEach(k -> listNodePosition.put(k, obj.getString(k)));
            } catch (JSONException je) {
                logger.error("can't parse json : ", je);
            }
        }
    }

    @Override
    public void checkValue() {
        // nothing to do here
    }

    public ArrayList<String> getProductsOfInterest() {
        return Arrays.stream(prodOfInterestC.getValue().get().split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toCollection(ArrayList::new));
    }

    public Individual publish(Ontology ontology, OntClass classProcess, JSONObject json, String uuid, int replicateID) {
        OntModel coreModel = ontology.getCoreModel();
        OntModel dataModel = this.processFile.getData().getDataModel();
        OntClass properInterval = coreModel.getOntClass(TIME.PROPER_INTERVAL.toString());
        Property phenomenonTime = coreModel.getOntProperty(JenaTools.SOSA + "phenomenonTime");
        Property wasInfluencedBy = coreModel.getOntProperty(PROV.WAS_INFLUENCED_BY.toString());

        String replicateUUID = replicateID == -1 ? "_" : "_" + replicateID+"_";

        myItinerary.put(replicateID,dataModel.createIndividual(this.processFile.getData().getDATA_NS() + "po2_" + uuid+replicateUUID+ Tools.normalize(getItiNumber().get()), classProcess));
        myInterval.put(replicateID, dataModel.createIndividual(this.processFile.getData().getDATA_NS() + "po2_interval_" + uuid+replicateUUID+ Tools.normalize(getItiNumber().get()), properInterval));
        myItinerary.get(replicateID).addProperty(phenomenonTime, myInterval.get(replicateID));

        Property hasStep = coreModel.getOntProperty(ontology.getCORE_NS() + "hasStep");
        Property intervalBefore = coreModel.getOntProperty(JenaTools.TIME + "intervalBefore");
        Property hasProductOfInterest = coreModel.getOntProperty(ontology.getCORE_NS()+"hasProductOfInterest");


        for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> iti : getItinerary()) {
            StepFile parent = iti.getLeft().getRight().get();
            StepFile enfant = iti.getRight().getRight().get();
            if (parent != null) {
                myItinerary.get(replicateID).addProperty(hasStep, parent.getStepIndividual(replicateID));
            }
            if (enfant != null) {
                myItinerary.get(replicateID).addProperty(hasStep, enfant.getStepIndividual(replicateID));
            }
            if (parent != null && enfant != null) {
                parent.getStepInterval(replicateID).addProperty(intervalBefore, enfant.getStepInterval(replicateID));
                enfant.getStepInterval(replicateID).addProperty(wasInfluencedBy,parent.getStepInterval(replicateID));
            }
        }

        // on publie les observations d'itinéraires

        for(ObservationFile observationFile : listObservation) {
            JSONObject objJSON = new JSONObject();
            objJSON.put("name", "observation " + observationFile.getNameProperty().get());
            objJSON.put("error", new JSONArray());
            objJSON.put("warning", new JSONArray());
            objJSON.put("subObject", new JSONArray());
            objJSON.put("type", "observation");
            json.getJSONArray("subObject").put(objJSON);
            observationFile.publish(ontology, myInterval.get(replicateID), objJSON, replicateID);
        }

        // on ajoute les product of interest
        for(String prod : getProductsOfInterest()) {
            OntClass classCompo = ontology.searchCompo(prod);
            if(classCompo == null) {
                if(prod.isEmpty()) {
                    classCompo = ontology.getCoreModel().getOntClass(ontology.getCORE_NS()+"Component");
                    json.getJSONArray("warning").put("Composition without type");
                } else {
                    logger.debug("création class compo : " + prod);
                    json.getJSONArray("warning").put("Composition class " + prod + " not found in ontology");
                    classCompo = this.processFile.getData().getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+Tools.shortUUIDFromString(prod));
                    classCompo.addProperty(SKOS.prefLabel, prod, "");
                    classCompo.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS()+"Component"));
                }
            }
            myItinerary.get(replicateID).addProperty(hasProductOfInterest, classCompo);
        }

        return myItinerary.get(replicateID);
    }

    public boolean contains(StepFile sub) {
        for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : itinerary) {
            if ((pair.getKey().getValue().get() != null && pair.getKey().getValue().get().equals(sub)) || (pair.getValue().getValue().get() != null && pair.getValue().getValue().get().equals(sub))) {
                return true;
            }
        }
        return false;
    }

    public void addObservation(ObservationFile newFile) {
        this.listObservation.add(newFile);
        this.data.setModified(true);
    }

    public String getNodePositionAsString() {
        return new JSONObject(this.listNodePosition).toString();
    }

}
