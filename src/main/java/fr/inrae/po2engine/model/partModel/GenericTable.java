/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import org.apache.jena.ontology.Individual;
import org.apache.jena.rdf.model.Resource;
import org.json.JSONObject;

import java.util.LinkedList;

public abstract class GenericTable {

    public void mapPart(DataPart part) {
        mapPart(part, -1);
    }
    public abstract void mapPart(DataPart part, int replicate);
    public abstract void unbind();
    public abstract ComplexField addColumn(Integer index);
    public abstract ComplexField addColumn(String value);
    public abstract LinkedList<LinkedList<String>> getDataForExcel(int replicateID);
    public LinkedList<LinkedList<String>> getDataForExcel() {
        return getDataForExcel(-1);
    };
    public abstract Integer getSize() ;
    public abstract void addMap(DataPart p, int replicateID) ;
    public abstract void checkValue();
    public Data data;


    public abstract Individual publish(Ontology ontology, Resource r, Boolean raw, Integer tableIndice, JSONObject json, String uuid, int replicateID);

    public abstract void cloneFrom(GenericTable table) throws CloneNotSupportedException;

    public Data getData() {
        return this.data;
    }
}
