/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

public class GeneralContentPart extends GenericPart {

    private static final KeyWords stepK = new KeyWords(true, true,"step@en","étape@fr", "etape@fr");
    private static final KeyWords substepK = new KeyWords(true, true,"substep@en","sous-étape@fr", "sous-etape@fr");
    private static final KeyWords measureK = new KeyWords(true, true,"measure@en","mesure@fr");
    private static final KeyWords measureLK = new KeyWords(true, true,"measure level@en","niveau de mesure@fr");
    private static final KeyWords fileNameK = new KeyWords(true, true,"filename@en", "nom du fichier@fr");
    private static Logger logger = LogManager.getLogger(GeneralContentPart.class);
    private static final List<KeyWords> listKeyWord = Arrays.asList(stepK, substepK,measureK, measureLK, fileNameK);

    private GeneralFile generalFile;
    private Map<KeyWords, List<ComplexField>> content = new LinkedHashMap<>();

    private Set<StepFile> listStep = new LinkedHashSet<>();

    public GeneralContentPart() {
        this.type = DataPartType.RAW_DATA;
        // only used for detection !!
    }
    public GeneralContentPart(GeneralFile generalFile) {
        this();
        this.generalFile = generalFile;
        this.data = this.generalFile.getData();
        content.put(stepK, new LinkedList<>());
        content.put(substepK, new LinkedList<>());
        content.put(measureK, new LinkedList<>());
        content.put(measureLK, new LinkedList<>());
        content.put(fileNameK, new LinkedList<>());
    }

    public Set<StepFile> getListStep() {
        return this.listStep;
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getLine(0));
    }

    public Map<KeyWords, List<ComplexField>> getContent() {
        return this.content;
    }
    @Override
    public void unbind() {
        for(ComplexField c : content.get(fileNameK)) {
            c.getValue().unbind();
        }
        for(ComplexField c : content.get(substepK)) {
            c.getValue().unbind();
        }
        for(ComplexField c : content.get(stepK)) {
            c.getValue().unbind();
        }
        for(ComplexField c : content.get(measureK)) {
            c.getValue().unbind();
        }
        for(ComplexField c : content.get(measureLK)) {
            c.getValue().unbind();
        }
    }

//    public void setDataNode(DataNode node) {
//        this.node = node;
//    }

//    public void unMapPart() {
//        content.get(stepK).clear();
//        content.get(substepK).clear();
//        content.get(measureK).clear();
//        content.get(measureLK).clear();
//        content.get(fileNameK).clear();
////        for(GeneralItineraryPart iti : this.generalFile.getItinerary()) {
////            iti.clear();
////        }
//
////        for(DataNode itiNode : this.getDataNode().getSubNode()) {
////            itiNode.getSubNode().clear();
////        }
////        this.getDataNode().clearSubNode();
//        this.generalFile.getDataNode().clearSubNode();
//        this.generalFile.getItinerary().clear();
//        listStep.clear();
//    }

    @Override
    public void mapPart(DataPart part) {
        List<String> valStep = getColumnValues(stepK, part);
        List<String> valSubStep = getColumnValues(substepK, part);
        List<String> valMeasure = getColumnValues(measureK, part);
        List<String> valMeasureL = getColumnValues(measureLK, part);
        List<String> valFileName = getColumnValues(fileNameK, part);

        for(String val : valStep) {
            content.get(stepK).add(new ComplexField(val));
        }
        for(String val : valSubStep) {
            content.get(substepK).add(new ComplexField(val));
        }
        for(String val : valMeasure) {
            content.get(measureK).add(new ComplexField(val));
        }
        for(String val : valMeasureL) {
            content.get(measureLK).add(new ComplexField(val));
        }
        for(String val : valFileName) {
            content.get(fileNameK).add(new ComplexField(val));
        }

        Tools.updateProgress(ProgressPO2.OPEN, "identifying objects", 0.0);
        Double ppO = 0.5/content.get(stepK).size();
        for(int i = 0; i < content.get(stepK).size(); i++) {
            Tools.updateProgress(ProgressPO2.OPEN, Tools.getProgress(ProgressPO2.OPEN).getProgress() + ppO);
            ComplexField s0 = content.get(stepK).get(i);
            StepFile step0 = null;

            ComplexField s1 = content.get(substepK).get(i);
            StepFile step1 = null;
            if(!s0.getValue().getValue().isEmpty() && !s0.getValue().getValue().equalsIgnoreCase("na")) {
                for(StepFile s : listStep) {
                    if(!s0.getUuidProperty().isEmpty().get()) {
                        if(s.getUuid(true).equalsIgnoreCase(s0.getUuidProperty().get())) {
                            step0 = s;
                        }
                    } else {
                        if(s.getOntoType().equalsIgnoreCase(s0.getValue().getValue()) && s.getId().equalsIgnoreCase(s0.getID().getValue())) {
                            step0 = s;
                        }
                    }
                }
                if(step0 == null) {
                    step0 = new StepFile(s0, generalFile);
                }
            }

            if(!s1.getValue().getValue().isEmpty() && !s1.getValue().getValue().equalsIgnoreCase("na")) {
                for(StepFile s : listStep) {
                    if(!s1.getUuidProperty().isEmpty().get()) {
                        if(s.getUuid(true).equalsIgnoreCase(s1.getUuidProperty().get())) {
                            step1 = s;
                        }
                    } else {
                        if (s.getOntoType().equalsIgnoreCase(s1.getValue().getValue()) && s.getId().equalsIgnoreCase(s1.getID().getValue())) {
                            step1 = s;
                        }
                    }
                }
                if(step1 == null) {
                    step1 = new StepFile(s1, generalFile);
                }
            }

            if(step0 != null && step1 != null) {
                step0.addSubStep(step1);
            }


            if(content.get(measureK).get(i).getValue().get().toLowerCase().startsWith("composition")) {
                String[] listCompo = content.get(fileNameK).get(i).getValue().get().replaceAll("[\n\r]", "").split(";");
                for(String compo : listCompo) {
//                    String filePath = this.data.getFilePath(compo); on cherche dans tout les fichiers du projet
                    String folderPath = this.generalFile.searchFolderPath(compo); //on cherche dans tout les fichiers du process :
                    if(folderPath != null && !folderPath.isEmpty()) {
                        // check if project folder path is in filePath

                        Boolean isInput = true;
                        if(content.get(measureK).get(i).getValue().get().toLowerCase().contains("output")) {
                            isInput = false;
                        }
                        // check if compo file is already loaded in an other step !
                        CompositionFile newCompo = null;
                        for(StepFile s : listStep) {
                            for(CompositionFile c : s.getCompositionFile().keySet()) {
                                if(c.getFolderPath().equalsIgnoreCase(folderPath) && c.getFileName().equalsIgnoreCase(compo)) {
                                    newCompo = c;
                                }
                            }
                        }
                        StepFile step = step1 != null ? step1 : step0;
                        if(step != null) {
                            if (newCompo == null) {
                                new CompositionFile(folderPath, compo, step, isInput);
                            } else {
                                step.addCompositionFile(newCompo, isInput);
                            }
                        } else {
                            // errreur composition sans step !!!!!!
                        }
                    } else {
                        logger.error("the composition file can't be found : " + compo);
                    }
                }
            } else {
                if (content.get(measureK).get(i).getValue().get().equalsIgnoreCase("conduite")) { // ancien format.
                    String[] listConduite = content.get(fileNameK).get(i).getValue().get().replaceAll("[\n\r]", "").split(";");
                    for(String cond : listConduite) {
//                        String filePath = this.data.getFilePath(cond); // on cherche dans le projet
                        String folderPath = this.generalFile.searchFolderPath(cond); // on cherche dans le process

                        if(folderPath != null && !folderPath.isEmpty()) {
                            StepFile step = step1 != null ? step1 : step0;
                            if(step != null) {
                                CompositionFile com = step.getCompositionFile().keySet().stream().findFirst().orElse(null);
                            }
                        } else {
                            logger.error("the conduite file can't be found : " + cond);
                        }
                    }
                } else {
                    if(content.get(measureK).get(i).getValue().get().equalsIgnoreCase("step data")) {
                        StepFile step = step1 != null ? step1 : step0;
                        String fileName = content.get(fileNameK).get(i).getValue().get();
                        String folderPath = this.generalFile.searchFolderPath(fileName);
                        if (step != null) {
                            if(folderPath == null || folderPath.isEmpty()) {
                                logger.error("the step file can't be found : " + fileName);
                                folderPath = generalFile.getFolderPath() + Tools.normalize(step.getCType().getValue().get()) + File.separator;
                                logger.error("setting a new path : " + folderPath+fileName);
                            }
                            step.setFile(folderPath, fileName);
                        }
                    } else {
                        if(content.get(measureK).get(i).getValue().get().equalsIgnoreCase("step material method")) {
                            String[] listStepMatMet = content.get(fileNameK).get(i).getValue().get().replaceAll("[\n\r]", "").split(";");
                            for(String matmet : listStepMatMet) {
    //                            String filePath = this.data.getFilePath(matmet); // on cherche dans le projet
    //                            String filePath = this.generalFile.searchFilePath(matmet); // on cherche dans le process :
                                String folderPath = this.generalFile.searchFolderPath(matmet);

                                if (folderPath != null && !folderPath.isEmpty()) {
    //                                StepFileTemp matMetFile = new StepFileTemp(folderPath, matmet, this.data, this.generalFile);
                                    if (step1 != null) {
                                        step1.constructDataFromOlderMatMetMethod(folderPath, matmet);
    //                                    step1.setStepMatMetFile(matMetFile);
    //                                    matMetFile.setStepFile(step1);
                                    } else {
                                        if (step0 != null) {
                                            step0.constructDataFromOlderMatMetMethod(folderPath, matmet);
    //                                        step0.setStepMatMetFile(matMetFile);
    //                                        matMetFile.setStepFile(step0);
                                        } else {
                                            // erreur matmetfile sans step !!!
                                        }
                                    }
                                } else {
                                    logger.error("the mat met file can't be found : " + matmet);
                                }
                            }
                        } else {
                            // il s'agit de fichier d'observation
                            String[] listObs = content.get(fileNameK).get(i).getValue().get().split("[\n\r;]");
                            for (String obs : listObs) {
                                obs = obs.trim();
                                if (obs != null && !obs.isEmpty()) {
                                    //                            String filePath = this.data.getFilePath(obs); // on cherche dans le projet
                                    String folderPath = this.generalFile.searchFolderPath(obs); // on cherche dans le process

                                    if (folderPath != null && !folderPath.isEmpty()) {
                                        StepFile stepFile = step1 != null ? step1 : step0;
                                        if (stepFile != null) {
                                            ObservationFile obsFile = new ObservationFile(folderPath, obs, stepFile);
                                            obsFile.setMeasureLevel(content.get(measureLK).get(i).getValue().get());
                                        } else {
                                            // erreur, observation sans step !!!
                                        }
                                    } else {
                                        logger.error("the obs file can't be found : " + obs);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

//        for(StepFile s : listStep) {
//            if(s.isRoot()) {
//                createStepDataNode(s);
//            }
//        }

        Tools.updateProgress(ProgressPO2.OPEN, "building steps");
        Double pp = 0.5/listStep.size();
        for(StepFile s : listStep) {
            Tools.updateProgress(ProgressPO2.OPEN, Tools.getProgress(ProgressPO2.OPEN).getProgress() + pp);
            if(s != null) {
                if (!s.getIsConstruct()) {
                    s.constructData();
                }

                for (ObservationFile f : s.getObservationFiles()) {
                    f.constructData();
                }

                for (Map.Entry<CompositionFile, Boolean> entry : s.getCompositionFile().entrySet()) {
                    if (!entry.getKey().getIsConstruct()) {
                        entry.getKey().constructData();
                    }
                }
            }
        }
        // content is built. We can remove it.
        content.values().forEach(List::clear);
    }

    @Override
    public void checkValue() {
        // nothing to do here
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public KeyWords getFileNameK() {
        return this.fileNameK;
    }


//    public void createStepDataNode(StepFile step) {
//        DataNode stepNode = new DataNode(step.getName(), DataNodeType.STEP);
////        step.setDataNode(stepNode);
//        stepNode.setFile(step);
//        stepNode.nameProperty().bind(Bindings.concat(step.getComplexName().getValue(), " (",step.getComplexName().getID(),")"));
//
//        for(StepFile s : step.getSubStep()) {
//            createStepDataNode(s);
//        }
//    }

}
