/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.utils.Tools;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.SKOS;
import org.json.JSONObject;

import java.util.*;

public class ComplexTable extends GenericTable{


    public static ComplexField numC = new ComplexField("#");
    public static ComplexField actionC = new ComplexField("action");
    private List<ComplexField> tableHeader;

    private DataPart part;

    private ObservableList<Map<ComplexField, ComplexField>> content = FXCollections.observableArrayList();

    private Individual myObsCollection = null;

    public ComplexTable(Data data) {
        this.data = data;
        tableHeader = new LinkedList<>();
        tableHeader.add(numC);
        tableHeader.add(actionC);
    }

    public List<ComplexField> getTableHeader() {
        return this.tableHeader;
    }

    public ObservableList<Map<ComplexField, ComplexField>> getContent() {
        return this.content;
    }

    public Integer getSize() {
        return content.size();
    }

    @Override
    public void addMap(DataPart p, int replicateID) {
        part.concat(p);
        mapPart(part, replicateID);
    }

    @Override
    public void checkValue() {
        for(ComplexField c : tableHeader) {
            if(c != numC && c != actionC) {
                c.checkValue();
            }
        }


    }

    public void cleanReplicatesValue(List<Integer> list) {
        for(Map<ComplexField, ComplexField> line : content) {
            for(ComplexField c : tableHeader) {
                if(c != numC && c != actionC) { //
                    line.get(c).cleanReplicateValues(list);
                }
            }
        }
    }

    public void mapPart(DataPart part, int replicateID) {
        this.part = part;
        if(replicateID == -1) {
            content.clear();
            tableHeader.clear();
            tableHeader.add(numC);
            tableHeader.add(actionC);
        }
        Map<ComplexField, List<String>> temp = new LinkedHashMap<>();
        for(Integer i = 0; i <  part.getHeader().size(); i++) {
            ComplexField c = null;
            if(replicateID == -1) {
              c =  new ComplexField(part.getHeader().get(i));
              tableHeader.add(c);
            } else {
                if(tableHeader.size() - 2 > i) { // -2 because of header for numC & actionC
                    c = tableHeader.get(i + 2);
                } else {
                    c =  new ComplexField(part.getHeader().get(i));
                    tableHeader.add(c);
                }
            }
            temp.put(c, getColumnValues(i, part));
        }


        if(!temp.isEmpty()) {
            ComplexField one = temp.keySet().iterator().next();
            for (int i = 0; i < temp.get(one).size(); i++) {
                Map<ComplexField, ComplexField> newLine = null;
                if(replicateID != -1) {
                    if(content.size() > i) {
                        newLine = content.get(i);
                    }
                }
                if(newLine == null) {
                    newLine  = new LinkedHashMap<>();
                    newLine.put(numC, new ComplexField("num"));
                    newLine.put(actionC, new ComplexField("action"));
                    content.add(newLine);
                }
                for (Map.Entry<ComplexField, List<String>> entry : temp.entrySet()) {
                    ComplexField c = null;
                    if(newLine.containsKey(entry.getKey())) {
                        c = newLine.get(entry.getKey());
                        c.unbindReplicatesValues();
                    }
                    else {
                        c = new ComplexField();
                    }
                    if(replicateID == -1) {
                        c = new ComplexField();
                        c.setValue(entry.getValue().get(i));
                    } else {
                        c.setValueWithReplicate(replicateID, entry.getValue().get(i));
                    }
                    newLine.put(entry.getKey(), c);
                }
            }

//            if(content.size() == 0) { // tableau sans ligne !!!! on ajoute une ligne vide
//                Map<ComplexField, ComplexField> newLine = new LinkedHashMap<>();
//                for (Map.Entry<ComplexField, List<String>> entry : temp.entrySet()) {
//                    newLine.put(entry.getKey(), new ComplexField());
//                }
//                newLine.put(numC, new ComplexField("num"));
//                newLine.put(actionC, new ComplexField("action"));
//                content.add(newLine);
//            }
        }

    }

    private List<String> getColumnValues(Integer colNum, DataPart part) {
        List<String> list = new LinkedList<>();
        if(colNum >= 0 && part.getListLine().size() > 0 && part.getHeader().size() > colNum) {
            for(List<String> li : part.getListLine()) {
                list.add(li.get(colNum));
            }
            list.remove(0);

        }
        return completeLine(list, part.getNBLine()-1);
    }

    private List<String> completeLine(List<String> list, int size) {
        while(list.size() < size) {
            list.add("");
        }
        return list;
    }


    @Override
    public void unbind() {
    }

    public void deleteColumn(ComplexField field) {
        for (Map<ComplexField, ComplexField> line : getContent()) {
            line.remove(field);
        }
        tableHeader.remove(field);
        data.setModified(true);
    }

    public Map<ComplexField, ComplexField> addLine() {
        return addLine(-1);
    }

    public Map<ComplexField, ComplexField> addLine(Integer index) {
        Map<ComplexField, ComplexField> newLine = new HashMap<>();

        for (ComplexField c : tableHeader) {
            newLine.put(c, new ComplexField());
        }
        if (index != null && index >= 0) {
            content.add(index, newLine);
        } else {
            content.add(newLine);
        }
        checkValue();
        return newLine;
    }

    public Map<ComplexField, ComplexField> getLine(Integer lineNB) {
        if(lineNB >= 0 && content.size() > lineNB) {
            return content.get(lineNB);
        }
        return null;
    }

    public ComplexField addColumn(ComplexField field) {
        tableHeader.add(field);
        for(Map<ComplexField, ComplexField> col : content) {
            col.put(field, new ComplexField());
        }
        data.setModified(true);
        return field;
    }

    public ComplexField addColumn(String attribut, String objects, String unit, String comment) {
        String value = attribut;
        if(!objects.equals("")) {
            value += " (object="+objects+")";
        }
        if(!unit.equals("")) {
            value += " (unit="+unit+")";
        }
        if(!comment.equals("")) {
            value += " (comment="+comment+")";
        }

        ComplexField newCol = new ComplexField(value);
        for(Map<ComplexField, ComplexField> col : content) {
            col.put(newCol, new ComplexField());
        }
        tableHeader.add(newCol);
        data.setModified(true);
        return newCol;
    }


    public ComplexField addColumn(String value) {
        ComplexField newCol = new ComplexField(value);
        for(Map<ComplexField, ComplexField> col : content) {
            col.put(newCol, new ComplexField());
        }
        tableHeader.add(newCol);
        data.setModified(true);
        return newCol;
    }


    @Override
    public ComplexField addColumn(Integer index) {
        ComplexField newCol = new ComplexField();
        for(Map<ComplexField, ComplexField> col : content) {
            col.put(newCol, new ComplexField());
        }
        tableHeader.add(newCol);
        data.setModified(true);
        return newCol;
    }

    public LinkedList<LinkedList<String>> getDataForExcel(int replicateID) {
        LinkedList<LinkedList<String>> result = new LinkedList<>();
        LinkedList<String> header = new LinkedList<>();
        for(ComplexField c : tableHeader) {
            if(c != numC && c != actionC) { // on enregistre pas le num de ligne ni la col action (add del row line)
                String hValue = c.getValue().get();
                if(c.getUnit().isNotEmpty().get()) {
                    hValue+=" {{unit="+c.getUnit().getValue()+"}} ";
                }
                if(c.getTooltip().isNotEmpty().get()) {
                    hValue+=" {{comment="+c.getTooltip().getValue()+"}} ";
                }
                if(c.getListObject().size() > 0) {
                    hValue+=" {{object=" + String.join("::", c.getListObject()) + "}} ";
                }
                header.add(hValue);
            }
        }
        result.add(header);
        for(Map<ComplexField, ComplexField> line : content) {
            LinkedList<String> newLine = new LinkedList<>();
            for(ComplexField c : tableHeader) {
                if(c != numC && c != actionC) { // on enregistre pas le num de ligne ni la col action (add del row line)
                    if(replicateID == -1) {
                        newLine.add(line.get(c).getValuesAsString());
                    } else {
                        newLine.add(line.get(c).getValueWithReplicate(replicateID).getValue());
                    }

                }
            }
            result.add(newLine);
        }

        return result;
    }

    @Override
    public Individual publish(Ontology ontology, Resource r, Boolean raw, Integer tableIndice, JSONObject json, String obsUuid, int replicateID) {
        Property hasObjectOfInterest = ontology.getCoreModel().getOntProperty(JenaTools.PO2CORE + "hasObjectOfInterest");
        OntClass classObsCollection = ontology.getCoreModel().getOntClass(JenaTools.SOSA + "ObservationCollection");
        Property sosaHasMember = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"hasMember");
        OntClass sosaObs = ontology.getCoreModel().getOntClass(JenaTools.SOSA+"Observation");
        Property observedProperty = ontology.getCoreModel().getOntProperty(JenaTools.SOSA + "observedProperty");

        Property rawResult = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"hasRawResult");
        Property calcResult = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"hasCalcResult");

        Property numLine = ontology.getCoreModel().getProperty(ontology.getCORE_NS()+"rowIndex");

        // 1 table = 1 observationCOllection and 1 line = 1 observationCollection also !!!
        myObsCollection = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_"+ obsUuid + "_"+tableIndice, classObsCollection);

        HashMap<ComplexField, Individual> listICol = new HashMap<>();
        int colCompteur = 0;
        for(ComplexField c : tableHeader) {
            if(c != numC && c != actionC) {
                String attr = c.getValue().get();
                if(!attr.isEmpty()) {
                    String comment = c.getTooltip().get();
                    OntClass classAttr = ontology.searchAttribut(attr);
                    if (classAttr == null) {
                        classAttr = data.getDataModel().createClass(ontology.getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(attr));
                        classAttr.addProperty(SKOS.prefLabel, attr, "");
                        classAttr.addSuperClass(ontology.getCoreModel().getOntClass(JenaTools.SSN + "Property"));
                        json.getJSONArray("warning").put("Attribut " + attr + " not found in ontology");
                    }
                    Individual iAttr = data.getDataModel().createIndividual(data.getDATA_NS() + "po2_" + obsUuid + "_" + Tools.normalize(attr) + "_" + tableIndice + "_" + colCompteur, classAttr);
                    iAttr.addProperty(SKOS.prefLabel, attr, "");
                    listICol.put(c, iAttr);
                    if (comment != null && !comment.isEmpty()) {
                        iAttr.addComment(comment, "");
                    }

                    for (String o : c.getListObject()) {
                        if (!o.isEmpty()) {
                            iAttr.addProperty(hasObjectOfInterest, ontology.searchOrCreateObj(o, json, data.getDataModel()));
                        }
                    }
                } else {
                    json.getJSONArray("warning").put("Empty attribut not found in ontology");
                }
                colCompteur++;
            }
        }


        Integer lineCompteur = 0;
        for(Map<ComplexField, ComplexField> line : content) {
            Individual lineCollection = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_"+ obsUuid + "_"+tableIndice + "_" + lineCompteur, classObsCollection);
            myObsCollection.addProperty(sosaHasMember, lineCollection); // each line is a member of the table collection
            lineCollection.addProperty(numLine, data.getDataModel().createTypedLiteral(lineCompteur));

            colCompteur = 0;
            for(ComplexField c : tableHeader) {
                if(c != numC && c != actionC && listICol.containsKey(c)) {
                    Individual cellObs = data.getDataModel().createIndividual(data.getDATA_NS() + "po2_" + obsUuid + "_" + tableIndice + "_" + lineCompteur + "_" + colCompteur, sosaObs);
                    lineCollection.addProperty(sosaHasMember, cellObs); // each cell is a member of the line collection
                    Individual propInd = listICol.get(c);
                    cellObs.addProperty(observedProperty, propInd);

                    String attr = c.getValue().get();
                    String value = line.get(c).getValueWithReplicate(replicateID).get();
                    String unit = c.getUnit().get();

                    Individual iResult = JenaTools.addQuantityQuality(ontology, data, null,  attr, value, unit, obsUuid + "_" + tableIndice + "_" + lineCompteur + "_" + colCompteur, json);
                    cellObs.addProperty(raw ? rawResult : calcResult, iResult);
                    colCompteur++;
                }
            }
            lineCompteur++;
        }
        return myObsCollection;
    }


    @Override
    public void cloneFrom(GenericTable table) throws CloneNotSupportedException {
        if(!(table instanceof ComplexTable)){
            throw new CloneNotSupportedException();
        }

        //clone du content
        HashMap<ComplexField, ComplexField> mappingComplexField = new HashMap<>();
        this.content.clear();
        this.tableHeader.clear();
        this.tableHeader.add(numC);
        this.tableHeader.add(actionC);
        for(ComplexField c :  ((ComplexTable) table).getTableHeader()) {
            if(! c.equals(numC) && ! c.equals(actionC)) {
                ComplexField newCol = new ComplexField();
                newCol.cloneFrom(c);
                mappingComplexField.put(c, newCol);
                tableHeader.add(newCol);
            }

        }

        for(Map<ComplexField, ComplexField> line : ((ComplexTable) table).content) {
            Map<ComplexField, ComplexField> newLine = new LinkedHashMap<>();

            for(ComplexField c : ((ComplexTable) table).getTableHeader()) {
                if(c.equals(numC) || c.equals(actionC)) {
                    newLine.put(c, new ComplexField());
                } else {
                    ComplexField newC = mappingComplexField.get(c);
                    ComplexField newCell = new ComplexField();
                    newCell.cloneFrom(line.get(c));
                    newLine.put(newC, newCell);
                }
            }
            this.content.add(newLine);
        }

    }
}
