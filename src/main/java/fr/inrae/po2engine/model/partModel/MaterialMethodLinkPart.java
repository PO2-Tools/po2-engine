/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import org.apache.commons.lang3.Validate;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MaterialMethodLinkPart extends GenericPart {

    private static final KeyWords nameK = new KeyWords(true, true,"name@en","nom@fr");
    private static final KeyWords identK = new KeyWords(true, true,"identifier@en","identifiant@fr"); // old identifier
    private static final KeyWords fileK = new KeyWords(true, true,"file@en","fichier@fr", "Nom du fichier descriptif@fr");
    private static final List<KeyWords> listKeyWord = List.of();


    private Data data = null;
    private String folder = null;
    private ConsignPart consignPart;

    private MaterialMethodPart materialMethodPart;

    public MaterialMethodLinkPart() {
        this.type = DataPartType.MATERIAL_USED;
        // only used for detection !!
    }
    public MaterialMethodLinkPart(Data data) {
        this();
        this.data = data;
        this.consignPart = new ConsignPart(data);
    }

    public MaterialMethodLinkPart(MaterialMethodPart materialMethodPart) throws NullPointerException{
        Validate.notNull(materialMethodPart);
        this.type = DataPartType.MATERIAL_USED;
        this.data = materialMethodPart.getData();
        this.consignPart = new ConsignPart(data);
        setMaterialMethodPart(materialMethodPart);
    }

    public ConsignPart getConsignPart() {
        return this.consignPart;
    }

    public void addCharacteristic(String attribut, String object, String value, String unit, String comment) {
        this.consignPart.addCharacteristic(attribut, object, value, unit, comment);
    }

    public void setConsignPart(ConsignPart consign) {
        this.consignPart = consign;
    }

    public void addFolderInfo(String folder) {
        this.folder = folder;
    }

    public void setType(KeyWords type) {
        this.type = type;
    }

    public KeyWords getIdentK() {
        return this.nameK;
    }

    public KeyWords getFileK() {
        return fileK;
    }

    public void setMaterialMethodPart(MaterialMethodPart matMetPart) {
        this.materialMethodPart = matMetPart;
        if(DataPartType.MATERIAL_RAW.equals(this.materialMethodPart.getPartType())) {
            this.type = DataPartType.MATERIAL_USED;
        }
        if(DataPartType.METHOD_RAW.equals(this.materialMethodPart.getPartType())) {
            this.type = DataPartType.METHOD_USED;
        }
    }
    @Override
    public Boolean checkPart(DataPart part) {
        // on regarde s'il y a une entete
        List<String> firstLine = part.getLine(0);
        if(firstLine != null && !firstLine.isEmpty()) {
            String firstCell = firstLine.get(0);
            if(DataPartType.MATERIAL_USED.map(firstCell)) {
                this.type = DataPartType.MATERIAL_USED;
                part.removeLine(0);
                part.setLineTable(false);
                return true;
            }
            if(DataPartType.METHOD_USED.map(firstCell)) {
                this.type = DataPartType.METHOD_USED;
                part.removeLine(0);
                part.setLineTable(false);
                return true;
            }
        }
        return false;
    }

    @Override
    public void unbind() {

    }

    @Override
    public void mapPart(DataPart part) {
        List<String> lineIdent = getLineValues(nameK, part);
        if(lineIdent == null) { // old identifier
            lineIdent = getLineValues(identK, part);
        }
        String materialID = "";
        if(lineIdent.size()>0) {
            materialID = lineIdent.get(0);
        }

        if(this.type.equals(DataPartType.MATERIAL_USED)) {
//            this.materialMethodPart = this.data.getMateriel(materialID);
            this.materialMethodPart = this.data.searchMaterialIdFile(materialID,folder);
        }
        if(this.type.equals(DataPartType.METHOD_USED)) {
//            this.materialMethodPart = this.data.getMethod(materialID);
            this.materialMethodPart = this.data.searchMethodIdFile(materialID, folder);
        }

    }

    @Override
    public void checkValue() {
        this.consignPart.checkValue();
    }

    @Override
    public List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public MaterialMethodPart getMaterialMethodPartPart() {
        return materialMethodPart;
    }


}
