/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.CompositionFile;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ObservationFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.lang3.tuple.MutablePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class ObservationMetaPart extends GenericPart {

    private static final KeyWords dateK = new KeyWords(true, true,"date@en","date@fr", "date de la mesure@fr");
    private static final KeyWords heureK = new KeyWords(true, true,"hour@en","heure@fr");
    private static final KeyWords dureeK = new KeyWords(true, true,"handling time@en","durée de la manipulation@fr", "duree de la manipulation@fr");
    private static final KeyWords nameK = new KeyWords(false, false,"name@en","nom@fr");
    private static final KeyWords idK = new KeyWords(false, false,"id@en","id@fr");
    private static final KeyWords scaleK = new KeyWords(true, true,"scale@en","échelle@fr", "echelle@fr");
    private static final KeyWords foi = new KeyWords(false,false,"Features of interest@en","Feature of interest@en");
    private static final KeyWords agentK = new KeyWords(false, true,"agent@en","agent@fr");
    private static final KeyWords UUIDK = new KeyWords(false, true,"uuid@en","uuid@fr", "uuid@fr");
    private static final KeyWords descriptionK = new KeyWords(false, false, "description@en", "description@fr");

    private static final List<KeyWords> listKeyWord = Arrays.asList(dateK, heureK,dureeK);
    ChangeListener l = (observableValue, o, t1) -> {
        data.setModified(true);
    };

    LinkedMap<KeyWords, ComplexField> map = null;
    private ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> agentsMap;

    public ObservationMetaPart() {
        this.type = DataPartType.GENERAL_INFO;
        dateK.setUsedInReplicate(true);
        heureK.setUsedInReplicate(true);
        dureeK.setUsedInReplicate(true);
        agentK.setUsedInReplicate(true);
        // only used for detection !!
    }
    public ObservationMetaPart(Data data) {
        this();
        this.data = data;

        map = new LinkedMap<>();

        map.put(dateK, new ComplexField());
        map.get(dateK).setValuesListener(l);
        map.get(dateK).setMendatory(true);

        map.put(heureK, new ComplexField());
        map.get(heureK).setValuesListener(l);
        map.get(heureK).setMendatory(true);

        map.put(dureeK, new ComplexField());
        map.get(dureeK).setValuesListener(l);
        map.get(dureeK).setMendatory(true);

        map.put(nameK, new ComplexField());
        map.get(nameK).setValuesListener(l);
        map.get(nameK).setMendatory(true);
        map.get(nameK).setListConstraint(Ontology.listObservationProperty());

        map.put(scaleK, new ComplexField());
        map.get(scaleK).setValuesListener(l);
        map.get(scaleK).setMendatory(true);
        map.get(scaleK).setListConstraint(Ontology.listScaleProperty());

        map.put(foi, new ComplexField());
        map.get(foi).setValuesListener(l);

        map.put(idK, new ComplexField());
        map.get(idK).setValuesListener(l);

        map.put(UUIDK, new ComplexField());
        map.get(UUIDK).setValuesListener(l);

        map.put(descriptionK, new ComplexField());
        map.get(descriptionK).setValuesListener(l);

        agentsMap = FXCollections.observableHashMap();
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void unbind() {
        for(KeyWords k : map.keySet()) {
            map.get(k).unbindReplicatesValues();
        }
        agentsMap.forEach((k, v) -> v.unbind());
    }

    @Override
    public List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public void setDate(String date) {
        map.get(dateK).getValue().set(date);
    }

    public String getDate() {
        return map.get(dateK).getValue().get();
    }

    public String getTime() {
        return map.get(heureK).getValue().get();
    }

    public String getScale() {
        return map.get(scaleK).getValue().get();
    }

    public String getDuration() {
        return map.get(dureeK).getValue().get();
    }

    public ComplexField getCDescription() {
        return map.get(descriptionK);
    }

    public String getName() {
        return map.get(nameK).getValue().get();
    }

    public String getID() {
        return map.get(idK).getValue().get();
    }

    public StringProperty getIDProperty() {
        return map.get(idK).getValue();
    }

    public ComplexField getCID() {
        return map.get(idK);
    }

    public ComplexField getCName() {
        return map.get(nameK);
    }

    public ComplexField getCDate() {
        return map.get(dateK);
    }

    public ComplexField getCDuree() {
        return map.get(dureeK);
    }

    public ComplexField getCHeure() {
        return map.get(heureK);
    }

    public ComplexField getCScale() {
        return map.get(scaleK);
    }

    public ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> getAgentsMap() {
        return agentsMap;
    }

    public void setAgents(String agents) {
        HashMap<Integer, String> listRep = Tools.convertReplicate(agents);
        listRep.forEach((k, v) -> {
            if(!agentsMap.containsKey(k)) {
                agentsMap.put(k, new SimpleObjectProperty<>());
                agentsMap.get(k).addListener(l);
            }
            HashMap<KeyWords, ComplexField> la = this.data.getProjectFile().getAgent(v);
            if(la == null) {
                la = Tools.createFakeAgentFromString(v);
            }
            agentsMap.get(k).setValue(la);
        });
    }

    public String getAgents() {
        if(agentsMap.size() == 1 && agentsMap.containsKey(0)) {
            return Tools.agentToString(agentsMap.get(0).get());
        }
        JSONArray listAgent = new JSONArray();
        agentsMap.forEach((k, v) -> {
            JSONObject o = new JSONObject();
            o.put("id", k);
            o.put("value", Tools.agentToString(v.get()));
            listAgent.put(o);
        });
        return listAgent.toString();
    }

    public String getAgent(int replicateID) {
        if(agentsMap.containsKey(replicateID)) {
            return Tools.agentToString(agentsMap.get(replicateID).get());
        }
        return "";
    }

    public String getFOI() {
        return map.get(foi).getValue().get();
    }

    public void setFOI(String lFoi) {
        map.get(foi).setValue(lFoi);
    }

    public void setID(String s) {
        map.get(idK).getValue().set(s);
    }

    public void setFOI(ObservableList<MutablePair<String,MutablePair<String,SimpleBooleanProperty>>> listItem) {
        ComplexField c = map.get(foi);
        c.setValue("");
        ArrayList<String> tot = new ArrayList<>();
        for(MutablePair<String,MutablePair<String,SimpleBooleanProperty>> item : listItem) {
            if(item.getRight().getRight().get()) {
                tot.add(item.getRight().getLeft());
            }
        }
        c.setValue(String.join("::",tot));
    }

    public ComplexField getCFoi() {
        return map.get(foi);
    }

    /* only used by early version of PO2 file */
    public void setFOI(String measure, StepFile stepFile) {
        if(map.get(foi).getValue().isEmpty().get()) {
            ArrayList<String> tot = new ArrayList<>();

            if ("Procédé".equalsIgnoreCase(measure) || "Etape".equalsIgnoreCase(measure)) {
                tot.add("step");
            }
            if ("Produit".equalsIgnoreCase(measure) || "Milieu".equalsIgnoreCase(measure)) {
                for (CompositionFile comp : stepFile.getCompositionFile().keySet()) {
                    tot.add(comp.getFileName());
                }
            }
            if ("Système".equalsIgnoreCase(measure) || "Systéme".equalsIgnoreCase(measure)) {
                tot.add("itinerary");
            }
            ComplexField c = map.get(foi);
            c.setValue(String.join("::", tot));
        }
    }

    public void mapPart(DataPart part, int replicateID) {
        for(Map.Entry<KeyWords, ComplexField> entry : map.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                if(!k.isUsedInReplicate() || replicateID == -1) {
                    map.get(k).setValue(val.get(0));
                } else {
                    map.get(k).setValueWithReplicate(replicateID, val.get(0));
                }
            }
        }
        List<String> val = getLineValues(agentK, part);
        if(val != null  && val.size() > 0) {
            if(replicateID == -1) {
                setAgents(val.get(0));
            } else {
                HashMap<KeyWords, ComplexField> findAgent = this.data.getProjectFile().getAgent(val.get(0));
                if(!agentsMap.containsKey(replicateID)) {
                    agentsMap.put(replicateID, new SimpleObjectProperty<>());
                    agentsMap.get(replicateID).addListener(l);
                }
                if(findAgent != null) {
                    agentsMap.get(replicateID).setValue(findAgent);
                } else {
                    agentsMap.get(replicateID).setValue(Tools.createFakeAgentFromString(val.get(0)));
                }
            }

        }
    }

    @Override
    public void mapPart(DataPart part) {
       mapPart(part, -1);
    }

    @Override
    public void checkValue() {
        map.get(nameK).checkValue();
    }


    public LinkedMap<KeyWords, ComplexField> getContent() {
        return map;
    }


    public void removeFomFOI(String fileName) {
        ComplexField c = map.get(foi);
        ArrayList<String> tot = new ArrayList<>();
        for(String s : c.getValue().get().split("::")) {
            s = s.trim();
            if (!s.equalsIgnoreCase(fileName)) {
                tot.add(s);
            }
        }
        c.setValue(String.join("::", tot));
    }

    public void setTime(String time) {
        map.get(heureK).getValue().set(time);
    }

    public void setDuration(String duration) {
        map.get(dureeK).getValue().set(duration);
    }

    public void setScale(String scale) {
        map.get(scaleK).getValue().set(scale);
    }

    public void setType(String s) {
        map.get(nameK).getValue().set(s);
    }

    public void bindType(SimpleStringProperty value) {
        map.get(nameK).getValue().bind(value);
    }

    public void clear(int replicateID) {
        if(replicateID == -1) {
            map.get(dateK).setValue("");
            map.get(heureK).setValue("");
            map.get(dureeK).setValue("");
            agentsMap.forEach((k, v) -> v.setValue(null));
        } else {
            map.get(dateK).setValueWithReplicate(replicateID, "");
            map.get(heureK).setValueWithReplicate(replicateID, "");
            map.get(dureeK).setValueWithReplicate(replicateID, "");
            agentsMap.computeIfPresent(replicateID, (k, v) -> { v.setValue(null); return v;});
        }
        map.get(nameK).setValue("");
        map.get(scaleK).setValue("");
        map.get(foi).setValue("");
        map.get(idK).setValue("");
        map.get(UUIDK).setValue("");
    }


    /**
     * Create a UUID for the file if there is none
     */
    public void generateUUID() {
        if(map.get(UUIDK).getValue().isEmpty().get()) {
            map.get(UUIDK).getValue().set(UUID.randomUUID().toString());
        }
    }

    /**
     * get the uuid of the file
     * @param force if the uuid is empty, a new ione is create
     * @return the uuid of the file
     */
    public String getUuid(boolean force) {
        if (force) generateUUID();
        return map.get(UUIDK).getValue().get();
    }

    public void cleanAgentReplicateValues(List<Integer> list) {
        // != 0 to prevent removing value for default replicate
        agentsMap.entrySet().removeIf(repIDValue -> repIDValue.getKey() != 0 && !list.contains(repIDValue.getKey()));
    }
}

