/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

public class ProjectContentPart extends GenericPart {

    private static final KeyWords processK = new KeyWords(true, true,"process@en","procédé@fr");
    private static final KeyWords fileNameK = new KeyWords(true, true,"filename@en", "nom du fichier@fr");
    private static Logger logger = LogManager.getLogger(ProjectContentPart.class);
    private static final List<KeyWords> listKeyWord = Arrays.asList(processK, fileNameK);

    private Map<KeyWords, List<ComplexField>> content = new LinkedHashMap<>();

    private Set<GeneralFile> listProcess = new LinkedHashSet<>();

    public ProjectContentPart() {
        this.type = DataPartType.RAW_DATA;
        // only used for detection
    }
    public ProjectContentPart(Data data) {
        this();
        this.data = data;
        content.put(processK, new LinkedList<>());
        content.put(fileNameK, new LinkedList<>());
    }

    public List<GeneralFile> getListProcess() {
        return this.data.getListGeneralFile();
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getLine(0));
    }

    public Map<KeyWords, List<ComplexField>> getContent() {
        return this.content;
    }
    @Override
    public void unbind() {
        for(ComplexField c : content.get(fileNameK)) {
            c.getValue().unbind();
        }
        for(ComplexField c : content.get(processK)) {
            c.getValue().unbind();
        }
    }

    @Override
    public void mapPart(DataPart part) {
        List<String> valProcess = getColumnValues(processK, part);
        List<String> valFileName = getColumnValues(fileNameK, part);

        for(String val : valProcess) {
            content.get(processK).add(new ComplexField(val));
        }

        for(String val : valFileName) {
            content.get(fileNameK).add(new ComplexField(val));
        }

        Tools.updateProgress(ProgressPO2.OPEN, "identifying objects", 0.0);
        Double ppO = 1.0/content.get(processK).size();
        for(int i = 0; i < content.get(processK).size(); i++) {
            ComplexField processLine = content.get(processK).get(i);
            String[] listProcessFile = content.get(fileNameK).get(i).getValue().get().replaceAll("[\n\r]", "").split(";");
            if(listProcessFile.length == 1) {
                String folderPath = this.data.searchFolderPath(listProcessFile[0]);
                if(folderPath != null && !folderPath.isEmpty()) {
                    GeneralFile generalFile = new GeneralFile(listProcessFile[0], this.data.getProjectFile(), folderPath );
                    if (generalFile.preConstructData(data)) {
                        // ok
                    }
                }
            } else {
                // error
            }
            Tools.updateProgress(ProgressPO2.OPEN, Tools.getProgress(ProgressPO2.OPEN).getProgress() + ppO);
        }
    }

    @Override
    public void checkValue() {
        // nothing to do here
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public KeyWords getFileNameK() {
        return this.fileNameK;
    }


//    public void createStepDataNode(StepFile step) {
//        DataNode stepNode = new DataNode(step.getName(), DataNodeType.STEP);
////        step.setDataNode(stepNode);
//        stepNode.setFile(step);
//        stepNode.nameProperty().bind(Bindings.concat(step.getComplexName().getValue(), " (",step.getComplexName().getID(),")"));
//
//        for(StepFile s : step.getSubStep()) {
//            createStepDataNode(s);
//        }
//    }

}
