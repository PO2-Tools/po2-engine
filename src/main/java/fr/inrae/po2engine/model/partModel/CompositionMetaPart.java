/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import javafx.beans.value.ChangeListener;
import org.apache.commons.collections4.map.LinkedMap;

import java.util.*;

public class CompositionMetaPart extends GenericPart {
    private static final KeyWords dateK = new KeyWords(true, true,"date@en","date@fr");
    private static final KeyWords heureK = new KeyWords(true, true,"hour@en","heure@fr");
    private static final KeyWords dureeK = new KeyWords(true, true,"handling time@en","durée de la manipulation@fr", "duree de la manipulation@fr");
    private static final KeyWords compoID = new KeyWords(false, true, "id@en", "id@fr");
    private static final KeyWords conduiteK = new KeyWords(false, false, "guideline@en", "conduite@fr");
    private static final KeyWords compoType = new KeyWords(false,false,"type@en","type@fr");
    private static final KeyWords UUIDK = new KeyWords(false, true,"uuid@en","uuid@fr", "uuid@fr");
    private static final List<KeyWords> listKeyWord = Arrays.asList(dateK, heureK,dureeK);

    LinkedMap<KeyWords, ComplexField> map = null;

    public CompositionMetaPart() {
        this.type = DataPartType.GENERAL_INFO;
        // only used for detection !!
    }
    public CompositionMetaPart(Data data) {
        this();
        this.data = data;
        ChangeListener<String> l = (observableValue, o, t1) -> {
            data.setModified(true);
        };

        map = new LinkedMap<>();

        map.put(dateK, new ComplexField());
        map.get(dateK).getValue().addListener(l);

        map.put(heureK, new ComplexField());
        map.get(heureK).getValue().addListener(l);

        map.put(dureeK, new ComplexField());
        map.get(dureeK).getValue().addListener(l);

        map.put(compoID, new ComplexField());
        map.get(compoID).getValue().addListener(l);

        map.put(conduiteK, new ComplexField());
        map.get(conduiteK).getValue().addListener(l);

        map.put(compoType, new ComplexField());
        map.get(compoType).getValue().addListener(l);
        map.get(compoType).setListConstraint(Ontology.listComponentProperty());

        map.put(UUIDK, new ComplexField());
        map.get(UUIDK).getValue().addListener(l);
    }

    @Override
    public List<KeyWords> getListKeywords() {
        return listKeyWord;
    }


//    public CompositionMetaPart(DataNode node, Data data) {
//        this(data);
//        this.node = node;
//    }
    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void unbind() {
        getCType().getValue().unbind();
    }

    public ComplexField getCType() {
        return map.get(compoType);
    }

//    public void bindType(TextField compoType) {
//        getCompoType().getValue().unbind();
//        compoType.setText(getCompoType().getValue().get());
//
//        getCompoType().getValue().bind(compoType.textProperty());
//        compoType.styleProperty().bind(Bindings.when(getCompoType().stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;-fx-text-fill: black;  -fx-opacity: 1;").otherwise(Bindings.when(getCompoType().stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;-fx-text-fill: black;  -fx-opacity: 1;").otherwise("-fx-text-fill: black;  -fx-opacity: 1;")));
//        InvalidationListener l = new InvalidationListener() {
//            @Override
//            public void invalidated(Observable observable) {
//                data.setModified(true);
//            }
//        };
//        listListener.get(getCompoType().getValue()).add(l);
//        getCompoType().getValue().addListener(l);
//    }

    @Override
    public void mapPart(DataPart part) {
        for(Map.Entry<KeyWords, ComplexField> entry : map.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                map.get(k).setValue(val.get(0));
            }
        }
        if(map.get(compoID).getValue().get().isEmpty()) {
            map.get(compoID).getValue().setValue("default");
        }
    }

    @Override
    public void checkValue() {
        map.get(compoType).checkValue();
    }

    public ComplexField getCompoType() {
        return map.get(compoType);
    }

    public ComplexField getCompositionID() {
        return map.get(compoID);
    }

    public LinkedMap<KeyWords, ComplexField> getContent() {
        return map;
    }

    public KeyWords getConduiteK() {
        return this.conduiteK;
    }

    public void validateData() {
    }


    public void setCompositionID(String text) {
        map.get(compoID).getValue().setValue(text);
    }

    public void setCompositionType(String type) {
        map.get(compoType).getValue().setValue(type);
    }

    public void generateUUID() {
        if(map.get(UUIDK).getValue().isEmpty().get()) {
            map.get(UUIDK).getValue().set(UUID.randomUUID().toString());
        }
    }

    public String getUuid(boolean force) {
        if (force) generateUUID();
        return map.get(UUIDK).getValue().get();
    }
}
