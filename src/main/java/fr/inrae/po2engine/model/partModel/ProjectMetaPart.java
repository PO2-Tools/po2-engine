/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import org.apache.commons.collections4.map.LinkedMap;

import java.util.*;

public class ProjectMetaPart extends GenericPart {
    LinkedMap<KeyWords, ComplexField> content = null;
    private static final KeyWords projectK = new KeyWords(true, true,"project name@en","nom du projet@fr");
    public static final KeyWords versionK = new KeyWords(false, false,"version@en","version@fr");
    public static final KeyWords PO2engineK = new KeyWords(false, false,"PO2 Engine@en","PO2 Engine@fr");
    private static final KeyWords contactNameK = new KeyWords(false, false,"contact name@en","nom du contact@fr");
    private static final KeyWords contactMailK = new KeyWords(false, false,"contact mail@en","email du contact@fr");
    private static final KeyWords descriptionK = new KeyWords(true, true,"description@en","description@fr");
    private static final KeyWords externalLinksK = new KeyWords(true, true,"external links@en","liens externe@fr");
    public static final KeyWords contactK = new KeyWords(false, true,"contact@en","contact@fr");
    public static final KeyWords fundingK = new KeyWords(false, false,"funding@en","financement@fr");
    private static final List<KeyWords> listKeyWord = Arrays.asList(projectK);


    private static final KeyWords UUIDK = new KeyWords(false, true,"uuid@en","uuid@fr", "uuid@fr");
    private ObjectProperty<HashMap<KeyWords, ComplexField>> contact;
    private ObjectProperty<HashMap<KeyWords, ComplexField>> funding;

    public ProjectMetaPart() {
        this.type = DataPartType.GENERAL_INFO;
        // only used for detection !!!
    }

    public ProjectMetaPart(Data data) {
        this();
        this.data = data;
        ChangeListener l = (observableValue, o, t1) -> {
            data.setModified(true);
        };

        content = new LinkedMap<>();

        content.put(projectK, new ComplexField());
        content.get(projectK).getValue().addListener(l);
        content.get(projectK).setMendatory(true);

        content.put(versionK, new ComplexField());

        content.put(PO2engineK, new ComplexField());
        content.get(PO2engineK).setValue(Tools.getEngineVersion());

        content.put(descriptionK, new ComplexField());
        content.get(descriptionK).getValue().addListener(l);

        content.put(externalLinksK, new ComplexField());
        content.get(externalLinksK).getValue().addListener(l);

        content.put(UUIDK, new ComplexField());
        content.get(UUIDK).getValue().addListener(l);

        contact = new SimpleObjectProperty<>();
        contact.addListener(l);

        funding = new SimpleObjectProperty<>();
        funding.addListener(l);
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getColumn(0));
    }

    @Override
    public void mapPart(DataPart part) {
        for(Map.Entry<KeyWords, ComplexField> entry : content.entrySet()) {
            KeyWords k = entry.getKey();
            List<String> val = getLineValues(k, part);
            if(val != null && val.size() > 0) {
                content.get(k).setValue(val.get(0));
            }
        }
        // checking for funding
        funding.setValue(Tools.createFakeAgentFromString(""));
        List<String> val = getLineValues(fundingK, part);
        if(val != null  && val.size() > 0) {
            HashMap<KeyWords, ComplexField> findAgent = this.data.getProjectFile().getAgent(val.get(0));
            if(findAgent != null) {
                funding.setValue(findAgent);
            } else {
                funding.setValue(Tools.createFakeAgentFromString(val.get(0)));
            }
        }
        // cheking for old contact name / mail
        contact.setValue(Tools.createFakeAgentFromString(""));
        List<String> valContactName  = getLineValues(contactNameK, part);
        if(valContactName != null && valContactName.size() > 0) {
            contact.getValue().get(ProjectAgentPart.agentFamilyNameK).setValue(valContactName.get(0));
        }
        List<String> valContactMail  = getLineValues(contactMailK, part);
        if(valContactMail != null && valContactMail.size() > 0) {
            contact.getValue().get(ProjectAgentPart.agentMailK).setValue(valContactMail.get(0));
        }

        // checking for contact
        List<String> valContact = getLineValues(contactK, part);
        if(valContact != null  && valContact.size() > 0) {
            HashMap<KeyWords, ComplexField> findAgent = this.data.getProjectFile().getAgent(valContact.get(0));
            if(findAgent != null) {
                contact.setValue(findAgent);
            } else {
                contact.setValue(Tools.createFakeAgentFromString(valContact.get(0)));
            }
        }


        if(getCName().getValue().isEmpty().get()) {
            getCName().setValue(data.getName().getValue());
        }

        // checking for version
        String version = content.get(versionK).getValue().get();
        if(!version.isEmpty()) {
            String[] ver = version.split("\\.");
            if(ver.length == 2) {
                data.setMajorVersion(ver[0]);
                data.setMinorVersion(ver[1]);
            }
        }


    }

    /**
     *  remap agent after mapping the agent Part.
     */
    public void remapAgent() {
        String contactString = Tools.agentToString(contact.getValue());
        HashMap<KeyWords, ComplexField> contactAgent = this.data.getProjectFile().getAgent(contactString);
        if(contactAgent != null) {
            contact.setValue(contactAgent);
        }

        String fundingString = Tools.agentToString(funding.getValue());
        HashMap<KeyWords, ComplexField> fundingAgent = this.data.getProjectFile().getAgent(fundingString);
        if(fundingAgent != null) {
            funding.setValue(fundingAgent);
        }
    }

    @Override
    public void checkValue() {
        // noting to do here !
    }

    @Override
    List<KeyWords> getListKeywords() {
        return listKeyWord;
    }

    public StringProperty titleProperty() {
        return getCName().getValue();
    }

    public ComplexField getCName() { return content.get(projectK);}

    public void setProjectName(String projectName) {
        getCName().setValue(projectName.substring(0,Math.min(100, projectName.length())));
    }

    public ObjectProperty<HashMap<KeyWords, ComplexField>> getContactProperty() {
        return contact;
    }

    public String getContact() {
        return Tools.agentToString(contact.get());
    }

    public ObjectProperty<HashMap<KeyWords, ComplexField>> getFundingProperty() {
        return funding;
    }
    public String getFunding() {
        return Tools.agentToString(funding.get());
    }
    public ComplexField getCDescription() { return content.get(descriptionK);}

    public ComplexField getCExternalLinks() { return content.get(externalLinksK);}


    public void unbind() {
        for(KeyWords k : content.keySet()) {
            content.get(k).getValue().unbind();
        }
        contact.unbind();
        funding.unbind();
    }

    public LinkedMap<KeyWords, ComplexField> getContent() {
        return content;
    }

    public void generateUUID() {
        if(content.get(UUIDK).getValue().isEmpty().get()) {
            content.get(UUIDK).getValue().set(UUID.randomUUID().toString());
        }
    }

    public String getUuid(boolean force) {
        if (force) generateUUID();
        return content.get(UUIDK).getValue().get();
    }
}
