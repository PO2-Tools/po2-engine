/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.utils.DataPartType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DataPart {

    private List<List<String>> listLine;
    private KeyWords type = null;
    private Boolean lineTable = null;
    private static Logger logger = LogManager.getLogger(DataPart.class);

    public DataPart(Data data) {
        listLine = new LinkedList<>();
        type = DataPartType.INIT;
    }

    public Integer getNBLine() {
        return listLine.size();
    }

    public List<String> getLine(Integer lineNB) {
        if(listLine != null && listLine.size() > lineNB) {
            return listLine.get(lineNB);
        }
        return new ArrayList<>();
    }

    public List<List<String>> getListLine() {
        return listLine;
    }


    public List<String> getColumn(Integer colNB) {
        List<String> col = new ArrayList<>();

        if(listLine != null && listLine.size()>0 && listLine.get(0).size() > colNB) {
            for(List<String> line : listLine) {
                if(line.size() <= colNB) {
                    logger.debug("pb !!!");
                }
                col.add(line.get(colNB));
            }
        }
        return col;
    }

    public void addLine(Integer pos, List<String> line) {
        listLine.add(pos, line);
    }

    public void addLine(List<String> line) {
        listLine.add(line);
    }

    public void removeEmptyCol() {
        Boolean firstColEmpty = true;
        Boolean lastColEmpty = true;
        Integer maxSize = 0;
        for(int i = 0; i < listLine.size(); i++) {
            maxSize = Math.max(listLine.get(i).size(), maxSize);
            if(!listLine.get(i).isEmpty() && !listLine.get(i).get(0).equalsIgnoreCase("")) {
                firstColEmpty = false;
            }
        }
        // on complete le tableau a la size max // Théoriquement une seule fois
        for(int i = 0; i < listLine.size(); i++) {
            while(listLine.get(i).size() < maxSize) {
                listLine.get(i).add("");
            }
        }

        for(int i = 0; i < listLine.size(); i++) {

            if(!listLine.get(i).isEmpty() && !listLine.get(i).get(maxSize-1).equalsIgnoreCase("")) {
                lastColEmpty= false;
            }
        }
        if(lastColEmpty) {
            for(int i = 0; i < listLine.size(); i++) {
                listLine.get(i).remove(maxSize-1);
            }
        }

        if(firstColEmpty) {
            for(int i = 0; i < listLine.size(); i++) {
                listLine.get(i).remove(0);
            }
        }
        if(lastColEmpty || firstColEmpty) {
            removeEmptyCol();
        }

    }

    public boolean isEmpty() {
        return listLine.isEmpty();
    }

    public void setType(KeyWords type) {
        this.type = type;
    }

    public KeyWords getType() {
        return type;
    }

    public void removeLine(int i) {
        if(listLine.size() >i)
            listLine.remove(i);
    }

    public void setLineTable(Boolean lineTable) {
        this.lineTable = lineTable;
    }

    public Boolean getLineTable() {
        return this.lineTable;
    }

    public List<String> getHeader() {
        if(this.lineTable != null) {
            if (this.lineTable) {
                return getLine(0);
            } else {
                return getColumn(0);
            }
        } else {
            return new LinkedList<>();
        }
    }

    public void concat(DataPart dataPart) {
        this.listLine.addAll(dataPart.getListLine());
        removeEmptyCol();
    }

    public void setValue(Integer line, Integer column, String value) {
        this.getLine(line).set(column, value);
    }

}
