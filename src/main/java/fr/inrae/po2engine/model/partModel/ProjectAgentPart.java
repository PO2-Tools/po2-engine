/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.partModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.StepFile;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;

import java.util.*;

public class ProjectAgentPart extends GenericPart {
    ObservableList<HashMap<KeyWords, ComplexField>> content = null;
    public static final KeyWords agentFamilyNameK = new KeyWords(true, true,"family name@en","nom de famille@fr");
    public static final KeyWords agentGivenNameK = new KeyWords(true, false,"given name@en","prénom@fr");
    public static final KeyWords agentMailK = new KeyWords(true, false,"email@en","email@fr");
    public static final KeyWords agentOrganisationK = new KeyWords(true, true,"organization@en","organisation@fr");
    private static final List<KeyWords> listKeyWord = Arrays.asList(agentFamilyNameK, agentGivenNameK, agentMailK, agentOrganisationK);

    private ChangeListener<String> l;

    public ProjectAgentPart() {
        this.type = DataPartType.AGENT;
        // only used for detection !!!
    }

    public ProjectAgentPart(Data data) {
        this();
        this.data = data;
        l = (observableValue, o, t1) -> {
            data.setModified(true);
        };

        content = FXCollections.observableArrayList();
    }

    @Override
    public Boolean checkPart(DataPart part) {
        return checkKeyword(part.getLine(0));
    }

    @Override
    public void mapPart(DataPart part) {
        List<String> valFam = getColumnValues(agentFamilyNameK, part);
        List<String> valGiv = getColumnValues(agentGivenNameK, part);
        List<String> valMail = getColumnValues(agentMailK, part);
        List<String> valOrg = getColumnValues(agentOrganisationK, part);

        for (int i = 0; i < valFam.size(); i++) {
            if(!valFam.get(i).isEmpty() || !valGiv.get(i).isEmpty() || !valMail.get(i).isEmpty() || !valOrg.get(i).isEmpty()) {
                addAgent(valOrg.get(i), valFam.get(i), valGiv.get(i), valMail.get(i));
            }
        }
    }

    @Override
    public void checkValue() {
        // noting to do here !
    }

    public ObservableList<HashMap<KeyWords, ComplexField>> getListAgent() {
        return content;
    }
    @Override
    public List<KeyWords> getListKeywords() {
        return listKeyWord;
    }


    public void unbind() {
        for( HashMap<KeyWords, ComplexField> m : content) {
            for(KeyWords k : m.keySet()) {
                m.get(k).getValue().unbind();
            }
        }
    }

    public HashMap<KeyWords, ComplexField> addAgent() {
        return this.addAgent("", "", "", "");
    }

    public HashMap<KeyWords, ComplexField> addAgent(String org, String familyName, String givenName, String mail) {
        ComplexField cFam = new ComplexField(familyName);
        cFam.getValue().addListener(l);

        ComplexField cGiv = new ComplexField(givenName);
        cGiv.getValue().addListener(l);

        ComplexField cMail = new ComplexField(mail);
        cMail.getValue().addListener(l);

        ComplexField cOrg = new ComplexField(org);
        cOrg.getValue().addListener(l);

        HashMap newAgent = new HashMap();
        newAgent.put(agentFamilyNameK, cFam);
        newAgent.put(agentGivenNameK, cGiv);
        newAgent.put(agentMailK, cMail);
        newAgent.put(agentOrganisationK, cOrg);
        content.add(newAgent);
        this.data.setModified(true);
        return newAgent;
    }

    public void removeAgent(HashMap<KeyWords, ComplexField> agent) {
        content.remove(agent);
        this.data.setModified(true);
    }

    public HashMap<KeyWords, ComplexField> getAgent(String agentFromString) {
        HashMap<KeyWords, ComplexField> hashMapAgent = content.stream().filter(keyWordsComplexFieldHashMap -> agentFromString.equalsIgnoreCase(Tools.agentToString(keyWordsComplexFieldHashMap))).findFirst().orElse(null);
        return hashMapAgent;
    }
}
