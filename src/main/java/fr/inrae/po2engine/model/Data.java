/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.exception.CantWriteException;
import fr.inrae.po2engine.exception.LocalLockException;
import fr.inrae.po2engine.exception.NotLoginException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.MaterialMethodPart;
import fr.inrae.po2engine.utils.*;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.util.Pair;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.GraphMaker;
import org.apache.jena.graph.impl.SimpleGraphMaker;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFWriter;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipException;

public class Data extends OntoData{

//    private File dataFile;
    private ProjectFile projectFile;
    private List<GeneralFile> listGeneralFile;
    private Map<String, File> listAllFile;
    private MaterialsMethodsFile materialMethodFile;

//    private ObservableList<VocabConcept> listProcess = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listMaterial = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listScale = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listMethod = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listStep = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listAttribute = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listComponent = FXCollections.observableArrayList();
//    private ObservableList<String> listUnit = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listObservation = FXCollections.observableArrayList();
//    private ObservableList<VocabConcept> listObject = FXCollections.observableArrayList();
    private DoubleProperty progressLoading = new SimpleDoubleProperty(-1.0);
    private static Logger logger = LogManager.getLogger(Data.class);


    private OntModel dataModel;
    private String DATA_NS;
    private Graph graphModel;
    public static ChangeListener changeListener;

    public Data(String name) {
        super(name);
        init();
    }

    @Override
    public String getType() {
        return "DATA";
    }

    public void init() {
        listGeneralFile = new ArrayList<>();
        listAllFile = new HashMap<>();
        materialMethodFile = null;
        progressLoading.setValue(-1.0);
        projectFile = null;
    }

    public String getLocalPath() {
        return projectFile.getFilePath();
    }

    public DoubleProperty getProgressProperty() {
        return this.progressLoading;
    }

//    public ObservableList<VocabConcept> listMaterialProperty() {
//        return listMaterial;
//    }
//
//    public ObservableList<VocabConcept> listMethodProperty() {
//        return listMethod;
//    }
//
//    public ObservableList<VocabConcept> listScaleProperty() {
//        return listScale;
//    }
//
//    public ObservableList<VocabConcept> listStepProperty() {
//        return listStep;
//    }
//
//    public ObservableList<VocabConcept> listAttributeProperty() {
//        return listAttribute;
//    }
//
//    public ObservableList<VocabConcept> listObjectProperty() {
//        return listObject;
//    }
//
//    public ObservableList<VocabConcept> listComponentProperty() {
//        return listComponent;
//    }
//
//    public ObservableList<VocabConcept> listProcessProperty() {
//        return listProcess;
//    }
//
//    public ObservableList<String> listUnitProperty() {
//        return listUnit;
//    }
//    public ObservableList<VocabConcept> listObservationProperty() {
//        return listObservation;
//    }



    private void loadDav() {
        File ontoFile = CloudConnector.getDavData(this);
        if (ontoFile != null) {
//            ontologyFile = ontoFile;
            privateLoaded.setValue(true);

        }
    }

    public void setProjectFile(ProjectFile project) {
        this.projectFile = project;
    }

    public ProjectFile getProjectFile() {
        return this.projectFile;
    }

    public void addGeneralFile(GeneralFile file) {
        if(!listGeneralFile.contains(file)) {
            listGeneralFile.add(file);
            setModified(true);
        }
    }

    public MaterialsMethodsFile getMaterialMethodFile() {
        return this.materialMethodFile;
    }

    public List<GeneralFile> getListGeneralFile() {
        return this.listGeneralFile;
    }

//    public OntModel getDomainModel() {
//        return this.domainModel;
//    }

//    public String getDOMAIN_NS() {
//        return this.DOMAIN_NS;
//    }

//    public void setDataModel(OntModel dataModel) {
//        this.dataModel = dataModel;
//    }
    public void setGraphModel(Graph graphModel) {
        this.graphModel = graphModel;
    }

    public OntModel createDataModel() {
        if(this.graphModel != null) {
            this.dataModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, ModelFactory.createModelForGraph(graphModel));
        }
        return this.dataModel;
    }

    public Graph getGraphModel() {
        return this.graphModel;
    }


    public OntModel getDataModel() {
        return this.dataModel;
    }

    public String getDATA_NS() {
        return this.DATA_NS;
    }

    public static void unzip(ZipFile zip, String name) throws IOException {
        Boolean containsXLSFile = false;
        File curfile = new File(Tools.dataPath + name);
        for (Enumeration<ZipArchiveEntry> ite2 = zip.getEntriesInPhysicalOrder(); ite2.hasMoreElements(); ) {
            ZipArchiveEntry entry2 = ite2.nextElement();
            File curfile2 = new File(curfile, entry2.getName());
            if (!entry2.isDirectory()) {
                File parent = curfile2.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                try {
                    OutputStream out2 = new FileOutputStream(curfile2);
                    IOUtils.copy(zip.getInputStream(entry2), out2);
                    out2.close();
                    if (!containsXLSFile && FilenameUtils.getExtension(curfile2.getPath()).contains("xls")) {
                        containsXLSFile = true;
                    }
                } catch (FileNotFoundException fnf) {
                    logger.error("Failed unzip file " + curfile.getAbsolutePath());
                } catch (ZipException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                curfile2.mkdirs();
            }
        }
        zip.close();
    }

    public Boolean load() {
        logger.info("Loading data " + getName().get());
        Tools.addProgress(ProgressPO2.OPEN, "Retrieving data");
        this.initResource();
        if (local) {
            File f = new File(Tools.dataPath + getName().get());
            if(!f.exists()) {
                f.mkdirs(); // on creer la structure en local si elle n'existe pas
            }
            localLoaded.setValue(true);
            canRead.setValue(true);
            setCurrentVersionLoaded("");

        } else {
            if (publiq) {
                if (downloadLink != null) {
                    ZipFile in = CloudConnector.getZipFile(this);
                    Tools.updateProgress(ProgressPO2.OPEN, 0.5);

                    for (Enumeration<ZipArchiveEntry> ite = in.getEntriesInPhysicalOrder(); ite.hasMoreElements(); ) {
                        ZipArchiveEntry entry = ite.nextElement();
                        try {
                            File curfile = new File(Tools.dataPath, entry.getName());
                            // clear du répertoire !!!

                            if (!entry.isDirectory()) {
                                if(FilenameUtils.getExtension(curfile.getPath()).equals("zip")) {
                                    File f = File.createTempFile("dataTemp", ".zip");
                                    FileOutputStream fo = new FileOutputStream(f);
                                    IOUtils.copy(in.getInputStream(entry), fo);
                                    ZipFile dataZip = new ZipFile(f);
                                    Boolean containsXLSFile = false;
                                    for (Enumeration<ZipArchiveEntry> ite2 = dataZip.getEntriesInPhysicalOrder(); ite2.hasMoreElements(); ) {
                                        ZipArchiveEntry entry2 = ite2.nextElement();
                                        File curfile2 = new File(curfile.getParent(), entry2.getName());
                                        if (!entry2.isDirectory()) {
                                            File parent = curfile2.getParentFile();
                                            if (!parent.exists()) {
                                                parent.mkdirs();
                                            }
                                            try {
                                                OutputStream out2 = new FileOutputStream(curfile2);
                                                IOUtils.copy(dataZip.getInputStream(entry2), out2);
                                                out2.close();
                                                if (!containsXLSFile && FilenameUtils.getExtension(curfile2.getPath()).contains("xls")) {
                                                    containsXLSFile = true;

                                                }
                                            } catch (FileNotFoundException fnf) {
                                                logger.error("Failed unzip file " + curfile.getAbsolutePath());
                                            }
                                        } else {
                                            curfile2.mkdirs();
                                        }
                                    }
                                    fo.close();
                                    dataZip.close();

                                    if(containsXLSFile) {
                                        publicloaded.setValue(true);
                                        canRead.setValue(true);
                                    }
                                } else {
                                    File parent = curfile.getParentFile();
                                    if (!parent.exists()) {
                                        parent.mkdirs();
                                    }
                                    try {
                                        OutputStream out = new FileOutputStream(curfile);
                                        IOUtils.copy(in.getInputStream(entry), out);
                                        out.close();
                                    } catch (FileNotFoundException fnf) {
                                        logger.error("Failed unzip file " + curfile.getAbsolutePath());
                                    }
                                }
                            } else {
                                deleteFolder(curfile);
                                curfile.mkdirs();
                            }
                        } catch (IOException e) {
                            logger.error("impossible de décompresser " + downloadLink, e);
                        }
                    }
                    try {
                        in.close();
                    } catch (IOException e) {
                        logger.error("impossible de fermer le zip ", e);

                    }
                }
            } else {
                // l'ontologie est privé. On utilise webDav . L'utilisateur est connecté sinon il ne pourrait pas etre ici
                loadDav();
            }
        }
        if(publicloaded.getValue() || privateLoaded.getValue() || localLoaded.getValue()) {
            refreshListAllFile();
            // setting the good changeListener
            changeListener = (observableValue, o, t1) -> {
                this.setModified(true);
            };
        }
        Tools.updateProgress(ProgressPO2.OPEN, 1.0);
        Tools.delProgress(ProgressPO2.OPEN);
        return publicloaded.getValue() || privateLoaded.getValue() || localLoaded.getValue();
    }

    public void refreshListAllFile() {
        listAllFile = DataTools.listFiles(new File(Tools.dataPath + this.getName().get()));
    }

    public void deleteFolder(File folder) {
        if(folder.exists()) {
            File[] files = folder.listFiles();
            if (files != null) { //some JVMs return null for empty dirs
                for (File f : files) {
                    if (f.isDirectory()) {
                        deleteFolder(f);
                    } else {
                        f.delete();
                    }
                }
            }
            folder.delete();
        }
    }
    public Boolean lockMode() throws AlreayLockException, LocalLockException, IOException, URISyntaxException, CantWriteException, NotLoginException {
        Tools.addProgress(ProgressPO2.LOCK, "lock aquisition");
        if (!local) {
            if(!isSynchro()) {
                init();
                load();
                DataTools.analyseFiles(this);
            }
            if (!privateLoaded.getValue()) {
                if (!CloudConnector.isInitUser()) {
                    Tools.delProgress(ProgressPO2.LOCK);
                    throw new NotLoginException();
                }
                this.loadDav();
            }
            if (privateLoaded.getValue() && canWrite.getValue()) {
                try {
                    lock = CloudConnector.lock(this);
                } catch (AlreayLockException e) {
                    Tools.updateProgress(ProgressPO2.LOCK, 1.0);
                    Tools.delProgress(ProgressPO2.LOCK);
                    throw e;
                }
            } else {
            lock = false;
            Tools.updateProgress(ProgressPO2.LOCK, 1.0);
            Tools.delProgress(ProgressPO2.LOCK);
            throw new CantWriteException();
        }
            if (lock == null) {
                lock = false;
            }
        } else {
            Tools.updateProgress(ProgressPO2.LOCK, 1.0);
            Tools.delProgress(ProgressPO2.LOCK);
            throw new LocalLockException();
        }

        Tools.updateProgress(ProgressPO2.LOCK, 1.0);
        Tools.delProgress(ProgressPO2.LOCK);
        return lock;
    }
    public void initMaterialMethod(String filePath) {
        if(materialMethodFile == null) {
            if(filePath != null) {
                materialMethodFile = new MaterialsMethodsFile(filePath, "useless", this);
            }
        }
    }

    public void addMaterialMethod(String path) {
        if(materialMethodFile == null) {
            initMaterialMethod(path);
        }
        MaterialsMethodsFile newFileMatMet = new MaterialsMethodsFile(path, "useless", this);
        newFileMatMet.constructData();
    }

    public MaterialMethodPart searchMaterialIdType(String id, String type) {
        if(materialMethodFile != null) {
            Optional<MaterialMethodPart> opt = materialMethodFile.getMaterialPart().stream().filter(materialMethodPart -> materialMethodPart.getOriginalID().equalsIgnoreCase(id) && materialMethodPart.getOntoType().getValue().get().equalsIgnoreCase(type)).findFirst();
            if(opt.isPresent()) {
                return opt.get();
            }
        }
        return null;
    }

    public MaterialMethodPart searchMaterialIdFile(String id, String file) {
        if(materialMethodFile != null) {
            Optional<MaterialMethodPart> opt = materialMethodFile.getMaterialPart().stream().filter(materialMethodPart -> materialMethodPart.getOriginalID().equalsIgnoreCase(id) && materialMethodPart.getOriginalFolder().contains(file)).findFirst();
            if(opt.isPresent()) {
                return opt.get();
            } else {
                Optional<MaterialMethodPart> opt2 = materialMethodFile.getMaterialPart().stream().filter(materialMethodPart -> materialMethodPart.getOriginalID().equalsIgnoreCase(id) || materialMethodPart.getID().getValue().get().equalsIgnoreCase(id)).findFirst();
                if(opt2.isPresent()) {
                    return opt2.get();
                }
            }
        }
        return null;
    }

    public MaterialMethodPart searchMethodIdType(String id, String type) {
        if(materialMethodFile != null) {
            Optional<MaterialMethodPart> opt = materialMethodFile.getMethodPart().stream().filter(materialMethodPart -> materialMethodPart.getOriginalID().equalsIgnoreCase(id) && materialMethodPart.getOntoType().getValue().get().equalsIgnoreCase(type)).findFirst();
            if(opt.isPresent()) {
                return opt.get();
            }
        }
        return null;
    }

    public MaterialMethodPart searchMethodIdFile(String id, String file) {
        if(materialMethodFile != null) {
            Optional<MaterialMethodPart> opt = materialMethodFile.getMethodPart().stream().filter(materialMethodPart -> materialMethodPart.getOriginalID().equalsIgnoreCase(id) && materialMethodPart.getOriginalFolder().contains(file)).findFirst();
            if(opt.isPresent()) {
                return opt.get();
            }else {
                Optional<MaterialMethodPart> opt2 = materialMethodFile.getMethodPart().stream().filter(materialMethodPart -> materialMethodPart.getOriginalID().equalsIgnoreCase(id) || materialMethodPart.getID().getValue().get().equalsIgnoreCase(id)).findFirst();
                if(opt2.isPresent()) {
                    return opt2.get();
                }
            }
        }
        return null;
    }



//    public MaterialMethodPart getMateriel(String id) {
//        if(materialMethodFile != null) {
//            return materialMethodFile.getMateriel(id);
//        } else {
//            return null;
//        }
//    }
//
//    public MaterialMethodPart getMethod(String id) {
//        if(materialMethodFile != null) {
//            return materialMethodFile.getMethod(id);
//        } else {
//            return null;
//        }
//    }

    public Boolean unlockMode() {
        if (privateLoaded.getValue() && lock) {
            Boolean res = CloudConnector.unlock(this);
            if (res) {
                lock = false;
            }
            return res;
        }
        return false;
    }

    public Boolean analyseProcess(GeneralFile processFile) {
        Tools.addProgress(ProgressPO2.OPEN, "Opening process");
        Boolean retour = processFile.constructData();
        Tools.updateProgress(ProgressPO2.OPEN, 1.0);
        Tools.delProgress(ProgressPO2.OPEN);
            return retour;
    }


    public Map<String, File> getListAllFile() {
        return listAllFile;
    }

    public File getFile(String name) {
        return listAllFile.get(name);
    }

    public void createEmptyMaterialMethodFile() {
        materialMethodFile = new MaterialsMethodsFile(projectFile.getFilePath(),"materialsAndMethods", this);
    }



//    public boolean saveProject() {
//        Tools.addProgress(ProgressPO2.SAVE, "local save in progress");
//
//        // savegarde du material&method
//        String matmetFilePath = null;
//        if(materialMethodFile != null) {
//            matmetFilePath = getLocalPath()+File.separator+"materials-methods.xlsx";
//            File f = new File(matmetFilePath);
//            if(f.exists()) {
//                f.delete();
//            } else {
//                f.getParentFile().mkdirs();
//            }
//            XSSFWorkbook book = new XSSFWorkbook();
//
//            XSSFSheet s = book.createSheet("data");
//
//            Integer compteur = 0;
//            Integer compteurMax = 0;
//            for(MaterialMethodPart material : materialMethodFile.getMaterialPart()) {
//                XSSFRow r = s.createRow(compteur++);
//                r.createCell(0).setCellValue("Material");
//                for(LinkedList<String> line : material.getDataForExcel()) {
//                    r = s.createRow(compteur++);
//                    Integer compteurCell = 0;
//                    for(String val : line ) {
//                        r.createCell(compteurCell++).setCellValue(val);
//                        if(compteurMax < compteurCell) {
//                            compteurMax = compteurCell;
//                        }
//                    }
//                }
//                s.createRow(compteur++);
//            }
//
//            for(MaterialMethodPart method : materialMethodFile.getMethodPart()) {
//                XSSFRow r = s.createRow(compteur++);
//                r.createCell(0).setCellValue("Method");
//                for(LinkedList<String> line : method.getDataForExcel()) {
//                    r = s.createRow(compteur++);
//                    Integer compteurCell = 0;
//                    for(String val : line ) {
//                        r.createCell(compteurCell++).setCellValue(val);
//                        if(compteurMax < compteurCell) {
//                            compteurMax = compteurCell;
//                        }
//                    }
//                }
//                s.createRow(compteur++);
//            }
//
//            Tools.autoResize(s, compteurMax);
//            FileOutputStream fout = null;
//            try {
//                fout = new FileOutputStream(matmetFilePath, false);
//                book.write(fout);
//                fout.close();
//            } catch (IOException e) {
//                Tools.updateProgress(ProgressPO2.SAVE, 1.0);
//                Tools.delProgress(ProgressPO2.SAVE);
//                e.printStackTrace();
//                return false;
//            }
//
//        }
//
//        for(GeneralFile generalFile : listGeneralFile) {
//                if(generalFile.isLoaded()) {
//                    try {
//                        generalFile.saveData(matmetFilePath);
//                    } catch (IOException | InvalidFormatException e) {
//                        Tools.updateProgress(ProgressPO2.SAVE, 1.0);
//                        Tools.delProgress(ProgressPO2.SAVE);
//                        e.printStackTrace();
//                        return false;
//                    }
//                }
//        }
//
//        listAllFile = DataTools.listFiles(new File(Tools.dataPath + this.getName().get()));
//
//        Tools.updateProgress(ProgressPO2.SAVE, 1.0);
//        Tools.delProgress(ProgressPO2.SAVE);
//        return true;
//    }

//
//    public Resource searchOrCreateObj(String name, JSONObject json) {
//
//            for (VocabConcept vn : listObject) {
//                if (vn.matchLabel(name)) {
//                    String uri = vn.getURI();
//                    return domainModel.getOntClass(uri);
//                }
//            }
//
//            OntClass newClass = getDataModel().createClass(getDOMAIN_NS() + "undefinedObject/" + Tools.normalize(name));
//            newClass.addProperty(SKOS.prefLabel,name, "");
//            json.getJSONArray("warning").put("Object "+ name + " not found in ontology");
//
//        return newClass;
//
//    }



    public void addNewMaterialMethod(MaterialMethodPart mPart) {
        if(DataPartType.MATERIAL_RAW.equals(mPart.getPartType())) {
            addNewMaterial(mPart);
        }
        if(DataPartType.METHOD_RAW.equals(mPart.getPartType())) {
            addNewMethod(mPart);
        }
    }

    private void addNewMaterial(MaterialMethodPart mPart) {
        if(this.materialMethodFile != null) {
            if(this.materialMethodFile.getMateriel(mPart.getID().getValue().get()) != null) {
                mPart.changeID();
            }
            this.materialMethodFile.getMaterialPart().add(mPart);
        }
    }

    private void addNewMethod(MaterialMethodPart mPart) {
        if(this.materialMethodFile != null) {
            if(this.materialMethodFile.getMethod(mPart.getID().getValue().get()) != null) {
                mPart.changeID();
            }
            this.materialMethodFile.getMethodPart().add(mPart);
        }
    }

    public Boolean publish(JSONObject listPath) {
        if(!listPath.keySet().contains("file_onto") || !listPath.keySet().contains("file_data") || !listPath.keySet().contains("onto_name")) {
            return false;
        }
        String projectName = "";
        String repoLabel = "";
        String ontologyID = Tools.normalize(listPath.getString("onto_name"));
        projectName = this.getProjectFile().getNameProperty().getValue();

        repoLabel = "repository for project " + projectName + "-- Ontology used : " + ontologyID;

        String projectID = Tools.normalize(projectName);
        try {
            RDF4JTools.createOrClearRepository(repoLabel, projectID);
            RDF4JTools.upload(projectID, "http://opendata.inrae.fr/PO2/ontology/" + projectID, new FileInputStream(listPath.getString("file_onto")), RDFFormat.TURTLE);
            RDF4JTools.upload(projectID, "http://opendata.inrae.fr/PO2/data/" + projectID, new FileInputStream(listPath.getString("file_data")), RDFFormat.TURTLE);
            if(RDF4JTools.shaclShapeExist(ontologyID, "Onto")) {
                logger.debug("A shacl graph exist in ontology {}. Adding it to the data", ontologyID);
                ByteArrayOutputStream shaclOnto = new ByteArrayOutputStream();
                RDF4JTools.downloadShaclGraph(ontologyID, shaclOnto, "Onto" );
                RDF4JTools.updateDataShaclGraph(projectID, new ByteArrayInputStream(shaclOnto.toByteArray()), "Onto");
            }

        } catch (IOException e) {
            logger.error("an error occured", e);
            e.printStackTrace();
            return false;
        }

        try {
            RDF4JTools.setFreeReadRepository(projectID, isPublic());
        } catch (AuthenticationException | IOException e) {
            e.printStackTrace();
            return false;
        }


        if (this.getGroup() != null && !this.getGroup().isEmpty()) {
            try {
                String groupName = Tools.normalize(this.getGroup());
                RDF4JTools.replaceGraph(groupName, "http://opendata.inrae.fr/PO2/data/" + projectID, new FileInputStream(listPath.getString("file_data")), "http://opendata.inrae.fr/PO2/ontology/"+ projectID, new FileInputStream(listPath.getString("file_onto")), RDFFormat.TURTLE);
                RDF4JTools.setFreeReadRepository(groupName, isPublic());
            } catch (IOException  | AuthenticationException e) {
                e.printStackTrace();
            }
        }

        CloudConnector.addLog(this, "published");
        RDF4JTools.initRepositotyInfos(this);
        return true;
    }

    public JSONObject semantize(Pair<GeneralFile, ArrayList<ItineraryFile>> process, Ontology onto) throws IOException {
        Tools.addProgress(ProgressPO2.PUBLISH, "Loading core ontology");

        JSONObject mess = new JSONObject();
        JSONArray error = new JSONArray();
        JSONArray listMessages = new JSONArray();
        mess.put("listMessages", listMessages);
        mess.put("error", error);
        if(!this.isLocked()) {
            error.put("data not locked");
        }
        if(!this.isLoaded().get()) {
            error.put("data not loaded");
        }
        if(this.isModified()) {
            error.put("data not saved");
        }
        if(onto == null) {
            error.put("ontology is null");
            return mess;
        }
        if(!onto.isLoaded().get()) {
            error.put("ontology not loaded");
        }
        if(onto.isModified()) {
            error.put("ontology not saved");
        }
        if(error.length() > 0) {
            Tools.delProgress(ProgressPO2.PUBLISH);
            return mess;
        }
        mess.put("onto_name", onto.getName().get());

        Tools.updateProgress(ProgressPO2.PUBLISH, "Loading core ontology");
        String PO2_domain = null;
        OntModel dataModel = null;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Tools.updateProgress(ProgressPO2.PUBLISH, "Loading domain ontology");

        logger.debug("Chargement de la domaine ontologie : " + onto.getName().get());
        OntModel domainModel = JenaTools.ReadModel(onto.getOntologyFile().getAbsolutePath());

        onto.setDomainModel(domainModel);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Tools.updateProgress(ProgressPO2.PUBLISH, "Settings NameSpaces");

        if(onto.getDomainModel() != null) {
            logger.debug("creating data model");
            GraphMaker gm = new SimpleGraphMaker();

            Graph dataGraph = gm.createGraph("http://www.testnom.com");


            DATA_NS = "http://opendata.inrae.fr/PO2/data/"+ Tools.normalize(projectFile.getNameProperty().getValue())+"/";
            this.setGraphModel(dataGraph);
            dataModel = this.createDataModel();

            dataModel.setNsPrefix("domain", onto.getDOMAIN_NS());
            dataModel.setNsPrefix("PO2", onto.getCORE_NS());
            dataModel.setNsPrefix("", this.getDATA_NS());
            dataModel.setNsPrefix("time", JenaTools.TIME);
            dataModel.setNsPrefix("skos", SKOS.getURI());
            dataModel.setNsPrefix("dcat", DCAT.getURI());
            dataModel.setNsPrefix("sosa", JenaTools.SOSA);
            dataModel.setNsPrefix("ssn", JenaTools.SSN);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Tools.updateProgress(ProgressPO2.PUBLISH, "Semantization", 0.0);

            // on refait une sauvegarde pour changer le num de version majeur
            boolean newSave = this.getProjectFile().saveData(true);
            if(!newSave) {
                Tools.delProgress(ProgressPO2.PUBLISH);
                error.put("unable to save data");
                return mess;
            } else {
                Boolean upload = CloudConnector.update(this);
                if(!upload ){
                    Tools.delProgress(ProgressPO2.PUBLISH);
                    error.put("unable to upload data");
                    return mess;
                }
            }

            File tempFile = this.getProjectFile().publish(onto, dataModel, listMessages, process);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Tools.updateProgress(ProgressPO2.PUBLISH, "Merge of graphs", -1.0);

            // fusion core && domain
            Model unionModel = onto.getCoreModel().union(onto.getDomainModel());
            File tempDomain = File.createTempFile("pref", "ttl");
            FileOutputStream output = new FileOutputStream(tempDomain);

            RDFWriter ontologyWriter = RDFWriter.create().source(unionModel).lang(Lang.TTL).build();
            ontologyWriter.output(output);
            output.flush();
            mess.put("file_onto", tempDomain.getAbsolutePath());
            mess.put("file_data", tempFile.getAbsolutePath());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Tools.delProgress(ProgressPO2.PUBLISH);

        return mess;
    }

    public Report startShaclValidation() throws IOException {
        Tools.addProgress(ProgressPO2.GRAPH, "Preliminary checks");
        Report report = new Report();

        String dataID = Tools.normalize(this.getName().get());

        String repoName = RDF4JTools.getRepository(dataID);
        if (repoName.equals("error"))
            report.addError("data not yet published");

        if (!RDF4JTools.shaclShapeExist(dataID, "Data") && !RDF4JTools.shaclShapeExist(dataID, "Onto"))
            report.addError("SHACL shape graph does not exists");

        if(report.success()) {
            Tools.delProgress(ProgressPO2.GRAPH);
            return report;
        }
    
        // Validation
        Tools.updateProgress(ProgressPO2.GRAPH, "SHACL validation...");
        report = RDF4JTools.startShaclValidation(dataID);
        Tools.delProgress(ProgressPO2.GRAPH);
        return report;
    }

    public String searchFolderPath(String fileName) {
        if(listAllFile.containsKey(fileName)) {
            return listAllFile.get(fileName).getParent() + File.separator;
        }
        return null;
    }
}
