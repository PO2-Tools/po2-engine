/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import java.util.Map;
import java.util.TreeMap;

public class Ontologies {

    private static TreeMap<String, OntoData> listOntologies = new TreeMap<>(String::compareToIgnoreCase);

    private Ontologies() {
    }

    public static Ontology addOntology(String ontoName) {
        Ontology o = null;
        if (!listOntologies.containsKey(ontoName)) {
            o = new Ontology(ontoName);
            listOntologies.put(ontoName, o);
        } else {
            if (listOntologies.get(ontoName).isLocal()) {
                o = (Ontology)listOntologies.get(ontoName);
            }
        }
        return o;
    }

    public static Ontology getOntology(String ontoName) {
        return (Ontology)listOntologies.get(ontoName);
    }

    public static Map<String, OntoData> getOntologies() {
        return listOntologies;
    }

}
