/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.*;
import javafx.beans.value.ObservableObjectValue;
import javafx.beans.value.ObservableStringValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author stephane
 */
public class VocabConcept implements Comparable<VocabConcept> {

    private boolean beingVisited = false;
    private boolean visited = false;
    private static Logger logger = LogManager.getLogger(VocabConcept.class);

    public static final List<String> listLang = Arrays.asList(
        "en",
        "fr",
        "de",
        "es",
        "it"
    );
    private StringProperty name;
    private StringProperty scopeNote;
    private StringProperty editorialNote;
    private StringProperty note;
    private ObservableMap<SimpleStringProperty, SimpleStringProperty> mapLabel;
    private ObservableMap<SimpleStringProperty, SimpleStringProperty> mapDef;
    private ObservableMap<SimpleStringProperty, SimpleStringProperty> mapAltLabel;
    private ObservableList<SimpleStringProperty> listExactMatch;
    private ObservableList<SimpleStringProperty> listCloseMatch;
    private ObservableList<SimpleStringProperty> listDimension; // only used by attribute
    private StringProperty uri;
    private String searchIn;
    private String po2Node;
    private ArrayList<VocabConcept> fathers;
    private ObservableList<VocabConcept> listSubNode;
    private ObservableList<SkosScheme> listSkosScheme;
    private BooleanProperty deprecated;
    private Ontology ontology;

    public Boolean isDeletable() {
        return deletable;
    }

    public void setDeletable(Boolean deletable) {
        this.deletable = deletable;
    }

    private Boolean deletable;

    public VocabConcept(Ontology ontology) {
        this(ontology.generateURI("c"), ontology);
    }


    public VocabConcept(String uri, Ontology ontology) {
        this.ontology = ontology;
        this.listSkosScheme = FXCollections.observableArrayList();
        this.listSubNode = FXCollections.observableArrayList();
        this.listExactMatch = FXCollections.observableArrayList();
        this.listCloseMatch = FXCollections.observableArrayList();
        this.listDimension = FXCollections.observableArrayList();

        this.mapLabel = FXCollections.observableHashMap();
        this.mapDef = FXCollections.observableHashMap();
        this.mapAltLabel = FXCollections.observableHashMap();
        for(String l : listLang) {
            mapLabel.put(new SimpleStringProperty(l), new SimpleStringProperty(""));
            mapDef.put(new SimpleStringProperty(l), new SimpleStringProperty(""));
            mapAltLabel.put(new SimpleStringProperty(l), new SimpleStringProperty(""));
        }
        this.uri = new SimpleStringProperty(uri);
        this.po2Node = null;
        this.name = new SimpleStringProperty("");
        this.fathers = new ArrayList<>();
        this.deletable = true;
        this.deprecated = new SimpleBooleanProperty(false);
        this.scopeNote = new SimpleStringProperty("");
        this.editorialNote = new SimpleStringProperty("");
        this.note = new SimpleStringProperty("");
        // always add default main SkosConceptScheme
        if(this.ontology != null) {
            addSkosScheme(this.ontology.getMainScheme());
        }

    }

    public void addSkosScheme(SkosScheme scheme) {
        if(scheme != null && !this.listSkosScheme.contains(scheme)) {
            this.listSkosScheme.add(scheme);
        } else {
            if(scheme == null) {
                logger.error("adding null schema on node " +this.getName());
            }
        }
    }

    public void removeSkosConceptScheme(SkosScheme scheme) {
        if(this.listSkosScheme.contains(scheme)) {
            this.listSkosScheme.remove(scheme);
        } else {
            logger.error("unable to remove scheme " + scheme.getName());
        }
    }

    public void removeSkosConceptSchemeAllSon(SkosScheme scheme) {
        if(this.listSkosScheme.contains(scheme)) {
            this.listSkosScheme.remove(scheme);
            listSubNode.forEach(vocabNode -> vocabNode.removeSkosConceptSchemeAllSon(scheme));
        } else {
            logger.error("unable to remove scheme " + scheme.getName());
        }
    }

    public Boolean getDeprecated() {
        return deprecated.get();
    }

    public void setDeprecated(Boolean deprecated) {
        this.deprecated.setValue(deprecated);
    }

    public BooleanProperty deprecatedProperty() {
        return this.deprecated;
    }

    public void addChildren(VocabConcept child) {
        if(child != null && !this.listSubNode.contains(child)) {
            this.listSubNode.add(child);
            FXCollections.sort(this.listSubNode);
        }
    }

    public void addChildren(List<VocabConcept> childs) {
        this.listSubNode.addAll(childs);
        FXCollections.sort(this.listSubNode);
    }

    public void addFather(VocabConcept father) {
        if(father != null && !this.fathers.contains(father)) {
            this.fathers.add(father);
        }
    }

    public String isPO2Node() {
        return po2Node;
    }

    public void setPO2Node(String nodeType) {
        if(nodeType != null) {
            po2Node = nodeType.toLowerCase();
            for (VocabConcept node : listSubNode) {
                node.setPO2Node(nodeType);
            }
        }
    }

    public VocabConcept getTopConcept() {
        if(this.getFathers().contains(ontology.getRootNode())) {
            return this;
        }
        VocabConcept firstF = this.getFathers().stream().findFirst().orElse(null);
        return firstF != null ? firstF.getTopConcept() : null;
    }

    public LinkedHashMap<String, LinkedHashMap<String, String>> exportNode() {
        LinkedHashMap<String, LinkedHashMap<String, String>> struct = new LinkedHashMap<>();
        SortedSet<String> lang = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.equalsIgnoreCase(o2)) {
                    return 0;
                }
                if ("en".equalsIgnoreCase(o1)) {
                    return -1;
                }
                if ("en".equalsIgnoreCase(o2)) {
                    return 1;
                }
                return o1.compareTo(o2);
            }
        });
        Set<String> langWithInfo = new HashSet<>();
        Set<String> listNodeDone = new HashSet<>();
        exportNode(lang, struct, langWithInfo, listNodeDone);
        lang.removeAll(langWithInfo);
        // now lang contains all lang to remove because no information available
        if(!lang.isEmpty()) {
            struct.values().forEach(sslh -> {
                lang.forEach(la -> {
                    sslh.keySet().removeIf(k -> k.contains("@"+la));
                });
            });
        }



        return struct;
    }
    public void exportNode(SortedSet<String> lang, LinkedHashMap<String, LinkedHashMap<String, String>> struct, Set<String> langWithInfo, Set<String> nodeDone) {
        if(nodeDone.contains(getURI())) {
            return;
        }

        String id = getURI();
        nodeDone.add(id);

        if(lang.isEmpty()) {
            lang.addAll(getLabel().keySet().stream().map(StringPropertyBase::get).collect(Collectors.toCollection(TreeSet::new)));
            lang.addAll(getAltLabel().keySet().stream().map(StringPropertyBase::get).collect(Collectors.toCollection(TreeSet::new)));
            lang.addAll(getDefinition().keySet().stream().map(StringPropertyBase::get).collect(Collectors.toCollection(TreeSet::new)));
        }


        if(!struct.containsKey(id)) {
            struct.put(id, new LinkedHashMap<>());
        }

        struct.get(id).put("uri",id);

        for(String l : lang) {
            SimpleStringProperty lab = getLabel(l);
            if(lab != null) {
                if(lab.isNotEmpty().get()) {
                    langWithInfo.add(l);
                }

                if (!struct.get(id).containsKey("label@" + l)) {
                    struct.get(id).put(("label@" + l), lab.get().trim());
                }


                SimpleStringProperty alt = getAltLabel(l);
                if (alt == null) {
                    alt = new SimpleStringProperty("");
                }
                if(alt.isNotEmpty().get()) {
                    langWithInfo.add(l);
                }
                if (!struct.get(id).containsKey("altLabel@" + l)) {
                    String listAlt = Arrays.stream(alt.get().split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.joining("::"));
                    struct.get(id).put("altLabel@" + l, listAlt);
                }


                SimpleStringProperty def = getDefinition(l);
                if (def == null) {
                    def = new SimpleStringProperty("");
                }
                if(def.isNotEmpty().get()) {
                    langWithInfo.add(l);
                }
                if (!struct.get(id).containsKey("description@" + l)) {
                    String listDes = Arrays.stream(def.get().split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.joining("::"));
                    struct.get(id).put("description@" + l, listDes);
                }
            }
        }

        if(!struct.get(id).containsKey("scopeNote")) {
            struct.get(id).put("scopeNote", getScopeNote());
        }

        if(!struct.get(id).containsKey("editorialNote")) {
            struct.get(id).put("editorialNote", getEditorialNote());
        }

        if(!struct.get(id).containsKey("note")) {
            struct.get(id).put("note", getNote());
        }

        if(!struct.get(id).containsKey("exactMatch")) {
            struct.get(id).put("exactMatch", getListExactMatch().stream().map(StringPropertyBase::get).collect(Collectors.joining("::")));
        }

        if(!struct.get(id).containsKey("closeMatch")) {
            struct.get(id).put("closeMatch", getListCloseMatch().stream().map(StringPropertyBase::get).collect(Collectors.joining("::")));
        }

        if(!struct.get(id).containsKey("conceptScheme")) {
            struct.get(id).put("conceptScheme", getListSkosScheme().stream().map(SkosScheme::getUri).collect(Collectors.joining("::")));
        }

        if("attribute".equalsIgnoreCase(isPO2Node())) { // add dimension
            if(!struct.get(id).containsKey("dimension")) {
                struct.get(id).put("dimension", getListDimension().stream().map(ssp -> ssp.get()).collect(Collectors.joining("::")));
            }
        }

        for(VocabConcept sn : listSubNode) {
            if(!sn.getDeprecated()) {
                sn.exportNode(lang, struct, langWithInfo, nodeDone);
            }
        }
    }

    public LinkedList<Pair<String, String>> exportHierarchy() {
        LinkedList<Pair<String, String>> hie = new LinkedList<>();
       exportHierarchy(hie);
        return hie;
    }

    public void exportHierarchy(LinkedList<Pair<String, String>> hierarchy) {
        String idF = null;
        if(getLabel("en") != null && !getLabel("en").getValue().isEmpty()) {
            idF = getLabel("en").getValue().trim();
        } else {
            idF = this.hashCode()+"_bad_value";
        }
        for(VocabConcept s : getSubNode().filtered(vocabConcept -> !vocabConcept.getDeprecated())) {
            String idS = null;
            if(s.getLabel("en") != null && !s.getLabel("en").getValue().isEmpty()) {
                idS = s.getLabel("en").getValue().trim();
            } else {
                idS = s.hashCode()+"_bad_value";
            }
            hierarchy.addLast(new Pair<>(idF, idS));
        }
        for(VocabConcept s : getSubNode().filtered(vocabConcept -> !vocabConcept.getDeprecated())) {
            s.exportHierarchy(hierarchy);
        }
        if(hierarchy.isEmpty()) {
            hierarchy.addLast(new Pair<>(idF, ""));
        }
    }

    public void setScopeNote(String scopeNote) {
        this.scopeNote.set(scopeNote);
    }

    public String getScopeNote() {
        return scopeNote.get();
    }

    public StringProperty scopeNoteProperty() {
        return scopeNote;
    }

    public String getEditorialNote() {
        return editorialNote.get();
    }

    public StringProperty editorialNoteProperty() {
        return editorialNote;
    }

    public void setEditorialNote(String editorialNote) {
        this.editorialNote.set(editorialNote);
    }

    public String getNote() {
        return note.get();
    }

    public StringProperty noteProperty() { return note; }

    public void setNote(String note) { this.note.set(note);}
    /**
     * @return
     */
    public String getURI() {
        return this.uri.get();
    }

    public void setURI(String uri) {
        this.uri = new SimpleStringProperty(uri);
    }


    public boolean setLabel(String lang, String label) {
        return setLabel(lang, label, false);
    }
    /**
     * @param lang
     * @param label
     */
    public boolean setLabel(String lang, String label, Boolean force) {
        if(lang == null || lang.isEmpty()) {
            lang = "en";
        }
        SimpleStringProperty l = null;
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapLabel.entrySet()) {
            if (entry.getKey().get().equalsIgnoreCase(lang)) {
                l = entry.getKey();
            }
        }
        if (l == null) {
            l = new SimpleStringProperty(lang);
            this.mapLabel.put(l, new SimpleStringProperty(""));
            this.mapDef.put(l, new SimpleStringProperty(""));
            this.mapAltLabel.put(l, new SimpleStringProperty(""));
        }
        return setLabel(l, label, force);
    }

    /**
     * @param lang
     * @param label
     */
    public boolean setLabel(SimpleStringProperty lang, String label, boolean force) {
       if(lang != null && mapLabel.containsKey(lang)) {

           if(label != null) label=label.trim();

//         checking if label already exist in EN!
           if(!force && lang.get().equalsIgnoreCase("en") && this.ontology.isLabelExist(label, lang.get())) {
               return false;
           }
           this.mapLabel.get(lang).setValue(label);
           if ("en".equalsIgnoreCase(lang.getValue())) {
               this.setName(label);
           } else {
               if (this.getName() == null || "".equals(this.getName())) { // evite d'avoir un name vide ou null sinon des exception sont levées
                   this.setName(label);
               }
           }
           setPattern();
           return true;

       }
       return false;
    }

    public void setAltLabel(SimpleStringProperty lang, String label, Boolean concat) {
        if(lang != null && mapAltLabel.containsKey(lang)) {
            if(label != null) label=label.trim();
            if(concat) {
                // check if label is already in altLabel
                if(!this.mapAltLabel.get(lang).getValue().toLowerCase().replace("::", " ").contains(label.toLowerCase())) {
                    this.mapAltLabel.get(lang).setValue(this.mapAltLabel.get(lang).getValue() + ":: "+label+"\n");
                }

            } else {
                this.mapAltLabel.get(lang).setValue(label);
            }
            setPattern();
        }
    }


    public void setAltLabel(String lang, String label, Boolean concat) {
        if(lang == null || lang.isEmpty()) {
            lang = "en";
        }
        if(label != null) label=label.trim();
        SimpleStringProperty l = null;
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapAltLabel.entrySet()) {
            if (entry.getKey().get().equalsIgnoreCase(lang)) {
                l = entry.getKey();
            }
        }
        if (l == null) {
            l = new SimpleStringProperty(lang);
            this.mapLabel.put(l, new SimpleStringProperty(""));
            this.mapDef.put(l, new SimpleStringProperty(""));
            this.mapAltLabel.put(l, new SimpleStringProperty(""));
        }
        setAltLabel(l, label, concat);

    }

    public void setExactMatch(String value) {
        listExactMatch.clear();
        Arrays.stream(value.split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).forEach(s -> listExactMatch.add(new SimpleStringProperty(s)));
    }

    public void setClosetMatch(String value) {
        listCloseMatch.clear();
        Arrays.stream(value.split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).forEach(s -> listCloseMatch.add(new SimpleStringProperty(s)));
    }

    public Boolean addExactMatch(String uri) {
        for (SimpleStringProperty val : listExactMatch) {
            if (val.get().equals(uri)) {
                return false;
            }
        }

        this.listExactMatch.add(new SimpleStringProperty(uri));
        return true;
    }

    public Boolean addCloseMatch(String uri) {
        for (SimpleStringProperty val : listCloseMatch) {
            if (val.get().equals(uri)) {
                return false;
            }
        }

        this.listCloseMatch.add(new SimpleStringProperty(uri));
        return true;
    }

    public void addDimension(String newDim) {
        listDimension.add(new SimpleStringProperty(newDim));
    }

    public ObservableList<SimpleStringProperty> getListDimension() { return listDimension; }

    public ObservableList<SimpleStringProperty> getListExactMatch() {
        return listExactMatch;
    }

    public ObservableList<SimpleStringProperty> getListCloseMatch() {
        return listCloseMatch;
    }

    public ObservableList<ObservableObjectValue<Hyperlink>> getListExactMatchHyperLink() {
        ObservableList<ObservableObjectValue<Hyperlink>> retour = FXCollections.observableArrayList();
        for (ObservableStringValue o : listExactMatch) {
            ObservableObjectValue<Hyperlink> no = new SimpleObjectProperty<>(new Hyperlink(o.getValue()));
            retour.add(no);
        }
        return retour;
    }

    public ObservableList<ObservableObjectValue<Hyperlink>> getListCloseMatchHyperLink() {
        ObservableList<ObservableObjectValue<Hyperlink>> retour = FXCollections.observableArrayList();
        for (ObservableStringValue o : listCloseMatch) {
            ObservableObjectValue<Hyperlink> no = new SimpleObjectProperty<>(new Hyperlink(o.getValue()));
            retour.add(no);
        }
        return retour;
    }


    /**
     * @param lang
     * @return
     */
    public SimpleStringProperty getLabel(String lang) {
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapLabel.entrySet()) {
            if (entry.getKey().get().equalsIgnoreCase(lang)) {
                return entry.getValue();
            }
        }
        return null;
    }

    public SimpleStringProperty getLabel(StringProperty lang) {
        if(mapLabel.containsKey(lang)) {
            return mapLabel.get(lang);
        }
        return null;
    }

    /**
     * @return
     */
    public ObservableMap<SimpleStringProperty, SimpleStringProperty> getLabel() {
        return this.mapLabel;
    }

    public ObservableMap<SimpleStringProperty, SimpleStringProperty> getAltLabel() {
        return this.mapAltLabel;
    }

    public SimpleStringProperty getAltLabel(String lang) {
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapAltLabel.entrySet()) {
            if (entry.getKey().get().equals(lang)) {
                return entry.getValue();
            }
        }
        return null;
    }
    /**
     * @param lang
     * @param definition
     */
    public void setDefinition(String lang, String definition, Boolean concat) {
        if(lang == null || lang.isEmpty()) {
            lang = "en";
        }
        SimpleStringProperty l = null;
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapDef.entrySet()) {
            if (entry.getKey().get().equalsIgnoreCase(lang)) {
                l = entry.getKey();
            }
        }
        if (l == null) {
            l = new SimpleStringProperty(lang);
            this.mapLabel.put(l, new SimpleStringProperty(""));
            this.mapDef.put(l, new SimpleStringProperty(""));
            this.mapAltLabel.put(l, new SimpleStringProperty(""));
        }
        setDefinition(l, definition, concat);

    }
    public void setDefinition(SimpleStringProperty lang, String definition, Boolean concat) {
        if(lang != null && mapDef.containsKey(lang)) {
            if(concat) {
                this.mapDef.get(lang).setValue(this.mapDef.get(lang).getValue() + ":: "+definition+"\n");
            } else {
                this.mapDef.get(lang).setValue(definition);
            }
            setPattern();
        }
    }

    /**
     * @param lang
     * @return
     */
    public SimpleStringProperty getDefinition(String lang) {
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapDef.entrySet()) {
            if (entry.getKey().get().equals(lang)) {
                return entry.getValue();
            }
        }
        return null;
    }

    /**
     * @return
     */
    public ObservableMap<SimpleStringProperty, SimpleStringProperty> getDefinition() {
        return this.mapDef;
    }


    public ObservableList<SkosScheme> getListSkosScheme() {
        return listSkosScheme;
    }

//    public ObservableList<VocabConcept> getSubNodeInScheme() {
//        try {
//            FXCollections.sort(listSubNode);
//        } catch (Exception e) {
//            logger.error(e.getClass() + " : " + e.getMessage() + " - impossible de trier la liste des fils", e);
//        }
//        return listSubNode.filtered(vocabNode -> vocabNode.listSkosScheme.contains(ontology.getCurrentScheme()));
//    }

    public ObservableList<VocabConcept> getSubNodeInScheme() {
        return listSubNode.filtered(vocabNode -> {
            if(ontology.isShowDeprecated()) {
                return vocabNode.listSkosScheme.contains(ontology.getCurrentScheme());
            }
            return vocabNode.listSkosScheme.contains(ontology.getCurrentScheme()) && !vocabNode.getDeprecated();
        });
    }


    public ObservableList<VocabConcept> getSubNode() {
        return listSubNode;
    }

    /**
     * @return
     */
    public String getName() {
        if(name.get().isEmpty()) {
            return "no label";
        }
        return name.get();
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * Set the Pattern for search (All label and Description)
     */
    private void setPattern() {
        List<ObservableStringValue> list = new ArrayList<>();
        list.addAll(mapLabel.values());
        list.addAll(mapDef.values());
        list.addAll(mapAltLabel.values());
        list.add(new SimpleStringProperty(getURI()));

        searchIn = list.stream().map(x -> x.get().toLowerCase()).collect(Collectors.joining(" "));
    }

    public String getSearchIn() {
        return searchIn;
    }

    public boolean conceptExist(String label, String type) { // type = label, uri

        switch (type) {
            case "label" :
                for(Entry<SimpleStringProperty, SimpleStringProperty> localentry : this.getLabel().entrySet()) {
                    if(localentry.getValue().get().equalsIgnoreCase(label)) {
                        return true;
                    }
                }
                for (VocabConcept node : this.getSubNode()) {
                    if (node.conceptExist(label, type)) {
                        return true;
                    }
                }
                break;
            case "uri" :
                if (uri.get().equalsIgnoreCase(label)) {
                    return true;
                }
                for (VocabConcept node : this.getSubNode()) {
                    if (node.conceptExist(label, type)) {
                        return true;
                    }
                }
                break;
        }

        return false;
    }


    /**
     * @param value the string to find
     * @return the list of VocabNode where value was find
     */
    public ObservableList<VocabConcept> search(String value) {
        ObservableList<VocabConcept> list = FXCollections.observableArrayList();
        if (searchIn != null) {
            if(Stream.of(value.toLowerCase().split(" ")).allMatch(searchIn::contains)) {
                list.add(this);
            }
        }

        for (VocabConcept node : this.getSubNode()) {
            list.addAll(node.search(value));
        }
        return list;
    }


    @Override
    public String toString() {
        return getName().concat(getDeprecated() ? " (Deprecated)" : "");
    }

    /**
     * @return
     */
    public ObservableStringValue labsToString() {
        StringBuilder back = new StringBuilder();
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapLabel.entrySet()) {
            back.append("- ").append(entry.getValue().get()).append(" (").append(entry.getKey().get()).append(")\n");
        }
        return new SimpleStringProperty(back.toString());
    }

    /**
     * @return
     */
    public SimpleStringProperty descToString() {
        StringBuilder back = new StringBuilder();
        for (Entry<SimpleStringProperty, SimpleStringProperty> entry : mapDef.entrySet()) {
            back.append("- ").append(entry.getValue().get()).append(" (").append(entry.getKey().get()).append(")\n");
        }
        return new SimpleStringProperty(back.toString());
    }

    @Override
    public int compareTo(VocabConcept o) {
        return this.toString().toLowerCase().compareTo(o.toString().toLowerCase());
    }

    public void grabInfo() {

        ArrayList<StringProperty> temp = new ArrayList<>();
        temp.addAll(listExactMatch);
        for (StringProperty val : temp) {
            try {
                HashMap<String, List<String>> labMatch = JenaTools.getListMatch(val.getValue());
                for (String same : labMatch.get("exact")) {
                    this.addExactMatch(same);
                }
                for (String same : labMatch.get("close")) {
                    this.addCloseMatch(same);
                }
            } catch (Exception e) {

            }
        }

        for (SimpleStringProperty val : listExactMatch) {
            try {
                HashMap<String, HashMap<String, List<String>>> labDesc = JenaTools.getLabelsAndDescription(val.getValue());
                for (Entry<String, List<String>> e : labDesc.get("label").entrySet()) {
                    SimpleStringProperty propLabel = this.getLabel(e.getKey());
                    if(propLabel != null && propLabel.get().isEmpty()) {
                        for(String ee : e.getValue()) {
                            this.setLabel(e.getKey(), ee);
                        }
                    }
                }

                for (Entry<String, List<String>> e : labDesc.get("altLabel").entrySet()) {
                    SimpleStringProperty propDesc = this.getAltLabel(e.getKey());
                    if(propDesc != null ) {
                        for(String ee : e.getValue()) {
                            this.setAltLabel(e.getKey(), ee, true);
                        }
                    }
                }


                for (Entry<String, List<String>> e : labDesc.get("Comment").entrySet()) {
                    SimpleStringProperty propDesc = this.getDefinition(e.getKey());
                    if(propDesc != null && propDesc.get().isEmpty()) {
                        for(String ee : e.getValue()) {
                            this.setDefinition(e.getKey(), ee, true);
                        }
                    }
                }



            } catch (Exception e) {

            }
        }
    }

    public void importFrom(String uri, String label, String desc) {
        this.addExactMatch(uri);
        try {
            HashMap<String, HashMap<String, List<String>>> labDesc = JenaTools.getLabelsAndDescription(uri);
            for (Entry<String, List<String>> e : labDesc.get("label").entrySet()) {
                SimpleStringProperty propLabel = this.getLabel(e.getKey());
                if(propLabel != null && propLabel.get().isEmpty()) {
                    for(String ee : e.getValue()) {
                        this.setLabel(e.getKey(), ee);
                    }
                }
            }

            for (Entry<String, List<String>> e : labDesc.get("altLabel").entrySet()) {
                SimpleStringProperty propDesc = this.getAltLabel(e.getKey());
                if(propDesc != null && propDesc.get().isEmpty()) {
                    for(String ee : e.getValue()) {
                        this.setAltLabel(e.getKey(), ee, true);
                    }
                }
            }


            for (Entry<String, List<String>> e : labDesc.get("Comment").entrySet()) {
                SimpleStringProperty propDesc = this.getDefinition(e.getKey());
                if(propDesc != null && propDesc.get().isEmpty()) {
                    for(String ee : e.getValue()) {
                        this.setDefinition(e.getKey(), ee, true);
                    }
                }
            }

            HashMap<String, List<String>> labMatch = JenaTools.getListMatch(uri);
            for (String same : labMatch.get("exact")) {
                this.addExactMatch(same);
            }
            for (String same : labMatch.get("close")) {
                this.addCloseMatch(same);
            }

        } catch (Exception e) {
            // impossible d'importer le vocabulaire
            this.setLabel("en", label);
            if (desc != null && !"".equals(desc)) {
                this.setDefinition("en", desc, false);
            }
        }
    }

    public VocabConcept getNodeByURI(String uri) {
        if (this.uri.getValue().equals(uri)) {
            return this;
        } else {
            for (VocabConcept child : listSubNode) {
                VocabConcept n = child.getNodeByURI(uri);
                if (n != null) {
                    return n;
                }
            }
        }
        return null;

    }

    public List<VocabConcept> getFathers() {
        return fathers;
    }

    private Stream<VocabConcept> getStreamName() {
        Stream<VocabConcept> retour = Stream.empty();
        retour = Stream.concat(retour, Stream.of(this));
//        for(Entry<ObservableStringValue, ObservableStringValue> entry : mapLabel.entrySet()) {
//            retour = Stream.concat(retour, Stream.of(entry.getValue().get()));
//        }
        return retour;
    }

    public Stream<VocabConcept> flattened() {
        return Stream.concat(
                getStreamName(),
            getSubNodeInScheme().stream().flatMap(VocabConcept::flattened));
    }

    public Boolean matchLabel(String label) {
        if(label == null || label.isEmpty()) {
            return false;
        }
        return mapLabel.values().stream().anyMatch(ssp -> ssp.getValue().equalsIgnoreCase(label));
    }

    public boolean containsLabelOrSynonym(String text) {
        for(ObservableStringValue s : mapLabel.values()) {
            if(s.getValue().toLowerCase().contains(text.toLowerCase())) {
                return true;
            }
        }
        for(ObservableStringValue s : mapAltLabel.values()) {
            if(s.getValue().toLowerCase().contains(text.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public boolean containsLabel(String text) {
        for(ObservableStringValue s : mapLabel.values()) {
            if(s.getValue().toLowerCase().contains(text.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public void removeDimension(String oldDim) {
        ObservableStringValue toRemove = null;
        for(ObservableStringValue s : listDimension) {
            if(s.get().equalsIgnoreCase(oldDim)) {
                toRemove = s;
            }
        }
        if(toRemove != null) {
            listDimension.remove(toRemove);
        }
    }

    public void removeExactMatch(String text) {
        ObservableStringValue toRemove = null;
        for(ObservableStringValue s : listExactMatch) {
            if(s.get().equalsIgnoreCase(text)) {
                toRemove = s;
            }
        }
        if(toRemove != null) {
            listExactMatch.remove(toRemove);
        }
    }

    public void removeCloseMatch(String text) {
        ObservableStringValue toRemove = null;
        for(ObservableStringValue s : listCloseMatch) {
            if(s.get().equalsIgnoreCase(text)) {
                toRemove = s;
            }
        }
        if(toRemove != null) {
            listCloseMatch.remove(toRemove);
        }
    }

    public void updateImage() {

    }

    public Node getGraphic() {
        updateImage();
        return null;
    }

    public boolean isBeingVisited() {
        return beingVisited;
    }

    public void setBeingVisited(boolean beingVisited) {
        this.beingVisited = beingVisited;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public void sortSubNode(boolean deepSort) {
        FXCollections.sort(this.listSubNode);
        if (deepSort) {
            this.listSubNode.forEach(n -> n.sortSubNode(true));
        }
    }
}
