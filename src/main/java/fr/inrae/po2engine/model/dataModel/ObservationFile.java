/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.model.vocabulary.PROV;
import org.eclipse.rdf4j.model.vocabulary.TIME;
import org.eclipse.rdf4j.query.algebra.In;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class ObservationFile extends GenericFile{

    private SimpleStringProperty nameProperty;
    private String measureLevel;
    private StepFile stepFile = null;
    private ItineraryFile itineraryPart = null;
    private ObservationMetaPart headerPart = null;
    private List<TablePart> contentList = new LinkedList<>();
    private ObservableList<MaterialMethodLinkPart> listMaterialMethod = FXCollections.observableArrayList();
    private ObservableList<MaterialMethodLinkPart> listMaterialMethodRO = FXCollections.unmodifiableObservableList(listMaterialMethod);
    private static Logger logger = LogManager.getLogger(ObservationFile.class);
    private static final List<GenericPart> listRule = Arrays.asList(new ObservationMetaPart(), new MaterialMethodLinkPart(), new ConsignPart(), new TablePart() );
    private Map<Integer, Individual> myObs = new HashMap<>();


    private ObservationFile() {};

    public ObservationFile(StepFile step) {
        this(step.getFolderPath() + "observation" + File.separator,"observation_" + Tools.generateUniqueTimeStamp(), step.getData());
        this.stepFile = step;
        this.stepFile.addObservationFile(this);
    }


    public ObservationFile(String folderPath, String fileName, StepFile step) {
        this(folderPath, fileName, step.getData());
        this.stepFile = step;
        this.stepFile.addObservationFile(this);
    }

    public ObservationFile(ItineraryFile itinerary) {
        this(itinerary.getProcessFile().getFolderPath() + "observation" + File.separator, "observation_" + Tools.generateUniqueTimeStamp(), itinerary.getProcessFile().getData());
        this.itineraryPart = itinerary;
        this.itineraryPart.addObservation(this);
    }

    public ObservationFile(String folderPath, String fileName, ItineraryFile itinerary) {
        this(folderPath, fileName, itinerary.getProcessFile().getData());
        this.itineraryPart = itinerary;
        this.itineraryPart.addObservation(this);
    }

    private ObservationFile(String folderPath, String fileName, Data data) {
        this.nameProperty = new SimpleStringProperty();
        this.data = data;
        this.folderPath = folderPath;
        this.fileName = fileName;
        this.filePath = folderPath+fileName+".xlsx";
        headerPart = new ObservationMetaPart(this.data);
        headerPart.setType("observation");
        nameProperty.bind(headerPart.getCID().getValue());

        this.stepFile = null;
        this.itineraryPart = null;
    }

    public void initPublish() {
        myObs = new HashMap<>();
    }

    public StepFile getStepFile() {
        return this.stepFile;
    }

    public void setStepFile(StepFile stepFile) {
        this.stepFile = stepFile;
        this.data.setModified(true);
    }

    public ItineraryFile getItineraryFile() {
        return this.itineraryPart;
    }

    public void setItineraryFile(ItineraryFile itineraryFile) {
        this.itineraryPart = itineraryFile;
        this.data.setModified(true);
    }

    public void removeFile() {
        if(stepFile != null) { // il s'agit d'une observation à une étape
            this.stepFile.getObservationFiles().remove(this);
            this.stepFile = null;
        }
        if(itineraryPart != null) { // il s'agit d'une observation sur un itinéraire
            this.itineraryPart.getListObservation().remove(this);
            this.itineraryPart = null;
        }
        this.data.setModified(true);
    }

    public TablePart createObsData(Boolean simple) {
        TablePart p = new TablePart(data);
        p.setType(DataPartType.RAW_DATA);
        p.initTablePart(simple);

        contentList.add(p);
        if(!simple) {
            // on ajoute une colonne par défaut
            ComplexTable ct = (ComplexTable) p.getTable();
//            ct.addColumn(ComplexTable.cDefault);
        }
        this.data.setModified(true);
        return p;
    }

    private void clearObs(int replicateID) {
        unbind();
        headerPart.clear(replicateID);
        for (TablePart tp : contentList) {
            tp.unbind();
        }
        if(replicateID == -1) {
            contentList.clear();
        }
        listMaterialMethod.clear();
    }



    public Report constructDataFromImport(File fileImport, boolean isNewObs, int replicateID) {
        Report back = new Report();
        boolean construct = constructData(fileImport, null, replicateID); // null tabName -> take the first tab.
        if(!construct) {
            back.addError("An error occured during import");
        }
        if(isNewObs && !this.getUuid(false).isBlank()) { // check for no UUID
            back.addError("You should not specify an uuid when importing a new observation");
        }
        if(!isNewObs && this.getUuid(false).isBlank()) { // check for UUID
            back.addError("You are trying to import the wrong observation");
        }
        return back;
    }


    private boolean buildObs() {
        return buildObs(-1);
    }
    private Boolean buildObs(int replicateID) {
        identifyParts();

        MaterialMethodLinkPart lasMatMet = null;
        KeyWords old = null;
        TablePart oldTable = null;
        int tableNumber = -1;
        for (DataPart p : listPart) {
            if (p.getType().equals(DataPartType.GENERAL_INFO)) {
                headerPart.mapPart(p, replicateID);
            }
            if (p.getType().equals(DataPartType.RAW_DATA) || p.getType().equals(DataPartType.CALC_DATA)) {
                tableNumber++;
                if(contentList.size() > tableNumber) {
                    oldTable = contentList.get(tableNumber);
                } else {
                    oldTable = new TablePart(data);
                    contentList.add(oldTable);
                }
                oldTable.setType(p.getType());
                oldTable.mapPart(p, replicateID);
            }
            if (p.getType().equals(DataPartType.MATERIAL_USED)) {
                lasMatMet = new MaterialMethodLinkPart(data);
                lasMatMet.addFolderInfo(folderPath);
                lasMatMet.setType(DataPartType.MATERIAL_USED);
                lasMatMet.mapPart(p);
                listMaterialMethod.add(lasMatMet);
            }
            if (p.getType().equals(DataPartType.METHOD_USED)) {
                lasMatMet = new MaterialMethodLinkPart(data);
                lasMatMet.addFolderInfo(folderPath);
                lasMatMet.setType(DataPartType.METHOD_USED);
                lasMatMet.mapPart(p);
                listMaterialMethod.add(lasMatMet);
            }
            if (p.getType().equals(DataPartType.CONSIGN)) {
                if(lasMatMet != null) {
                    lasMatMet.getConsignPart().mapPart(p);
                }
            }
            if (p.getType().equals(DataPartType.UNDEFINED)) {
                if (old != null) {
                    if (old.equals(DataPartType.CALC_DATA) || old.equals(DataPartType.RAW_DATA)) {
                        oldTable.addMap(p, replicateID);
                    }
                }

            }
            old = p.getType();
        }
        setFOI();
        this.data.setModified(true);

        return true;
    }

    public Boolean constructData(File f, String tabName, int replicateID) {
        try {
            detectPart(f, tabName);
            if(listPart.size() > 0) {
                clearObs(replicateID);
                return buildObs(replicateID);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void checkValue() {
        headerPart.checkValue();
        contentList.forEach(TablePart::checkValue);
        listMaterialMethod.forEach(MaterialMethodLinkPart::checkValue);
    }

    public Boolean constructData() {
        return constructData(-1);
    }

    public Boolean constructData(int replicateID) {
        try {
            detectPart();
            return buildObs(replicateID);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public StringProperty getNameProperty() {
        return nameProperty;
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    public ComplexField getCID() {
        return headerPart.getCID();
    }

    public void setId(String id) { getCID().setValue(id);}

    public ComplexField getCName() {
        return headerPart.getCName();
    }

    public ComplexField getCType() {
        return getCName();
    }

    public void setType(String type) { getCType().setValue(type);}

    public ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> getAgentsMap() {
        return this.headerPart.getAgentsMap();
    }

    public void setAgents(String agents) {
        this.headerPart.setAgents(agents);
    }

    public String getAgents() {
        return this.headerPart.getAgents();
    }

    public String getAgent(int replicateID) {
        return this.headerPart.getAgent(replicateID);
    }

    public String getUuid(boolean force) {
        return headerPart.getUuid(force);
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {
        logger.debug("mapping ok  for " + type);
    }

    @Override
    public void unbind() {
        if(headerPart != null) {
            headerPart.unbind();
        }
        for(TablePart p : contentList) {
            p.unbind();
        }

    }

    public ObservableList<MaterialMethodLinkPart> getListMaterialMethodRO() {
        return listMaterialMethodRO;
    }

    public Boolean addMaterialMethod(MaterialMethodLinkPart materialOrMethod) {
        if(materialOrMethod.getMaterialMethodPartPart().isMaterial() && listMaterialMethod.filtered(m -> m.getMaterialMethodPartPart().isMaterial()).stream().findAny().isPresent()) {
            return false;
        } else {
            listMaterialMethod.add(materialOrMethod);
        }
        return true;
    }

    public Boolean removeMaterialMethod(MaterialMethodLinkPart materialOrMethod) {
        return listMaterialMethod.remove(materialOrMethod);
    }

    public void setMeasureLevel(String measureLevel) {
        this.measureLevel = measureLevel;
    }
    public void setFOI() {
        headerPart.setFOI(this.measureLevel, stepFile);
        this.measureLevel = "";
    }

    public void setFoi(String foi) {
        getCFoi().setValue(foi);
    }

    public ComplexField getCFoi() {
        return headerPart.getCFoi();
    }

    public ComplexField getCDate() {
        return headerPart.getCDate();
    }

    public void setDate(String date) { getCDate().setValue(date);}

    public ComplexField getCDuree() {
        return headerPart.getCDuree();
    }

    public void setDuree(String duree) { getCDuree().setValue(duree);}

    public ComplexField getCHeure() {
        return headerPart.getCHeure();
    }

    public void setHour(String hour) { getCHeure().setValue(hour);}

    public ComplexField getCDescrition() {
        return this.headerPart.getCDescription();
    }

    public String getDescription() {
        return getCDescrition().getValue().get();
    }

    public void setDescription(String description) {
        this.getCDescrition().setValue(description);
    }

    public ComplexField getCScale() {
        return headerPart.getCScale();
    }

    public void setScale(String scale) { getCScale().setValue(scale);}

    public List<TablePart> getContentList() {
        return this.contentList;
    }

    public void removeTablePart(TablePart part) {
        this.contentList.remove(part);
        data.setModified(true);
    }

    public StringProperty titleProperty() {
        return headerPart.getCName().getValue();
    }

    public LinkedList<LinkedList<String>> getDataForExcel() {
        return getDataForExcel(-1);
    }
    public LinkedList<LinkedList<String>> getDataForExcel(int replicateID) {
        LinkedList<LinkedList<String>> result = new LinkedList<>();
        LinkedList<String> header = new LinkedList<>();
        GeneralFile generalFile = stepFile != null ? stepFile.getGeneralFile() : itineraryPart.getProcessFile();
        // checking UUID
        headerPart.generateUUID();
        header.add(headerPart.getPartType().getPrefLabel("en"));
        result.add(header);
//        LinkedList<String> nameObs = new LinkedList<>();
//        nameObs.add("name");
//        nameObs.add(getComplexName().getValue().get());
        for(KeyWords k : headerPart.getContent().keySet()) {
            LinkedList<String> line = new LinkedList<>();
            line.add(k.getPrefLabel("en"));
            if(k.isUsedInReplicate()) {
                if(replicateID == -1) {
                    headerPart.getContent().get(k).cleanReplicateValues(generalFile.getListReplicate().stream().map(Replicate::getId).toList());
                    line.add(headerPart.getContent().get(k).getValuesAsString());
                } else {
                    line.add(headerPart.getContent().get(k).getValueWithReplicate(replicateID).getValue());
                }
            } else {
                line.add(headerPart.getContent().get(k).getValue().getValue());
            }
            result.add(line);
        }
        // add agent manually
        LinkedList<String> line = new LinkedList<>();
        line.add(StepMetaPart.agentK.getPrefLabel("en"));
        headerPart.cleanAgentReplicateValues(generalFile.getListReplicate().stream().map(Replicate::getId).toList());
        if(replicateID == -1) {
            line.add(headerPart.getAgents());
        } else {
            line.add(headerPart.getAgent(replicateID));
        }
        result.add(line);
//        result.add(nameObs);

        if(listMaterialMethod.size() > 0) {
            for(MaterialMethodLinkPart lpart : listMaterialMethod) {
                if(lpart.getMaterialMethodPartPart() != null) { // si null alors mat || met = not exist
                    result.add(new LinkedList<>()); // ligne vide
                    // entete
                    LinkedList<String> lineH = new LinkedList<>();
                    lineH.add(lpart.getPartType().getPrefLabel("en"));
                    result.add(lineH);
                    LinkedList<String> lineD = new LinkedList<>();
                    lineD.add(lpart.getMaterialMethodPartPart().getIDCaract().getValue().get());
                    lineD.add(lpart.getMaterialMethodPartPart().getID().getValue().get());
                    result.add(lineD);

                    LinkedList<String> lineMMF = new LinkedList<>();
                    lineMMF.add(lpart.getFileK().getPrefLabel("en"));
                    lineMMF.add("materials-methods");
                    result.add(lineMMF);

                    if (lpart.getConsignPart() != null && lpart.getConsignPart().getSize() > 0) {
                        result.add(new LinkedList<>()); // ligne vide
                        LinkedList<String> h = new LinkedList<>();
                        h.add(lpart.getConsignPart().getPartType().getPrefLabel("en"));
                        result.add(h);
                        result.addAll(lpart.getConsignPart().getDataForExcel());
                    }
                }
            }
        }

        // data
        for(TablePart dataObs : contentList) {
            dataObs.cleanReplicatesValue(generalFile.getListReplicate().stream().map(Replicate::getId).toList());
            result.add(new LinkedList<>());
            LinkedList<String> headerObs = new LinkedList<>();
            headerObs.add(dataObs.getPartType().getPrefLabel("en"));
            result.add(headerObs);
            result.addAll(dataObs.getDataForExcel(replicateID));
        }

        return result;
    }

    public void setFOI(ObservableList<MutablePair<String,MutablePair<String,SimpleBooleanProperty>>> listItem) {
        headerPart.setFOI(listItem);
    }

    public void removeFromFOI(String fileName) {
        headerPart.removeFomFOI(fileName);
    }

    public void publish(Ontology ontology, Resource interval, JSONObject json, int replicateID) {
        headerPart.generateUUID();
        Property hasFOI = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"hasFeatureOfInterest");
        Property timeContains = ontology.getCoreModel().getOntProperty(TIME.INTERVAL_CONTAINS.toString());
        Property wasassociatedwith = ontology.getCoreModel().getOntProperty(PROV.WAS_ASSOCIATED_WITH.toString());
        Property wasInfluencedBy = ontology.getCoreModel().getOntProperty(PROV.WAS_INFLUENCED_BY.toString());

        String replicateUUID = replicateID == -1 ? "" : "_" + replicateID+"_";
        if(!myObs.containsKey(replicateID)) {
            Property hasObservationReplicate = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"hasObservationReplicate");
            Individual newObs = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_"+headerPart.getUuid(true)+replicateUUID, ontology.getCoreModel().getOntClass(JenaTools.PO2CORE+"Observation"));
            myObs.forEach((k, rep) -> rep.addProperty(hasObservationReplicate, newObs));
            myObs.put(replicateID, newObs);

            myObs.get(replicateID).addProperty(SKOS.prefLabel, headerPart.getID(),"");
            myObs.get(replicateID).addProperty(SKOS.scopeNote, getDescription(), "");

            String scale = headerPart.getScale();
            if(scale != null && !scale.isEmpty()) {
                Property hasScale = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS() + "hasScale");
                OntClass ontScale = ontology.searchScale(scale);
                if(ontScale == null) {
                    json.getJSONArray("warning").put("Scale class " + scale + " not found in ontology");
                    ontScale = data.getDataModel().createClass(ontology.getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(scale));
                    ontScale.addProperty(SKOS.prefLabel, scale, "");
                    ontScale.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS() + "Scale"));
                }
                myObs.get(replicateID).addProperty(hasScale, ontScale);
            }

            // add date, time, time duration, scale, repetition
            String date = headerPart.getDate();
            String time = headerPart.getTime();
            String duration = headerPart.getDuration();

            LocalDateTime ldate = Tools.convertDate(date, time, json);
            LocalTime ldur = Tools.convertTime(duration, json);
            Duration d = Duration.between( LocalTime.of(0,0,0), ldur);
            LocalDateTime end = null;
            if(ldate != null) {
                end = ldate.plus(d);
            }
            Individual myInterval = JenaTools.addDate(ontology.getCoreModel(), data.getDataModel(), myObs.get(replicateID), ldate, end);

            String agentString = getAgent(replicateID);
            if(!agentString.isBlank()) {
                Individual agent = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_agent_"+Tools.shortUUIDFromString(agentString), FOAF.Agent);
                myObs.get(replicateID).addProperty(wasassociatedwith, agent);
            }

            // Materiel/Methode
            Property madeBySensor = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"madeBySensor");
            Property usedProcedure = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"usedProcedure");
            Property hasSubSystem = ontology.getCoreModel().getOntProperty(JenaTools.SSN+"hasSubSystem");
            OntClass classSensor = ontology.getCoreModel().getOntClass(JenaTools.SOSA+"sensor");
            ArrayList<Individual> listSensor = new ArrayList<>();
            ArrayList<Individual> listProc = new ArrayList<>();

            Integer nbMaterial = 0;
            Integer matMetCompteur = 0;
            for(MaterialMethodLinkPart mat : listMaterialMethod) {
                MaterialMethodPart matMetPart = mat.getMaterialMethodPartPart();
                if(matMetPart != null) {
                    Individual myMatMeth = matMetPart.getMatMethIndividual();

                    JSONObject actProcJSON = new JSONObject();
                    actProcJSON.put("error", new JSONArray());
                    actProcJSON.put("warning", new JSONArray());
                    actProcJSON.put("subObject", new JSONArray());
                    json.getJSONArray("subObject").put(actProcJSON);

                    // myMatMeth is the system or procedure. Now creating a sensor
                    if(matMetPart.isMaterial()) { // creating an actuator
                        actProcJSON.put("name", "Sensor " + matMetPart.getID().getValue().get());
                        actProcJSON.put("type", "Sensor");

                        Individual sensor = data.getDataModel().createIndividual(data.getDATA_NS()+"sensor_"+headerPart.getUuid(true)+"_"+matMetCompteur, classSensor);
                        myMatMeth.addProperty(hasSubSystem, sensor);

                        // add consigne on actuator
                        ConsignPart cons = mat.getConsignPart();
                        cons.publish(ontology, sensor,actProcJSON, headerPart.getUuid(true) );
                        listSensor.add(sensor);
                        nbMaterial++;
                    }
                    if(matMetPart.isMethod()) {
                        actProcJSON.put("name", "Procedure " + matMetPart.getID().getValue().get());
                        actProcJSON.put("type", "Procedure");

                        // creating an individual from an individual for procedure. Its a way to bypass the subSystem for material -> actuator
                        Individual iProcedure = data.getDataModel().createIndividual(data.getDATA_NS()+"procedure_"+headerPart.getUuid(true)+"_"+matMetCompteur, myMatMeth);
                        ConsignPart cons = mat.getConsignPart();
                        cons.publish(ontology, iProcedure,actProcJSON, headerPart.getUuid(true) );
                        listProc.add(iProcedure);
                    }
                }
                matMetCompteur++;
            }
            if(nbMaterial > 1) {
                json.getJSONArray("error").put("you have more than one material for observation " + headerPart.getCName().getValue().get());
            }

            // object observed only if stepFile != null else it's an obs on iti
            ArrayList<Individual> listFOI = new ArrayList<>();
            if(stepFile != null) {
                String foi = headerPart.getFOI();
                for (String s : foi.split("::")) {
                    s = s.trim();
                    if (s.equalsIgnoreCase("itinerary")) {
                        // n'est plus un cas qui peut se présenter normalement (pour les nouvelles données)
                    } else {
                        if (s.equalsIgnoreCase("step")) {
                            listFOI.add(stepFile.getStepIndividual(replicateID));
                        } else {
                            // il s'agit d'une composition
                            for (CompositionFile comp : stepFile.getCompositionFile().keySet()) {
                                if (comp.getFileName().equalsIgnoreCase(s) && comp.getCompoIndividual(replicateID) != null) {
                                    listFOI.add(comp.getCompoIndividual(replicateID));
                                }
                            }
                        }
                    }
                }
            }
            //        obs on itinerary
            if(itineraryPart != null) {
                listFOI.add(itineraryPart.getMyItinerary(replicateID));
            }
            if(listFOI.isEmpty()) {
                json.getJSONArray("error").put("Observation without Feature Of Interest (Object observed) is not possible");
            }
            // DATA : 1 table = 1 sosa observation collection
            Property hasObservationCollection = ontology.getCoreModel().getOntProperty(JenaTools.PO2CORE + "hasObservationCollection");
            Integer tableIndice = 0;
            for(TablePart t : contentList) {
                Boolean raw = t.getPartType().equals(DataPartType.RAW_DATA);
                Individual obsCollection = t.getTable().publish(ontology, myObs.get(replicateID), raw, tableIndice++, json, headerPart.getUuid(true)+replicateUUID, replicateID);
                myObs.get(replicateID).addProperty(hasObservationCollection, obsCollection);

                // adding actuator & procedure on the collection
                listSensor.forEach(individual -> obsCollection.addProperty(madeBySensor, individual));
                listProc.forEach(individual -> obsCollection.addProperty(usedProcedure, individual));

                // add FOI
                listFOI.forEach(individual -> obsCollection.addProperty(hasFOI, individual));

            }






            // lien etape ou iti /observation
            interval.addProperty(timeContains, myInterval);
            if(stepFile != null) {
                stepFile.getStepInterval(replicateID).addProperty(timeContains, myInterval);
                myInterval.addProperty(wasInfluencedBy, stepFile.getStepInterval(replicateID));
            }
            if(getItineraryFile() != null) {
                getItineraryFile().getMyInterval(replicateID).addProperty(timeContains, myInterval);
                myInterval.addProperty(wasInfluencedBy,getItineraryFile().getMyInterval(replicateID));
            }
        }


    }

    public void cloneFrom(ObservationFile obs, Map<CompositionFile, CompositionFile> mappingCompo) {
        // clone header
        headerPart.setDate(obs.headerPart.getDate());
        headerPart.setTime(obs.headerPart.getTime());
        headerPart.setDuration(obs.headerPart.getDuration());
        headerPart.setScale(obs.headerPart.getScale());
        headerPart.setID(obs.headerPart.getID());
        headerPart.setType(obs.headerPart.getName());
        setDescription(obs.getDescription());
        setAgents(obs.getAgents());

        // clone object observed !!!
        ArrayList<String> newFOI = new ArrayList<>();
        for(String s : obs.headerPart.getFOI().split("::")) {
            s = s.trim();
            if(!s.isEmpty()) { // sinon soit pas de FOI (si obs d'étape) ou FOI non pertinent (obs d'itinéraire)
                if (s.equalsIgnoreCase("itinerary")) {
                    newFOI.add("itinerary");
                } else {
                    if (s.equalsIgnoreCase("step")) {
                        newFOI.add("step");
                    } else {
                        // il s'agit d'une composition
                        for (CompositionFile comp : obs.getStepFile().getCompositionFile().keySet()) {
                            if (comp.getFileName().equalsIgnoreCase(s)) {
                                CompositionFile compoMapped = mappingCompo.get(comp);
                                if (compoMapped != null) {
                                    newFOI.add(compoMapped.getFileName());
                                }
                            }
                        }
                    }
                }
            }
        }
        this.headerPart.setFOI(String.join("::", newFOI));

        // clone materiel & methode
        for(MaterialMethodLinkPart matMet :  obs.getListMaterialMethodRO()) {
            try {
                MaterialMethodLinkPart newMatMet = new MaterialMethodLinkPart(matMet.getMaterialMethodPartPart());
                newMatMet.getConsignPart().cloneFrom(matMet.getConsignPart());
                this.listMaterialMethod.add(newMatMet);
            } catch (NullPointerException e) {
                logger.error("null material or method", e);
            }
        }

        // clonage data
        for(TablePart data : obs.getContentList()) {
            TablePart newPart = null;
            if(data.getTable() != null && data.getTable() instanceof SimpleTable) {
                newPart = this.createObsData(true);
            } else {
                newPart = this.createObsData(false);
            }

            newPart.cloneFrom(data);

        }
    }

    public StringProperty getIDProperty() {
        return headerPart.getIDProperty();
    }
}
