/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MaterialsMethodsFile extends GenericFile{

    private ObservableList<MaterialMethodPart> materialsPart = FXCollections.observableArrayList() ;
    private ObservableList<MaterialMethodPart> methodsPart = FXCollections.observableArrayList() ;
    private static Logger logger = LogManager.getLogger(MaterialsMethodsFile.class);
    private static List<GenericPart> listRule = Arrays.asList(new MaterialMethodPart() );

    private MaterialsMethodsFile() {};

    public MaterialsMethodsFile(String filePath, String fileName, Data data) {
        this.nameProperty = new SimpleStringProperty();
        this.data = data;
        this.filePath = filePath;
        this.fileName = fileName;
    }

    @Override
    public void checkValue() {
        materialsPart.forEach(MaterialMethodPart::checkValue);
        methodsPart.forEach(MaterialMethodPart::checkValue);
    }

    @Override
    public Boolean constructData() {
        try {
            detectPart();
            // ancienne version : on recolle si necessaire des parties
            for(int i = 0; i < listPart.size(); i++) {
                if(listPart.get(i).getNBLine() == 1 && listPart.get(i).getLine(0).size() == 1) {
                    String val = listPart.get(i).getLine(0).get(0);
                    if(StringUtils.containsIgnoreCase(val, "méthode") || StringUtils.containsIgnoreCase(val, "method") || StringUtils.containsIgnoreCase(val, "matériel") || StringUtils.containsIgnoreCase(val, "material") || StringUtils.containsIgnoreCase(val, "materiel")) {
                        // on groupe cette part avec la suivante.
                        List<String> identifiant = new LinkedList<>();
                        identifiant.add("Name");
                        identifiant.add(val);
                        listPart.get(i+1).addLine(1, identifiant);
                        listPart.get(i).concat(listPart.get(i+1));
                        listPart.remove(i+1);
                        /////////////////////////////////////////////


                        if(StringUtils.containsIgnoreCase(val, "méthode") || StringUtils.containsIgnoreCase(val, "method")) {
                            listPart.get(i).getLine(0).set(0, "méthode");
                        }
                        if(StringUtils.containsIgnoreCase(val, "matériel") || StringUtils.containsIgnoreCase(val, "material") || StringUtils.containsIgnoreCase(val, "materiel")) {
                            listPart.get(i).getLine(0).set(0, "matériel");
                        }

                    }
                }
            }

            identifyParts();

            for(DataPart p : listPart) {
                if(p.getType().equals(DataPartType.MATERIAL_RAW)) {
                    MaterialMethodPart materialContentPart = new MaterialMethodPart(data.getProjectFile(), DataPartType.MATERIAL_RAW, false);
                    materialContentPart.addOriginalFolder(FilenameUtils.getFullPath(filePath));
                    materialContentPart.mapPart(p);
                    // Checking if materiel already exist

                    MaterialMethodPart existing = data.searchMaterialIdType(materialContentPart.getOriginalID(), materialContentPart.getOntoType().getValue().get());
                    if( existing == null) {
                        data.addNewMaterialMethod(materialContentPart);
                    } else {
                        existing.addOriginalFolder(FilenameUtils.getFullPath(filePath));
                    }
                }
                if(p.getType().equals(DataPartType.METHOD_RAW)) {
                    MaterialMethodPart methodContentPart = new MaterialMethodPart(data.getProjectFile(), DataPartType.METHOD_RAW, false);
                    methodContentPart.addOriginalFolder(FilenameUtils.getFullPath(filePath));
                    methodContentPart.mapPart(p);
                    // Checking if method already exist
                    MaterialMethodPart existing = data.searchMethodIdType(methodContentPart.getOriginalID(), methodContentPart.getOntoType().getValue().get());
                    if( existing == null) {
                        data.addNewMaterialMethod(methodContentPart);
                    } else {
                        existing.addOriginalFolder(FilenameUtils.getFullPath(filePath));
                    }
                }
            }

        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    public ObservableList<MaterialMethodPart> getMaterialPart() {
        return this.materialsPart;
    }

    public ObservableList<MaterialMethodPart> getMethodPart() {
        return this.methodsPart;
    }

    public MaterialMethodPart getMateriel(String id) {
        for(MaterialMethodPart mat : materialsPart) {
            if(mat.getID().getValue().get().equalsIgnoreCase(id)) {
                return mat;
            }
        }
        return null;
    }

    public MaterialMethodPart getMethod(String id) {
        for(MaterialMethodPart met : methodsPart) {
            if(met.getID().getValue().get().equalsIgnoreCase(id)) {
                return met;
            }
        }
        return null;
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {
        logger.debug("mapping ok  for " + type);
    }



    public void unbind() {

    }

    @Override
    public void removeFile() {
        this.data.setModified(true);
    }
}
