/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class CompositionFile extends GenericFile{

    private CompositionMetaPart headerPart = null;
    private StepFile stepFile;
    private Map<Integer, Individual> myCompo = new HashMap<>();
    private static Logger logger = LogManager.getLogger(CompositionFile.class);
    private static final List<GenericPart> listRule = Arrays.asList(new CompositionMetaPart(), new MaterialMethodLinkPart(), new ConsignPart(), new TablePart() );


    private CompositionFile() {};
    public CompositionFile(StepFile step, Boolean isInput) {
        this(step.getFolderPath() + "composition-conduite" + File.separator,"composition_" + Tools.generateUniqueTimeStamp(), step, isInput);
    }
    public CompositionFile(String folderPath, String fileName, StepFile step, Boolean isInput) {
        this.nameProperty = new SimpleStringProperty();
        this.data = step.getData();
        this.stepFile = step;
        this.folderPath = folderPath;
        this.fileName = fileName;
        this.filePath = folderPath+fileName+".xlsx";
        initCompositionFile();
        this.stepFile.addCompositionFile(this, isInput);
    }


//    public void setStepFile(StepFile stepFile) {
//        this.stepFile = stepFile;
//    }

    public CompositionMetaPart getHeaderPart() {
        return headerPart;
    }

    public void initCompositionFile() {
        headerPart = new CompositionMetaPart(this.data);
        nameProperty.bind(Bindings.concat(headerPart.getCompoType().getValue(), " (", headerPart.getCompositionID().getValue(), ")"));
    }

    public Boolean buildComposition() {
        MaterialMethodLinkPart lasMatMet = null;
        for(DataPart p : listPart) {
            if(p.getType().equals(DataPartType.GENERAL_INFO)) {
                headerPart.mapPart(p);
            }
            if(p.getType().equals(DataPartType.RAW_DATA) || p.getType().equals(DataPartType.CALC_DATA)) {
                if(p.getNBLine() > 1) { // first line is for header (attri / value / object / unit/ comment )
                    // need to create new Observations
                    for(int i = 1; i< p.getNBLine(); i++) { // line 0 is header
                        // create new DataPart. Only keeping currentLine
                        DataPart newPart = new DataPart(data);
                        newPart.setType(p.getType());
                        newPart.setLineTable(p.getLineTable());
                        newPart.addLine(new ArrayList<>(p.getHeader())); // clone header
                        newPart.addLine(new ArrayList<>(p.getLine(i))); // clone ingredient description

                        ObservationFile obsFile = new ObservationFile(stepFile);
                        obsFile.setFoi(this.getFileName());
                        obsFile.setId("composition for " + this.getCompositionID().getValue().get());

                        TablePart newTable = obsFile.createObsData(true);
                        newTable.mapPart(newPart);
                        SimpleTable st = ((SimpleTable)newTable.getTable());
                        if(st.getSize() >= 0 && st.getLine(0).get(SimpleTable.ObjK) != null) {
                            String ingr = st.getLine(0).get(SimpleTable.ObjK).getValue().get();
                            obsFile.setId("composition in "+ingr+" for " + this.getCompositionID().getValue().get());
                        }
                    }
                }
            }
            if(p.getType().equals(DataPartType.MATERIAL_USED)) { ///////////////////////////////////////////:: on remonte ces infos sur l'étape
                lasMatMet = new MaterialMethodLinkPart(data);
                lasMatMet.setType(DataPartType.MATERIAL_USED);
                lasMatMet.mapPart(p);
                stepFile.addMaterialMethod(lasMatMet, true);
            }
            if(p.getType().equals(DataPartType.METHOD_USED)) { ///////////////////////////////////////////:: on remonte ces infos sur l'étape
                lasMatMet = new MaterialMethodLinkPart(data);
                lasMatMet.setType(DataPartType.METHOD_USED);
                lasMatMet.mapPart(p);
                stepFile.addMaterialMethod(lasMatMet);
            }
            if(p.getType().equals(DataPartType.CONSIGN)) {
                if(lasMatMet != null) {
                    lasMatMet.getConsignPart().mapPart(p);
                }
            }
        }
        isConstruct = true;

        return true;
    }

    @Override
    public void checkValue() {
        headerPart.checkValue();
    }

    @Override
    public Boolean constructData() {
        try {
            detectPart();
            identifyParts();
            return buildComposition();
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    public Boolean constructData(File f, String tabName) {
        try {
            detectPart(f, tabName);
            if(listPart.size() > 0) {
                identifyParts();
                unbind();
                return buildComposition();
            }
        } catch (IOException e) {
            return false;
        }
        return false;
    }



    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {
        logger.debug("mapping ok  for " + type);
    }

    @Override
    public void removeFile() {

    }

    @Deprecated
    public void addComponent(String attribut, String component, String value, String unit, String comment) {
    }

    @Deprecated
    public TablePart getContentPart() {
        return null;
    }

    public ComplexField getCompositionID() {
        if(headerPart != null)  {
            return headerPart.getCompositionID();
        }
        return null;
    }


    public ComplexField getCType() {
        return headerPart.getCType();
    }
//    public void bindType(TextField compoType) {
//        if(headerPart != null) {
//            headerPart.bindType(compoType);
//        }
//    }

    @Override
    public void unbind() {
        if(headerPart != null) {
            headerPart.unbind();
        }
    }

    public LinkedList<LinkedList<String>> getDataForExcel() {
        LinkedList<LinkedList<String>> result = new LinkedList<>();
        LinkedList<String> header = new LinkedList<>();

        headerPart.generateUUID();

        header.add(headerPart.getPartType().getPrefLabel("en"));
        result.add(header);

        for (KeyWords k : headerPart.getContent().keySet()) {
            LinkedList<String> line = new LinkedList<>();
            line.add(k.getPrefLabel("en"));
            line.add(headerPart.getContent().get(k).getValue().get());
            result.add(line);
        }

        return result;
    }

    public void setCompositionID(String text) {
        if(headerPart != null) {
            headerPart.setCompositionID(text);
        }
    }

    public void setCompositionType(String text) {
        if(headerPart != null) {
            headerPart.setCompositionType(text);
        }
    }

    public String getCompositionType() {
        if(headerPart != null) {
            return headerPart.getCompoType().getValue().getValue();
        }
        return null;
    }

    public String getUuid(boolean force) {
        return headerPart.getUuid(force);
    }



    public void initPublish() {
        myCompo = new HashMap<>();
    }

    public Individual publish(Ontology ontology, JSONObject json, int replicateID) {
        headerPart.generateUUID();
        if(!myCompo.containsKey(replicateID)) {
            String replicateUUID = replicateID == -1 ? "" : "_" + replicateID+"_";
            String compoType = headerPart.getCompoType().getValue().get();
            OntClass classCompo = ontology.searchCompo(compoType);
            if(classCompo == null) {
                if(compoType.isEmpty()) {
                    classCompo = ontology.getCoreModel().getOntClass(ontology.getCORE_NS()+"Component");
                    json.getJSONArray("warning").put("Composition without type");
                } else {
                    logger.debug("création class compo : " + compoType);
                    json.getJSONArray("warning").put("Composition class " + compoType + " not found in ontology");
                    classCompo = stepFile.getData().getDataModel().createClass(ontology.getDOMAIN_NS()+"temp_"+Tools.shortUUIDFromString(compoType));
                    classCompo.addProperty(SKOS.prefLabel, compoType, "");
                    classCompo.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS()+"Component"));
                }
            }
            myCompo.put(replicateID, stepFile.getData().getDataModel().createIndividual(stepFile.getData().getDATA_NS()+"po2_compo_"+headerPart.getUuid(false)+replicateUUID,classCompo));
            String lab = headerPart.getCompositionID().getValue().get();
            if(lab.isEmpty()) {
                lab = compoType;
            }
            myCompo.get(replicateID).addProperty(SKOS.prefLabel, lab, "");
        }

        return myCompo.get(replicateID);
    }

    public Individual getCompoIndividual(int replicateID) {
        return myCompo.get(replicateID);
    }

    public void cloneFrom(CompositionFile compo) {
        headerPart.setCompositionType(compo.getHeaderPart().getCompoType().getValue().get());
    }
}
