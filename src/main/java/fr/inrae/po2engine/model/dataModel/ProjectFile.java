/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.importExport.ExportDataNouveauFormat;
import fr.inrae.po2engine.importExport.ImportDataNouveauFormat;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.rulesys.OWLFBRuleReasoner;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.rdf4j.model.vocabulary.PROV;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


// Abstract File. Project file doesn't exist from now
public class ProjectFile extends GenericFile{



    private ComplexField projectField = new ComplexField();
    private static Logger logger = LogManager.getLogger(ProjectFile.class);
    private ProjectMetaPart headerPart;
    private ProjectContentPart contentPart;
    private ProjectAgentPart agentPart;
    private Individual myProject;
    private static List<GenericPart> listRule = Arrays.asList(new ProjectMetaPart(),  new ProjectContentPart(), new ProjectAgentPart() );



    private ProjectFile() {}

    public ProjectFile(String projectName) {
        // creation d'un nouveau projet from scratch
        this(new Data(projectName.substring(0,Math.min(100, projectName.length()))));
        data.load();
        DataTools.analyseFiles(data);
        headerPart.setProjectName(projectName.substring(0,Math.min(100, projectName.length())));
    }

    public ProjectFile(Data data) {
        this("ProjectFile",Tools.dataPath+data.getName().get() + File.separator, data);
    }


    public ProjectFile(String fileName, String folderPath, Data data ) {
        this.data = data;
        this.fileName = fileName;
        this.folderPath = folderPath;
        this.filePath = folderPath+fileName+".xlsx";
        this.data.setProjectFile(this);
        this.nameProperty = new SimpleStringProperty();
        this.nameProperty.bind(this.data.getName());
        this.headerPart = new ProjectMetaPart(this.data);
        this.contentPart = new ProjectContentPart(this.data);
        this.agentPart = new ProjectAgentPart(this.data);
    }



    public void importMaterialFromPlatform(PlatformMaterial newMat) {
        MaterialMethodPart materialMethodPart = new MaterialMethodPart(this, DataPartType.MATERIAL_RAW);
        materialMethodPart.setId(newMat.getName());
        materialMethodPart.setOntoType(newMat.getOntoType());
        for(Iterator<MaterialAttr> ite = newMat.getListCharacteristic().iterator(); ite.hasNext();) {
            MaterialAttr ma = ite.next();
            materialMethodPart.addCharacteristic(ma.getAttr(), ma.getObject(), ma.getValue(), ma.getUnit(), ma.getComment());
        }
    }


    @Override
    public void checkValue() {
        headerPart.checkValue();
        contentPart.checkValue();
    }

    @Override
    public Boolean constructData() {
        try {
            detectPart();
            identifyParts();

            for(DataPart p : listPart) {
                if(p.getType().equals(DataPartType.GENERAL_INFO)) {
                    headerPart.mapPart(p);
                }
                if(p.getType().equals(DataPartType.RAW_DATA)) {
                    contentPart.mapPart(p);
                }
                if(p.getType().equals(DataPartType.AGENT)) {
                    agentPart.mapPart(p);
                }
            }
            headerPart.remapAgent();
            isConstruct = true;
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    @Override
    public StringProperty getNameProperty() {
        return this.nameProperty;
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    public ComplexField getCName() { return this.headerPart.getCName();}

    public void setName(String projectName) {
        getCName().setValue(projectName);
    }

    public ObjectProperty<HashMap<KeyWords, ComplexField>> getContactProperty() {
        return this.headerPart.getContactProperty();
    }
    public ObjectProperty<HashMap<KeyWords, ComplexField>> getFundingProperty() {
        return this.headerPart.getFundingProperty();
    }
    public ComplexField getCDescription() { return this.headerPart.getCDescription();}

    public ComplexField getCExternalLinks() { return this.headerPart.getCExternalLinks();}

    public ArrayList<String> getExternalLinks() {
        return Arrays.stream(getCExternalLinks().getValue().get().split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toCollection(ArrayList::new));
    }

    public Data getData() {
        return data;
    }

    public void setDescription(String desc) {
        getCDescription().setValue(desc);
    }

    public void setExternalLink(String links) {
        getCExternalLinks().setValue(links);
    }

    public void addExternalLink(String newLink) {
        ArrayList<String> temp = getExternalLinks();
        temp.add(newLink);
        setExternalLink(temp.stream().collect(Collectors.joining("::")));
    }

    public void removeProcess(GeneralFile process) {
        process.removeFile();
        try {
            FileUtils.deleteDirectory(new File(process.getFolderPath()));
        } catch (IOException e) {
            logger.error("error on deleting process " + process.getCTitle(), e);
        }
    }

    public HashMap<KeyWords, ComplexField> addAgent() {
        return this.agentPart.addAgent();
    }
    public HashMap<KeyWords, ComplexField> addAgent(String org, String familyName, String givenName, String mail) {
        return this.agentPart.addAgent(org, familyName, givenName, mail);
    }

    public void removeAgent(HashMap<KeyWords, ComplexField> agent) {
        this.agentPart.removeAgent(agent);
    }
    public ObservableList<HashMap<KeyWords, ComplexField>> getListAgent() {
        return this.agentPart.getListAgent();
    }
    public HashMap<KeyWords, ComplexField> getAgent(String agent) {
        return this.agentPart.getAgent(agent);
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {

    }

    @Override
    public void unbind() {
        this.nameProperty.unbind();
        this.getCName().getValue().unbind();
        this.headerPart.unbind();
        this.getCDescription().getValue().unbind();
    }

    @Override
    public void removeFile() {
        this.data.setModified(true);
    }

    public File publish(Ontology ontology, OntModel dataModel, JSONArray listMessages, Pair<GeneralFile, ArrayList<ItineraryFile>> process) throws IOException {
        File f = null;
        Tools.updateProgress(ProgressPO2.PUBLISH, 0.0);
        headerPart.generateUUID();
        Double pp = 1.0/data.getListGeneralFile().size();
        String ontologieOutput	= "PO2_output_" + projectField.getValue().get().replaceAll(" ", "_") + ".ttl";

        ArrayList<Pair<GeneralFile, ArrayList<ItineraryFile>>> listProcessToDeal = new ArrayList<>();
        if(process == null) {
            listProcessToDeal.addAll(data.getListGeneralFile().stream().map(generalFile -> new Pair<GeneralFile, ArrayList<ItineraryFile>>(generalFile, null)).collect(Collectors.toCollection(ArrayList::new)));
        } else {
            listProcessToDeal.add(process);
        }

        myProject = dataModel.createIndividual(data.getDATA_NS()+"po2_"+headerPart.getUuid(true), DCAT.Catalog);
        myProject.addProperty(SKOS.prefLabel,getNameProperty().get(), "");
        myProject.addProperty(DCTerms.title,getNameProperty().get());
        myProject.addProperty(DCTerms.description, getCDescription().getValue().getValue());

        Property versionProp = dataModel.createProperty(DCAT.NS+"version");
        myProject.addProperty(versionProp, data.getMajorVersion().getValue()+"."+data.getMinorVersion().getValue());

        // Lien Externe DCAT
        for(String link : getExternalLinks()) {
            myProject.addProperty(DCTerms.relation, link);
        }

        // agent / organisation

        for(HashMap<KeyWords, ComplexField> agent2 : getListAgent()) {
            String agentString = Tools.agentToString(agent2);
            String orgaString = "";
            Individual agentIndividual = null;
            Individual orgaIndividual = null;
            if(agent2.get(ProjectAgentPart.agentOrganisationK).getValue().isNotEmpty().get()) {
                orgaString = agent2.get(ProjectAgentPart.agentOrganisationK).getValue().get();
                orgaIndividual = dataModel.createIndividual(data.getDATA_NS()+"po2_agent_"+Tools.shortUUIDFromString(orgaString), FOAF.Organization);
                orgaIndividual.addProperty(FOAF.name, orgaString);
            }

            if(agentString != null && !agentString.equalsIgnoreCase(orgaString)) {
                agentIndividual = dataModel.createIndividual(data.getDATA_NS()+"po2_agent_"+Tools.shortUUIDFromString(agentString), FOAF.Person);
                agentIndividual.addProperty(FOAF.mbox, agent2.get(ProjectAgentPart.agentMailK).getValue().get());
                agentIndividual.addProperty(FOAF.givenName, agent2.get(ProjectAgentPart.agentGivenNameK).getValue().get());
                agentIndividual.addProperty(FOAF.familyName, agent2.get(ProjectAgentPart.agentFamilyNameK).getValue().get());
                if(orgaIndividual != null) {
                    agentIndividual.addProperty(dataModel.createProperty(PROV.ACTED_ON_BEHALF_OF.toString()), orgaIndividual);
                }
            }
//            myProject.addProperty(dataModel.createProperty(PROV.WAS_ASSOCIATED_WITH.toString()), agentIndividual);
        }

        // Contact (nom + mail)
        Individual agent = dataModel.createIndividual(data.getDATA_NS()+"po2_agent_"+Tools.shortUUIDFromString(Tools.agentToString(getContactProperty().getValue())), FOAF.Person);
        myProject.addProperty(DCTerms.publisher, agent);

        // Funding DCAT
        String fundingString = Tools.agentToString(getFundingProperty().get());
        if(!fundingString.isBlank()) {
            Individual attribution = dataModel.createIndividual(dataModel.createResource(PROV.ATTRIBUTION.toString()));

            myProject.addProperty(dataModel.createProperty(PROV.QUALIFIED_ATTRIBUTION.toString()), attribution );

            Individual funder = dataModel.createIndividual(data.getDATA_NS()+"po2_agent_"+Tools.shortUUIDFromString(fundingString), FOAF.Organization);
            attribution.addProperty(dataModel.createProperty(PROV.AGENT_PROP.toString()), funder);
            attribution.addProperty(dataModel.createProperty(PROV.HAD_ROLE.toString()), dataModel.createResource("http://registry.it.csiro.au/def/isotc211/CI_RoleCode/funder"));
        }

        // material / method

        JSONObject jsonMatMeth = new JSONObject();
        jsonMatMeth.put("name", "Material/Method ");
        jsonMatMeth.put("type", "Material/Method");
        jsonMatMeth.put("error", new JSONArray());
        jsonMatMeth.put("warning", new JSONArray());
        jsonMatMeth.put("subObject", new JSONArray());
        listMessages.put(jsonMatMeth);

        AtomicInteger counter = new AtomicInteger();
        data.getMaterialMethodFile().getMaterialPart().forEach(materialMethodPart -> {
            System.out.println("publishing " + materialMethodPart.getID());
            JSONObject matJSON = new JSONObject();

            matJSON.put("name", "System " + materialMethodPart.getID().getValue().get());
            matJSON.put("type", "Material");

            matJSON.put("error", new JSONArray());
            matJSON.put("warning", new JSONArray());
            matJSON.put("subObject", new JSONArray());
            jsonMatMeth.getJSONArray("subObject").put(matJSON);

            materialMethodPart.publish(ontology, headerPart.getUuid(true), matJSON, counter.getAndIncrement());
        });
        AtomicInteger counter2 = new AtomicInteger();
        data.getMaterialMethodFile().getMethodPart().forEach(materialMethodPart -> {
            System.out.println("publishing " + materialMethodPart.getID());
            JSONObject methJSON = new JSONObject();

            methJSON.put("name", "Procedure " + materialMethodPart.getID().getValue().get());
            methJSON.put("type", "Method");

            methJSON.put("error", new JSONArray());
            methJSON.put("warning", new JSONArray());
            methJSON.put("subObject", new JSONArray());
            jsonMatMeth.getJSONArray("subObject").put(methJSON);

            materialMethodPart.publish(ontology, headerPart.getUuid(true), methJSON, counter2.getAndIncrement());
        });

        for(Pair<GeneralFile, ArrayList<ItineraryFile>> processToDeal : listProcessToDeal) {
            Boolean toUnload = false;
            GeneralFile generalFile = processToDeal.getKey();
            ArrayList<ItineraryFile> listIti = processToDeal.getValue();
            if(!generalFile.isLoaded()) {
                getData().analyseProcess(generalFile);
                toUnload = true;
            }
            JSONObject processJSON = new JSONObject();
            processJSON.put("name", "process " + generalFile.processNameProperty().getValue());
            processJSON.put("error", new JSONArray());
            processJSON.put("warning", new JSONArray());
            processJSON.put("subObject", new JSONArray());
            processJSON.put("type", "process");
            listMessages.put(processJSON);

            generalFile.publish(ontology, processJSON, listIti);

            // lien Projet Process DCAT dataset
            myProject.addProperty(DCAT.dataset, generalFile.getMyProcess());

            Tools.updateProgress(ProgressPO2.PUBLISH, Tools.getProgress(ProgressPO2.PUBLISH).getProgress() + pp);

            if(toUnload) {
                generalFile.unload();
            }
        }


        try {
//            if(!checkModelConsistency(dataModel).isValid()) {
//                System.out.printf("data are not valid !!!");
//            }
            f = File.createTempFile("pref","ttl");
            OutputStream output = new FileOutputStream(f);

            RDFDataMgr.write(output, data.getGraphModel(), Lang.TTL);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    public ValidityReport checkModelConsistency(Model dataModel) {
        Reasoner reasoner = ReasonerRegistry.getRDFSReasoner();
        InfModel infModel = ModelFactory.createInfModel(reasoner, dataModel);
        return infModel.validate();
    }

    public boolean saveData(boolean forPublication) throws IOException {
        Tools.addProgress(ProgressPO2.SAVE, "local save in progress");

        // savegarde du material&method
        String matmetFilePath = null;
        if(this.data.getMaterialMethodFile() != null) {
            matmetFilePath = this.folderPath+"materials-methods.xlsx";
            File f = new File(matmetFilePath);
            if(f.exists()) {
                f.delete();
            } else {
                f.getParentFile().mkdirs();
            }
            XSSFWorkbook book = new XSSFWorkbook();

            XSSFSheet s = Tools.createSheet(book,"data");

            Integer compteur = 0;
            Integer compteurMax = 0;
            for(MaterialMethodPart material : this.data.getMaterialMethodFile().getMaterialPart()) {
                XSSFRow r = s.createRow(compteur++);
                r.createCell(0).setCellValue("Material");
                for(LinkedList<String> line : material.getDataForExcel()) {
                    r = s.createRow(compteur++);
                    Integer compteurCell = 0;
                    for(String val : line ) {
                        r.createCell(compteurCell++).setCellValue(val);
                        if(compteurMax < compteurCell) {
                            compteurMax = compteurCell;
                        }
                    }
                }
                s.createRow(compteur++);
            }

            for(MaterialMethodPart method : this.data.getMaterialMethodFile().getMethodPart()) {
                XSSFRow r = s.createRow(compteur++);
                r.createCell(0).setCellValue("Method");
                for(LinkedList<String> line : method.getDataForExcel()) {
                    r = s.createRow(compteur++);
                    Integer compteurCell = 0;
                    for(String val : line ) {
                        r.createCell(compteurCell++).setCellValue(val);
                        if(compteurMax < compteurCell) {
                            compteurMax = compteurCell;
                        }
                    }
                }
                s.createRow(compteur++);
            }

            Tools.autoResize(s, compteurMax);
            FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(matmetFilePath, false);
                book.write(fout);
                fout.close();
            } catch (IOException e) {
                Tools.updateProgress(ProgressPO2.SAVE, 1.0);
                Tools.delProgress(ProgressPO2.SAVE);
                e.printStackTrace();
                return false;
            }

        }

        Integer maxColumn = 0;
        File f = new File(filePath);
        if(f.exists()) {
            f.delete();
        } else {
            f.getParentFile().mkdirs();
        }
//        this.listUpdateFile.add(filePath); seulement a partir des process ???????????

        XSSFWorkbook book = new XSSFWorkbook();


        XSSFSheet s = Tools.createSheet(book,"data");
        Integer compteur = 0;
        // header
        // checking uuid is set.
        headerPart.generateUUID();
        XSSFRow rowh = s.createRow(compteur++);
        XSSFCell cell0h = rowh.createCell(0);
        cell0h.setCellValue("Project");
        // increment minor version on save
        if(forPublication) {
            // changement du numero de version majeur
            data.incMajorVersion();
            data.setMinorVersion("0");
        } else {
            // changement du numero de version mineur
            data.incMinorVersion();
        }
        for(Map.Entry<KeyWords, ComplexField> entry : headerPart.getContent().entrySet()) {
            XSSFRow row = s.createRow(compteur++);
            XSSFCell cell0 = row.createCell(0);
            cell0.setCellValue(entry.getKey().getPrefLabel("en"));
            XSSFCell cell1 = row.createCell(1);
            if(entry.getKey().equals(ProjectMetaPart.versionK)) {
                cell1.setCellValue(data.getMajorVersion().get()+"."+ data.getMinorVersion().get());
            } else {
                cell1.setCellValue(entry.getValue().getValue().get());
            }
        }
        // add contact manually
        XSSFRow contactRow = s.createRow(compteur++);
        XSSFCell cell0Contact = contactRow.createCell(0);
        cell0Contact.setCellValue(ProjectMetaPart.contactK.getPrefLabel("en"));
        XSSFCell cell1Contact = contactRow.createCell(1);
        cell1Contact.setCellValue(headerPart.getContact());

        // add funding manually
        XSSFRow fundingRow = s.createRow(compteur++);
        XSSFCell cell0Funding = fundingRow.createCell(0);
        cell0Funding.setCellValue(ProjectMetaPart.fundingK.getPrefLabel("en"));
        XSSFCell cell1Funding = fundingRow.createCell(1);
        cell1Funding.setCellValue(headerPart.getFunding());

        maxColumn = 1;
        // ajout d'une ligne vide
        s.createRow(compteur++);

        Tools.autoResize(s, maxColumn);

        XSSFRow header = s.createRow(compteur++);
        Integer colCompteur = 0;
        for(Map.Entry<KeyWords, List<ComplexField>> entry : contentPart.getContent().entrySet()) {
            header.createCell(colCompteur++).setCellValue(entry.getKey().getPrefLabel("en"));
        }
        for(GeneralFile processFile : contentPart.getListProcess()) {
            XSSFRow row = s.createRow(compteur++);
            XSSFCell cell0 = row.createCell(0);
            cell0.setCellValue(processFile.getOntoType());
            XSSFCell cell1 = row.createCell(1);
            cell1.setCellValue(processFile.getFileName());
            try {
                if(processFile.isLoaded()) {
                    processFile.saveData(matmetFilePath);
                }
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            }
        }
        Tools.autoResize(s, maxColumn);

        XSSFSheet sheetAgent = book.createSheet("agent");
        compteur = 0;

        // header
        header = sheetAgent.createRow(compteur++);
        colCompteur = 0;
        for(KeyWords k : agentPart.getListKeywords()) {
            header.createCell(colCompteur++).setCellValue(k.getPrefLabel("en"));
        }
        for(HashMap<KeyWords, ComplexField> agent : agentPart.getListAgent()) {
            XSSFRow row = sheetAgent.createRow(compteur++);
            colCompteur = 0;
            for(KeyWords k : agentPart.getListKeywords()) {
                XSSFCell cell = row.createCell(colCompteur++);
                cell.setCellValue(agent.get(k).getValue().getValue());
            }
        }



        FileOutputStream fout = new FileOutputStream(filePath, false);
        book.write(fout);
        fout.close();

        for(GeneralFile generalFile : this.data.getListGeneralFile()) {
            if(generalFile.isLoaded()) {
                try {
                    generalFile.saveData(matmetFilePath);
                } catch (IOException | InvalidFormatException e) {
                    Tools.updateProgress(ProgressPO2.SAVE, 1.0);
                    Tools.delProgress(ProgressPO2.SAVE);
                    e.printStackTrace();
                    return false;
                }
            }
        }

        this.data.refreshListAllFile();

        Tools.updateProgress(ProgressPO2.SAVE, 1.0);
        Tools.delProgress(ProgressPO2.SAVE);
        return true;
    }

    /**
     * Export full project to xlsx file
     * @return return xlsx file containing project data or null on error
     */
    public File exportProject() {
        ExportDataNouveauFormat exporte = new ExportDataNouveauFormat();
        return exporte.lire(this);
    }

    public Report importProject(File importFile) throws Exception {
        Tools.addProgress(ProgressPO2.IMPORT, "Importing project");
        // on vide le projet existant
        Tools.updateProgress(ProgressPO2.IMPORT, "Cleaning existing informations");
        this.getData().getListGeneralFile().forEach(GeneralFile::unload);
        new ArrayList<>(this.getData().getListGeneralFile()).forEach(this::removeProcess);
        // on supprime le mat & meth
        this.getData().getMaterialMethodFile().getMaterialPart().clear();
        this.getData().getMaterialMethodFile().getMethodPart().clear();

        Tools.updateProgress(ProgressPO2.IMPORT, "Importing project");
        ImportDataNouveauFormat importe = new ImportDataNouveauFormat();
        Report report = importe.convertirFichier(this, importFile);
        Tools.delProgress(ProgressPO2.IMPORT);
        return report;
    }
}
