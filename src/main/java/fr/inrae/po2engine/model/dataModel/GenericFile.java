/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.partModel.DataPart;
import fr.inrae.po2engine.model.partModel.GenericPart;
import fr.inrae.po2engine.utils.DataPartType;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableObjectValue;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class GenericFile {

    protected String filePath;
    protected SimpleStringProperty nameProperty;
    protected String fileName;
    protected List<DataPart> listPart = new LinkedList<>();
    protected Boolean isConstruct = false;
    private Boolean selectedInTree = false;
    protected String folderPath  = null;
    protected String oldFolderPath = null;
    protected Data data = null;
    private static Logger logger = LogManager.getLogger(GenericFile.class);
    public abstract void checkValue();

    public Boolean getIsConstruct() {
        return isConstruct;
    }

    public abstract Boolean constructData() throws IOException;

    public StringProperty getNameProperty() {
        return nameProperty;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void addError(String message) {
        addError(message, null, null);
    }

    public void addError(String message, Integer line, Integer column) {

    }

    abstract protected List<GenericPart> getListRules();

    public String getFolderPath() {
        return this.folderPath;
    }

    public void changeFolderPath(String newFolderPath) {
        try {
            this.oldFolderPath = this.folderPath;
            FileUtils.moveFileToDirectory(new File(filePath), new File(newFolderPath), true);
            this.folderPath = newFolderPath;
            this.filePath = newFolderPath+File.separator+fileName;
            logger.debug("new FilePath" + this.filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void detectPart(File f, String tabName) throws IOException {
        listPart.clear();
        FileInputStream fis = new FileInputStream(f);
        XSSFWorkbook book = new XSSFWorkbook(fis);
        XSSFFormulaEvaluator evaluator = new XSSFFormulaEvaluator(book);
        for (Iterator<Sheet> iteSheet = book.iterator(); iteSheet.hasNext(); ) {
            XSSFSheet s = (XSSFSheet) iteSheet.next();
            if((tabName != null && s.getSheetName().equalsIgnoreCase(tabName)) || tabName == null) {
                detectPart(s, evaluator);
            }
        }
        fis.close();
    }



    protected void detectPart() throws IOException {
        detectPart(new File(filePath), null);
    }
//        listPart.clear();
//        FileInputStream fis = new FileInputStream(filePath);
//        XSSFWorkbook book = new XSSFWorkbook(fis);
//        evaluator = new XSSFFormulaEvaluator(book);
//        for (Iterator<Sheet> iteSheet = book.iterator(); iteSheet.hasNext(); ) {
//            XSSFSheet s = (XSSFSheet) iteSheet.next();
//            detectPart(s);
//        }
//        fis.close();
//    }

    public Boolean isSelectedInTree() {
        return selectedInTree;
    }
    public void setSelectedInTree(Boolean selected) {
        this.selectedInTree = selected;
    }

    protected void detectPart(XSSFSheet mySheet, XSSFFormulaEvaluator evaluator) throws IOException {

        DataPart table = new DataPart(this.data);
        Integer rowNum = mySheet.getLastRowNum();
        for(int currentRow = 0; currentRow <= rowNum; currentRow++) {
//        for (Iterator<Row> rowIte = mySheet.rowIterator(); rowIte.hasNext(); ) {
//            XSSFRow rowi = (XSSFRow) rowIte.next();
            XSSFRow rowi = mySheet.getRow(currentRow);
            if (rowi != null) {
                List<String> line = new LinkedList<>();
                Boolean isEmptyLine = true;
                for (int j = 0; j < rowi.getLastCellNum(); j++ ) {
                    XSSFCell c = rowi.getCell(j);

                    if(c == null) {
                        line.add("");
                    } else {
                        CellType type = c.getCellType();
                        if(type.equals(CellType.FORMULA)) {
                            try {
                                c = evaluator.evaluateInCell(c);
                                type = c.getCellType();
                            } catch (Exception ex) {

                            }
                        }

                        if (type.equals(CellType.BLANK) || (type.equals(CellType.STRING) && c.getStringCellValue().trim().isEmpty())) {
                            try {
                                line.add(c.getColumnIndex(), c.getStringCellValue().trim());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            isEmptyLine = false;
                            if (type.equals(CellType.STRING)) {
                                try {
                                    line.add(c.getColumnIndex(), c.getStringCellValue().trim());
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                if (type.equals(CellType.NUMERIC)) {
                                    try {
                                        if(DateUtil.isCellDateFormatted(c)) {
                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                            line.add(c.getColumnIndex(), String.valueOf(dateFormat.format(c.getDateCellValue())).trim());
                                        } else {
                                            line.add(c.getColumnIndex(), String.valueOf(c.getNumericCellValue()).trim());
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    if (type.equals(CellType.FORMULA)) {
                                        line.add(c.getColumnIndex(), "unable to evaluate formula");
                                        logger.debug("formula !!! " + c);
//                                        try {
//                                            c = evaluator.evaluateInCell(c);
//                                            if(v.getCellTypeEnum().equals(CellType.STRING)) {
//                                                line.add(c.getColumnIndex(), v.getStringValue().trim());
//                                            }
//                                            if(v.getCellTypeEnum().equals(CellType.NUMERIC)) {
//                                                line.add(c.getColumnIndex(), String.valueOf(v.getNumberValue()).trim());
//                                            }
//                                        } catch (Exception ex) {
//                                            line.add(c.getColumnIndex(), "unable to evaluate formula");
////                                            ex.printStackTrace();
//                                        }
                                    } else {
                                        if(type.equals(CellType.ERROR)) {
                                            line.add(c.getColumnIndex(), c.getErrorCellString().trim());
                                        } else {
                                            line.add(c.getColumnIndex(), "");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (isEmptyLine) {
                    // on oublie la ligne qu'on vient de faire.
                    if (!table.isEmpty()) {
                        table.removeEmptyCol();
                        listPart.add(table);
                    }
                    table = new DataPart(this.data);
                } else {
                    //line is complete on l'ajoute au tableau.
                    table.addLine(line);
                }

            } else {
                // ligne == null -> empty line
                if (!table.isEmpty()) {
                    table.removeEmptyCol();
                    listPart.add(table);
                }
                table = new DataPart(this.data);
            }
        }
        if (!table.isEmpty()) {
            table.removeEmptyCol();
            listPart.add(table);
        }

        if (listPart.isEmpty()) {
            addError("No data found in file");
        }

    }

    protected void identifyParts() {
        List<GenericPart> listRule = getListRules();
        for(DataPart part : listPart) {
            boolean ident = !part.getType().equals(DataPartType.INIT);
            Iterator<GenericPart> ite = listRule.iterator();
            while(!ident && ite.hasNext()) {
                GenericPart rule = ite.next();
                if(rule.checkPart(part)) {
                    ident = true;
                    part.setType(rule.getPartType());
                }
            }
            if(!ident) {
                part.setType(DataPartType.UNDEFINED);
                logger.debug("Part not identified");
            }
        }
    }



    public abstract void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data);

    public abstract void unbind();

    public abstract void removeFile();

//    public void setDataNode(DataNode dataNode) {
//        this.dataNode = dataNode;
//    }

//    public DataNode getDataNode() {
//        return this.dataNode;
//    }

    public Data getData() {
        return this.data;
    }

    @Override
    public String toString() {
        return getNameProperty().get();
    }
}
