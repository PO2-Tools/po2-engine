/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ConduiteFile extends GenericFile{

    private ConduiteMetaPart headerPart = null;
    private TablePart contentPart = null;
    private ObservableList<MaterialMethodLinkPart> listMaterialMethod = FXCollections.observableArrayList();
    private static Logger logger = LogManager.getLogger(ConduiteFile.class);
    private static final List<GenericPart> listRule = Arrays.asList(new ConduiteMetaPart(), new MaterialMethodLinkPart(), new ConsignPart(), new TablePart() );


    private ConduiteFile() {};
    public ConduiteFile(CompositionFile compo) {
        this(compo.getFolderPath(), "conduite_" + Tools.generateUniqueTimeStamp(), compo);
    }
    public ConduiteFile(String folderPath, String fileName, CompositionFile compo) {
        this.nameProperty = new SimpleStringProperty();
        this.data = compo.getData();
        this.folderPath = folderPath;
        this.fileName = fileName;
        this.filePath = folderPath+fileName+".xlsx";
        initConduitePart();
    }

    public void initConduitePart() {
        headerPart = new ConduiteMetaPart(this.data);
        contentPart = new TablePart(this.data);
        contentPart.setType(DataPartType.RAW_DATA);
        contentPart.initTablePart(true);
        nameProperty.setValue("to be defined");
    }

    @Override
    public void checkValue() {
        headerPart.checkValue();
        contentPart.checkValue();
    }

    @Override
    public Boolean constructData() {
        try {
            initConduitePart();
            detectPart();
            identifyParts();

//            Boolean lastIsMaterial = null;
            MaterialMethodLinkPart lasMatMet = null;
            for(DataPart p : listPart) {
                if(p.getType().equals(DataPartType.GENERAL_INFO)) {
                    headerPart = new ConduiteMetaPart(this.data);
                    headerPart.mapPart(p);
                }
                if(p.getType().equals(DataPartType.RAW_DATA) || p.getType().equals(DataPartType.CALC_DATA)) {
                    contentPart.setType(p.getType());
                    contentPart = new TablePart(data);
                    contentPart.mapPart(p);
                }
                if(p.getType().equals(DataPartType.MATERIAL_USED)) {
                    lasMatMet = new MaterialMethodLinkPart(data);
                    lasMatMet.setType(DataPartType.MATERIAL_USED);
                    lasMatMet.mapPart(p);
                    listMaterialMethod.add(lasMatMet);
                }
                if(p.getType().equals(DataPartType.METHOD_USED)) {
                    lasMatMet = new MaterialMethodLinkPart(data);
                    lasMatMet.setType(DataPartType.METHOD_USED);
                    lasMatMet.mapPart(p);
                    listMaterialMethod.add(lasMatMet);
                }
                if(p.getType().equals(DataPartType.CONSIGN)) {
                    if(lasMatMet != null) {
                        lasMatMet.getConsignPart().mapPart(p);
                    }
                }
            }

        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {
        logger.debug("mapping ok  for " + type);
    }

    @Override
    public void unbind() {
        if(headerPart != null) {
            headerPart.unbind();
        }
        if(contentPart != null) {
            contentPart.unbind();
        }
    }

    @Override
    public void removeFile() {
        this.data.setModified(true);
    }

    public TablePart getContentPart() {
        return contentPart;
    }

//    public void bindData(TableView compoTable) {
//        if(contentPart != null) {
//            UITools.bindTable(contentPart.getTable(), compoTable);
//        }
//    }

    public ObservableList<MaterialMethodLinkPart> getListMaterialMethod() {
        return listMaterialMethod;
    }

    public LinkedList<LinkedList<String>> getDataForExcel() {
        LinkedList<LinkedList<String>> result = new LinkedList<>();
        LinkedList<String> header = new LinkedList<>();
        header.add(headerPart.getPartType().getPrefLabel("en"));
        result.add(header);
        headerPart.generateUUID();
        for(KeyWords k : headerPart.getContent().keySet()) {
            LinkedList<String> line = new LinkedList<>();
            line.add(k.getPrefLabel("en"));
            line.add(headerPart.getContent().get(k).getValue().get());
            result.add(line);
        }

        if(listMaterialMethod.size() > 0) {
            for(MaterialMethodLinkPart pair : listMaterialMethod) {
                result.add(new LinkedList<>()); // ligne vide
                // entete
                LinkedList<String> lineH = new LinkedList<>();
                lineH.add(pair.getPartType().getPrefLabel("en"));
                result.add(lineH);
                LinkedList<String> lineD = new LinkedList<>();
                lineD.add(pair.getMaterialMethodPartPart().getIDCaract().getValue().get());
                lineD.add(pair.getMaterialMethodPartPart().getID().getValue().get());
                result.add(lineD);

                LinkedList<String> lineMMF = new LinkedList<>();
                lineMMF.add(pair.getFileK().getPrefLabel("en"));
                lineMMF.add("materials-methods");
                result.add(lineMMF);

                if(pair.getConsignPart() != null && pair.getConsignPart().getSize() > 0) {
                    result.add(new LinkedList<>()); // ligne vide
                    LinkedList<String> h = new LinkedList<>();
                    h.add(pair.getConsignPart().getPartType().getPrefLabel("en"));
                    result.add(h);
                    result.addAll(pair.getConsignPart().getDataForExcel());
                }
            }
        }

        // data
        result.add(new LinkedList<>());
        LinkedList<String> headerData = new LinkedList<>();
        headerData.add(DataPartType.RAW_DATA.getPrefLabel("en"));
        result.add(headerData);
        result.addAll(contentPart.getDataForExcel());
        return result;
    }

//    public ObservableList<Pair<MaterialMethodLinkPart, ConsignPart>> getListMethod() {
//        return listMethod;
//    }

//    public ObservableList getMaterialMethodPart() {
//        ObservableList<MaterialMethodPart> list = FXCollections.observableArrayList();
//        for(MaterialLinkPart p : listMaterial.keySet()) {
//            list.add(p.getMaterialPart());
//        }
//        for(MethodLinkPart p : listMethod.keySet()) {
//            list.add(p.getMethodPart());
//        }
//        return list;
//    }

//    public Map<MaterialMethodPart,ConsignPart> getListConsign() {
//        Map<MaterialMethodPart, ConsignPart> list = new HashMap<>();
//        for(Map.Entry<MaterialLinkPart, ConsignPart> p : listMaterial.entrySet()) {
//            list.put(p.getKey().getMaterialPart(), p.getValue());
//        }
//        for(Map.Entry<MethodLinkPart, ConsignPart> p : listMethod.entrySet()) {
//            list.put(p.getKey().getMethodPart(), p.getValue());
//        }
//        return list;
//    }
}
