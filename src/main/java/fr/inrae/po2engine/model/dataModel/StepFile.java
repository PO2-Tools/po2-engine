/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.SKOS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.rdf4j.model.vocabulary.PROV;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class StepFile extends GenericFile{

    private StepMetaPart headerPart = null;
    private ObservableList<MaterialMethodLinkPart> listMaterialMethod = FXCollections.observableArrayList();
    private ObservableList<MaterialMethodLinkPart> listMaterialMethodRO = FXCollections.unmodifiableObservableList(listMaterialMethod);
    private static Logger logger = LogManager.getLogger(StepFile.class);
    private List<ObservationFile> listObservation;
    private GeneralFile generalFile;
    private Map<Integer, Individual> myStep = new HashMap<>();
    private Map<Integer, Individual> myInterval = new HashMap<>();
    private HashMap<CompositionFile, Boolean> listCompo = new HashMap<>();
    private StepFile father = null;
    private Set<StepFile> subStep;
    private static final List<GenericPart> listRule = Arrays.asList(new StepMetaPart(), new MaterialMethodLinkPart(), new ConsignPart(), new CompositionImportPart() );



    private StepFile() {};
    public StepFile(String type, GeneralFile generalFile) {
        this(new ComplexField(type), generalFile);
    }
    public StepFile(ComplexField type, GeneralFile generalFile) {
        this(generalFile.getFolderPath() + Tools.normalize(type.getValue().get()) + File.separator, "step-data_" + Tools.generateUniqueTimeStamp(), generalFile.getData(), generalFile);
        headerPart.getCType().setValue(type.getValue().getValue());
        headerPart.getCId().setValue(type.getID().getValue());
        headerPart.getCUuid().setValue(type.getUuidProperty().getValue());
    }
    public StepFile(String folderPath, String fileName, Data data, GeneralFile generalFile) {
        this.nameProperty = new SimpleStringProperty();
        this.generalFile = generalFile;
        this.data = data;
        this.folderPath = folderPath;
        this.fileName = fileName;
        this.filePath = folderPath + fileName + ".xlsx";
        this.listObservation = new LinkedList<>();
        this.subStep = new LinkedHashSet<>();

        this.generalFile.getListStep().add(this);
        initStepFile();
    }

    public void addCompositionFile(CompositionFile compositionFile, Boolean isInput) {
        this.listCompo.put(compositionFile, isInput);
        this.data.setModified(true);
    }

    public void removeCompositionFile(CompositionFile compositionFile) {
        if(this.listCompo.containsKey(compositionFile)) {
            this.listCompo.remove(compositionFile);
            for(ObservationFile obsFile : this.getObservationFiles()) {
                obsFile.removeFromFOI(compositionFile.getFileName());
            }
            this.data.setModified(true);
        }
    }

    public ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> getAgentsMap() {
        return this.headerPart.getAgentsMap();
    }

    public void setAgents(String agents) {
        this.headerPart.setAgents(agents);
    }

    public String getAgents() {
        return this.headerPart.getAgents();
    }

    public String getAgent(int replicateID) {
        return this.headerPart.getAgent(replicateID);
    }

    public void setFile(String folderPath, String fileName) {
        this.folderPath = folderPath;
        this.fileName = fileName;
        this.filePath = folderPath + fileName + ".xlsx";
    }

    private void setFather(StepFile step) {
        this.father = step;
    }

    public GeneralFile getGeneralFile() {
        return this.generalFile;
    }

    public boolean isRoot() {
        return father == null;
    }

    public StepFile getFather() {
        return this.father;
    }

    public HashMap<CompositionFile, Boolean> getCompositionFile() {
        return listCompo;
    }

//    public void setStepFile(StepFileTemp s) {
//        this.stepFile = s;
//    }
    public void removeFather() {
        if(this.getFather() != null) {
            this.getFather().getSubStep().remove(this);
            this.setFather(null);
            this.data.setModified(true);
        }
    }

    public void addSubStep(StepFile step) {
        if(this.getFather() != null) return; // the current step is already a subStep !
        if(step != null && step != this) {
            this.subStep.add(step);
            step.setFather(this);
            this.data.setModified(true);
        }
    }

    public String getUuid(boolean force) {
        return headerPart.getUuid(force);
    }

    public Set<StepFile> getSubStep() {
        return subStep;
    }

    public boolean isLeaf() {
        return subStep.isEmpty();
    }

    public StepMetaPart getHeaderPart() {
        return headerPart;
    }

    public void initStepFile() {
        if(headerPart == null) { // not null if old Constructor
            headerPart = new StepMetaPart(this.data);
        }
        this.nameProperty.bind(Bindings.concat(headerPart.getCType().getValue(), " (", headerPart.getCId().getValue(), ")"));

    }

    public boolean buildStep() {
        return buildStep(-1);
    }
    public Boolean buildStep(int replicateID) {


//            Boolean lastIsMaterial = null;
        MaterialMethodLinkPart lasMatMet = null;
        CompositionFile lastCompo = null;
        for(DataPart p : listPart) {
            if(p.getType().equals(DataPartType.GENERAL_INFO)) {
                headerPart.mapPart(p, replicateID);
            }
            if(p.getType().equals(DataPartType.MATERIAL_USED)) { ///////////////////////////////////////////:: on remonte ces infos sur l'étape
                lasMatMet = new MaterialMethodLinkPart(data);
                lasMatMet.setType(DataPartType.MATERIAL_USED);
                lasMatMet.mapPart(p);
                listMaterialMethod.add(lasMatMet);
            }
            if(p.getType().equals(DataPartType.METHOD_USED)) { ///////////////////////////////////////////:: on remonte ces infos sur l'étape
                lasMatMet = new MaterialMethodLinkPart(data);
                lasMatMet.setType(DataPartType.METHOD_USED);
                lasMatMet.mapPart(p);
                listMaterialMethod.add(lasMatMet);
            }
            if(p.getType().equals(DataPartType.CONSIGN)) {
                if(lasMatMet != null) {
                    lasMatMet.getConsignPart().mapPart(p);
                }
            }
        }
        isConstruct = true;
        return true;
    }

    @Override
    public void checkValue() {
        headerPart.checkValue();
        listMaterialMethod.forEach(MaterialMethodLinkPart::checkValue);
        listCompo.keySet().forEach(CompositionFile::checkValue);
    }

    @Override
    public Boolean constructData() {
        try {
            initStepFile();
            if(Files.exists(Paths.get(filePath))) {
                detectPart();
                identifyParts();
            }
            return buildStep();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {
        logger.debug("mapping ok  for " + type);
    }

    @Override
    public void unbind() {
        if(headerPart != null) {
            headerPart.unbind();
        }
    }

    @Override
    public void removeFile() {
        for (StepFile son : getSubStep()) {
            son.simpleRemoveFile();
        }
        this.simpleRemoveFile();

        if (this.father != null) {
            this.father.getSubStep().remove(this);
//            for(DataNode ns : this.father.getListNode()) {
//                ns.removeSubNode(this.listDataNode);
//            }
        }
        this.generalFile.getItinerary().forEach(ItineraryFile::removeEmptyLink);
        this.data.setModified(true);
    }

    public void addObservationFile(ObservationFile observationFile) {
        this.listObservation.add(observationFile);
        this.data.setModified(true);
    }

    public List<ObservationFile> getObservationFiles() {
        return listObservation;
    }

    public LinkedList<LinkedList<String>> getDataForExcel(Boolean withHeader) {
        return getDataForExcel(withHeader, -1);
    }
    public LinkedList<LinkedList<String>> getDataForExcel(Boolean withHeader, int replicateID) {
        LinkedList<LinkedList<String>> result = new LinkedList<>();

        // checking uuid
        headerPart.generateUUID();

        if(withHeader) {
            LinkedList<String> header = new LinkedList<>();
            header.add(headerPart.getPartType().getPrefLabel("en"));
            result.add(header);

            for (KeyWords k : headerPart.getContent().keySet()) {
                LinkedList<String> line = new LinkedList<>();
                line.add(k.getPrefLabel("en"));
                if(k.isUsedInReplicate()) {
                    if(replicateID == -1) {
                        headerPart.getContent().get(k).cleanReplicateValues(generalFile.getListReplicate().stream().map(Replicate::getId).toList());
                        line.add(headerPart.getContent().get(k).getValuesAsString());
                    } else {
                        line.add(headerPart.getContent().get(k).getValueWithReplicate(replicateID).getValue());
                    }
                } else {
                    line.add(headerPart.getContent().get(k).getValue().getValue());
                }
                result.add(line);
            }
            // add agent manually
            LinkedList<String> line = new LinkedList<>();
            line.add(StepMetaPart.agentK.getPrefLabel("en"));
            headerPart.cleanAgentReplicateValues(generalFile.getListReplicate().stream().map(Replicate::getId).toList());
            if(replicateID == -1) {
                line.add(headerPart.getAgents());
            } else {
                line.add(headerPart.getAgent(replicateID));
            }

            result.add(line);
            result.add(new LinkedList<>()); // ligne vide
        }

        if(listMaterialMethod.size() > 0) {
            for(MaterialMethodLinkPart lpart : listMaterialMethod) {
                if(lpart.getMaterialMethodPartPart() != null) { // si null alors mat || met = not exist

                    // entete
                    LinkedList<String> lineH = new LinkedList<>();
                    lineH.add(lpart.getPartType().getPrefLabel("en"));
                    result.add(lineH);
                    LinkedList<String> lineD = new LinkedList<>();
                    lineD.add(lpart.getMaterialMethodPartPart().getIDCaract().getValue().get());
                    lineD.add(lpart.getMaterialMethodPartPart().getID().getValue().get());
                    result.add(lineD);

                    if (lpart.getConsignPart() != null && lpart.getConsignPart().getSize() > 0) {
                        result.add(new LinkedList<>()); // ligne vide
                        LinkedList<String> h = new LinkedList<>();
                        h.add(lpart.getConsignPart().getPartType().getPrefLabel("en"));
                        result.add(h);
                        result.addAll(lpart.getConsignPart().getDataForExcel());
                    }
                    result.add(new LinkedList<>()); // ligne vide
                }
            }
        }

        return result;
    }


    public ObservableList<MaterialMethodLinkPart> getListMaterialMethodRO() {
        return listMaterialMethod;
    }

    public Boolean addMaterialMethod(MaterialMethodLinkPart materialOrMethod) {
        if(materialOrMethod.getMaterialMethodPartPart().isMaterial() && listMaterialMethod.filtered(m -> m.getMaterialMethodPartPart().isMaterial()).stream().findAny().isPresent()) {
            return false;
        } else {
            listMaterialMethod.add(materialOrMethod);
        }
        return true;
    }

    public Boolean addMaterialMethod(MaterialMethodLinkPart materialOrMethod, Boolean forceAdd) {
        if(forceAdd) {
            listMaterialMethod.add(materialOrMethod);
        } else {
            return addMaterialMethod(materialOrMethod);
        }
        return true;
    }

    public Boolean removeMaterialMethod(MaterialMethodLinkPart materialOrMethod) {
        return listMaterialMethod.remove(materialOrMethod);
    }

    public void setOntoType(String type) {
        this.headerPart.getCType().setValue(type);
    }

    public ComplexField getCType() {
        return this.headerPart.getCType();
    }

    public String getOntoType() {
        return getCType().getValue().get();
    }


    public ComplexField getCId() {
        return this.headerPart.getCId();
    }

    public String getId() {
        return getCId().getValue().getValue();
    }

    public void setId(String id) {
        this.getCId().setValue(id);
    }

    public ComplexField getCDescrition() {
        return this.headerPart.getCDescription();
    }

    public String getDescription() {
        return getCDescrition().getValue().get();
    }

    public void setDescription(String description) {
        this.getCDescrition().setValue(description);
    }

    public ComplexField getCDate() {
        return this.headerPart.getCDate();
    }

    public String getDate() {
        return getCDate().getValue().get();
    }

    public void setDate(String date) {
        this.getCDate().setValue(date);
    }

    public ComplexField getCDuration() {
        return this.headerPart.getCDuration();
    }

    public String getDuration() {
        return getCDuration().getValue().get();
    }

    public void setDuration(String duree) {
        this.getCDuration().setValue(duree);
    }

    public ComplexField getCTime() {
        return this.headerPart.getCTime();
    }

    public String getTime() {
        return getCTime().getValue().get();
    }

    public void setTime(String heure) {
        this.getCTime().setValue(heure);
    }

    public ComplexField getCScale() {
        return headerPart.getCScale();
    }
    public String getScale() {
        return getCScale().getValue().get();
    }
    public void setScale(String scale) { getCScale().setValue(scale);}

    public Individual getStepIndividual(int replicateID) {
        if(this.myStep.containsKey(replicateID)) return this.myStep.get(replicateID);
        return null;
    }
    public Individual getStepInterval(int replicateID) {
        if(this.myInterval.containsKey(replicateID)) return this.myInterval.get(replicateID);
        return null;
    }

    public Individual publish(Ontology ontology, JSONObject json, int replicateID) {
        logger.info("publishing step " + getOntoType());
        headerPart.generateUUID();
        if(!myStep.containsKey(replicateID)) {
            Property phenomenonTime = ontology.getCoreModel().getOntProperty(JenaTools.SOSA + "phenomenonTime");
            Property subStepProperty = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS() + "hasSubStep");
            Property timeContains = ontology.getCoreModel().getOntProperty(JenaTools.TIME+ "intervalContains");
            Property wasassociatedwith = ontology.getCoreModel().getOntProperty(PROV.WAS_ASSOCIATED_WITH.toString());

            OntClass classStep = ontology.searchStep(getOntoType());
            if (classStep == null) {
                if(getOntoType().isEmpty()) {
                    classStep = ontology.getCoreModel().getOntClass(ontology.getCORE_NS() + "Step");
                    json.getJSONArray("error").put("step without type ");
                } else {
                    logger.debug("création class step : " + getOntoType());
                    json.getJSONArray("warning").put("Step class " + getOntoType() + " not found in ontology");
                    classStep = data.getDataModel().createClass(ontology.getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(getOntoType()));
                    classStep.addProperty(SKOS.prefLabel, getOntoType(), "");
                    classStep.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS() + "Step"));
                }
            } else {
                logger.debug("founded class step : " + getOntoType());
            }
            String replicateUUID = replicateID == -1 ? "" : "_" + replicateID+"_";

            org.apache.jena.rdf.model.Property hasStepReplicate = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS()+"hasStepReplicate");

            Individual newIndStep = data.getDataModel().createIndividual(data.getDATA_NS() + "po2_step_" + headerPart.getUuid(true)+replicateUUID, classStep);
            myStep.forEach((k, rep) -> rep.addProperty(hasStepReplicate, newIndStep));
            myStep.put(replicateID, newIndStep);

            String scale = getScale();
            if(scale != null && !scale.isEmpty()) {
                Property hasScale = ontology.getCoreModel().getOntProperty(ontology.getCORE_NS() + "hasScale");
                OntClass ontScale = ontology.searchScale(scale);
                if(ontScale == null) {
                    json.getJSONArray("warning").put("Scale class " + scale + " not found in ontology");
                    ontScale = data.getDataModel().createClass(ontology.getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(scale));
                    ontScale.addProperty(SKOS.prefLabel, scale, "");
                    ontScale.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS() + "Scale"));
                }
                myStep.get(replicateID).addProperty(hasScale, ontScale);
            }

            String date = getDate();
            String time = getTime();
            String duration = getDuration();

            LocalDateTime ldate = Tools.convertDate(date, time, json);
            LocalTime ldur = Tools.convertTime(duration, json);
            Duration d = Duration.between( LocalTime.of(0,0,0), ldur);
            LocalDateTime end = null;
            if(ldate != null) {
                end = ldate.plus(d);
            }
            myInterval.put(replicateID, JenaTools.addDate(ontology.getCoreModel(), data.getDataModel(), myStep.get(replicateID), ldate, end));

            String lab = getId();
            if (lab.isEmpty()) {
                lab = getOntoType();
            }
            myStep.get(replicateID).addProperty(SKOS.prefLabel, lab, "");
            myStep.get(replicateID).addProperty(SKOS.scopeNote, getDescription(), "");

            String agentString = getAgent(replicateID);
            if(!agentString.isBlank()) {
                Individual agent = data.getDataModel().createIndividual(data.getDATA_NS()+"po2_agent_"+Tools.shortUUIDFromString(agentString), FOAF.Agent);
                myStep.get(replicateID).addProperty(wasassociatedwith, agent);
            }

            for (StepFile ss : getSubStep()) {
                JSONObject stepJSON = new JSONObject();
                stepJSON.put("name", "step " + ss.getNameProperty());
                stepJSON.put("error", new JSONArray());
                stepJSON.put("warning", new JSONArray());
                stepJSON.put("subObject", new JSONArray());
                stepJSON.put("type", "step");
                json.getJSONArray("subObject").put(stepJSON);
                if(!ss.getSubStep().isEmpty()) {
                    stepJSON.getJSONArray("error").put("1 level of subStep allowed");
                }

                Individual iss = ss.publish(ontology, stepJSON, replicateID);
                myStep.get(replicateID).addProperty(subStepProperty, iss);
                this.getStepInterval(replicateID).addProperty(timeContains, ss.getStepInterval(replicateID));
            }

            // Materiel/Methode
            Property madeByActuator = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"madeByActuator");
            Property usedProcedure = ontology.getCoreModel().getOntProperty(JenaTools.SOSA+"usedProcedure");
            Property hasSubSystem = ontology.getCoreModel().getOntProperty(JenaTools.SSN+"hasSubSystem");
            OntClass classActuator = ontology.getCoreModel().getOntClass(JenaTools.SOSA+"actuator");

            Integer matMetCompteur = 0;
            for(MaterialMethodLinkPart mat : listMaterialMethod) {
                MaterialMethodPart matMetPart = mat.getMaterialMethodPartPart();
                if(matMetPart != null) {
                    Individual myMatMeth = matMetPart.getMatMethIndividual();

                    JSONObject actProcJSON = new JSONObject();
                    actProcJSON.put("error", new JSONArray());
                    actProcJSON.put("warning", new JSONArray());
                    actProcJSON.put("subObject", new JSONArray());
                    json.getJSONArray("subObject").put(actProcJSON);

                    // myMatMeth is the system or procedure. Now creating an actuator
                    if(matMetPart.isMaterial()) { // creating an actuator
                        actProcJSON.put("name", "Actuator " + matMetPart.getID().getValue().get());
                        actProcJSON.put("type", "Actuator");

                        Individual actuator = data.getDataModel().createIndividual(data.getDATA_NS()+"actuator_"+headerPart.getUuid(true)+"_"+matMetCompteur, classActuator);
                        myMatMeth.addProperty(hasSubSystem, actuator);

                        // add consigne on actuator
                        ConsignPart cons = mat.getConsignPart();
                        cons.publish(ontology, actuator,actProcJSON, headerPart.getUuid(true) );
                        myStep.get(replicateID).addProperty(madeByActuator, actuator);

                    }
                    if(matMetPart.isMethod()) {
                        actProcJSON.put("name", "Procedure " + matMetPart.getID().getValue().get());
                        actProcJSON.put("type", "Procedure");

                        // creating an individual from an individual for procedure. Its a way to bypass the subSystem for material -> actuator
                        Individual iProcedure = data.getDataModel().createIndividual(data.getDATA_NS()+"actuator_"+headerPart.getUuid(true)+"_"+matMetCompteur, myMatMeth);
                        ConsignPart cons = mat.getConsignPart();
                        cons.publish(ontology, iProcedure,actProcJSON, headerPart.getUuid(true) );
                        myStep.get(replicateID).addProperty(usedProcedure, iProcedure);
                    }

                }
                matMetCompteur++;
            }


            // composition !!!
            Property hasInput = ontology.getCoreModel().getProperty(ontology.getCORE_NS() + "hasInput");
            Property hasOutput = ontology.getCoreModel().getProperty(ontology.getCORE_NS() + "hasOutput");
            for (Map.Entry<CompositionFile, Boolean> compoEntry : listCompo.entrySet()) {
                JSONObject compoJSON = new JSONObject();
                compoJSON.put("name", "composition " + compoEntry.getKey().getCompositionID().getValue().get());
                compoJSON.put("error", new JSONArray());
                compoJSON.put("warning", new JSONArray());
                compoJSON.put("subObject", new JSONArray());
                compoJSON.put("type", "composition");

                json.getJSONArray("subObject").put(compoJSON);
                Individual compo = compoEntry.getKey().publish(ontology, compoJSON, replicateID);
                if(compo != null) {
                    if (compoEntry.getValue()) {
                        myStep.get(replicateID).addProperty(hasInput, compo);
                    } else {
                        myStep.get(replicateID).addProperty(hasOutput, compo);
                    }
                }
            }

            for(ObservationFile observationFile : getObservationFiles()) {
                JSONObject objJSON = new JSONObject();
                objJSON.put("name", "observation " + observationFile.getNameProperty().get());
                objJSON.put("error", new JSONArray());
                objJSON.put("warning", new JSONArray());
                objJSON.put("subObject", new JSONArray());
                objJSON.put("type", "observation");
                json.getJSONArray("subObject").put(objJSON);
                observationFile.publish(ontology, myInterval.get(replicateID), objJSON, replicateID);
            }
        }

        return myStep.get(replicateID);
    }

    public void initPublish() {
        myStep = new HashMap<>();
        myInterval = new HashMap<>();

        listCompo.keySet().forEach(CompositionFile::initPublish);
        listObservation.forEach(ObservationFile::initPublish);
    }

    public Report constructDataFromImport(File fileImport, boolean isNewStep, int replicateID) {
        Report back = new Report();
        ImportReport importReport = realPreConst(fileImport, isNewStep);
        if(!importReport.success()) {
            back.getError().addAll(importReport.getError());
            return back;
        }
        try {
            FileInputStream fis = new FileInputStream(fileImport);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            org.apache.commons.io.IOUtils.copy(fis, baos);
            byte[] bytes = baos.toByteArray();
            fis.close();


            detectPart(fileImport, importReport.getStepSheet());

            if (listPart.size() > 0) {
                identifyParts();
                // on vide les mat & met
                listMaterialMethod.clear();
                unbind();
                buildStep(replicateID);
            } else {
                back.addError("No part found in step sheet");
            }

            for (Map.Entry<XSSFSheet, ObservationFile> entry : importReport.getObsMapping().entrySet()) {
                ObservationFile obs = entry.getValue();
                if(obs == null) {
                    // create new obs
                    obs = new ObservationFile(this);
                }
                File tempFile = File.createTempFile("blalbla", "xlsx", null);
                FileUtils.copyInputStreamToFile(new ByteArrayInputStream(bytes), tempFile);
                if (!obs.constructData(tempFile, entry.getKey().getSheetName(), replicateID)) {
                    back.addError("Error while importing sheet : " + entry.getKey());
                }
                this.data.setModified(true);
            }

            for (ObservationFile oo : importReport.getObsDelete()) {
                oo.removeFile();
            }

            for (Map.Entry<XSSFSheet, Pair<CompositionFile, Boolean>> entry : importReport.getCompoMapping().entrySet()) {
                CompositionFile compo = entry.getValue().getLeft();
                if(compo == null) {
                    // create new composition
                    compo = new CompositionFile(this, entry.getValue().getRight());
                }
                File tempFile = File.createTempFile("blalbla", "xlsx", null);
                FileUtils.copyInputStreamToFile(new ByteArrayInputStream(bytes), tempFile);
                if (!compo.constructData(tempFile, entry.getKey().getSheetName())) {
                    back.addError("Error while importing sheet : " + entry.getKey());

                }
                // setting input/output compo if change was made in import file
                listCompo.put(compo, entry.getValue().getRight());
                this.data.setModified(true);
            }

            for (CompositionFile oo : importReport.getCompoDelete()) {
                listCompo.remove(oo);
            }

        } catch (IOException e) {
            e.printStackTrace();
            back.addError("Exception : " + e.getMessage());

        }
        return back;
    }

    /***
     *
     * @param fileImport
     * @return
     */
    public ImportReport realPreConst(File fileImport, boolean isNewStep) {
        ImportReport back = new ImportReport();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileImport);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            org.apache.commons.io.IOUtils.copy(fis, baos);
            byte[] bytes = baos.toByteArray();
            fis.close();

            XSSFWorkbook book = new XSSFWorkbook(new ByteArrayInputStream(bytes));

//            Map<String, ObservationFile> mapSheetOsb = new HashMap<>();
            for (Iterator<Sheet> iteSheet = book.iterator(); iteSheet.hasNext(); ) {
                XSSFSheet s = (XSSFSheet) iteSheet.next();
                String sheeName = s.getSheetName();
                logger.debug("sheetName : "+sheeName + "step|"+ DigestUtils.md5Hex(getNameProperty().getValue()) + sheeName.startsWith("step|"+ DigestUtils.md5Hex(getNameProperty().getValue())));
//                if(("step|"+ DigestUtils.md5Hex(getNameProperty().getValue())).startsWith(sheeName)) {
                if(sheeName.startsWith("step|")) {
                    if(back.getStepSheet() != null) {
                        back.addError("Multiple step found");
                        return back;
                    }
                    back.setStepSheet(sheeName);
                } else {
                    if(sheeName.startsWith("obs|")) {
                        back.addObsMapping(s, null);
                    } else {
                        if(sheeName.startsWith("compo-")) {
                            back.addCompoMapping(s, null, sheeName.startsWith("compo-input"));
                        }
                    }
                }
            }


            if(back.getStepSheet() == null || StringUtils.isBlank(back.getStepSheet())) {
                back.addError("No step found");
                return back;
            }
            // ckecking step xls uuid with this one
            XSSFSheet sh = book.getSheet(back.getStepSheet());
            if(!isNewStep && !Tools.searchString(sh, getHeaderPart().getUuid(true))) {  // bypassed if importing a new step from an xlsx file
                // bad import file or stepFile
                back.addError("You are trying to import the wrong step");
                return back;
            }
            // if importing a new step from an xlsx file, uuid must not be present
            if(isNewStep && Tools.uuidIsPresent(sh)) {
                back.addError("You should not specify an uuid when importing a new step");
                return back;
            }

            for(ObservationFile obsF : getObservationFiles()) {
                for(XSSFSheet s : back.getObsMapping().keySet()) {
                    if(back.getObsMapping().get(s) == null && Tools.searchString(s, obsF.getUuid(true))) {
                        back.getObsMapping().put(s, obsF);
                    }
                }
            }

            for (ObservationFile obsF : getObservationFiles()) {
                if (!back.getObsMapping().containsValue(obsF)) {
                    back.addObsDelete(obsF);
                }
            }

            for(CompositionFile compoF : getCompositionFile().keySet()) {
                for(XSSFSheet s : back.getCompoMapping().keySet()) {
                    if(back.getCompoMapping().get(s).getLeft() == null && Tools.searchString(s, compoF.getUuid(true))) {
                        Boolean isInput = back.getCompoMapping().get(s).getRight();
                        back.getCompoMapping().put(s, new ImmutablePair<>(compoF, isInput));
                    }
                }
            }

            for (CompositionFile compoF : getCompositionFile().keySet()) {
                if(back.getCompoMapping().values().stream().map(Pair::getLeft).noneMatch(c -> c.equals(compoF))) {
                    back.addCompoDelete(compoF);
                }
            }

//            back = new MutablePair<>(mapSheetOsb, obsToDelete);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return back;
    }

    public Report checkUpdateObservationFromXLSX(File fileImport, ObservationFile obs, int replicateID){
        ObservationFile obsTemp = new ObservationFile(this);
        Report report = obsTemp.constructDataFromImport(fileImport, false, replicateID);
        obsTemp.removeFile();
        // checking the uuid
        if(report.success() && !obsTemp.getUuid(true).equals(obs.getUuid(true))) {
            report.addError("You are trying to import the wrong observation");
        }
        return report;
    }

    public Report checkCreateObservationFromXLSX(File fileImport, int replicateID){
        ObservationFile obs = new ObservationFile(this);
        Report report = obs.constructDataFromImport(fileImport, true, replicateID);
        obs.removeFile();
        return report;
    }

    public Report updateObservationFromXLSX(File fileImport, ObservationFile obs, int replicateID){
        Report report = obs.constructDataFromImport(fileImport, false, replicateID);
        return report;
    }

    public ObservationFile createObservationFromXLSX(File fileImport, int replicateID){
        ObservationFile obs = new ObservationFile(this);
        Report report = obs.constructDataFromImport(fileImport, true, replicateID);
        if(!report.success()) {
            obs.removeFile();
        }
        return obs;
    }

    public void simpleRemoveFile() {
        for (StepFile son : getSubStep()) {
            son.simpleRemoveFile();
        }
        for (ItineraryFile iti : generalFile.getItinerary()) {
            for (Iterator<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> ite = iti.getItinerary().iterator(); ite.hasNext(); ) {
                Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> line = ite.next();
                if (line.getKey().getValue().get() != null && line.getKey().getValue().get().equals(this)) {
                    line.getKey().getValue().setValue(null);
                    line.getKey().getKey().setValue("");
//                    ite.remove();
                } else {
                    if (line.getValue().getValue().get() != null && line.getValue().getValue().get().equals(this)) {
                        line.getValue().getValue().setValue(null);
                        line.getValue().getKey().setValue("");
                    }
                }
            }
        }

        generalFile.getItinerary().forEach(itineraryFile -> itineraryFile.getListStep().remove(this));
        generalFile.getContentPart().getListStep().remove(this);

    }

    public void cloneFrom(StepFile initial, ItineraryFile itinerayPart, Map<CompositionFile, CompositionFile> mappingCompo, ItineraryFile originalelIti, Map<StepFile, StepFile> mappingStep) {
        if(mappingStep != null) {
            mappingStep.put(initial, this);
        }

        if(originalelIti.getListStep().contains(initial)) {
            itinerayPart.addStep(this);
        }

        // clonage header
        setOntoType(initial.getOntoType());
        setDate(initial.getDate());
        setTime(initial.getTime());
        setDuration(initial.getDuration());
        setDescription(initial.getDescription());
        setAgents(initial.getAgents());

        for(MaterialMethodLinkPart matMet : initial.getListMaterialMethodRO()) {
            try {
                MaterialMethodLinkPart newMatMet = new MaterialMethodLinkPart(matMet.getMaterialMethodPartPart());
                newMatMet.getConsignPart().cloneFrom(matMet.getConsignPart());
                listMaterialMethod.add(newMatMet);
            } catch (NullPointerException e) {
                logger.error("material or method null", e);
            }
        }

        // clonage des compositions
        for(Map.Entry<CompositionFile, Boolean> entry : initial.getCompositionFile().entrySet()) {

            // vérification de l'existance de la composition
            if(mappingCompo.containsKey(entry.getKey())) {
                this.addCompositionFile(mappingCompo.get(entry.getKey()), entry.getValue());
            } else {
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                String uniqueCompo = "" + Tools.generateUniqueTimeStamp();
//                String fileNameCompo = "composition_" + uniqueCompo;
//                String fileNameConduite = "conduite_" + uniqueCompo;
//                String filePathCompo = Paths.get(this.getGeneralFile().getFilePath()).getParent().toAbsolutePath().toString() + File.separator+ stepName + File.separator+ "composition-conduite"+ File.separator + fileNameCompo + ".xlsx";
//                String filePathConduite = Paths.get(this.getGeneralFile().getFilePath()).getParent().toAbsolutePath().toString() +  File.separator + stepName + File.separator+ "composition-conduite"+ File.separator + fileNameConduite + ".xlsx";

                CompositionFile newCompo = new CompositionFile(this, entry.getValue());
                mappingCompo.put(entry.getKey(), newCompo);
                newCompo.setCompositionID(entry.getKey().getCompositionID().getValue().get());
                newCompo.cloneFrom(entry.getKey());
//                this.getData().addToListAllFile(newCompo.getFilePath());
//                this.getData().addToListAllFile(newCompo.getConduiteFile().getFilePath());
            }
        }

        // clonage des observations

        for(ObservationFile obs : initial.getObservationFiles()) {
            // creating new observation
            ObservationFile newFile = new ObservationFile(this);
            newFile.cloneFrom(obs, mappingCompo);
        }

        // clonage des substep
        for(StepFile sub : initial.subStep) {
            //// creating new substep
            //// uniquement si sub est dans l'itinéraire !!!!!
            if(originalelIti.contains(sub)) {
                StepFile newSub = new StepFile(sub.getOntoType(), generalFile);
                newSub.setId(sub.getId());
                itinerayPart.addStep(newSub);
//                generalFile.getContentPart().createStepDataNode(newSub);

                if(mappingStep == null) { // on ne clone pas l'itinéraire
                    itinerayPart.addLinkItinerary(newSub, null);
//                    itinerayPart.generateStepDataNode(newSub);
                }

                newSub.cloneFrom(sub, itinerayPart, mappingCompo, originalelIti, mappingStep);
                this.addSubStep(newSub);
            }
        }
    }

    public void saveData() throws IOException {
        // fichier step-data
        Integer maxColumn = 0;
        logger.debug("saving step " + nameProperty.get() + " with path " + getFilePath());
        File f = new File(getFilePath());
        if (f.exists()) {
            f.delete();
        } else {
            f.getParentFile().mkdirs();
        }
        logger.debug("adding file as updated " + getFilePath());
        generalFile.getListUpdateFile().add(getFilePath());

        XSSFWorkbook book = new XSSFWorkbook();
        XSSFSheet sheet = Tools.createSheet(book,"data");
        Integer compteur = 0;
        for (LinkedList<String> line : getDataForExcel(true)) {
            XSSFRow r = sheet.createRow(compteur++);
            Integer compteurCell = 0;
            for (String val : line) {
                r.createCell(compteurCell++).setCellValue(val);
                if (maxColumn < compteurCell) {
                    maxColumn = compteurCell;
                }
            }
        }
        Tools.autoResize(sheet, maxColumn);


        FileOutputStream fout = new FileOutputStream(getFilePath(), false);
        book.write(fout);
        fout.close();


        // sauvegarde de la composition
        for (Map.Entry<CompositionFile, Boolean> entry : listCompo.entrySet()) {
            if (entry.getKey() != null) {
                CompositionFile compositionFile = entry.getKey();


                if (compositionFile != null) {
                    // check if compo (and conduite) already saved (composition can be used in many step)
                    if(!generalFile.getListUpdateFile().contains(compositionFile.getFilePath())) {
                        maxColumn = 0;
                        f = new File(compositionFile.getFilePath());
                        if (f.exists()) {
                            f.delete();
                        } else {
                            f.getParentFile().mkdirs();
                        }
                        generalFile.getListUpdateFile().add(compositionFile.getFilePath());
                        book = new XSSFWorkbook();
                        sheet = Tools.createSheet(book,"data");

                        compteur = 0;
                        for (LinkedList<String> line : compositionFile.getDataForExcel()) {
                            XSSFRow r = sheet.createRow(compteur++);
                            Integer compteurCell = 0;
                            for (String val : line) {
                                r.createCell(compteurCell++).setCellValue(val);
                                if (maxColumn < compteurCell) {
                                    maxColumn = compteurCell;
                                }
                            }
                        }

                        Tools.autoResize(sheet, maxColumn);
                        fout = new FileOutputStream(compositionFile.getFilePath(), false);
                        book.write(fout);
                        fout.close();
                    }
                }
            }
        }


        // sauvegarde du materiel & mehode
//        if(getListMaterialMethod().size() > 0) {

//        }

        // sauvegarde des observations
        for (ObservationFile obs : getObservationFiles()) {
            maxColumn = 0;
            logger.debug("saving observation " + obs.getNameProperty().get() + " with path " + obs.getFilePath());
            f = new File(obs.getFilePath());
            if (f.exists()) {
                f.delete();
            } else {
                f.getParentFile().mkdirs();
            }

            logger.debug("adding observation as updated " + getFilePath());
            generalFile.getListUpdateFile().add(obs.getFilePath());

            book = new XSSFWorkbook();
            sheet = Tools.createSheet(book,"data");

            compteur = 0;
            for (LinkedList<String> line : obs.getDataForExcel()) {
                XSSFRow r = sheet.createRow(compteur++);
                Integer compteurCell = 0;
                for (String val : line) {
                    r.createCell(compteurCell++).setCellValue(val);
                    if (maxColumn < compteurCell) {
                        maxColumn = compteurCell;
                    }
                }
            }
            Tools.autoResize(sheet, maxColumn);


            fout = new FileOutputStream(obs.getFilePath(), false);
            book.write(fout);
            fout.close();
        }
    }


    public Boolean constructDataFromOlderMatMetMethod(String folderPath, String fileName) {
        try {
            initStepFile();
            detectPart(new File(folderPath + fileName+".xlsx"), null);
            identifyParts();

//            Boolean lastIsMaterial = null;
            MaterialMethodLinkPart lasMatMet = null;
            for(DataPart p : listPart) {
                if(p.getType().equals(DataPartType.GENERAL_INFO)) {
                    headerPart.mapPart(p);
                }
                if(p.getType().equals(DataPartType.MATERIAL_USED)) { ///////////////////////////////////////////:: on remonte ces infos sur l'étape
                    lasMatMet = new MaterialMethodLinkPart(data);
                    lasMatMet.setType(DataPartType.MATERIAL_USED);
                    lasMatMet.mapPart(p);
                    listMaterialMethod.add(lasMatMet);
                }
                if(p.getType().equals(DataPartType.METHOD_USED)) { ///////////////////////////////////////////:: on remonte ces infos sur l'étape
                    lasMatMet = new MaterialMethodLinkPart(data);
                    lasMatMet.setType(DataPartType.METHOD_USED);
                    lasMatMet.mapPart(p);
                    listMaterialMethod.add(lasMatMet);
                }
                if(p.getType().equals(DataPartType.CONSIGN)) {
                    if(lasMatMet != null) {
                        lasMatMet.getConsignPart().mapPart(p);
                    }
                }
            }
            isConstruct = true;
        } catch (IOException ex) {
            return false;
        }
        return true;
    }
}
