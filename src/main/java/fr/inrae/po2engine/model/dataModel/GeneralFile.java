/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.DataTools;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.rdf4j.model.ModelFactory;
import org.eclipse.rdf4j.model.vocabulary.PROV;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class GeneralFile extends GenericFile{

    private GeneralMetaPart headerPart = null;
    private GeneralContentPart contentPart = null;
    private List<ItineraryFile> itineraryFiles = new LinkedList<>();
    private SimpleBooleanProperty loaded = new SimpleBooleanProperty(false);
    private ArrayList<String> listFileForAnalyse = new ArrayList<>();
    private OntClass myProcess;
    private Map<String, File> listAllFile;
    private List<String> listUpdateFile;
    private static Logger logger = LogManager.getLogger(GeneralFile.class);
    private StringProperty nameProperty;
    private SimpleObjectProperty<Replicate> currentReplicate = new SimpleObjectProperty();

    private static List<GenericPart> listRule = Arrays.asList(new GeneralMetaPart(),  new GeneralContentPart(), new GeneralItineraryPart() );


    private GeneralFile() {};
    public GeneralFile(ProjectFile projectFile) {
        this(projectFile, true);
        constructData(); // creation d'un nouveau process. On le charge automatiquement
        setOntoType("Process");
    }

    public GeneralFile(String processType, ProjectFile projectFile) {
        this(projectFile, true);
        getCType().setValue(processType);
    }

    public GeneralFile(ProjectFile projectFile, Boolean addToData) {
        String name = "Process_"+ Tools.generateUniqueTimeStamp();
        this.data = projectFile.getData();
        this.nameProperty = new SimpleStringProperty();
        this.fileName = name;
        this.folderPath = this.data.getProjectFile().getFolderPath()+this.fileName+File.separator;
        this.filePath = folderPath+fileName+".xlsx";
        listAllFile = listFiles(new File(folderPath));
        headerPart = new GeneralMetaPart(this);
        contentPart = new GeneralContentPart(this);
        this.nameProperty.bind(Bindings.concat(headerPart.getCType().getValue(), " (",headerPart.processNameProperty(),") "));

        if(addToData) {
            this.data.addGeneralFile(this);
        }
    }

    public GeneralFile(String fileName, ProjectFile projectFile, String folderPath, Boolean addToData ) {
        this(projectFile, addToData);
        this.fileName = fileName;
        this.folderPath = folderPath;
        this.filePath = folderPath+fileName+".xlsx";
        listAllFile = listFiles(new File(folderPath));
    }

    public GeneralFile(String fileName, ProjectFile projectFile, String folderPath ) {
        this(fileName, projectFile, folderPath, true);
    }

    public void addItineraryFile(ItineraryFile itinerary) {
        this.itineraryFiles.add(itinerary);
        this.data.setModified(true);
    }

    public Map<String, File> getListAllProcessFile() {
        return listAllFile;
    }

    public Set<StepFile> getListStep() {
        if(contentPart != null) {
            return contentPart.getListStep();
        }
        return null;
    }

    public void checkValue() {
        headerPart.checkValue();
        contentPart.checkValue();
    }

    public String getOldSampleType() {
        return this.headerPart.getOldSampleType();
    }

    public List<String> getListUpdateFile() {
        return this.listUpdateFile;
    }

    public Boolean isLoaded() {
        return loaded.getValue();
    }

    public BooleanProperty getLoaded() {
        return loaded;
    }

//    public void buildDataNode(DataNode root) {
//        this.dataNode = new DataNode(DataNodeType.PROCESS);
//        this.dataNode.setFile(this);
//
//        root.addSubNode(this.dataNode);
//        this.dataNode.addFather(root);
//
//        this.dataNode.nameProperty().bind(Bindings.concat(headerPart.processNameProperty(), " (",headerPart.sampleNameProperty(),")"));
//    }

    public StringProperty titleProperty() {
        return headerPart.titleProperty();
    }

    public Boolean preConstructData(Data data) {
        try {
//            buildDataNode(root);
            detectPart();
            identifyParts();
            for(DataPart p : listPart) {
                if(p.getType().equals(DataPartType.GENERAL_INFO)) {
                    headerPart.mapPart(p);
                    currentReplicate.set(headerPart.getReplicate(0));
                }

                if(p.getType().equals(DataPartType.RAW_DATA)) {
                    // on ne fait que lister les fichier utilisés
                    List<String> valFileName = contentPart.getColumnValues(contentPart.getFileNameK(), p);

                    for(String val : valFileName) {
                        String[] listFile = val.split("[\n\r;]");
                        listFileForAnalyse.addAll(Arrays.asList(listFile));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<String> getListFileForAnalyse() {
        return listFileForAnalyse;
    }

    public void unMapPart() {
        contentPart = new GeneralContentPart(this);
        itineraryFiles.clear();
//        contentPart.unMapPart();
//        dataNode.clearSubNode();
//        dataNode.updateImage();
    }
    @Override
    public Boolean constructData() {
        boolean oldModifiedState = this.data.isModified();
        try {
            logger.debug("construct data " + this.data.getName().get());
//            initGeneralFile(root);
            try {
                detectPart();
            } catch (IOException e) {

            }
            identifyParts();




            for(DataPart p : listPart) {
//                if(p.getType().equals(DataPartType.GENERAL_INFO)) {
//
//                    headerPart.mapPart(p);
//                    headerPart.mapProjectName(data.getName());
//                }
                if(p.getType().equals(DataPartType.RAW_DATA)) {
                    contentPart.mapPart(p);
                }

                if(p.getType().equals(DataPartType.ITINERARY)) {
                    ItineraryFile itiFile = new ItineraryFile(this);
//                    DataNode dataNodeIti = new DataNode(DataNodeType.ITINERARY);
//                    MainApp.getDataControler().addNode(itiFile, dataNodeIti, MainApp.getDataControler().getDataNode(this));
//                    itineraryPartT.setDataNode(dataNodeIti);
//                    dataNodeIti.setItineraryPart(itineraryPartT);
                    itiFile.mapPart(p);
//                    dataNodeIti.nameProperty().bind(Bindings.concat(itineraryPartT.getItiNumber(), " (",itineraryPartT.getItiName(),")"));

//                    this.dataNode.addSubNode(dataNodeIti);
                }

            }

            for(ItineraryFile iti : itineraryFiles) {
//                part.generateStepDataNode();
                // on construit les observation d'itinéraire
                for(ObservationFile obs : iti.getListObservation()) {
                    obs.constructData();
                }
            }
            loaded.setValue(true);
//            dataNode.updateImage();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        this.data.setModified(oldModifiedState);

        return true;
    }

    @Override
    public StringProperty getNameProperty() {
        return nameProperty;
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {
        logger.debug("mapping ok  for " + type);
    }

    public Property<String> processNameProperty() { return headerPart.processNameProperty(); }

    public ComplexField getCTitle() {
        return headerPart.getCTitle();
    }

    public ComplexField getCExternalLinks() { return this.headerPart.getCExternalLinks();}

    public ArrayList<String> getExternalLinks() {
        return Arrays.stream(getCExternalLinks().getValue().get().split("\\r?\\n?\\::")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toCollection(ArrayList::new));
    }

    public void setExternalLink(String links) {
        getCExternalLinks().setValue(links);
    }

    public void addExternalLink(String newLink) {
        ArrayList<String> temp = getExternalLinks();
        temp.add(newLink);
        setExternalLink(temp.stream().collect(Collectors.joining("::")));
    }

    public void setTitle(String title) { getCTitle().setValue(title);}
//    public void setTitle(TextField title) {
//        headerPart.bindTitle(title);
//    }

    public void unbind() {
        headerPart.unbind();
        contentPart.unbind();
    }

    @Override
    public void removeFile() {
        this.data.getListGeneralFile().remove(this);
//        this.data.getProjectFile().getDataNode().removeSubNode(this.dataNode);
        this.data.setModified(true);
    }

    public GeneralContentPart getContentPart() {
        return contentPart;
    }

    public ComplexField getCType() {
        return headerPart.getCType();
    }

    public String getOntoType() {
        return getCType().getValue().get();
    }

    public void setOntoType(String type) {
        headerPart.setProcessType(type);
    }


    public ComplexField getCStartDate() {
        return headerPart.getCStartDate();
    }

    public void setStartDate(String date) {
        getCStartDate().setValue(date);
    }

    public ComplexField getCEndDate() {
        return headerPart.getCEndDate();
    }

    public void setEndDate(String date) {
        getCEndDate().setValue(date);
    }


    public ComplexField getCDescription() { return headerPart.getCDescription();}

    public void setDescription(String desc) {
        getCDescription().setValue(desc);
    }

//    public void setDate(TextField date) {
//        headerPart.bindDate(date);
//    }

//    public ComplexField getCSampleCode() {
//        return headerPart.getCSampleCode();
//    }
//    public void setSampleCode(String code) {getCSampleCode().setValue(code);}
//    public void setSampleCode(TextField sampleCode) {
//        headerPart.bindSampleCode(sampleCode);
//    }
//    public StringProperty sampleProperty() {
//        return headerPart.sampleProperty();
//    }

//    public ComplexField getCSampleName() {
//        return headerPart.getCSampleName();
//    }
//    public void setSampleName(String name) { getCSampleName().setValue(name);}
//    public void setSampleName(TextField sampleName) {
//        headerPart.bindSampleName(sampleName);
//    }

    public void removeItinerary(ItineraryFile itinerary) {
        itineraryFiles.remove(itinerary);
        this.data.setModified(true);
    }
    public List<ItineraryFile> getItinerary() {
        return itineraryFiles;
    }

    public ObservableList<Replicate> getListReplicate()  {
        return this.headerPart.getListReplicate();
    }

    public boolean removeReplicate(Replicate replicate) {
        return this.headerPart.removeReplicate(replicate);
    }

    public void addReplicate(Replicate replicate) {
        this.headerPart.addReplicate(replicate);
    }
    public Replicate addReplicate(String name, String description) {
        return this.headerPart.addReplicate(name, description);
    }

    public Data getData() {
        return data;
    }

    public void saveData(String matmetFilePath) throws IOException, InvalidFormatException {
        listUpdateFile = new ArrayList<>();
        if(matmetFilePath != null) {
            listUpdateFile.add(matmetFilePath);
        }
        Integer maxColumn = 0;
        File f = new File(filePath);
        if(f.exists()) {
            f.delete();
        } else {
            f.getParentFile().mkdirs();
        }
        this.listUpdateFile.add(filePath);

        XSSFWorkbook book = new XSSFWorkbook();


        XSSFSheet s = Tools.createSheet(book, "data");
        Integer compteur = 0;
            // header
        // checking uuid is set.
        headerPart.generateUUID();
        for(Map.Entry<KeyWords, ComplexField> entry : headerPart.getContent().entrySet()) {
            XSSFRow row = s.createRow(compteur++);
            XSSFCell cell0 = row.createCell(0);
            cell0.setCellValue(entry.getKey().getPrefLabel("en"));
            XSSFCell cell1 = row.createCell(1);
            cell1.setCellValue(entry.getValue().getValue().get());
        }
        if(!headerPart.getListReplicate().isEmpty()) {
            XSSFRow row = s.createRow(compteur++);
            XSSFCell cell0 = row.createCell(0);
            cell0.setCellValue(GeneralMetaPart.replicateK.getPrefLabel("en"));
            XSSFCell cell1 = row.createCell(1);
            cell1.setCellValue(headerPart.getReplicateAsString());
        }
        maxColumn = 1;
            // ajout d'une ligne vide
        s.createRow(compteur++);

            // data
        XSSFRow header = s.createRow(compteur++);
        Integer colCompteur = 0;
        for(Map.Entry<KeyWords, List<ComplexField>> entry : contentPart.getContent().entrySet()) {
            header.createCell(colCompteur++).setCellValue(entry.getKey().getPrefLabel("en"));
        }
        for(StepFile step : contentPart.getListStep()) {
                if(step.isRoot()) {
                    LinkedList<LinkedList<String>> toWrite = getStepForExcel(step);
                    for(LinkedList<String> l : toWrite) {
                        XSSFRow row = s.createRow(compteur++);
                        colCompteur = 0;
                        for(String val : l) {
                            row.createCell(colCompteur++).setCellValue(val);
                            if(maxColumn < colCompteur) {
                                maxColumn = colCompteur;
                            }
                        }
                    }
                }

                step.saveData();
        }
        Tools.autoResize(s, maxColumn);

        // deuxieme onglet (itinéraire)

        XSSFSheet i = Tools.createSheet(book,"itineraries");
        compteur = 0;
        Integer itiCompteur = 1;
        for(ItineraryFile itF : itineraryFiles) {
            XSSFRow r = i.createRow(compteur++);
            String itiName = itF.getItineraryName().isEmpty() ? "itinerary" : itF.getItineraryName();
            r.createCell(0).setCellValue(itiName);
            // itinerary observation
            if(itF.getListObservation().isEmpty()) {
                r = i.createRow(compteur++);
                r.createCell(0).setCellValue("observation itinerary");
            }
            for(ObservationFile obs : itF.getListObservation()) {
                r = i.createRow(compteur++);
                r.createCell(0).setCellValue("observation itinerary");
                r.createCell(1).setCellValue(obs.getFileName());

                    Integer maxColumnObs = 0;
                    File fObs = new File(obs.getFilePath());
                    if (fObs.exists()) {
                        fObs.delete();
                    } else {
                        fObs.getParentFile().mkdirs();
                    }
                    this.getListUpdateFile().add(obs.getFilePath());

                    XSSFWorkbook bookObs = new XSSFWorkbook();
                    XSSFSheet sheetObs = Tools.createSheet(bookObs,"data");

                    Integer compteurObs = 0;
                    for (LinkedList<String> line : obs.getDataForExcel()) {
                        XSSFRow rObs = sheetObs.createRow(compteurObs++);
                        Integer compteurCellObs = 0;
                        for (String val : line) {
                            rObs.createCell(compteurCellObs++).setCellValue(val);
                            if (maxColumnObs < compteurCellObs) {
                                maxColumnObs = compteurCellObs;
                            }
                        }
                    }
                    Tools.autoResize(sheetObs, maxColumnObs);


                    FileOutputStream foutObs = new FileOutputStream(obs.getFilePath(), false);
                    bookObs.write(foutObs);
                foutObs.close();

            }
            r = i.createRow(compteur++);
            r.createCell(0).setCellValue(GeneralItineraryPart.prodOfInterestK.getPrefLabel("en"));
            r.createCell(1).setCellValue(itF.getCProductsOfInterest().getValue().get());
            r = i.createRow(compteur++);
            r.createCell(0).setCellValue(GeneralItineraryPart.graphLayoutK.getPrefLabel("en"));
            r.createCell(1).setCellValue(itF.getNodePositionAsString());
            // header
            r = i.createRow(compteur++);
            r.createCell(0).setCellValue(GeneralItineraryPart.fatherK.getPrefLabel("en"));
            r.createCell(1).setCellValue(GeneralItineraryPart.sonK.getPrefLabel("en"));

            ObservableList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> iti =  itF.getItinerary();
            for(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : iti) {
                if(pair.getKey().getValue().getValue() != null || pair.getValue().getValue().getValue() != null) {
                    XSSFRow rl = i.createRow(compteur++);

                    if (pair.getKey().getValue().get() != null) {
                        rl.createCell(0).setCellValue(pair.getKey().getValue().get().getOntoType() + " {{uuid=" + pair.getKey().getValue().get().getUuid(true) + "}}");
                    }
                    if (pair.getValue().getValue().get() != null) {
                        rl.createCell(1).setCellValue(pair.getValue().getValue().get().getOntoType() + " {{uuid=" + pair.getValue().getValue().get().getUuid(true) + "}}");
                    }
                }
            }
            i.createRow(compteur++);

        }
        Tools.autoResize(i, 1);
        FileOutputStream fout = new FileOutputStream(filePath, false);
        book.write(fout);
        fout.close();

        // remove case in listUpdateFile. Important for Windows. Folder in windows are not case sensitive
        List listUpdateFileLowerCase = listUpdateFile.stream().map(String::toLowerCase).collect(Collectors.toList());
        for (File oldFile : listAllFile.values()) {
            if (!listUpdateFileLowerCase.contains(oldFile.getAbsolutePath().toLowerCase())) {
                logger.debug("remove file : " + oldFile.getName() + " with path : " + oldFile.getAbsolutePath());
                oldFile.delete();
            }
        }

        listAllFile = DataTools.listFiles(new File(this.folderPath));
    }

    private LinkedList<LinkedList<String>> getStepForExcel(StepFile step) {
        LinkedList<LinkedList<String>> retour = new LinkedList<>();

        LinkedList<String> newLine = new LinkedList<>();
        if(step.getFather() == null) {
            newLine.add(step.getOntoType() + " {{uuid="+step.getUuid(true)+"}}");
            newLine.add("");
        } else {
            newLine.add(step.getFather().getOntoType() + " {{uuid="+step.getFather().getUuid(true)+"}}");
            newLine.add(step.getOntoType() + " {{uuid="+step.getUuid(true)+"}}");
        }
        newLine.add("step data");
        newLine.add("");
        newLine.add(step.getFileName());
        retour.add(newLine);

        for(Map.Entry<CompositionFile, Boolean> entry : step.getCompositionFile().entrySet()) {
            newLine = new LinkedList<>();
            if(step.getFather() == null) {
                newLine.add(step.getOntoType() + " {{uuid="+step.getUuid(true)+"}}");
                newLine.add("");
            } else {
                newLine.add(step.getFather().getOntoType() + " {{uuid="+step.getFather().getUuid(true)+"}}");
                newLine.add(step.getOntoType() + " {{uuid="+step.getUuid(true)+"}}");
            }
            newLine.add("Composition " + (entry.getValue() ? "input" : "output"));
            newLine.add("Milieu");
            newLine.add(entry.getKey().getFileName());
            retour.add(newLine);
        }

        for(ObservationFile obs : step.getObservationFiles()) {
            newLine = new LinkedList<>();
            if(step.isRoot()) {
                newLine.add(step.getOntoType() + " {{uuid="+step.getUuid(true)+"}}");
                newLine.add("");
            } else {
                newLine.add(step.getFather().getOntoType() + " {{uuid="+step.getFather().getUuid(true)+"}}");
                newLine.add(step.getOntoType() + " {{uuid="+step.getUuid(true)+"}}");
            }
            newLine.add("observation");
            newLine.add("");
            newLine.add(obs.getFileName());
            retour.add(newLine);
        }
        for(StepFile son : step.getSubStep()) {
            retour.addAll(getStepForExcel(son));
        }
        return retour;
    }

    public void publish(Ontology ontology, JSONObject processJSON, ArrayList<ItineraryFile> listIti) {
        Tools.addProgress(ProgressPO2.PUBLISHSTEP, titleProperty().get());
        Tools.updateProgress(ProgressPO2.PUBLISHSTEP, 0.0);
        // création de la sous classe transformation process
        headerPart.generateUUID();
        OntModel dataModel = data.getDataModel();

        OntClass classProcess = ontology.searchProcess(getOntoType());
        if (classProcess == null) {
            if(getOntoType().isEmpty()) {
                classProcess = ontology.getCoreModel().getOntClass(ontology.getCORE_NS() + "Process");
                processJSON.getJSONArray("error").put("process without type ");
            } else {
                logger.debug("création class process : " + getOntoType());
                processJSON.getJSONArray("warning").put("Process class " + getOntoType() + " not found in ontology");
                classProcess = data.getDataModel().createClass(ontology.getDOMAIN_NS() + "temp_" + Tools.shortUUIDFromString(getOntoType()));
                classProcess.addProperty(SKOS.prefLabel, getOntoType(), "");
                classProcess.addSuperClass(ontology.getCoreModel().getOntClass(ontology.getCORE_NS() + "Process"));
            }
        } else {
            logger.debug("founded class process : " + getOntoType());
        }

        myProcess = dataModel.createClass(data.getDATA_NS()+"po2_"+headerPart.getUuid(true));
        myProcess.addProperty(SKOS.prefLabel,titleProperty().get(), "");
        myProcess.addSuperClass(classProcess);


        ////DCAT
        myProcess.addProperty(RDF.type, DCAT.Dataset);
        myProcess.addProperty(DC_11.title, titleProperty().get());
        myProcess.addProperty(DC_11.description, getCDescription().getValue().get());

        // Lien Externe DCAT
        for(String link : getExternalLinks()) {
            myProcess.addProperty(DCTerms.relation, link);
        }

        JenaTools.addDate(ontology.getCoreModel(), dataModel, myProcess, Tools.convertDate(headerPart.startDateProperty().get(), "", processJSON), Tools.convertDate(headerPart.endDateProperty().get(), "", processJSON));

        ArrayList<ItineraryFile> listItineraryToDeal = new ArrayList<>();
        if(listIti == null) {
            listItineraryToDeal.addAll(itineraryFiles);
        } else {
            listItineraryToDeal.addAll(listIti);
        }

        Set<StepFile> listAllStepToDeal = new HashSet<>();
        for(ItineraryFile gip : listItineraryToDeal) {
            for(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> p : gip.getItinerary()) {
                StepFile s1 = p.getKey().getValue().get();
                StepFile s2 = p.getValue().getValue().get();
                if(s1 != null) {
                    listAllStepToDeal.add(s1);
                    while (!s1.isRoot()) {
                        listAllStepToDeal.add(s1.getFather());
                        s1 = s1.getFather();
                    }
                }
                if(s2 != null) {
                    listAllStepToDeal.add(s2);
                    while (!s2.isRoot()) {
                        listAllStepToDeal.add(s2.getFather());
                        s2 = s2.getFather();
                    }
                }
            }
        }

        for(StepFile s : listAllStepToDeal) {
           s.initPublish();
        }
        Double pp = 1.0/listAllStepToDeal.size();
        for(StepFile s : listAllStepToDeal) {
            if(s.isRoot()) {
                JSONObject stepJSON = new JSONObject();
                stepJSON.put("name", "step " + s.getNameProperty().getValue());
                stepJSON.put("error", new JSONArray());
                stepJSON.put("warning", new JSONArray());
                stepJSON.put("subObject", new JSONArray());
                stepJSON.put("type", "step");
                processJSON.getJSONArray("subObject").put(stepJSON);
                if(getListReplicate().isEmpty()) {
                    s.publish(ontology, stepJSON, 0);
                    Tools.updateProgress(ProgressPO2.PUBLISHSTEP, Tools.getProgress(ProgressPO2.PUBLISHSTEP).getProgress() + pp);
                } else {
                    for(Replicate replicate : getListReplicate()) {
                        s.publish(ontology, stepJSON, replicate.getId());
                    }
                    Tools.updateProgress(ProgressPO2.PUBLISHSTEP, Tools.getProgress(ProgressPO2.PUBLISHSTEP).getProgress() + pp);
                }
            }
        }

        for(ItineraryFile itinerary : listItineraryToDeal) {
            if(getListReplicate().isEmpty()) {
                Individual itiInd = itinerary.publish(ontology, myProcess, processJSON, headerPart.getUuid(true), 0);
                itiInd.addProperty(SKOS.prefLabel, itinerary.getItineraryNumber() + " - " + itinerary.getItineraryName(), "en");
            } else {
                for(Replicate replicate : getListReplicate()) {
                    Individual itiInd = itinerary.publish(ontology, myProcess, processJSON, headerPart.getUuid(true), replicate.getId());
                    itiInd.addProperty(SKOS.prefLabel, itinerary.getItineraryNumber() + " - " + itinerary.getItineraryName() + " ("+replicate.getName()+")", "en");
                    itiInd.addProperty(SKOS.definition, replicate.getDescription(), "en");
                }
            }
        }
        Tools.delProgress(ProgressPO2.PUBLISHSTEP);
    }

    public OntClass getMyProcess() {
        return myProcess;
    }

    public String searchFilePath(String fileName) {
        if(listAllFile.containsKey(fileName)) {
            return listAllFile.get(fileName).getAbsolutePath();
        }
        return null;
    }

    public String searchFolderPath(String fileName) {
        if(listAllFile.containsKey(fileName)) {
            return listAllFile.get(fileName).getParent() + File.separator;
        }
        logger.error("unable to find file : " + fileName);
        return null;
    }

    public static Map<String, File> listFiles(File parentFile) {
        Map<String, File> listTemp = new HashMap<>();
        File[] listOf = parentFile.listFiles();
        if(listOf != null) {
            for (int i = 0; i < listOf.length; i++) {
                if (listOf[i].isFile()) {
                    if (!listOf[i].getName().startsWith("~") && !listOf[i].getName().startsWith(".") && FilenameUtils.getExtension(listOf[i].getName()).contains("xls")) { // fichier caché windows + unix
                        listTemp.put(FilenameUtils.removeExtension(listOf[i].getName()), listOf[i]);
                    }
                } else if (listOf[i].isDirectory()) {
                    if (listOf[i].listFiles().length == 0) {
                        // empty directory // removing
                        try {
                            FileUtils.deleteDirectory(listOf[i]);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        listTemp.putAll(listFiles(listOf[i]));
                    }

                }
            }
        }
        return listTemp;
    }

    public void unload() {
        this.unbind();
        this.unMapPart();
        loaded.setValue(false);
    }

    public String getReplicateAsString() {
        return headerPart.getReplicateAsString();
    }

    public Replicate getCurrentReplicate() {
        return currentReplicate.get();
    }

    public SimpleObjectProperty<Replicate> currentReplicateProperty() {
        return currentReplicate;
    }

    public void setCurrentReplicate(Replicate currentReplicate) {
        this.currentReplicate.set(currentReplicate);
    }

    public void setReplicate(String processReplicate) {
        JSONArray arr = new JSONArray(processReplicate);
        arr.forEach(o -> {
            JSONObject obj = (JSONObject) o;
            if(obj.optInt("id", -1) != -1 && !obj.optString("name").isEmpty()) {
                Replicate r = new Replicate(obj.optInt("id", -1),obj.optString("name") );
                r.setDescription(obj.optString("description"));
                addReplicate(r);
            }
        });
    }
}
