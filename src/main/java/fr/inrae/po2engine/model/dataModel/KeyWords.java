/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KeyWords {
    private Map<String, String> mapPLabel = new HashMap<>();
    private Map<String, List<String>> mapALabel = new HashMap<>();
    private Boolean usedForDetection = false;
    private Boolean mendatory = false;
    private List<String> object = new ArrayList<>();
    private String unit = null;
    private String comment = null;
    private boolean usedInReplicate = false;

    public Boolean map(String value) {
        value = value.replaceAll("\\((.+?)\\)", "").trim(); // on ne prend pas en compte les parenthése pour les keywords

        for(Map.Entry<String, String> entry : mapPLabel.entrySet()) {
            if(entry.getValue().equalsIgnoreCase(value)) {

                    return true;

            }
        }
        for(Map.Entry<String, List<String>> entryA : mapALabel.entrySet()) {
            for(String plab : entryA.getValue()) {
                if (plab.equalsIgnoreCase(value)) {

                        return true;

                }
            }
        }
        return false;
    }


    public void addObject(String object) {
        this.object.add(object);
    }

    public Boolean isMandatory() {
        return mendatory;
    }

    public Boolean isUsedForDetection() {
        return usedForDetection;
    }

    private KeyWords(Boolean usedForDetection, Boolean mendatory, Map<String, String> mapPLabel, Map<String, List<String>> mapALabel, List<String> object, String unit, String comment) {
        this.usedForDetection = usedForDetection.booleanValue();
        this.mendatory = mendatory.booleanValue();
        this.mapPLabel.putAll(mapPLabel);
        this.mapALabel.putAll(mapALabel);
        this.object.addAll(object);
        this.unit = unit;
        this.comment = comment;

    }

    public KeyWords(Boolean withObject, String val) {
        if(withObject) {
            this.usedForDetection = false;
            this.mendatory = false;

            Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(val);
            if(m.find()) {
                this.object.addAll(Arrays.asList(m.group(1).split(";")));
                val = val.replace(m.group(1), "").replaceAll("\\(", "").replaceAll("\\)", "").trim();
            }
            mapPLabel.put("en",val );
            mapPLabel.put("fr",val);
        }
    }
    public KeyWords(Boolean usedForDetection, Boolean mendatory,  String... args) {
            this(args);
            this.usedForDetection = usedForDetection;
            this.mendatory = mendatory;
    }
    private KeyWords(String... args) {
        for(String arg : args) {
            String[] t = arg.split("@");
            if(t.length == 2) {
                String lab = t[0];
                String lang = t[1];
                if(mapPLabel.containsKey(lang)) {
                    if(!mapALabel.containsKey(lang)) {
                        mapALabel.put(lang, new ArrayList<>());
                    }
                    mapALabel.get(lang).add(lab);
                } else {
                    mapPLabel.put(lang, lab);
                }
            }
        }
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getPrefLabel(String lang) {
        return mapPLabel.get(lang);
    }
    public KeyWords newInstance() {
        return new KeyWords(this.usedForDetection, this.mendatory, this.mapPLabel, this.mapALabel, this.object, this.unit, this.comment);
    }


    public boolean isUsedInReplicate() {
        return usedInReplicate;
    }

    public void setUsedInReplicate(boolean usedInReplicate) {
        this.usedInReplicate = usedInReplicate;
    }
}
