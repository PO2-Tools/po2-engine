/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model.dataModel;

import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.Replicate;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.ImportReport;
import fr.inrae.po2engine.utils.Report;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.ObservableList;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ItineraryFile extends GenericFile {

    private GeneralItineraryPart itiPart = null;
    private static Logger logger = LogManager.getLogger(ItineraryFile.class);
    private static List<GenericPart> listRule = List.of();

    public SimpleLongProperty connectedComposant = new SimpleLongProperty(0);

    private ItineraryFile() {};

    public ItineraryFile(GeneralFile processFile) {
        this.nameProperty = new SimpleStringProperty();
        this.data = processFile.getData();
        this.itiPart = new GeneralItineraryPart(processFile, this);
        processFile.addItineraryFile(this);
        this.nameProperty.bind(Bindings.concat(itiPart.getItiNumber(), " (",itiPart.getItiName(),")"));
    }

    public void removeLinkItinerary(Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link) {
        this.itiPart.removeLinkItinerary(link);
        this.calcConnectedComponent();
    }

    public void addLinkItinerary(StepFile father, StepFile son) {
        this.itiPart.addLinkItinerary(father, son);
        this.calcConnectedComponent();
    }

    public void removeLinkItinerary(StepFile father, StepFile son) {
        this.itiPart.removeLinkItinerary(father, son);
        this.calcConnectedComponent();
    }

    public void removeStepFromItinerary(StepFile step) {
        this.itiPart.removeStepFromItinerary(step);
        this.calcConnectedComponent();
    }

    public String getItineraryNumber() {
        return this.itiPart.getItiNumber().get();
    }

    public StringProperty itineraryNumberProperty() {
        return this.itiPart.getItiNumber();
    }

    public String getItineraryName() {
        return this.itiPart.getItiName().get();
    }

    public StringProperty itineraryNameProperty() {
        return this.itiPart.getItiName();
    }

    public void setItineraryName(String itiName) {
        itineraryNameProperty().setValue(itiName);
    }

    public ComplexField getCProductsOfInterest() {
        return this.itiPart.getCProdOfInterest();
    }

    public ArrayList<String> getProductsOfInterest() {
        return this.itiPart.getProductsOfInterest();
    }

    public void setProductsOfInterest(String list) {
        getCProductsOfInterest().setValue(list);
    }

    public void addProductsOfInterest(String product) {
        ArrayList<String> temp = getProductsOfInterest();
        temp.add(product);
        String newVal = temp.stream().collect(Collectors.joining("::"));
        setProductsOfInterest(newVal);
    }

    public void mapPart(DataPart part) {
        this.itiPart.mapPart(part);
        this.calcConnectedComponent();
    }

    public ObservableList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> getItinerary() {
        return this.itiPart.getItinerary();
    }

    public ObservableList<ObservationFile> getListObservation() {
        return this.itiPart.getListObservation();
    }

    public ObservableList<StepFile> getListStep() {
        return this.itiPart.getListStep();
    }

    public boolean contains(StepFile sub) {
        return this.itiPart.contains(sub);
    }

    public void addStep(StepFile s) {
        addLinkItinerary(s, null);
        this.calcConnectedComponent();
    }

    public GeneralFile getProcessFile() {
        return this.itiPart.getProcessFile();
    }

    public void setNodePosition(String uuid, double xCoord, double yCoord) {
        this.itiPart.setNodePosition(uuid, xCoord ,yCoord);
    }

    public String getNodePosition(String uuid) {
        return this.itiPart.getNodePosition(uuid);
    }

    public void clearNodePosition() {
        this.itiPart.clearNodePosition();
    }

    public void addObservation(ObservationFile newFile) {
       this.itiPart.addObservation(newFile);
    }
    public void removeEmptyLink() {

        ArrayList<Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>>> toRemove = new ArrayList<>();
        for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> link : this.itiPart.getItinerary()) {
            if (link.getValue().getValue().getValue() == null && link.getKey().getValue().get() == null) {
                toRemove.add(link);
            }
        }
        this.itiPart.getItinerary().removeAll(toRemove);
        this.calcConnectedComponent();
    }

    public Individual getMyItinerary(int replicateID) {
        return itiPart.getMyItinerary(replicateID);
    }

    public Individual getMyInterval(int replicateID) {
        return itiPart.getMyInterval(replicateID);
    }

    @Override
    public void checkValue() {
        itiPart.checkValue();
    }

    @Override
    public Boolean constructData() {
        return null;
    }

    @Override
    protected List<GenericPart> getListRules() {
        return listRule;
    }

    public Long getConnectedComposant() {
        return connectedComposant.get();
    }

    public SimpleLongProperty connectedComposantProperty() {
        return connectedComposant;
    }

    private void setConnectedComposant(Long connectedComposant) {
        this.connectedComposant.set(connectedComposant);
    }

    @Override
    public void map(DataPartType type, LinkedMap<KeyWords, ObservableObjectValue> data) {

    }

    @Override
    public void unbind() {

    }

    @Override
    public void removeFile() {

    }
    public Individual publish(Ontology ontology, OntClass classProcess, JSONObject json, String uuid, int replicateID) {
        if(getConnectedComposant() > 1) {
            json.getJSONArray("warning").put(this.getNameProperty().getValue() + " has more than 1 connected component" );
        }
        return this.itiPart.publish(ontology, classProcess, json, uuid, replicateID);
    }

    public void cloneFrom(ItineraryFile originalIti, Map<StepFile, StepFile> mappingStep) {
        for (Pair<Pair<ComplexField, ObjectProperty<StepFile>>, Pair<ComplexField, ObjectProperty<StepFile>>> pair : originalIti.getItinerary()) {
            StepFile oFather = pair.getKey().getValue().get();
            StepFile oSon = pair.getValue().getValue().get();
            StepFile cFather = mappingStep.get(oFather);
            StepFile cSon = mappingStep.get(oSon);
            this.addLinkItinerary(cFather, cSon);
        }
        this.setItineraryName("copy " + originalIti.getItineraryName());
        this.setProductsOfInterest(originalIti.getCProductsOfInterest().getValue().getValue());
    }

    public ImportReport checkImportStepFromXLSX(File fileImport){
        StepFile step = new StepFile("new step ini", this.getProcessFile());
        this.addLinkItinerary(step, null);
        ImportReport report = step.realPreConst(fileImport, true);
        step.removeFile();
        return report;
    }

    public StepFile importStepFromXLSX(File fileImport, int replicateID){
        StepFile step = new StepFile("new step ini", this.getProcessFile());
        this.addLinkItinerary(step, null);
        Report report = step.constructDataFromImport(fileImport, true, replicateID);
        if(!report.success()) {
            step.removeFile();
        }
        return step;
    }

    public Report checkUpdateObservationFromXLSX(File fileImport, ObservationFile obs, int replicateID){
        ObservationFile obsTemp = new ObservationFile(this);
        Report report = obsTemp.constructDataFromImport(fileImport, false, replicateID);
        obsTemp.removeFile();
        // checking the uuid
        if(report.success() && !obsTemp.getUuid(true).equals(obs.getUuid(true))) {
            report.addError("You are trying to import the wrong observation");
        }
        return report;
    }
    public Report checkCreateObservationFromXLSX(File fileImport, int replicateID){
        ObservationFile obs = new ObservationFile(this);
        Report report = obs.constructDataFromImport(fileImport, true, replicateID);
        obs.removeFile();
        return report;
    }

    public Report updateObservationFromXLSX(File fileImport, ObservationFile obs, int replicateID){
        Report report = obs.constructDataFromImport(fileImport, false, replicateID);
        return report;
    }
    public ObservationFile createObservationFromXLSX(File fileImport, int replicateID){
        ObservationFile obs = new ObservationFile(this);
        Report report = obs.constructDataFromImport(fileImport, true, replicateID);
        if(!report.success()) {
            obs.removeFile();
        }
        return obs;
    }

    public void clear() {
        this.itiPart.clear();
    }

    public boolean isLinked(StepFile s1, StepFile s2) {
        if(s1 != null && s2 != null) {
            if (this.itiPart.directLinkExist(s1, s2)) {
                return true;
            } else {
                List<StepFile> next = getItinerary().stream().filter(pairPairPair -> pairPairPair.getKey().getValue().getValue() == s1).map(pairPairPair -> pairPairPair.getValue().getValue().getValue()).collect(Collectors.toList());
                for (StepFile ss : next) {
                    if (isLinked(ss, s2)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void calcConnectedComponent() {
        HashMap<StepFile, Integer> ccTable = new HashMap<StepFile, Integer>();
        List<StepFile> listLeaf = getListStep().stream().filter(StepFile::isLeaf).collect(Collectors.toList());
        listLeaf.forEach(s -> ccTable.put(s, -1));
        int ccIndex = 0;
        for(StepFile s : listLeaf) {
            dfs(s, ccTable, ccIndex++);
        }
        setConnectedComposant(ccTable.values().stream().distinct().count());
    }
    private void dfs(StepFile s, HashMap<StepFile, Integer> ccTable, Integer ccIndex) {
        if(ccTable.containsKey(s) && ccTable.get(s) == -1) { // not already done
            ccTable.replace(s, ccIndex);
            // get List of all step linked
            List<StepFile> next = getItinerary().stream().filter(pairPairPair -> pairPairPair.getKey().getValue().getValue() == s && pairPairPair.getValue().getValue().getValue() != null).map(pairPairPair -> pairPairPair.getValue().getValue().getValue()).collect(Collectors.toList());
            next.forEach(sn -> dfs(sn, ccTable, ccIndex));
            List<StepFile> prev = getItinerary().stream().filter(pairPairPair -> pairPairPair.getKey().getValue().getValue() != null && pairPairPair.getValue().getValue().getValue() == s).map(pairPairPair -> pairPairPair.getKey().getValue().getValue()).collect(Collectors.toList());
            prev.forEach(sn -> dfs(sn, ccTable, ccIndex));
        }
    }

    public String getNodePositionAsString() {
        return itiPart.getNodePositionAsString();
    }
}
