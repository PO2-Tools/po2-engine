/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inrae.po2engine.utils.FieldState;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComplexField {
    //private SimpleStringProperty stringProperty = null;
    private SimpleMapProperty<Integer, SimpleStringProperty> valuesMap = null;

    private SimpleStringProperty complexID = null;
    private SimpleStringProperty styleProperty = null;
    private SimpleStringProperty toolTip = new SimpleStringProperty("");
    private SimpleStringProperty statusMessage = new SimpleStringProperty();
    private ObjectProperty<FieldState> state = new SimpleObjectProperty<>();
    private SimpleBooleanProperty isInContrainedList = new SimpleBooleanProperty(false);
    private StringProperty unit = new SimpleStringProperty("");
    private ObservableList<String> listObject = null;
    private SimpleBooleanProperty isMendatory = new SimpleBooleanProperty();
    private SimpleListProperty<VocabConcept> listConstraint = new SimpleListProperty<>(FXCollections.observableArrayList());
    private Boolean editableField = true;
    private Boolean isConstrained = false;
    private Boolean checkValue = true;
    private BooleanProperty isUnique = new SimpleBooleanProperty(true); // utiliser seulement pour les étapes de même type avec id différents
    private ArrayList<String> conceptConstraint = new ArrayList<>();
    private SimpleStringProperty uuidProperty = new SimpleStringProperty("");
    private ChangeListener changeListener;

    public ComplexField() {
//        this.stringProperty = new SimpleStringProperty("");
        this.complexID = new SimpleStringProperty("");
        this.valuesMap = new SimpleMapProperty<>(FXCollections.observableHashMap());
        this.valuesMap.put(0, new SimpleStringProperty(""));
        this.valuesMap.get(0).addListener((observable, oldValue, newValue) -> {
            if(checkValue && newValue != null && !newValue.isEmpty()) {
                String[] ts = newValue.split("::");
                Boolean tb = true;
                for (String s : ts) {
                    if (!listConstraint.stream().anyMatch(x -> x.matchLabel(s))) {
                        tb = false;
                    }
                }
                isInContrainedList.setValue(tb);
            } else {
                isInContrainedList.setValue(true);
            }
        });


        this.styleProperty = new SimpleStringProperty("");
        this.listObject = FXCollections.observableArrayList();

        this.statusMessage.set("");
        this.state.setValue(FieldState.VALID);
        this.isMendatory.setValue(false);

        styleProperty.bind(Bindings.when(stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-table-cell-border-color: red; -fx-text-fill: black; -fx-border-color: red; -fx-opacity: 1;").otherwise(Bindings.when(valuesMap.sizeProperty().greaterThan(1)).then("-fx-table-cell-border-color: DarkSlateBlue; -fx-text-fill: black; -fx-border-color: DarkSlateBlue; -fx-opacity: 1;").otherwise(" -fx-text-fill: black;  -fx-opacity: 1;")));
        addAutoControl();
    }

    public void setValuesListener(ChangeListener l) {
        this.changeListener = l;
        this.valuesMap.values().forEach(ssp -> ssp.addListener(this.changeListener));
    }

    public void checkValue() {
        if(checkValue && !getValue().get().isEmpty()) {
                String[] ts = getValue().get().split("::");
                boolean tb = true;
                for (String s : ts) {
                    if (!listConstraint.stream().anyMatch(x ->x.matchLabel(s))) {
                        tb = false;
                    }
                }
                isInContrainedList.setValue(tb);
            } else {
                isInContrainedList.setValue(true);
            }
    }
    public void unbindReplicatesValues() {
        valuesMap.values().forEach(SimpleStringProperty::unbind);
    }

    public void setCheckValue(boolean check) {
        this.checkValue = check;
    }

    public void parseValue(String value) {
        // prepare the string to be parsed. Need some tweek to prevent bug !!! change the pattern detection
        value = value.replaceAll("\\{\\{", "|{|");
        // need to reverse the string because replaceAll can't start by the end !
        value = Tools.reverseString(value);
        value = value.replaceAll("\\}\\}", "|}|");
        // 2nd reverse to put the string in right way
        value = Tools.reverseString(value);
        // now we can use the regex
        Matcher m = Pattern.compile("\\|\\{\\|(.*?)\\|\\}\\|").matcher(value);
        boolean find = false;
        while(m.find()) {
            find = true;
            Boolean map = false;
            String t = m.group(1);
            Matcher mObject = Pattern.compile("^object=(.+)$").matcher(t);
            Matcher mComment = Pattern.compile("^comment=(.+)$").matcher(t);
            Matcher mUnit = Pattern.compile("^unit=(.+)$").matcher(t);
            Matcher mId = Pattern.compile("^id=(.+)$").matcher(t);
            Matcher mUuid = Pattern.compile("^uuid=(.+)$").matcher(t);

            if(mObject.find()) {
                map = true;
                this.listObject.addAll(Arrays.asList(mObject.group(1).split("\\s*::\\s*")));
            }
            if(mComment.find()) {
                map = true;
                this.toolTip.setValue(this.toolTip.getValue().concat(mComment.group(1).trim()));
            }
            if(mUnit.find()) {
                map = true;
                this.unit.setValue(mUnit.group(1).trim());
            }
            if(mId.find()) {
                map = true;
                this.complexID.setValue(mId.group(1).trim());
            }
            if(mUuid.find()) {
                map = true;
                this.uuidProperty.setValue(mUuid.group(1).trim());
            }
            if(map) {
                value =  value.replace("|{|"+t+"|}|", "");
            }

        }
        if(find) {
            setValue(value.trim());
        } else {
            olderParseValue(value);
        }


    }


    public void olderParseValue(String value) {
        Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(value);
        List<String> objTemp = new ArrayList<>();

        while(m.find()) {
            boolean map = false;
            String t = m.group(1);
            Matcher mObject = Pattern.compile("^object=(.+)$").matcher(t);
            Matcher mComment = Pattern.compile("^comment=(.+)$").matcher(t);
            Matcher mUnit = Pattern.compile("^unit=(.+)$").matcher(t);
            Matcher mId = Pattern.compile("^id=(.+)$").matcher(t);
            if(mObject.find()) {
                map = true;
                this.listObject.addAll(Arrays.asList(mObject.group(1).split("\\s*::\\s*")));
            }
            if(mComment.find()) {
                map = true;
                this.toolTip.setValue(this.toolTip.getValue().concat(mComment.group(1).trim()));
            }
            if(mUnit.find()) {
                map = true;
                this.unit.setValue(mUnit.group(1).trim());
            }
            if(mId.find()) {
                map = true;
                this.complexID.setValue(mId.group(1).trim());
            }
            if(map) {
                value =  value.replace("("+t+")", "");
            }

        }
        setValue(value.trim());
    }

    private void addAutoControl() {
        BooleanBinding mand = Bindings.when(isMendatory).then(getValue().isEmpty().not()).otherwise(true);
        BooleanBinding list = Bindings.when(listConstraint.emptyProperty().not()).then(isInContrainedList).otherwise(true);
        ObjectBinding<FieldState> control= Bindings.when(Bindings.and(mand, list).and(isUnique)).then(FieldState.VALID).otherwise(FieldState.ERROR);

        this.state.bind(control);
    }

    public BooleanProperty isUniqueProperty() {
        return isUnique;
    }

    public ComplexField(String init_value, String toolTip) {
        this(init_value);
        this.toolTip.setValue(toolTip);
    }
    public ComplexField(String init_value) {
        this();
        parseValue(init_value);
    }


    public StringProperty getUnit() {
        return unit;
    }

    public Boolean isEditable() {
        return this.editableField;
    }

    public Boolean isConstained() {
        return this.isConstrained;
    }
    public void setMendatory(Boolean isMendatory) {
        this.isMendatory.setValue(isMendatory);
    }

    public ObservableList<String> getListObject() {
        return listObject;
    }

    public void setState(FieldState state, String statusMessage) {
        this.state.setValue(state);
        this.statusMessage.setValue(statusMessage);
    }

    public SimpleListProperty<VocabConcept> getListConstraint() {
        return listConstraint;
    }

    public void setListConstraint(ObservableList<VocabConcept> contraint) {
        Bindings.bindContent(listConstraint, contraint);
    }

    public void setConceptConstraint(String...concepts) {
        for(String c : concepts) {
            conceptConstraint.add(c);
        }
    }

    public ArrayList<String> getConceptConstraint() {
        return conceptConstraint;
    }

    public ObjectProperty<FieldState> stateProperty() {
        return state;
    }

    public void setValue(String value) {
        HashMap<Integer, String> listRep = Tools.convertReplicate(value);
        listRep.forEach(this::setValueWithReplicate);
    }

    public void setValueWithReplicate(int replicateID, String value ) {
        if(!valuesMap.containsKey(replicateID)) {
            valuesMap.put(replicateID, new SimpleStringProperty());
            if(this.changeListener != null) {
                valuesMap.get(replicateID).addListener(this.changeListener);
            }
        }
        valuesMap.get(replicateID).set(value);
    }

    public SimpleStringProperty getValue() {
        return getValueWithReplicate(0);
    }

    public ObservableMap<Integer, SimpleStringProperty> getValues() {
        return valuesMap;
    }
    public SimpleStringProperty getValueWithReplicate(int replicateId) {
        if(!valuesMap.containsKey(replicateId)) {
            setValueWithReplicate(replicateId, "");
        }
        return valuesMap.get(replicateId);
    }

    public boolean containsReplicate() {
        return valuesMap.keySet().size() > 1;
    }

    /**
     *
     * @return a String representation of all values. A simple string for a single value, a jsonarray for pultiple values
     */
    public String getValuesAsString() {
        if(valuesMap.size() == 1) {
            return getValue().get();
        } else {
            JSONArray arr = new JSONArray();
            valuesMap.forEach((i, s) -> {
                JSONObject obj = new JSONObject();
                obj.put("id", i);
                obj.put("value", s.get());
                arr.put(obj);
            });
            return arr.toString();
        }

    }

    public SimpleStringProperty getID() { return complexID;}

    public SimpleStringProperty getUuidProperty() { return uuidProperty;}

    public String toString() {
        if(valuesMap.size() == 1) {
            return valuesMap.get(0).get();
        }
        else {
            List<Map<String, Object>> list = new ArrayList<>();
            for (Map.Entry<Integer, SimpleStringProperty> entry : valuesMap.entrySet()) {
                list.add(Map.of("id", entry.getKey(), "value", entry.getValue().get()));
            }
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.writeValueAsString(list);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void setEditable(boolean editable) {
        this.editableField = editable;
    }

    public void setIsConstrained(boolean constrained) {
        this.isConstrained = constrained;
    }

    public StringProperty styleProperty() {
        return styleProperty;
    }

    public StringProperty getTooltip() {
        return toolTip;
    }

    public void setId(String s) {
        this.complexID.setValue(s);
    }

    public void cloneFrom(ComplexField complexField) {
        complexField.valuesMap.forEach((integer, simpleStringProperty) -> this.setValueWithReplicate(integer, simpleStringProperty.getValue()));
        this.getTooltip().setValue(complexField.getTooltip().get());
        this.getUnit().setValue(complexField.getUnit().get());
        this.getID().set(complexField.getID().get());
        this.getListObject().addAll(complexField.getListObject());
    }

    public void cleanReplicateValues(List<Integer> list) {
        // != 0 to prevent removing value for default replicate
        valuesMap.entrySet().removeIf(repIDValue -> repIDValue.getKey() != 0 && !list.contains(repIDValue.getKey()));
    }
}
