/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;


import fr.inrae.po2engine.vocabsearch.Cible;
import fr.inrae.po2engine.vocabsearch.Language;
import fr.inrae.po2engine.vocabsearch.TypeCible;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Platform {
    private static HashMap<String, List<PlatformMaterial>> listPlatform = new HashMap<>();

    public static HashMap<String, List<PlatformMaterial>> getListPlatform() {
        return listPlatform;
    }

    public static void init(InputStream config) {
        String jsonFile;
        listPlatform.clear();
        try {
            jsonFile = IOUtils.toString(config, StandardCharsets.UTF_8);

            JSONArray array = new JSONArray(jsonFile);
            int n = array.length();
            for (int i = 0; i < n; i++) {
                List<PlatformMaterial> listMat = new ArrayList<>();
                JSONObject platform = array.getJSONObject(i);
                String platformName = platform.optString("name");
                listPlatform.put(platformName, listMat);
                JSONArray listMatJson = platform.getJSONArray("material");
                for(int m = 0; m < listMatJson.length(); m++) {
                    PlatformMaterial newMat = new PlatformMaterial();
                    newMat.setName(listMatJson.getJSONObject(m).optString("name"));
                    newMat.setType(listMatJson.getJSONObject(m).optString("type"));
                    // characteristic
                    JSONArray charac = listMatJson.getJSONObject(m).optJSONArray("characteristic");
                    if(charac != null) {
                        for (int c = 0; c < charac.length(); c++) {
                            MaterialAttr att = new MaterialAttr();
                            att.setAttr(charac.getJSONObject(c).optString("attr"));
                            att.setValue(charac.getJSONObject(c).optString("value"));
                            att.setUnit(charac.getJSONObject(c).optString("unit"));
                            att.setComment(charac.getJSONObject(c).optString("comment"));
                            att.setObject(charac.getJSONObject(c).optString("object"));
                            newMat.addCharacteristic(att);
                        }
                    }

                    // parameter
                    JSONArray param = listMatJson.getJSONObject(m).optJSONArray("parameter");
                    if(param != null) {
                        for (int p = 0; p < param.length(); p++) {
                            MaterialAttr att = new MaterialAttr();
                            att.setAttr(param.getJSONObject(p).optString("attr"));
                            att.setValue(param.getJSONObject(p).optString("value"));
                            att.setUnit(param.getJSONObject(p).optString("unit"));
                            att.setComment(param.getJSONObject(p).optString("comment"));
                            att.setObject(param.getJSONObject(p).optString("object"));
                            newMat.addParameter(att);
                        }
                    }

                    listMat.add(newMat);
                }
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

}

