/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import java.util.ArrayList;

public class Report {
    ArrayList<String> errorMessage;
    ArrayList<String> warningMessage;
    ArrayList<String> infosMessage;

    public Report() {
        errorMessage = new ArrayList<>();
        warningMessage = new ArrayList<>();
        infosMessage = new ArrayList<>();
    }

    public Boolean success() {
        return (errorMessage.isEmpty());
    }

    public ArrayList<String> getError() {
        return this.errorMessage;
    }

    public ArrayList<String> getWarning() {
        return this.warningMessage;
    }

    public ArrayList<String> getInfos() {
        return this.infosMessage;
    }

    @Override
    public String toString() {
        return this.errorMessage.toString();
    }

    public String prettyPrintError() {
        return String.join("\n", this.errorMessage);
    }

    public String prettyPrintWarning() {
        return String.join("\n", this.warningMessage);
    }

    public String prettyPrintInfos() {
        return String.join("\n", this.infosMessage);
    }

    public void addError(String error) {
        this.errorMessage.add(error);
    }

    public void addWarning(String warning) {
        this.warningMessage.add(warning);
    }

    public void addInfo(String info) {
        this.infosMessage.add(info);
    }
}
