/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import fr.inrae.po2engine.model.dataModel.KeyWords;

import java.util.ArrayList;
import java.util.List;

public class DataPartType {
    public final static KeyWords INIT = new KeyWords(false, false, "init@en", "init@fr");
    public final static KeyWords UNDEFINED = new KeyWords(false, false, "undefined@en", "non défini@fr");
    public final static KeyWords GENERAL_INFO = new KeyWords(false, false, "generals informations@en", "informations générales@fr");
    public final static KeyWords PRODUCT_DESC = new KeyWords(false, false, "product description@en", "description du produit@fr");
    public final static KeyWords MATERIAL_USED = new KeyWords(false, false, "material used@en", "matériel utilisé@fr");
    public final static KeyWords METHOD_USED = new KeyWords(false, false, "method used@en", "méthode utilisée@fr");
    public final static KeyWords RAW_DATA = new KeyWords(false, false, "raw data@en", "données brutes@fr");
    public final static KeyWords CALC_DATA = new KeyWords(false, false, "calc data@en", "données calculées@fr");
    public final static KeyWords MATERIAL_RAW = new KeyWords(false, false, "material@en", "matériel@fr");
    public final static KeyWords METHOD_RAW = new KeyWords(false, false, "method@en", "méthode@fr");
    public final static KeyWords ITINERARY = new KeyWords(false, false, "itinerary@en", "itinéraire@fr");
    public final static KeyWords AGENT = new KeyWords(false, false, "agent@en", "agent@fr");
    public final static KeyWords CONSIGN = new KeyWords(false, false, "guideline@en", "guidelines@en", "consigne@fr", "consignes@fr");
    public final static KeyWords COMPOSITION_IMPORT = new KeyWords(false, false, "composition import@en");




    public final static List<KeyWords> listKeywords = new ArrayList<KeyWords>() {{
        add(INIT);
        add(UNDEFINED);
        add(GENERAL_INFO);
        add(PRODUCT_DESC);
        add(MATERIAL_USED);
        add(METHOD_USED);
        add(RAW_DATA);
        add(CALC_DATA);
        add(MATERIAL_RAW);
        add(METHOD_RAW);
        add(ITINERARY);
        add(AGENT);
        add(CONSIGN);
    }};

//    INIT,  // special value. Identification not yes started
//    NOT_FOUND,
//    META_DATA,
//    RAW_DATA,
//    CALC_DATA,
//    ITINERARY,
//    MATERIAL_RAW_DATA,
//    METHOD_RAW_DATA,
//    PROCESS_DESC;

}
