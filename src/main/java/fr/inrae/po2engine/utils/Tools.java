/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.model.partModel.ProjectAgentPart;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.taskdefs.Manifest;
import org.apache.tools.ant.taskdefs.ManifestException;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.eclipse.rdf4j.common.net.ParsedIRI;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import systems.uom.ucum.format.UCUMFormat;

import javax.measure.Unit;
import javax.measure.UnitConverter;
import javax.measure.format.UnitFormat;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Tools {

    public static final Map<String, String> prefixName;
    public static final Map<String, String> unitName;
    private static final Map<Character, Character> brackets = new HashMap<>();
    public static String path = System.getProperty("user.home") + File.separator + "PO2VocabManager" + File.separator;
    public static String dataPath = path + "Data" + File.separator;
    public static String ontoPath = path + "Ontologies" + File.separator;
    private static String regInterval = "^([\\[\\]])[ ]?([-+]?(?:(?:[0-9]+(?:[.,][0-9]+)?(?:[eE][-+]?[0-9]+)?))|-inf)[ ]?;[ ]?([-+]?(?:(?:[0-9]+(?:[.,][0-9]+)?(?:[eE][-+]?[0-9]+)?))|[+]?inf)[ ]?([\\[\\]])$";
    private static String regMoyEcar = "^\\[\\[[ ]?([-+]?[0-9]+(?:[.,][0-9]+)?(?:[eE][-+]?[0-9]+)?)[ ]?;[ ]?([-+]?[0-9]+(?:[.,][0-9]+)?(?:[eE][-+]?[0-9]+)?)[ ]?\\]\\]$";
    private static String regScalaire = "^([-+]?(?:(?:[0-9]+(?:[.,][0-9]+)?(?:[eE][-+]?[0-9]+)?))|[-+]?inf)$";
    private static ExecutorService uniqueExecutor = Executors.newFixedThreadPool(1);
    private static ObservableMap<String, ProgressPO2> listProgress = FXCollections.observableHashMap();
    private static Logger logger = LogManager.getLogger(Tools.class);
    private static Manifest manifest;
    private static String seed;
    private static Pattern patternRegScalaire = Pattern.compile(regScalaire);
    private static Pattern patternRegInterval = Pattern.compile(regInterval);
    private static Pattern patternRegMoyEcar = Pattern.compile(regMoyEcar);

    static {
        try {
            Enumeration<URL> listResources = Tools.class.getClassLoader().getResources("resources/manifest");
            listResources.asIterator().forEachRemaining(url -> {
                try {
                    InputStream is = url.openStream();
                    Manifest temp = new Manifest(new InputStreamReader(is));
                    Manifest.Section section = temp.getSection("app_info");
                    if(section != null && section.getAttribute("Specification-Title").getValue().equalsIgnoreCase("PO2Engine")) {
                        manifest = temp;
                    }
                } catch (ManifestException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    };


    static {
        brackets.put('[', ']');
        brackets.put('{', '}');
        brackets.put('(', ')');
    }

    static {
        prefixName = new LinkedHashMap<String, String>();
        prefixName.put("Y", "yotta");
        prefixName.put("Z", "zetta");
        prefixName.put("E", "exa");
        prefixName.put("P", "peta");
        prefixName.put("T", "tera");
        prefixName.put("G", "giga");
        prefixName.put("M", "mega");
        prefixName.put("k", "kilo");
        prefixName.put("h", "hecto");
        prefixName.put("da", "deka");
        prefixName.put("d", "deci");
        prefixName.put("c", "centi");
        prefixName.put("m", "milli");
        prefixName.put("u", "micro");
        prefixName.put("n", "nano");
        prefixName.put("p", "pico");
        prefixName.put("f", "femto");
        prefixName.put("a", "atto");
        prefixName.put("z", "zepto");
        prefixName.put("y", "yocto");
    }

    static {
        unitName = new HashMap<String, String>();
        unitName.put("[dpt_us]", "dry pint");
        unitName.put("sph", "sphere");
        unitName.put("[min_br]", "minim");
        unitName.put("[ppm]", "parts per million");
        unitName.put("Pa", "pascal");
        unitName.put("rad", "radian");
        unitName.put("Bd", "baud");
        unitName.put("g%", "gram percent");
        unitName.put("[psi]", "pound per sqare inch");
        unitName.put("gon", "gon, grade");
        unitName.put("%", "percent");
        unitName.put("L", "liter");
        unitName.put("[min_us]", "minim");
        unitName.put("[dqt_us]", "dry quart");
        unitName.put("[Btu_m]", "mean British thermal unit");
        unitName.put("[fdr_us]", "fluid dram");
        unitName.put("[tbs_us]", "tablespoon");
        unitName.put("[gr]", "grain");
        unitName.put("mho", "mho");
        unitName.put("cal_[15]", "calorie at 15 °C");
        unitName.put("A", "ampère");
        unitName.put("[pnt_pr]", "Printer's point");
        unitName.put("C", "coulomb");
        unitName.put("F", "farad");
        unitName.put("eV", "electronvolt");
        unitName.put("H", "henry");
        unitName.put("J", "joule");
        unitName.put("[gal_us]", "Queen Anne's wine gallon");
        unitName.put("[pied]", "pied, French foot");
        unitName.put("K", "kelvin");
        unitName.put("N", "newton");
        unitName.put("[srd_us]", "square rod");
        unitName.put("S", "siemens");
        unitName.put("T", "tesla");
        unitName.put("V", "volt");
        unitName.put("W", "watt");
        unitName.put("[oz_ap]", "ounce");
        unitName.put("Gb", "Gilbert");
        unitName.put("[ppb]", "parts per billion");
        unitName.put("sr", "steradian");
        unitName.put("cd", "candela");
        unitName.put("deg", "degree");
        unitName.put("[pca]", "pica");
        unitName.put("[Btu_59]", "British thermal unit at 59 °F");
        unitName.put("[ft_i]", "foot");
        unitName.put("[lb_tr]", "pound");
        unitName.put("m", "meter");
        unitName.put("Bq", "becquerel");
        unitName.put("s", "second");
        unitName.put("[fth_us]", "fathom");
        unitName.put("Ci", "Curie");
        unitName.put("circ", "circle");
        unitName.put("mo_s", "synodal month");
        unitName.put("R", "Roentgen");
        unitName.put("Sv", "sievert");
        unitName.put("[Btu_39]", "British thermal unit at 39 °F");
        unitName.put("[PRU]", "peripheral vascular resistance unit");
        unitName.put("[G]", "Newtonian constant of gravitation");
        unitName.put("Ao", "Ångström");
        unitName.put("Bi", "Biot");
        unitName.put("sb", "stilb");
        unitName.put("[crd_us]", "cord");
        unitName.put("ph", "phot");
        unitName.put("[acr_us]", "acre");
        unitName.put("[c]", "velocity of light");
        unitName.put("[sct]", "section");
        unitName.put("Gal", "Gal");
        unitName.put("[fdr_br]", "fluid dram");
        unitName.put("[e]", "elementary charge");
        unitName.put("[qt_us]", "quart");
        unitName.put("[ch_us]", "Gunter's chain, Surveyor's chain");
        unitName.put("min", "minute");
        unitName.put("cal_th", "thermochemical calorie");
        unitName.put("pc", "parsec");
        unitName.put("[ligne]", "ligne, French line");
        unitName.put("a", "year");
        unitName.put("t", "tonne");
        unitName.put("[k]", "Boltzmann constant");
        unitName.put("[Cal]", "nutrition label Calories");
        unitName.put("[h]", "Planck constant");
        unitName.put("Cel", "degree Celsius");
        unitName.put("bar", "bar");
        unitName.put("[mil_i]", "mil");
        unitName.put("Oe", "Oersted");
        unitName.put("l", "liter");
        unitName.put("[mi_br]", "mile");
        unitName.put("[lton_av]", "long ton, British ton");
        unitName.put("[rd_us]", "rod");
        unitName.put("[foz_us]", "fluid ounce");
        unitName.put("[g]", "standard acceleration of free fall");
        unitName.put("[dr_ap]", "dram, drachm");
        unitName.put("AU", "astronomic unit");
        unitName.put("G", "Gauss");
        unitName.put("[m_p]", "proton mass");
        unitName.put("Mx", "Maxwell");
        unitName.put("'", "minute (angle)");
        unitName.put("[lk_br]", "link for Gunter's chain");
        unitName.put("Wb", "weber");
        unitName.put("[mi_i]", "mile");
        unitName.put("[fur_us]", "furlong");
        unitName.put("[sc_ap]", "scruple");
        unitName.put("ar", "are");
        unitName.put("[eps_0]", "permittivity of vacuum");
        unitName.put("RAD", "radiation absorbed dose");
        unitName.put("[cyd_i]", "cubic yard");
        unitName.put("[rd_br]", "rod");
        unitName.put("[HP]", "horsepower");
        unitName.put("[ft_br]", "foot");
        unitName.put("[gal_br]", "gallon");
        unitName.put("[nmi_i]", "nautical mile");
        unitName.put("[syd_i]", "square yard");
        unitName.put("a_t", "tropical year");
        unitName.put("[lcwt_av]", "long hunderdweight, British hundredweight");
        unitName.put("[pca_pr]", "Printer's pica");
        unitName.put("[in_us]", "inch");
        unitName.put("[kn_br]", "knot");
        unitName.put("[bu_us]", "bushel");
        unitName.put("u", "unified atomic mass unit");
        unitName.put("mol", "mole");
        unitName.put("bit", "bit");
        unitName.put("[rch_us]", "Ramden's chain, Engineer's chain");
        unitName.put("Gy", "gray");
        unitName.put("Lmb", "Lambert");
        unitName.put("[acr_br]", "acre");
        unitName.put("[mi_us]", "mile");
        unitName.put("[stone_av]", "stone, British stone");
        unitName.put("dyn", "dyne");
        unitName.put("[lne]", "line");
        unitName.put("By", "byte");
        unitName.put("[foz_br]", "fluid ounce");
        unitName.put("cal_m", "mean calorie");
        unitName.put("[qt_br]", "quart");
        unitName.put("[lbf_av]", "pound force");
        unitName.put("[bu_br]", "bushel");
        unitName.put("[dr_av]", "dram");
        unitName.put("Hz", "hertz");
        unitName.put("[ch_br]", "Gunter's chain");
        unitName.put("[oz_tr]", "ounce");
        unitName.put("[pk_br]", "peck");
        unitName.put("[fth_i]", "fathom");
        unitName.put("[tsp_us]", "teaspoon");
        unitName.put("[m_e]", "electron mass");
        unitName.put("Ky", "Kayser");
        unitName.put("[Btu_IT]", "international table British thermal unit");
        unitName.put("att", "technical atmosphere");
        unitName.put("St", "Stokes");
        unitName.put("[cup_us]", "cup");
        unitName.put("[pwt_tr]", "pennyweight");
        unitName.put("[hd_i]", "hand");
        unitName.put("atm", "standard atmosphere");
        unitName.put("[kn_i]", "knot");
        unitName.put("mo_g", "mean Gregorian month");
        unitName.put("h", "hour");
        unitName.put("[gil_br]", "gill");
        unitName.put("[pt_br]", "pint");
        unitName.put("erg", "erg");
        unitName.put("P", "Poise");
        unitName.put("[ly]", "light-year");
        unitName.put("[cin_i]", "cubic inch");
        unitName.put("[Btu_60]", "British thermal unit at 60 °F");
        unitName.put("[pptr]", "parts per trillion");
        unitName.put("[sin_i]", "square inch");
        unitName.put("[pH]", "pH");
        unitName.put("lm", "lumen");
        unitName.put("''", "second (angle)");
        unitName.put("[rlk_us]", "link for Ramden's chain");
        unitName.put("[in_br]", "inch");
        unitName.put("[scwt_av]", "short hundredweight, U.S. hundredweight");
        unitName.put("lx", "lux");
        unitName.put("[lb_av]", "pound");
        unitName.put("cal_IT", "international table calorie");
        unitName.put("[fth_br]", "fathom");
        unitName.put("[didot]", "didot, Didot's point");
        unitName.put("cal_[20]", "calorie at 20 °C");
        unitName.put("[oz_av]", "ounce");
        unitName.put("[Btu]", "British thermal unit");
        unitName.put("[pc_br]", "pace");
        unitName.put("a_g", "mean Gregorian year");
        unitName.put("[pouce]", "pouce, French inch");
        unitName.put("[ppth]", "parts per thousand");
        unitName.put("[ft_us]", "foot");
        unitName.put("Ohm", "ohm");
        unitName.put("[car_m]", "metric carat");
        unitName.put("[pk_us]", "peck");
        unitName.put("g", "gram");
        unitName.put("[lb_ap]", "pound");
        unitName.put("[gal_wi]", "historical winchester gallon");
        unitName.put("[pt_us]", "pint");
        unitName.put("[pnt]", "point");
        unitName.put("b", "barn");
        unitName.put("[bbl_us]", "barrel");
        unitName.put("[mu_0]", "permeability of vacuum");
        unitName.put("[cicero]", "cicero, Didot's pica");
        unitName.put("gf", "gram-force");
        unitName.put("[in_i]", "inch");
        unitName.put("[bf_i]", "board foot");
        unitName.put("[yd_us]", "yard");
        unitName.put("d", "day");
        unitName.put("[car_Au]", "carat of gold alloys");
        unitName.put("[degF]", "degree Fahrenheit");
        unitName.put("[ston_av]", "short ton, U.S. ton");
        unitName.put("[pi]", "the number pi ");
        unitName.put("[cml_i]", "circular mil");
        unitName.put("mo", "month");
        unitName.put("[twp]", "township");
        unitName.put("[yd_i]", "yard");
        unitName.put("[sft_i]", "square foot");
        unitName.put("[gil_us]", "gill");
        unitName.put("[nmi_br]", "nautical mile");
        unitName.put("[cft_i]", "cubic foot");
        unitName.put("st", "stere");
        unitName.put("[yd_br]", "yard");
        unitName.put("[mil_us]", "mil");
        unitName.put("eq", "equivalents");
        unitName.put("osm", "osmole");
        unitName.put("wk", "week");
        unitName.put("a_j", "mean Julian year");
        unitName.put("mo_j", "mean Julian month");
        unitName.put("REM", "radiation equivalent man");
        unitName.put("[cr_i]", "cord");
        unitName.put("[lk_us]", "link for Gunter's chain");
        unitName.put("[smi_us]", "square mile");
        unitName.put("cal", "calorie");
        unitName.put("[Btu_th]", "thermochemical British thermal unit");
        unitName.put("bit_s", "bit");
        unitName.put("m[H2O]", "meter of water column");
        unitName.put("[in_i'H2O]", "inch of water column");
        unitName.put("m[Hg]", "meter of mercury column");
        unitName.put("[in_i'Hg]", "inch of mercury column");
        unitName.put("[wood'U]", "Wood unit");
        unitName.put("[drp]", "drop");
        unitName.put("kat", "katal");
        unitName.put("U", "Unit");
        unitName.put("[iU]", "international unit");
        unitName.put("[IU]", "international unit");
        unitName.put("Np", "neper");
        unitName.put("B", "bel");
        unitName.put("B[kW]", "bel kilowatt");
        unitName.put("[smoot]", "Smoot");
        unitName.put("[S]", "Svedberg unit");
    }

    private Tools() {
    }

    public static XSSFSheet createSheet(XSSFWorkbook book, String sheetName) {
        String newSheetName = sheetName;
        if(newSheetName.length() > 31) {
            newSheetName = newSheetName.substring(0, 31);
        }
        boolean nameIsOk = false;
        int indexForFile = 0;
        while(!nameIsOk) {
            nameIsOk = true;

            for (Iterator<Sheet> iteS = book.sheetIterator(); iteS.hasNext(); ) {
                Sheet s = iteS.next();
                if(s.getSheetName().equalsIgnoreCase(newSheetName)) {
                    nameIsOk = false;
                    indexForFile++;
                    int lenghtToRemove = 1 + String.valueOf(indexForFile).length();
                    int sub = Math.min(31-lenghtToRemove, newSheetName.length());
                    newSheetName = newSheetName.substring(0, sub) + "-"+indexForFile;
                }
            }
        }
        return book.createSheet(newSheetName);
    }

    /**
     *  Get the used heap memory in percentage
     * @return the used heap space in percentage
     */
    public static double getUsedMemory() {
        MemoryUsage heapMemoryUsage = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        return  (heapMemoryUsage.getUsed() * 1.0 / heapMemoryUsage.getMax()) * 100;
    }

    public static String reverseString(String stringToReverse) {
        StringBuilder sb = new StringBuilder();
        sb.append(stringToReverse);
        return sb.reverse().toString();
    }

    public static String agentToString(HashMap<KeyWords, ComplexField> agent) {
        if (agent == null)
            return "";
        String agentName = agent.get(ProjectAgentPart.agentFamilyNameK) + "  " + agent.get(ProjectAgentPart.agentGivenNameK);
        return agent.get(ProjectAgentPart.agentOrganisationK) + (!agentName.isBlank() ? (" / "+ agentName) : "");
    }

    public static HashMap<KeyWords, ComplexField> createFakeAgentFromString(String agent) {
        HashMap<KeyWords, ComplexField> fakeAgent = new HashMap<>();
        fakeAgent.put(ProjectAgentPart.agentOrganisationK, new ComplexField(agent));
        fakeAgent.put(ProjectAgentPart.agentFamilyNameK,new ComplexField(""));
        fakeAgent.put(ProjectAgentPart.agentGivenNameK,new ComplexField(""));
        fakeAgent.put(ProjectAgentPart.agentMailK,new ComplexField(""));
        return fakeAgent;
    }


    public static String getEngineVersion() {
        Manifest.Section section = manifest.getSection("app_info");
        Manifest.Attribute attrVersion = section.getAttribute("Specification-Version");

        return attrVersion.getValue();
    }

    public static void initProgress() {
        listProgress = FXCollections.observableHashMap();
    }

    public static void addProgress(String progressName, String message) {
        ProgressPO2 newBar = new ProgressPO2(progressName, message);
        listProgress.put(progressName, newBar);
    }

    public static ProgressPO2 getProgress(String progressName) {
        return listProgress.get(progressName);
    }

    public static ObservableMap<String, ProgressPO2> getListProgress() {
        return listProgress;
    }
    public static void updateProgress(String progressType, String message, Double progress) {
        if(!listProgress.containsKey(progressType)) {
            addProgress(progressType, "");
        }
        if(listProgress.containsKey(progressType)) {
            if(message != null) {
                listProgress.get(progressType).setText(message);
            }
            if(progress != null) {
                listProgress.get(progressType).setProgress(progress);
            }
        }
    }

    public static void updateProgress(String progressType, String message) {
        if(!listProgress.containsKey(progressType)) {
            addProgress(progressType, "");
        }
        if(listProgress.containsKey(progressType)) {
            if(message != null) {
                listProgress.get(progressType).setText(message);
            }
        }
    }

    public static void updateProgress(String progressType, Double progress) {
        if(!listProgress.containsKey(progressType)) {
            addProgress(progressType, "");
        }
        if(listProgress.containsKey(progressType)) {
            if(progress != null) {
                listProgress.get(progressType).setProgress(progress);
            }
        }
    }

    public static void delProgress(String progressName) {
        listProgress.remove(progressName);
    }

    public static boolean isReplicateFormat(String value) {
        AtomicBoolean jsonFormat = new AtomicBoolean(true);
        try {
            JSONArray test = new JSONArray(value);
            test.forEach(o -> {
                if (!(o instanceof JSONObject)) {
                    jsonFormat.set(false); // L'élément n'est pas un objet JSON
                } else {
                    JSONObject obj = (JSONObject) o;
                    if (obj.optInt("id", -1) == -1) jsonFormat.set(false);
                    if ("DefaultValueIfNoKeyInObject".equals(obj.optString("value", "DefaultValueIfNoKeyInObject")))
                        jsonFormat.set(false);
                }
            });
        } catch (JSONException e) {
            jsonFormat.set(false);
        }
        return jsonFormat.get();
    }

    public static HashMap<Integer, String> convertReplicate(String value) {
        HashMap<Integer, String> result = new HashMap<>();
        if(value != null) {
            if(isReplicateFormat(value)) {
                JSONArray stringReplicate = new JSONArray(value);
                stringReplicate.forEach(o -> {
                    JSONObject obj = (JSONObject) o;
                    result.put(obj.getInt("id"), obj.getString("value"));
                });
            } else {
                result.put(0, value);
            }
        }
        return result;
    }

    public static String generateUniqueTimeStamp() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return String.valueOf(Calendar.getInstance().getTimeInMillis());
    }

    public static void addThreadToRun(Runnable t) {
        uniqueExecutor.submit(t);
    }

    public static void shutDownExecutor() {
        if(uniqueExecutor != null && !uniqueExecutor.isShutdown()) {
            uniqueExecutor.shutdown();
        }
    }

    public static Pair<Double, Double> getQuantValueAsMinMax(String value, JSONObject json) {
        DecimalFormat df = new DecimalFormat("#.000");

        Double minValue = null;
        Double maxValue = null;

        Boolean foundFormat = false;

        Matcher mScalar = patternRegScalaire.matcher(value);

        if (mScalar.find()) {
            foundFormat = true;
            try {
                Double val = Double.parseDouble(mScalar.group(1).replaceAll(",","."));
                minValue = val;
                maxValue = val;
            } catch (NumberFormatException e) {
                Double newVal = Double.NaN;
                if(mScalar.group(1).equalsIgnoreCase("inf") || mScalar.group(1).equalsIgnoreCase("+inf")) {
                    newVal = Double.POSITIVE_INFINITY;
                } else {
                    if (mScalar.group(1).equalsIgnoreCase("-inf")) {
                        newVal = Double.NEGATIVE_INFINITY;
                    } else {
                        json.getJSONArray("error").put("Bad quantity value : " + value);
                    }
                }
                minValue = newVal;
                maxValue = newVal;
            }
        }

        Matcher mInterval = patternRegInterval.matcher(value);
        if (mInterval.find()) {
            foundFormat = true;
            try {
                Double val = Double.parseDouble(mInterval.group(2).replaceAll(",","."));
                minValue = val;
            } catch (NumberFormatException e) {
                Double newVal = Double.NaN;

                if(mInterval.group(2).equalsIgnoreCase("inf") || mInterval.group(2).equalsIgnoreCase("+inf")) {
                    newVal = Double.POSITIVE_INFINITY;
                } else {
                    if (mInterval.group(2).equalsIgnoreCase("-inf")) {
                        newVal = Double.NEGATIVE_INFINITY;
                    } else {
                        json.getJSONArray("error").put("Bad quantity value : " + value);
                    }
                }
                minValue = newVal;
            }

            try {
                Double val = Double.parseDouble(mInterval.group(3).replaceAll(",","."));
                maxValue = val;
            } catch (NumberFormatException e) {
                Double newVal = Double.NaN;
                if(mInterval.group(3).equalsIgnoreCase("inf") || mInterval.group(3).equalsIgnoreCase("+inf")) {
                    newVal = Double.POSITIVE_INFINITY;
                } else {
                    if (mInterval.group(3).equalsIgnoreCase("-inf")) {
                        newVal = Double.NEGATIVE_INFINITY;
                    } else {
                        json.getJSONArray("error").put("Bad quantity value : " + value);
                    }
                }
                maxValue = newVal;
            }
        }

        Matcher mMeanSD = patternRegMoyEcar.matcher(value);
        if (mMeanSD.find()) {
            foundFormat = true;
            try {
                Double mean = Double.parseDouble(mMeanSD.group(1).replaceAll(",","."));
                Double sd = Double.parseDouble(mMeanSD.group(2).replaceAll(",","."));
                minValue = mean - (2 * sd);
                maxValue = mean + (2 * sd);
            } catch (NumberFormatException e) {
                json.getJSONArray("error").put("Bad quantity value : " + value);
                minValue = Double.NaN;
                maxValue = Double.NaN;
            }
        }
        if(!foundFormat) {
            json.getJSONArray("error").put("Bad quantity value : " + value);
            minValue = Double.NaN;
            maxValue = Double.NaN;
        }
        return new MutablePair<Double, Double>(minValue, maxValue);
    }

    public static Pair<Pair<String, String>, Pair<String, String>> getQuantValueAsFuzzy(String value, JSONObject json) {
        DecimalFormat df = new DecimalFormat("00.###E0");

        String minKernel = "";
        String maxKernel = "";
        String minSupport = "";
        String maxSupport = "";

        Pattern patternRegScalaire = Pattern.compile(regScalaire);
        Pattern patternRegInterval = Pattern.compile(regInterval);
        Pattern patternRegMoyEcar = Pattern.compile(regMoyEcar);

        Boolean foundFormat = false;

        Matcher mScalar = patternRegScalaire.matcher(value);

        if (mScalar.find()) {
            foundFormat = true;
            try {
                Double val = Double.parseDouble(mScalar.group(1).replaceAll(",","."));
                minKernel = df.format(val);
                maxKernel = df.format(val);
                minSupport = df.format(val);
                maxSupport = df.format(val);
            } catch (NumberFormatException e) {
                String newVal = "NAN";
                if (mScalar.group(1).equalsIgnoreCase("inf") || mScalar.group(1).equalsIgnoreCase("-inf")) {
                    newVal = mScalar.group(1).toUpperCase();
                } else {
                    json.getJSONArray("error").put("Bad quantity value : " + value);
                }
                minKernel = newVal;
                maxKernel = newVal;
                minSupport = newVal;
                maxSupport = newVal;
            }
        }

        Matcher mInterval = patternRegInterval.matcher(value);
        if (mInterval.find()) {
            foundFormat = true;
            try {
                Double val = Double.parseDouble(mInterval.group(2).replaceAll(",","."));
                minKernel = df.format(val);
                minSupport = df.format(val);
            } catch (NumberFormatException e) {
                String newVal = "NAN";
                if (mInterval.group(2).equalsIgnoreCase("inf") || mInterval.group(2).equalsIgnoreCase("-inf")) {
                    newVal = mInterval.group(2).toUpperCase();
                } else {
                    json.getJSONArray("error").put("Bad quantity value : " + value);
                }
                minKernel = newVal;
                minSupport = newVal;
            }

            try {
                Double val = Double.parseDouble(mInterval.group(3).replaceAll(",","."));
                maxSupport = df.format(val);
                maxKernel = df.format(val);
            } catch (NumberFormatException e) {
                String newVal = "NAN";
                if (mInterval.group(3).equalsIgnoreCase("inf") || mInterval.group(3).equalsIgnoreCase("-inf")) {
                    newVal = mInterval.group(3).toUpperCase();
                } else {
                    json.getJSONArray("error").put("Bad quantity value : " + value);
                }
                maxSupport = newVal;
                maxKernel = newVal;
            }
        }

        Matcher mMeanSD = patternRegMoyEcar.matcher(value);
        if (mMeanSD.find()) {
            foundFormat = true;
            try {
                Double mean = Double.parseDouble(mMeanSD.group(1).replaceAll(",","."));
                Double sd = Double.parseDouble(mMeanSD.group(2).replaceAll(",","."));
                minKernel = df.format(mean - (2 * sd));
                minSupport = df.format(mean - (2 * sd));
                maxKernel = df.format(mean + (2 * sd));
                maxSupport = df.format(mean + (2 * sd));
            } catch (NumberFormatException e) {
                json.getJSONArray("error").put("Bad quantity value : " + value);
                minKernel = "NAN";
                maxKernel = "NAN";
                minSupport = "NAN";
                maxSupport = "NAN";
            }
        }

        Pair<String, String> kernel = new MutablePair<>(minKernel, maxKernel);
        Pair<String, String> support = new MutablePair<>(minSupport, maxSupport);

        if (!foundFormat) {
            json.getJSONArray("error").put("Bad quantity value : " + value);
        }
        return new MutablePair<>(kernel, support);
    }

    public static String getQuantValueType(String value) {

        Pattern patternRegScalaire = Pattern.compile(regScalaire);
        Pattern patternRegInterval = Pattern.compile(regInterval);
        Pattern patternRegMoyEcar = Pattern.compile(regMoyEcar);

        Matcher mScalar = patternRegScalaire.matcher(value);
        if (mScalar.find()) {
            return "scalar";
        }

        Matcher mInterval = patternRegInterval.matcher(value);
        if (mInterval.find()) {
            return "min / max";
        }

        Matcher mMeanSD = patternRegMoyEcar.matcher(value);
        if (mMeanSD.find()) {
            return "mean / SD";
        }

        return "none";

    }

    public static LocalTime convertTime(String time, JSONObject json) {
        LocalTime lt = LocalTime.of(0, 0, 0);
        if (time != null && !time.isEmpty()) {
            try {
                lt = LocalTime.parse(time, DateTimeFormatter.ISO_LOCAL_TIME);
            } catch (DateTimeParseException e) {
                json.getJSONArray("error").put("time conversion failed : " + time);
            }
        }
        return lt;
    }

    public static LocalDateTime convertDate(String date, String time, JSONObject json) {
        LocalDate ld = convertDate(date, json);
        if (ld != null) {
            LocalTime lt = convertTime(time, json);
            return ld.atTime(lt);
        }
        return null;
    }

    public static LocalDate convertDate(String date, JSONObject json) {
        LocalDate d = null;
        if (date != null && !date.isEmpty()) {
            LinkedList<SimpleDateFormat> listFormat = new LinkedList<>();
            listFormat.add(new SimpleDateFormat("yyyy-MM-dd"));
            listFormat.add(new SimpleDateFormat("dd/MM/yyyy"));
            listFormat.add(new SimpleDateFormat("yyyy/MM/dd"));
            listFormat.add(new SimpleDateFormat("yyyy/MM"));
            listFormat.add(new SimpleDateFormat("yyyy"));

            for(SimpleDateFormat sdf : listFormat) {
                if(d == null) {
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    try {
                        d = new java.sql.Timestamp(sdf.parse(date).getTime()).toLocalDateTime().toLocalDate();
                    } catch (ParseException e) {
                    }
                }
            }
        }

        if (d == null && date != null && !date.isEmpty()) {
            logger.debug("date conversion failed : " + date);
            json.getJSONArray("error").put("date conversion failed : " + date);
        }
        return d;
    }

    public static String removeNonAlphaNumChar(String value) {
        return normalize(value).replaceAll("[^a-zA-Z0-9_]", "");
    }

    public static String normalize(String value) {

        return ParsedIRI.create(Normalizer.normalize(value, Normalizer.Form.NFD)
                .replaceAll(" ", "_").replaceAll(",", "_").replaceAll("[^\\p{ASCII}]", "")).toString();

    }

    public static Boolean isEditable(String uri) {
        for (String k : NonEditableKeyWords.keyWords) {
            Pattern p = Pattern.compile(k);
            if (p.matcher(uri).find()) {
                return false;
            }
        }
        return true;
    }

    public static UnitConverter getConverter(Unit unit) {
        if(unit != null) {
            try {
                return unit.getConverterTo(unit.getSystemUnit());
            } catch (Exception e) {
                System.err.println("failed get converter " + unit);
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Unit getUnit(String unitCode) {
        UCUMFormat parse = UCUMFormat.getInstance(UCUMFormat.Variant.CASE_SENSITIVE);
        if (unitCode != null && !unitCode.contains(" ") && Tools.isBalanced(unitCode) && StringUtils.isAsciiPrintable(unitCode)) {
            try {
                Unit currentUnit = parse.parse(unitCode);
                return currentUnit;
            } catch (Exception e) {
                System.err.println("failed get unit " + unitCode);

            }
        }
       return null;
    }

    public static String getPrettyName(String unitCode) {
        return "";
    }


    public static Boolean checkDimension(String dimension) {
        String[] availableDim = {"L","M","T","I","\u0398","N","J"};
        Boolean retour = true;
        Pattern pattern = Pattern.compile("\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(dimension);
        if (matcher.find())
        {
            for(int i = 1 ; i <= matcher.groupCount(); i++) {
                int finalI = i;
                if(Arrays.stream(availableDim).noneMatch(s -> s.equals(matcher.group(finalI)))) {
                    return false;
                }
            }
        }

        return retour;
    }

    public static String getDimension(String unitCode) {
        String goo = null;

        UnitFormat parse = UCUMFormat.getInstance(UCUMFormat.Variant.CASE_SENSITIVE);
        if (unitCode != null && !unitCode.contains(" ") && Tools.isBalanced(unitCode) && StringUtils.isAsciiPrintable(unitCode)) {
            try {
                Unit currentUnit = parse.parse(unitCode);
                goo = currentUnit.getDimension().toString();

            } catch (Exception e) {
                goo = null;
            }
        } else {
            goo = null;
        }
        return goo;
    }

    public static String getPrettyUnit(String unitCode) {
        if (unitCode.isEmpty()) {
            return "";
        }
        String goo;
        UnitFormat print = UCUMFormat.getInstance(UCUMFormat.Variant.PRINT);
        UnitFormat parse = UCUMFormat.getInstance(UCUMFormat.Variant.CASE_SENSITIVE);
        if (unitCode != null && !unitCode.contains(" ") && Tools.isBalanced(unitCode) && StringUtils.isAsciiPrintable(unitCode)) {
            try {
                Unit currentUnit = parse.parse(unitCode);
                goo = print.format(currentUnit);

            } catch (Exception e) {
                goo = null;
//                                                        e.printStackTrace();
            }
        } else {
            goo = null;
        }

        return goo;
    }

    public static File compressZipfile(ProjectFile projectFile) throws IOException {
        Data data = projectFile.getData();
        String sourceDir = Tools.dataPath + data.getName().get();
        String outputFile = Tools.dataPath + data.getName().get() + ".zip";

        // check for existing zip file
        File temp = new File(outputFile);
        if(temp.exists()) {
            temp.delete();
        }

        ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(outputFile));
        compressDirectoryToZipfile(sourceDir, sourceDir, zipFile);
        zipFile.close();
        return new File(Tools.dataPath + data.getName().get() + ".zip");

    }

    private static void compressDirectoryToZipfile(String rootDir, String sourceDir, ZipOutputStream out) throws IOException, FileNotFoundException {
        for (File file : new File(sourceDir).listFiles()) {
            if (file.isDirectory()) {
                compressDirectoryToZipfile(rootDir, sourceDir + File.separator + file.getName(), out);
            } else {
                if (!StringUtils.startsWith(file.getName(),".")) {
                    ZipEntry entry = null;
                    if(sourceDir.equals(rootDir)) {
                        entry = new ZipEntry(file.getName());
                    } else {
                        entry = new ZipEntry(sourceDir.replace(rootDir + File.separator, "") + File.separator + file.getName());
                    }
                    out.putNextEntry(entry);

                    FileInputStream in = new FileInputStream(sourceDir + File.separator + file.getName());
                    IOUtils.copy(in, out);
                    in.close();
                }
            }
        }
    }


    public static boolean isBalanced(String str) {
        str = str.replaceAll("[^\\[\\]\\(\\)\\{\\}]", "");
        if (str.length() == 0) {
            return true;
        }
        // odd number would always result in false
        if ((str.length() % 2) != 0) {
            return false;
        }

        final Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            if (brackets.containsKey(str.charAt(i))) {
                stack.push(str.charAt(i));
            } else if (stack.empty() || (str.charAt(i) != brackets.get(stack.pop()))) {
                return false;
            }
        }
        return true;
    }

    public static boolean uuidIsPresent(XSSFSheet sheet) {
        if(sheet != null) {
            for(Row r : sheet) {
                for(Cell c : r) {
                    if(c.toString().toLowerCase().contains("uuid")) {
                        if(c.getColumnIndex() == 0 && sheet.getRow(c.getRowIndex()).getCell(1, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL) != null) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static Boolean searchString(XSSFSheet sheet, String string) {
        if(sheet != null && string != null && !StringUtils.isBlank(string)) {
            for(Row r : sheet) {
                for(Cell c : r) {
                    if(c.toString().toLowerCase().contains(string.toLowerCase())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void clearFile(XSSFWorkbook book) {
        ArrayList<Sheet> listToRemove = new ArrayList<>();
        for (Iterator<Sheet> is = book.sheetIterator(); is.hasNext(); ) {
            Sheet s = is.next();
            if (s.getSheetName().contains("temp")) {
                listToRemove.add(s);
            }
        }
        for (Sheet s : listToRemove) {
            book.removeSheetAt(book.getSheetIndex(s));
        }
    }

    public static void autoResize(XSSFSheet sheet, Integer max) {
        for (Integer i = 0; i <= max; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    public static String shortUUID() {
        return RandomStringUtils.randomAlphanumeric(6);
    }

    public static String shortUUIDFromString(String input) {
        if(input.isEmpty()) {
            input = shortUUID();
        }
        if (Tools.seed == null || Tools.seed.isEmpty()) {
            seed = Tools.shortUUID();
        }
        return UUID.nameUUIDFromBytes(seed.concat(input).getBytes()).toString().replace("-", "").substring(0, 8);
    }
}
