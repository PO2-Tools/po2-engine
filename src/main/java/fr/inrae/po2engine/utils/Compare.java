/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Compare {
    public static void main(String[] args) throws FileNotFoundException {
        Model m1 = RDFDataMgr.loadModel("/home/stephane/Téléchargements/data_test_sem2/data_test.ttl");
        Model m2 = RDFDataMgr.loadModel("/home/stephane/Téléchargements/data_test_sem3/data_test.ttl");

        Model diff = m1.difference(m2);
        System.out.println("equal 12 : " + diff.isEmpty());
        System.out.println("iso 12 : " + m1.isIsomorphicWith(m2));

        diff = m2.difference(m1);
        System.out.println("equal 21 : " + diff.isEmpty());
        System.out.println("iso 21 : " + m2.isIsomorphicWith(m1));
    }
}
