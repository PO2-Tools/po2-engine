/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.iv.RandomIvGenerator;
import org.jasypt.properties.EncryptableProperties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PO2Properties {
    private static final String file = "/PO2Engine.properties";
    private static Properties properties;
    private static Logger logger = LogManager.getLogger(PO2Properties.class);

    private static BooleanProperty productionMode = new SimpleBooleanProperty(true);
    public static boolean isProductionMode() {
        return productionMode.get();
    }

    public static BooleanProperty productionModeProperty() {
        return productionMode;
    }

    public static void setProductionMode(boolean productionMode) {
        PO2Properties.productionMode.set(productionMode);
    }

    public static String getProperty(String prop) {
        if(isProductionMode()){
            prop = "production."+prop;
        } else {
            prop = "sandbox."+prop;
        }
        return properties.getProperty(prop);
    }

    static {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        String key = System.getProperty("PO2MasterKey");
        if(key == null || key.isEmpty()) {
            // try with env (dev Mode / API Mode)
            key = System.getenv("PO2MasterKey");
            if(key != null) {
                logger.warn("loading master key from env variable");
            }
        }
        if(key == null || key.isEmpty()) {
            logger.error("No master key found. Unable to read encoded property values");
            key = "badPassword";
        }
        encryptor.setPassword(key);
        encryptor.setAlgorithm("PBEWITHSHA1ANDDESEDE");
        encryptor.setIvGenerator(new RandomIvGenerator());
        properties = new EncryptableProperties(encryptor);
        InputStream stream;
        try {
            stream = PO2Properties.class.getResourceAsStream(file);
            if(stream != null) {
                properties.load(stream);
                stream.close();
                // searching for the default mode
                String defaultMode = properties.getProperty("default");
                if(defaultMode != null && !defaultMode.isEmpty()) {
                    setProductionMode("production".equalsIgnoreCase(defaultMode));
                }
                logger.debug("Property file loaded");
            } else {
                logger.error("Unable to find property file : "+file);
            }
        } catch (IOException e) {
            logger.error("Unable to load property file : "+file, e);
        }
    }
}
