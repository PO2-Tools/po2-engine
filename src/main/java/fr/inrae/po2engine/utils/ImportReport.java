/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import fr.inrae.po2engine.model.dataModel.CompositionFile;
import fr.inrae.po2engine.model.dataModel.ObservationFile;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImportReport {
    ArrayList<String> errorMessage;
    String stepSheet = null;
    Map<XSSFSheet, ObservationFile> obsMapping = new HashMap<>();
    Map<XSSFSheet, Pair<CompositionFile, Boolean>> compoMapping = new HashMap<>();
    List<ObservationFile> obsDelete = new ArrayList<>();
    List<CompositionFile> compoDelete = new ArrayList<>();

    public ImportReport() {
        errorMessage = new ArrayList<>();
    }


    public String getStepSheet() {
        return stepSheet;
    }

    public void setStepSheet(String stepSheet) {
        this.stepSheet = stepSheet;
    }

    public Map<XSSFSheet, ObservationFile> getObsMapping() {
        return obsMapping;
    }

    public List<ObservationFile> getObsDelete() {
        return obsDelete;
    }

    public void setObsDelete(List<ObservationFile> obsDelete) {
        this.obsDelete = obsDelete;
    }

    public void addObsMapping(XSSFSheet sheet, ObservationFile o) {
        this.obsMapping.put(sheet, o);
    }

    public void addObsDelete(ObservationFile obsF) {
        this.obsDelete.add(obsF);
    }

    public Map<XSSFSheet, Pair<CompositionFile, Boolean>> getCompoMapping() {
        return compoMapping;
    }

    public List<CompositionFile> getCompoDelete() {
        return compoDelete;
    }

    public void setCompoDelete(List<CompositionFile> compoDelete) {
        this.compoDelete = compoDelete;
    }

    public void addCompoMapping(XSSFSheet sheet, CompositionFile o, Boolean isInput) {
        this.compoMapping.put(sheet, new ImmutablePair<>(o, isInput));
    }

    public void addCompoDelete(CompositionFile compoF) {
        this.compoDelete.add(compoF);
    }

    public Boolean success() {
        return errorMessage.isEmpty();
    }

    public ArrayList<String> getError() {
        return this.errorMessage;
    }

    @Override
    public String toString() {
        return this.errorMessage.toString();
    }

    public void addError(String error) {
        this.errorMessage.add(error);
    }
}


