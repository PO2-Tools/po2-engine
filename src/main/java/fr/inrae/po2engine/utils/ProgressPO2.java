/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class ProgressPO2 {
    public static final String CREATE = "create";
    public static final String CREATE_PROJECT = "create_project";
    public static final String LOCK = "lock";
    public static final String SAVE = "save";
    public static final String OPEN = "open";
    public static final String EXPORT = "export";
    public static final String SEARCH = "search";
    public static final String IMPORT = "import";
    public static final String PUBLISH = "publish";
    public static final String PUBLISHSTEP = "publishstep";
    public static final String ANALYSE = "analyse";
    public static final String GRAPH = "graph";
    public static final String CONSTAINT = "constraint";
    private String type;
    private SimpleStringProperty text;
    private SimpleDoubleProperty progress;

    private ProgressPO2() {}

    public ProgressPO2(String type, String text) {
        this.type = type;
        this.text = new SimpleStringProperty(text);
        this.progress = new SimpleDoubleProperty(-1.0);
    }

    public String getText() {
        return text.get();
    }

    public void setText(String text) {
        this.text.set(text);
    }

    public SimpleStringProperty textProperty() {
        return text;
    }

    public double getProgress() {
        return progress.get();
    }

    public void setProgress(double progress) {
        this.progress.set(progress);
    }

    public SimpleDoubleProperty progressProperty() {
        return progress;
    }
}
