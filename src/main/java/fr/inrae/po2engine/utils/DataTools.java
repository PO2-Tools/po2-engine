/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import com.github.sardine.model.Link;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.rules.GeneralFileRule;
import fr.inrae.po2engine.rules.MatMetFileRule;
import fr.inrae.po2engine.rules.ProjectFileRule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class DataTools {
    private static Logger logger = LogManager.getLogger(DataTools.class);

    public static Map<String, File> listFiles(File parentFile) {
        Map<String, File> listTemp = new HashMap<>();
        File[] listOf = parentFile.listFiles();
        if(listOf != null) {
            for (int i = 0; i < listOf.length; i++) {
                if (listOf[i].isFile()) {
                    if (!listOf[i].getName().startsWith("~") && !listOf[i].getName().startsWith(".") && FilenameUtils.getExtension(listOf[i].getName()).contains("xls")) { // fichier caché windows + unix
                        listTemp.put(FilenameUtils.removeExtension(listOf[i].getName()), listOf[i]);
                    }
                }
            }
            for (int i = 0; i < listOf.length; i++) {
               if (listOf[i].isDirectory()) {
                   if (listOf[i].listFiles().length == 0) {
                       // empty directory // removing
                       try {
                           FileUtils.deleteDirectory(listOf[i]);
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                   } else {
                       listTemp.putAll(listFiles(listOf[i]));
                   }
               }
            }
        }
        return listTemp;
    }

    public static Report analyseFiles(Data data) {
        Report retour = new Report();
        ArrayList<File> matMetFile = new ArrayList<>();
        Tools.addProgress(ProgressPO2.ANALYSE, "Searching for project");
        Tools.updateProgress(ProgressPO2.ANALYSE, 0.0);
        Tools.addProgress(ProgressPO2.OPEN, "Loading data");

        ProjectFile project = data.getProjectFile();


        // search all generalFile

        LinkedHashMap<File, Mutable<Boolean>> listAllFileWithFlag = new LinkedHashMap<>();
        for(File f : data.getListAllFile().values()) {
            listAllFileWithFlag.put(f, new MutableBoolean(true));
        }
        ProjectFileRule rulesProject = new ProjectFileRule();
        GeneralFileRule rulesGeneral = new GeneralFileRule();
        MatMetFileRule rulesMatMet = new MatMetFileRule();
        Double pp = 1.0/listAllFileWithFlag.entrySet().size();
        Tools.updateProgress(ProgressPO2.OPEN, 0.0);
        logger.info("Searching for project file and matMeth File in " + data.getListAllFile().size());

        Iterator<Map.Entry<File, Mutable<Boolean>>> iterator = listAllFileWithFlag.entrySet().iterator();
        while(iterator.hasNext() && !(project != null && !matMetFile.isEmpty())) {
            Map.Entry<File, Mutable<Boolean>> element = iterator.next();
            if(element.getValue().getValue()) {
                File f = element.getKey();
                try {
                    if (project == null && rulesProject.startTypeDetection(f)) {
                        project = new ProjectFile(FilenameUtils.removeExtension(f.getName()), f.getParentFile().getAbsolutePath()+File.separator, data);
                        project.constructData();
                    } else {
                        if(rulesMatMet.startTypeDetection(f)) {
                            matMetFile.add(f);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
            }
            Tools.updateProgress(ProgressPO2.ANALYSE, Tools.getProgress(ProgressPO2.ANALYSE).getProgress() + pp);
        }
        if(project == null) {
            // pas de fichier projet trouvé, on le créé
            logger.info("No project file found. Searching for general files in " + data.getListAllFile().size());
            project = new ProjectFile(data);
            for(Map.Entry<File, Mutable<Boolean> > entry : listAllFileWithFlag.entrySet()) {
                Tools.updateProgress(ProgressPO2.OPEN, Tools.getProgress(ProgressPO2.OPEN).getProgress() + pp);
                if (entry.getValue().getValue()) {
                    File f = entry.getKey();
                    try {
                        if (rulesGeneral.startTypeDetection(f)) {
                            // on renomme le fichier si jamais il existe plusieurs fichiers general avec le même nom (ficherDescriptive ...)
                            File renamedFile = new File(f.getParentFile(), "generalFile" + data.getListGeneralFile().size()+"."+FilenameUtils.getExtension(f.getName()));
                            FileUtils.moveFile(f, renamedFile);
                            GeneralFile gf = new GeneralFile(FilenameUtils.removeExtension(renamedFile.getName()), project, renamedFile.getParentFile().getAbsolutePath() + File.separator, false);

                            for (GeneralFile gg : data.getListGeneralFile()) {
                                if (gf.getFolderPath().concat(File.separator).contains(gg.getFolderPath().concat(File.separator)) || gg.getFolderPath().concat(File.separator).contains(gf.getFolderPath().concat(File.separator))) {
                                    retour.addError("Bad structure in project " + renamedFile.getName() + " detected.");
                                    Tools.updateProgress(ProgressPO2.OPEN, 1.0);
                                    Tools.delProgress(ProgressPO2.OPEN);
                                    return retour;
                                }
                            }

                            data.addGeneralFile(gf);
                            if (gf.preConstructData(data)) {
                                for (String ff : gf.getListFileForAnalyse()) {
                                    File aze = data.getFile(ff);
                                    if (aze != null) {
                                        listAllFileWithFlag.get(aze).setValue(false);
                                    }
                                }
                            }
                        } else {

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        }
        Tools.delProgress(ProgressPO2.ANALYSE);
        //traitement du/des (pour elisabeth) matMet
        for(File f : matMetFile) {
            data.addMaterialMethod(f.getAbsolutePath());
        }

//        if(!lazyLoading) {
//            Double[] compt = new Double[1];
//            compt[0] = 0.0;
//            for(GeneralFile gf : data.getListGeneralFile()) {
//                data.analyseProcess(gf);
//                compt[0]++;
//                Platform.runLater(() -> data.getProgressProperty().setValue(compt[0] / data.getListGeneralFile().size()));
//
//            }
//        }

        logger.info("found " + data.getListGeneralFile().size() +" generals files");
//        data.analyseFile(root);
        // si pas de fichier de materiel et méthode alors on le créer
        if(data.getMaterialMethodFile() == null) {
            logger.info("init mat & met file");
            // on vérifie qu'il n'existe pas le fichier générique (materials-methods.xlsx)
//            String matmet = data.getFilePath("materials-methods");
//            if(matmet != null) {
//                data.addMaterialMethod(matmet);
//            } else {
                data.createEmptyMaterialMethodFile();
//            }
        }
        Tools.updateProgress(ProgressPO2.OPEN, 1.0);
        Tools.delProgress(ProgressPO2.OPEN);
        return retour;
    }
}
