/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.externalTools;


import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Ontology;
import fr.inrae.po2engine.model.SkosScheme;
import fr.inrae.po2engine.model.VocabConcept;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.SimpleStringProperty;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.atlas.web.TypedInputStream;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.http.HttpOp;
import org.apache.jena.ontology.*;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFWriter;
import org.apache.jena.sparql.exec.http.QueryExecutionHTTPBuilder;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.rio.RDFWriterFactory;
import org.json.JSONObject;
import systems.uom.ucum.format.UCUMFormat;

import javax.measure.Unit;
import javax.measure.UnitConverter;
import javax.measure.format.UnitFormat;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author stephane
 */
public class JenaTools {
    private static final Model fakeModel = ModelFactory.createDefaultModel();

    public static final String TIME	= "http://www.w3.org/2006/time#";
    public static final String SOSA = "http://www.w3.org/ns/sosa/";
//    public static final String QUDT = "http://qudt.org/schema/qudt/";
    public static final String SSN = "http://www.w3.org/ns/ssn/";
    public static final String SCH = "http://schema.org/";
    public static final String PO2CORE = "http://opendata.inrae.fr/PO2/core/";
    public static final String OMV = "http://omv.ontoware.org/2005/05/ontology#";
    public static final String PROV = "http://www.w3.org/ns/prov#";
    public static String COREVERSION = "";


    public static final Property hasDimension = fakeModel.createProperty(PO2CORE+"hasDimension");

    private static Logger logger = LogManager.getLogger(JenaTools.class);

    public static final List<String> uriToLoad = Arrays.asList(
            PO2CORE+"Process",
            PO2CORE+"Component",
            SSN+"Property",
            SOSA+"System", // old actuator
            SOSA+"Procedure",
            PO2CORE+"Step",
            PO2CORE+"observationScale" // on laisse pour les supprimer s'il en reste !
    );

    public static final Map<String, List<String>> oldURIToLoad ;
    static {
        Map<String, List<String>> aMap = new HashMap<>();
        aMap.put(SSN+"Property", Arrays.asList("http://opendata.inra.fr/PO2/attribute"));
        aMap.put(SOSA+"System", Arrays.asList(SOSA+"Actuator", "http://opendata.inra.fr/PO2/material", SOSA+"Sampler"));
        aMap.put(PO2CORE+"Step", Arrays.asList("http://opendata.inrae.fr/PO2/core/step", "http://opendata.inra.fr/PO2/step", "http://opendata.inra.fr/PO2/core/step"));
        aMap.put(PO2CORE+"Component", Arrays.asList("http://opendata.inrae.fr/PO2/core/component", "http://opendata.inra.fr/PO2/component", "http://opendata.inra.fr/PO2/core/component"));
        aMap.put(PO2CORE+"Process", Arrays.asList("http://opendata.inrae.fr/PO2/core/Transformation_Process", "http://opendata.inra.fr/PO2/Transformation_Process", "http://opendata.inra.fr/PO2/core/Transformation_Process"));
        aMap.put(PO2CORE+"observationScale", Arrays.asList("http://opendata.inra.fr/PO2/observationScale" ,"http://opendata.inra.fr/PO2/core/observationScale"));

//        aMap.put("http://opendata.inra.fr/PO2/TransformationProcessObservation", Arrays.asList("http://opendata.inra.fr/PO2/observation"));
        oldURIToLoad = Collections.unmodifiableMap(aMap);
    }
    private static OntModel baseModel = null;
    private static String baseURI = null;


    /**
     *
     */
    private JenaTools() {

    }

    public static Individual addDate(OntModel coreModel, OntModel dataModel, Resource ind, LocalDateTime debut, LocalDateTime fin) {

        Calendar de = Calendar.getInstance();
        if(debut != null) {
            de.setTime(Timestamp.valueOf(debut));
        }
        XSDDateTime deb = new XSDDateTime(de);
        Calendar fir = Calendar.getInstance();
        if(fin != null) {
            fir.setTime(Timestamp.valueOf(fin));
        }
        XSDDateTime fi = new XSDDateTime(fir);

        ObjectProperty hasBeginning = coreModel.getObjectProperty(TIME + "hasBeginning");
        ObjectProperty hasEnd = coreModel.getObjectProperty(TIME + "hasEnd");
        OntClass instant = coreModel.getOntClass(TIME + "Instant");
        Property inXSD = coreModel.getProperty(TIME + "inXSDDateTime");

        // old version using an intermediate temporalEntity. To remove in a future (spoq compatibility)
        Property phenomenonTime = coreModel.getProperty(SOSA+"phenomenonTime");
        OntClass properInterval = coreModel.getOntClass(TIME + "ProperInterval");
        Individual t = dataModel.createIndividual(ind.getURI() + "_Interval", properInterval);
        ind.addProperty(phenomenonTime, t);
        ////////////////////////////////////////////////////////////////////////////////////////////// fin du remove !!!
        if (debut != null) {
            Literal l = dataModel.createTypedLiteral(deb);
            Individual i = dataModel.createIndividual(ind.getURI() + "_start", instant);
            i.addProperty(inXSD, l);
            t.addProperty(hasBeginning, i);
        }
        if (fin != null) {
            Literal l = dataModel.createTypedLiteral(fi);
            Individual i = dataModel.createIndividual(ind.getURI() + "_end", instant);
            i.addProperty(inXSD, l);
            t.addProperty(hasEnd, i);
        }
        return t;
    }

    public static HashMap<String, List<String>> getListMatch(String uri) {
        List<String> resultExact = new ArrayList<>();
        List<String> resultClose = new ArrayList<>();
        HashMap<String, List<String>> result = new HashMap<>();
        result.put("exact", resultExact);
        result.put("close", resultClose);

        String querySameAs = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#> " + "select ?exact " + "Where  {<" + uri + "> skos:exactMatch ?exact}";
        final TypedInputStream is = HttpOp.httpGet(uri, "application/rdf+xml");
        if(is != null && is.getContentType().equalsIgnoreCase("application/rdf+xml")) {

            Model m = ModelFactory.createDefaultModel();
            m.read(is, null);
            Query q = QueryFactory.create(querySameAs);
            QueryExecution qexec = QueryExecutionFactory.create(q, m);
            ResultSet results = qexec.execSelect();

            while (results.hasNext()) {
                QuerySolution binding = results.nextSolution();
                if (binding.get("exact") != null) {
                    resultExact.add(binding.get("exact").toString());
                }
            }

            String queryClose = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#> " + "select ?close " + "Where  {<" + uri + "> skos:closeMatch ?close}";
            final TypedInputStream isC = HttpOp.httpGet(uri, "application/rdf+xml");
            Model mC = ModelFactory.createDefaultModel();
            mC.read(isC, null);
            Query qC = QueryFactory.create(queryClose);
            QueryExecution qexecC = QueryExecutionFactory.create(qC, mC);
            ResultSet resultsC = qexecC.execSelect();

            while (resultsC.hasNext()) {
                QuerySolution binding = resultsC.nextSolution();
                if (binding.get("close") != null) {
                    resultClose.add(binding.get("close").toString());
                }
            }
        }
        return result;
    }

    public static HashMap<String, HashMap<String, List<String>>> getLabelsAndDescription(String uri) {
        Tools.addProgress(ProgressPO2.SEARCH, "getting info");
        HashMap<String, HashMap<String, List<String>>> result = new HashMap<>();

        String queryLabel = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
                + " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                + " PREFIX rdfs:   <http://www.w3.org/2000/01/rdf-schema#>"
                + " PREFIX dbo: <http://dbpedia.org/ontology/>"
                + " select ?stringLab ?lang"
                + " Where {"
                + " 	{ {<" + uri + "> rdfs:label ?lab} UNION {<" + uri + "> skos:prefLabel ?lab}. BIND(STR(?lab) as ?stringLab).BIND(LANG(?lab) as ?lang).}"
                + " }";

        String queryAltLabel = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
                + " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                + " PREFIX rdfs:   <http://www.w3.org/2000/01/rdf-schema#>"
                + " PREFIX dbo: <http://dbpedia.org/ontology/>"
                + " select ?stringLab ?lang"
                + " Where {"
                + " 	  <" + uri + "> skos:altLabel ?lab. BIND(STR(?lab) as ?stringLab).BIND(LANG(?lab) as ?lang)."
                + " }";

        String queryComment = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
                + " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                + " PREFIX rdfs:   <http://www.w3.org/2000/01/rdf-schema#>"
                + " PREFIX dbo: <http://dbpedia.org/ontology/>"
                + " select ?stringLab ?lang"
                + " Where {"
                + " 	{ {<" + uri + "> rdfs:comment ?lab} UNION {<" + uri + "> skos:skopNote ?lab}. BIND(STR(?lab) as ?stringLab).BIND(LANG(?lab) as ?lang).}"
                + " }";

        final TypedInputStream is2 = HttpOp.httpGet(uri, "application/rdf+xml");
        if(is2 != null && is2.getContentType().equalsIgnoreCase("application/rdf+xml")) {
            Model m2 = ModelFactory.createDefaultModel();

            m2.read(is2, null);
            Query qLab = QueryFactory.create(queryLabel);
            QueryExecution qexec = QueryExecutionFactory.create(qLab, m2);
            ResultSet results = qexec.execSelect();
            HashMap<String, List<String>> listLabel = new HashMap<>();
            while (results.hasNext()) {
                QuerySolution binding = results.nextSolution();
                if (binding.get("stringLab") != null) {
                    String lab = binding.get("stringLab").toString();
                    String lang = binding.get("lang").toString().toLowerCase();
                    if (VocabConcept.listLang.contains(lang)) {
                        if(!listLabel.containsKey(lang)) {
                            listLabel.put(lang, new ArrayList<>());
                        }
                        listLabel.get(lang).add(lab);
                    }

                }
            }

            Query qAlt = QueryFactory.create(queryAltLabel);
            QueryExecution qexecAlt = QueryExecutionFactory.create(qAlt, m2);
            ResultSet resultsAlt = qexecAlt.execSelect();
            HashMap<String, List<String>> listAltLabel = new HashMap<>();
            while (resultsAlt.hasNext()) {
                QuerySolution binding = resultsAlt.nextSolution();
                if (binding.get("stringLab") != null) {
                    String lab = binding.get("stringLab").toString();
                    String lang = binding.get("lang").toString().toLowerCase();
                    if (VocabConcept.listLang.contains(lang)) {
                        if(!listAltLabel.containsKey(lang)) {
                            listAltLabel.put(lang, new ArrayList<>());
                        }
                        listAltLabel.get(lang).add(lab);
                    }

                }
            }

            Query qCom = QueryFactory.create(queryComment);
            QueryExecution qexec2 = QueryExecutionFactory.create(qCom, m2);
            ResultSet results2 = qexec2.execSelect();
            HashMap<String, List<String>> listCom = new HashMap<>();
            while (results2.hasNext()) {
                QuerySolution binding = results2.nextSolution();
                if (binding.get("stringLab") != null) {
                    String lab = binding.get("stringLab").toString();
                    String lang = binding.get("lang").toString().toLowerCase();
                    if (VocabConcept.listLang.contains(lang)) {
                        if(!listCom.containsKey(lang)) {
                            listCom.put(lang, new ArrayList<>());
                        }
                        listCom.get(lang).add(lab);
                    }
                }
            }
            result.put("label", listLabel);
            result.put("altLabel", listAltLabel);
            result.put("Comment", listCom);

            qexec.close();
            qexec2.close();
        }
        Tools.updateProgress(ProgressPO2.SEARCH, 1.0);
        Tools.delProgress(ProgressPO2.SEARCH);
        return result;
    }

    public static OntModel ReadCoreModel() {
        try {
            OntDocumentManager mgr = new OntDocumentManager();
            mgr.setProcessImports(true);
            OntModelSpec s = new OntModelSpec(OntModelSpec.OWL_MEM);
            s.setDocumentManager(mgr);
            OntModel model = ModelFactory.createOntologyModel(s, null);
            InputStream is = JenaTools.class.getClassLoader().getResourceAsStream("resources/PO2_core/PO2.ttl");
            model.read(is, null, "TTL");

            for ( ResIterator iteVersion = model.listSubjectsWithProperty(OWL2.versionInfo); iteVersion.hasNext(); ) {
                Resource r = iteVersion.nextResource();
                COREVERSION = r.getProperty(OWL2.versionInfo).getLiteral().getString();
            }

            model.loadImports();
            logger.info("Finished reading core model");

            return model;
        } catch (Exception ex) {
            logger.error("##### Error Fonction: readModel #####", ex.getMessage());
            return null;
        }
    }

    public static OntModel ReadModel(String modelName) {

        try {
            OntDocumentManager mgr = new OntDocumentManager();
            mgr.setProcessImports(false);
            OntModelSpec s = new OntModelSpec(OntModelSpec.OWL_MEM);
            s.setDocumentManager(mgr);
            OntModel model = ModelFactory.createOntologyModel(s, null);
            RDFDataMgr.read(model, modelName);
            logger.info("Finished reading model");

            return model;
        } catch (Exception ex) {
            logger.error("##### Error Fonction: readModel #####", ex.getMessage());
            return null;
        }
    }

    public static Model createModel(String filePath) {
        Model baseModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
        RDFDataMgr.read(baseModel, filePath);
        return baseModel;
    }

    /**component
     * Charge l'ontologie dans la liste vocabNode
     *
     * @param onto
     */
    public static void loadModel(Ontology onto) throws UnsupportedEncodingException, FileNotFoundException {
        Tools.addProgress(ProgressPO2.OPEN, "Loading vocabulary");
        String path = onto.getOntologyFile().getAbsolutePath();
        ArrayList<OntClass> listToRemove = new ArrayList<>();
        logger.info("Chargement du model " + path);
        FileInputStream fis = new FileInputStream(path);
        baseModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
        RDFDataMgr.read(baseModel, fis, Lang.TTL);
        baseURI = baseModel.getNsPrefixMap().get("domain");
        onto.setBaseURI(baseURI);
        // remove prefix core (will be set by the core ontology)
        if(baseModel.getNsPrefixMap().containsKey("core"))
            baseModel.removeNsPrefix("core");
        logger.info("end read");
        // recherche des auteurs
        org.apache.jena.ontology.Ontology o = getOnto(baseModel);
        VocabConcept temp = new VocabConcept("root", onto);
        temp.setName("root-node");
        onto.setRootNode(temp);
        if(o != null) {
            for (StmtIterator iteAuth = o.listProperties(DC_11.contributor); iteAuth.hasNext(); ) {
                Statement s = iteAuth.nextStatement();
                Literal auth = s.getLiteral();
                onto.addAuthor(auth.getString());
                iteAuth.remove();
            }
            for (StmtIterator iteAuth = o.listProperties(DCTerms.contributor); iteAuth.hasNext(); ) {
                Statement s = iteAuth.nextStatement();
                Literal auth = s.getLiteral();
                onto.addAuthor(auth.getString());
                iteAuth.remove();
            }
            // suppression des anciennes dates
            for (StmtIterator iteDate = o.listProperties(DC_11.date); iteDate.hasNext(); ) {
                Statement s = iteDate.nextStatement();
                iteDate.remove();
            }
            for (StmtIterator iteDate = o.listProperties(DCTerms.date); iteDate.hasNext(); ) {
                Statement s = iteDate.nextStatement();
                iteDate.remove();
            }

            // gestion des keywords
            Property p = fakeModel.createProperty(OMV+"keywords");
            for (StmtIterator iteKeywords = o.listProperties(p); iteKeywords.hasNext(); ) {
                Statement s = iteKeywords.nextStatement();
                onto.setListKeyword(s.getLiteral().getString());
                iteKeywords.remove();
            }

            // recup de la version courante
            for (StmtIterator iteVersion = o.listProperties(OWL2.versionInfo); iteVersion.hasNext(); ) {
                Statement s = iteVersion.nextStatement();
                String[] ver = s.getString().split("\\.");
                if(ver.length == 2) {
                    onto.setMajorVersion(ver[0]);
                    onto.setMinorVersion(ver[1]);
                }
                iteVersion.remove();
            }

        }

        // Checking if main skos concept scheme exist
        if(baseModel.listIndividuals(SKOS.ConceptScheme).toList().size() == 0) {
            Individual defaultScheme = baseModel.createIndividual(onto.getBaseURI()+"cs_"+Tools.shortUUID(), SKOS.ConceptScheme);
            convertToSkosConceptScheme(defaultScheme);
        }

        // list all skos concept scheme
        for (ExtendedIterator<Individual> iteScheme = baseModel.listIndividuals(SKOS.ConceptScheme); iteScheme.hasNext(); ) {
            Individual scheme = iteScheme.next();
            SkosScheme newScheme = new SkosScheme(scheme.getURI());
            String name = scheme.getProperty(SKOS.prefLabel, "en").getString();
            newScheme.setName(name);
            String comment = scheme.getProperty(SKOS.scopeNote, "en").getString();
            newScheme.setComment(comment);
            onto.addSkosScheme(newScheme);
            onto.getRootNode().addSkosScheme(newScheme);
            if("main".equalsIgnoreCase(name)) {
                onto.setMainSkosScheme(newScheme);
            }
        }

        Tools.updateProgress(ProgressPO2.OPEN, 0.0);
        Double pas = 1.0/(uriToLoad.size() + 1);

        for (String uri : uriToLoad) {

            OntClass node = baseModel.getOntClass(uri);
            if(node == null) {
                // check in old uri
                List<String> oldsURI = oldURIToLoad.get(uri);
                if(oldsURI != null) {
                    node = oldsURI.stream().filter(s ->
                        baseModel.getOntClass(s) != null
                    ).findFirst().map(s -> baseModel.getOntClass(s)).orElse(null);
                }
            }

            if(node != null) {
                VocabConcept vNode = new VocabConcept(uri, onto);
                vNode.setDeletable(false);


                // recherche des skosConceptScheme pour ce node
                for (StmtIterator iteSch = node.listProperties(SKOS.inScheme); iteSch.hasNext(); ) {
                    Statement s = iteSch.next();
                    String uriScheme = s.getObject().asResource().getURI();
                    SkosScheme scheme = onto.getSkosScheme(uriScheme);
                    if(scheme != null) {
                        vNode.addSkosScheme(scheme);
                    } else {
                        logger.info("Unable to find skos concept scheme " + uriScheme);
                    }
                }


                for (ExtendedIterator<RDFNode> iteLab = node.listLabels(null); iteLab.hasNext(); ) {
                    Literal l = iteLab.next().asLiteral();
                    vNode.setLabel(l.getLanguage(), l.getString());
                }
//                if(uri.equalsIgnoreCase("http://www.w3.org/ns/sosa/Sampler")) {
//                    vNode.setLabel("en","material & human");
//                }

                for (ExtendedIterator<RDFNode> iteLab = node.listComments(null); iteLab.hasNext(); ) {
                    Literal l = iteLab.next().asLiteral();
                    vNode.setDefinition(l.getLanguage(), l.getString(), true);
                }

                for (ExtendedIterator<? extends Resource> iteLab = node.listSameAs(); iteLab.hasNext(); ) {
                    OntResource l = (OntResource) iteLab.next();
                    vNode.addExactMatch(l.getURI());
                }

                for (StmtIterator iteLab = node.listProperties(SKOS.prefLabel); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    if(st.getObject().isLiteral()) {
                        Literal li = st.getObject().asLiteral();
                        vNode.setLabel(li.getLanguage(), li.getString());
                    }
                }

                vNode.addFather(onto.getRootNode());
                onto.getRootNode().addChildren(vNode);

                for (StmtIterator iteLab = node.listProperties(SKOS.altLabel); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    if(st.getObject().isLiteral()) {
                        Literal li = st.getObject().asLiteral();
                        vNode.setAltLabel(li.getLanguage(), li.getString(), true);
                    }
                }

                // if editorialNote exist => scope is now scope. not def
                Boolean scopeNoteConversion = node.getProperty(SKOS.editorialNote) == null;

                for (StmtIterator iteLab = node.listProperties(SKOS.definition); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    if(st.getObject().isLiteral()) {
                        Literal li = st.getObject().asLiteral();
                        vNode.setDefinition(li.getLanguage(), li.getString(), true);
                    }
                }

                for (StmtIterator iteLab = node.listProperties(SKOS.editorialNote); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    if(st.getObject().isLiteral()) {
                        Literal li = st.getObject().asLiteral();
                        vNode.setEditorialNote(li.getString());
                    }
                }

                for (StmtIterator iteLab = node.listProperties(SKOS.scopeNote); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    if(st.getObject().isLiteral()) {
                        Literal li = st.getObject().asLiteral();
                        if(scopeNoteConversion) {
                            vNode.setDefinition(li.getLanguage(), li.getString(), true);
                        } else {
                            vNode.setScopeNote(li.getString());
                        }
                    }
                }

                for (StmtIterator iteLab = node.listProperties(SKOS.note); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    if(st.getObject().isLiteral()) {
                        Literal li = st.getObject().asLiteral();
                        vNode.setNote(li.getString());
                    }
                }

                for (StmtIterator iteLab = node.listProperties(SKOS.exactMatch); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    vNode.addExactMatch(st.getObject().toString());
                }

                for (StmtIterator iteLab = node.listProperties(SKOS.closeMatch); iteLab.hasNext(); ) {
                    Statement st = iteLab.next();
                    vNode.addCloseMatch(st.getObject().toString());
                }

                for (StmtIterator iteDim = node.listProperties(JenaTools.hasDimension); iteDim.hasNext(); ) {
                    Statement st = iteDim.next();
                    vNode.addDimension(st.getLiteral().getString());
                }

                for (ExtendedIterator<OntClass> iteClass = node.listSubClasses(true); iteClass.hasNext(); ) {
                    OntClass next = iteClass.next();
                    createNode(next, vNode, listToRemove,onto);
                }

                listToRemove.add(node);
            } else {
                // impossible de trouver une classe
                if(uri.equals(SOSA + "Procedure")) {
                    VocabConcept vNode = new VocabConcept(uri, onto);
                    vNode.setLabel("en", "Method");
                    vNode.addFather(onto.getRootNode());
                    onto.getRootNode().addChildren(vNode);
                    vNode.addSkosScheme(onto.getMainScheme());
                }
                if(uri.equals("http://opendata.inrae.fr/PO2/core/observationScale")) {
                    VocabConcept vNode = new VocabConcept(uri, onto);
                    vNode.setLabel("en", "Scale");
                    vNode.addFather(onto.getRootNode());
                    onto.getRootNode().addChildren(vNode);
                    vNode.addSkosScheme(onto.getMainScheme());
                }
                if(uri.equals("http://opendata.inrae.fr/PO2/core/Transformation_Process")) {
                    VocabConcept vNode = new VocabConcept(uri, onto);
                    vNode.setLabel("en", "Process");
                    vNode.addFather(onto.getRootNode());
                    onto.getRootNode().addChildren(vNode);
                    vNode.addSkosScheme(onto.getMainScheme());
                }
            }
            Tools.updateProgress(ProgressPO2.OPEN, Tools.getProgress(ProgressPO2.OPEN).getProgress() + pas);
        }
        listToRemove.forEach(OntResource::remove);
        // remove skos ConceptScheme
        List<Individual> conceptToRemove = new ArrayList<>();
        for (ExtendedIterator<Individual> iteScheme = baseModel.listIndividuals(SKOS.ConceptScheme); iteScheme.hasNext(); ) {
            Individual scheme = iteScheme.next();
            conceptToRemove.add(scheme);
        }
        conceptToRemove.forEach(Individual::remove);

        for (VocabConcept node : onto.getRootNode().getSubNode()) {
            node.setPO2Node(node.toString());
            node.setName("PO2 / " + node.getLabel("en").get());
            node.sortSubNode(true);
        }

        Tools.updateProgress(ProgressPO2.OPEN, 1.0);
        Tools.delProgress(ProgressPO2.OPEN);
    }

    private static void convertToSkosConceptScheme(Individual defaultScheme) {
        // creating default SkosConceptScheme
        defaultScheme.addProperty(SKOS.prefLabel, "main", "en");
        defaultScheme.addProperty(SKOS.scopeNote, "main skos concept scheme", "en");
        for (String uri : uriToLoad) {
            OntClass node = baseModel.getOntClass(uri);
            if(node == null) {

                // check in old uri
                List<String> oldsURI = oldURIToLoad.get(uri);
                if(oldsURI != null) {
                    node = oldsURI.stream().filter(s ->
                            baseModel.getOntClass(s) != null
                    ).findFirst().map(s -> baseModel.getOntClass(s)).orElse(null);
                }
            }

            if(node != null) {
                defaultScheme.addProperty(SKOS.hasTopConcept, node);
                addInConceptScheme(defaultScheme, node);
            }
        }
    }

    private static void addInConceptScheme(Individual skosScheme, OntClass node) {
        node.addProperty(SKOS.inScheme, skosScheme);
        for (ExtendedIterator<OntClass> iteClass = node.listSubClasses(true); iteClass.hasNext(); ) {
            OntClass next = iteClass.next();
            addInConceptScheme(skosScheme, next);
        }
    }


    private static org.apache.jena.ontology.Ontology getOnto(OntModel model) {
        for(Individual i : model.listIndividuals(OWL.Ontology).toList()) {
            return i.asOntology();
        }
        return null;
    }

    public static Boolean saveModel(Ontology onto) {
        return saveModel(onto, false);
    }

    /**
     * @param onto
     * @return
     */
    public static Boolean saveModel(Ontology onto, Boolean forPublication) {
        Tools.addProgress(ProgressPO2.SAVE, "save in progress");
        if(!onto.isLocked()) {
            logger.info("Ontology not locked " + onto.getName().getValue());
            Tools.delProgress(ProgressPO2.SAVE);
            return false;
        }
        String path = onto.getLocalPath();
        logger.info("Savegarde du model " + path);
        Boolean success = true;
        OntModel modelTemp = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
        modelTemp.add(baseModel);
        org.apache.jena.ontology.Ontology o = getOnto(modelTemp);
        Tools.updateProgress(ProgressPO2.SAVE, "Local save in progress ...");

        if(o != null) {
            if(forPublication) {
                // changement du numero de version majeur
                onto.incMajorVersion();
                onto.setMinorVersion("0");
            } else {
                // changement du numero de version mineur
                onto.incMinorVersion();
            }

            // setting missing NameSpace
            modelTemp.setNsPrefix("prov", PROV);
            modelTemp.setNsPrefix("omv", OMV);
            modelTemp.setNsPrefix("dct", DCTerms.NS);

            // metadata
            HashMap<String, String> mapProperty = new HashMap<>();

            mapProperty.put(DCTerms.modified.toString(), LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            mapProperty.put(OWL2.versionInfo.toString(), onto.getMajorVersion().get()+"."+ onto.getMinorVersion().get());
            mapProperty.put(PROV+"wasGeneratedBy", "PO² Engine V" + Tools.getEngineVersion());
            mapProperty.put(PROV+"wasDerivedFrom", "PO² Core ontology V" + COREVERSION);
            if(onto.isPublic()) {
                mapProperty.put(DCTerms.rights.getURI(), "http://publications.europa.eu/resource/authority/access-right/PUBLIC");
            } else {
                mapProperty.put(DCTerms.rights.getURI(), "http://publications.europa.eu/resource/authority/access-right/RESTRICTED");
            }
            mapProperty.put(DCTerms.source.getURI(), onto.getCORE_NS());
            mapProperty.put(DCTerms.language.getURI(), "en");
//            mapProperty.put(DCTerms.identifier.getURI(), "https://doi.org/10.57745/DWX7W6");
            mapProperty.put(DCTerms.publisher.getURI(), "INRAE");
            mapProperty.put(OMV+"knownUsage", "https://quantum.mia-ps.inrae.fr/spoq");
            mapProperty.put(OMV+"keywords", onto.getListKeyword());

            mapProperty.forEach((k, v) -> {
                Property p = fakeModel.createProperty(k);
                for (StmtIterator iteDate = o.listProperties(p); iteDate.hasNext(); ) {
                    Statement s = iteDate.nextStatement();
                    iteDate.remove();
                }
                if(v != null) {
                    try {
                        o.setPropertyValue(p, modelTemp.createResource(String.valueOf(new URL(v).toURI())));
                    } catch (URISyntaxException | MalformedURLException e) {
                        o.setPropertyValue(p, modelTemp.createTypedLiteral(v));
                    }
                }
            });


            // sauvegarde des autheurs
            for (String author : onto.getListAuthor()) {
                o.addProperty(DCTerms.contributor, author);
            }
            // sauvegarde de la date

        }

        // create all skos concept scheme
        onto.getlistConceptScheme().forEach(skosScheme -> {
            Individual scheme = modelTemp.createIndividual(skosScheme.getUri(), SKOS.ConceptScheme);
            scheme.addProperty(SKOS.prefLabel,skosScheme.getName(),"en");
            if(skosScheme.getComment() != null) {
                scheme.addProperty(SKOS.scopeNote, skosScheme.getComment(), "en");
            }
        });


        for (VocabConcept node : onto.getRootNode().getSubNode()) {
//            if(!node.getDeprecated())
                saveNodeToModel(node, modelTemp);
        }

        // remove old observation specialisation
        OntClass observationClass = modelTemp.getOntClass("http://opendata.inra.fr/PO2/TransformationProcessObservation");
        if (observationClass != null) {
            ArrayList<OntResource> listToRemo = new ArrayList<>();
            for(ExtendedIterator<OntClass> ei = observationClass.listSubClasses(false); ei.hasNext();) {
                listToRemo.add(ei.next());
            }
            listToRemo.forEach(OntResource::remove);
        }
        observationClass = modelTemp.getOntClass("http://opendata.inrae.fr/PO2/core/TransformationProcessObservation");
        if (observationClass != null) {
            ArrayList<OntResource> listToRemo = new ArrayList<>();
            for(ExtendedIterator<OntClass> ei = observationClass.listSubClasses(false); ei.hasNext();) {
                listToRemo.add(ei.next());
            }
            listToRemo.forEach(OntResource::remove);
        }
        logger.info("fin save to node " + path);
        ArrayList<OntModel> listModel = new ArrayList<>();
        for (ExtendedIterator<OntModel> ite = modelTemp.listSubModels(true); ite.hasNext(); ) {
            OntModel m = ite.next();
            listModel.add(m);
        }
        for (Iterator<OntModel> ite = listModel.iterator(); ite.hasNext(); ) {
            OntModel m = ite.next();
            modelTemp.removeSubModel(m);
        }

        FileOutputStream output = null;
        try {
            output = new FileOutputStream(new File(path));
            RDFWriter ontologyWriter = RDFWriter.create().source(modelTemp).lang(Lang.TTL).build();
            ontologyWriter.output(output);
            output.flush();
            logger.debug("sauvegarde locale reussie");
        } catch (IOException e) {
            logger.error(e.toString());
            success = false;
        } finally {
            if(output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                    success = false;
                    logger.error(ex.toString());
                }
            }
        }

        Tools.updateProgress(ProgressPO2.SAVE,1.0);
        Tools.delProgress(ProgressPO2.SAVE);
        return success;
    }

    private static OntClass saveNodeToModel(VocabConcept node, OntModel model) {
        OntClass cla = model.getOntClass(node.getURI());
        if (cla == null) {

            cla = model.createClass(node.getURI());
            if(node.getDeprecated()) {
                cla.addProperty(OWL2.deprecated, model.createTypedLiteral(true));
            }

            OntClass finalCla = cla;
            node.getListSkosScheme().forEach(skosScheme -> {
                Individual i = model.getIndividual(skosScheme.getUri());
                if(i != null) {
                    finalCla.addProperty(SKOS.inScheme, i);
                }
            });


//            for (Entry<SimpleStringProperty, SimpleStringProperty> e : node.getLabel().entrySet()) {
//                if(e.getKey().get() != null && !e.getKey().get().isEmpty()) {
//                    cla.addLabel(e.getValue().get(), e.getKey().get());
//                }
//            }
//
//            for (Entry<SimpleStringProperty, SimpleStringProperty> e : node.getDescription().entrySet()) {
//                if(e.getKey().get() != null && !e.getKey().get().isEmpty()) {
//                    cla.addComment(e.getValue().get(), e.getKey().get());
//                }
//            }

//            for (SimpleStringProperty i : node.getListExactMatch()) {
//                cla.addSameAs(model.getResource(i.get()));
//            }

            for (Entry<SimpleStringProperty, SimpleStringProperty> e : node.getLabel().entrySet()) {
                if(e.getKey().get() != null && !e.getKey().get().isEmpty()) {
                    cla.addProperty(SKOS.prefLabel, e.getValue().get(), e.getKey().get());
                }
            }

            for (Entry<SimpleStringProperty, SimpleStringProperty> e : node.getAltLabel().entrySet()) {
                if(e.getKey().get() != null && !e.getKey().get().isEmpty()) {
                    String[] listAlt = e.getValue().get().split("\\r?\\n?\\::");
                    for(String s : listAlt) {
                        if(!s.trim().isEmpty()) {
                            cla.addProperty(SKOS.altLabel, s.trim(), e.getKey().get());
                        }
                    }
                }
            }


            for (Entry<SimpleStringProperty, SimpleStringProperty> e : node.getDefinition().entrySet()) {
                if(e.getKey().get() != null && !e.getKey().get().isEmpty()) {
                    String[] listAlt = e.getValue().get().split("\\r?\\n?\\::");
                    for(String s : listAlt) {
                        if(!s.trim().isEmpty()) {
                            cla.addProperty(SKOS.definition, s.trim(), e.getKey().get());
                        }
                    }
                }
            }

            cla.addProperty(SKOS.scopeNote, node.getScopeNote());
            cla.addProperty(SKOS.editorialNote, node.getEditorialNote());
            cla.addProperty(SKOS.note, node.getNote());

            for (SimpleStringProperty i : node.getListExactMatch()) {
                cla.addProperty(SKOS.exactMatch, model.getResource(i.get()));
            }

            for (SimpleStringProperty i : node.getListCloseMatch()) {
                cla.addProperty(SKOS.closeMatch, model.getResource(i.get()));
            }

            for (SimpleStringProperty i : node.getListDimension()) {
                cla.addProperty(hasDimension, model.createTypedLiteral(i.get()));
            }

        }

        for (VocabConcept son : node.getSubNode()) {
//            if(!son.getDeprecated())
                cla.addSubClass(saveNodeToModel(son, model));
        }

        return cla;

    }


    /**
     * @param nodeClassToLoad
     * @param nodeLocalRoot
     */
    private static void createNode(OntClass nodeClassToLoad, VocabConcept nodeLocalRoot, ArrayList<OntClass> listToRemove, Ontology ontology) {

        // on regarde s'il y a plusieur concept

        // est-ce que le node existe deja !
        VocabConcept node = ontology.getNodeByURI(nodeClassToLoad.getURI());
        if (node == null) {
            node = new VocabConcept(nodeClassToLoad.getURI(), ontology);

            node.addFather(nodeLocalRoot);
            nodeLocalRoot.addChildren(node);

            // recherche des skosConceptScheme pour ce node
            for (StmtIterator iteSch = nodeClassToLoad.listProperties(SKOS.inScheme); iteSch.hasNext(); ) {
                Statement s = iteSch.next();
                String uriScheme = s.getObject().asResource().getURI();
                SkosScheme scheme = ontology.getSkosScheme(uriScheme);
                if(scheme != null) {
                    node.addSkosScheme(scheme);
                } else {
                    logger.info("Unable to find skos concept scheme " + uriScheme);
                }
            }

            // check for deprecated
            Statement deprec = nodeClassToLoad.getProperty(OWL2.deprecated);
            if(deprec != null) {
                node.setDeprecated(true);
            }

            // RDF Part to be removed in future released
            for (ExtendedIterator<RDFNode> iteLab = nodeClassToLoad.listLabels(null); iteLab.hasNext(); ) {
                Literal l = iteLab.next().asLiteral();
                node.setLabel(l.getLanguage(), l.getString());
            }

            for (ExtendedIterator<RDFNode> iteLab = nodeClassToLoad.listComments(null); iteLab.hasNext(); ) {
                Literal l = iteLab.next().asLiteral();
                node.setDefinition(l.getLanguage(), l.getString(), true);
            }


            for (ExtendedIterator<? extends Resource> iteLab = nodeClassToLoad.listSameAs(); iteLab.hasNext(); ) {
                OntResource l = (OntResource) iteLab.next();
                node.addExactMatch(l.getURI());
            }

            // SKOS Part

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.prefLabel); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                if(st.getObject().isLiteral()) {
                    Literal li = st.getObject().asLiteral();
                    node.setLabel(li.getLanguage(), li.getString(), true);
                }
            }

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.altLabel); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                if(st.getObject().isLiteral()) {
                    Literal li = st.getObject().asLiteral();
                    node.setAltLabel(li.getLanguage(), li.getString(), true);
                }
            }

            // if editorialNote exist => scope is now scope. not def
            Boolean scopeNoteConversion = nodeClassToLoad.getProperty(SKOS.editorialNote) == null;

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.definition); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                if(st.getObject().isLiteral()) {
                    Literal li = st.getObject().asLiteral();
                    node.setDefinition(li.getLanguage(), li.getString(), true);
                }
            }

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.editorialNote); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                if(st.getObject().isLiteral()) {
                    Literal li = st.getObject().asLiteral();
                    node.setEditorialNote(li.getString());
                }
            }

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.scopeNote); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                if(st.getObject().isLiteral()) {
                    Literal li = st.getObject().asLiteral();
                    if(scopeNoteConversion) {
                        node.setDefinition(li.getLanguage(), li.getString(), true);
                    } else {
                        node.setScopeNote(li.getString());
                    }
                }
            }

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.note); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                if(st.getObject().isLiteral()) {
                    Literal li = st.getObject().asLiteral();
                    node.setNote(li.getString());
                }
            }

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.exactMatch); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                node.addExactMatch(st.getObject().toString());
            }

            for (StmtIterator iteLab = nodeClassToLoad.listProperties(SKOS.closeMatch); iteLab.hasNext(); ) {
                Statement st = iteLab.next();
                node.addCloseMatch(st.getObject().toString());
            }

            for (StmtIterator iteDim = nodeClassToLoad.listProperties(JenaTools.hasDimension); iteDim.hasNext(); ) {
                Statement st = iteDim.next();
                node.addDimension(st.getLiteral().getString());
            }

            for (ExtendedIterator<OntClass> iteClass = nodeClassToLoad.listSubClasses(true); iteClass.hasNext(); ) {
                OntClass next = iteClass.next();
                createNode(next, node, listToRemove, ontology);
            }



            listToRemove.add(nodeClassToLoad);
//            nodeClassToLoad.remove();

        } else {
            node.addFather(nodeLocalRoot);
            nodeLocalRoot.addChildren(node);
        }

    }

    /**
     *
     * @param onto
     * @param data
     * @param individual if null then create a new individual
     * @param attr
     * @param value
     * @param unit
     * @param hash
     * @param json
     * @return a new individual if individual paramater is null.
     */
    public static Individual addQuantityQuality(Ontology onto, Data data, Individual individual, String attr, String value, String unit, String hash, JSONObject json) {
        Individual iResult = individual;
        if(value != null) {

            if (unit != null && !unit.isEmpty()) { // quantity

                if(iResult == null) {
                    iResult = data.getDataModel().createIndividual(data.getDATA_NS() + "result_line_" + hash, onto.getCoreModel().getOntClass(SCH + "QuantitativeValue"));
                } else {
                    //individual already axiste. Only add Type
                    iResult.addRDFType(onto.getCoreModel().getOntClass(SCH + "QuantitativeValue"));
                }
                Property minValueProperty = onto.getCoreModel().getProperty(SCH+"minValue");
                Property maxValueProperty = onto.getCoreModel().getProperty(SCH+"maxValue");
                Property valueProperty = onto.getCoreModel().getProperty(SCH+"value");
                Property unitCodeProperty = onto.getCoreModel().getProperty(SCH+"unitCode");
                Property unitTextProperty = onto.getCoreModel().getProperty(SCH+"unitText");

                Pair<Double, Double> res = Tools.getQuantValueAsMinMax(value, json);
                iResult.addProperty(SKOS.prefLabel,attr + " " +value + " " + unit, "");
                iResult.addProperty(valueProperty, value);
                iResult.addProperty(unitTextProperty, unit);
                System.out.println(unit);
                Unit un = Tools.getUnit(unit);
                UnitConverter converter = Tools.getConverter(un);
                if(converter != null) {
                    Double minValue;
                    Double maxValue;
                    if(res.getLeft().isNaN() || res.getLeft().isInfinite()) {
                        minValue = res.getLeft();
                    } else {
                        minValue = converter.convert(res.getLeft()).doubleValue();
                    }
                    if(res.getRight().isNaN() || res.getRight().isInfinite()) {
                        maxValue = res.getRight();
                    } else {
                        maxValue = converter.convert(res.getRight()).doubleValue();
                    }

                    iResult.addProperty(minValueProperty, data.getDataModel().createTypedLiteral(minValue, XSDDatatype.XSDdouble));
                    iResult.addProperty(maxValueProperty, data.getDataModel().createTypedLiteral(maxValue, XSDDatatype.XSDdouble));

                    UnitFormat print = UCUMFormat.getInstance(UCUMFormat.Variant.CASE_SENSITIVE);
                    String unitCode = print.format(un.getSystemUnit());
                    if(unitCode.isEmpty()) {
                        unitCode = unit;
                    }
                    iResult.addProperty(unitCodeProperty, unitCode);
                } else {
                    Double minValue = res.getLeft();
                    Double maxValue = res.getRight();
                    iResult.addProperty(minValueProperty, data.getDataModel().createTypedLiteral(minValue, XSDDatatype.XSDdouble));
                    iResult.addProperty(maxValueProperty, data.getDataModel().createTypedLiteral(maxValue, XSDDatatype.XSDdouble));
                    iResult.addProperty(unitCodeProperty, unit);
                }
            } else { // quality
                if(iResult == null) {
                    iResult = data.getDataModel().createIndividual(data.getDATA_NS() + "result_line_" + hash, onto.getCoreModel().getOntClass(SCH + "QualitativeValue"));
                } else {
                    iResult.addRDFType(onto.getCoreModel().getOntClass(SCH + "QualitativeValue"));
                }
                iResult.addProperty(SKOS.prefLabel, attr + " " +value, "");
                iResult.addProperty(onto.getCoreModel().getProperty(SCH+"value"), value);
            }
        }
        return iResult;
    }
}
