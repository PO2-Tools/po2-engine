/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.externalTools;

import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.github.sardine.impl.SardineException;
import com.github.sardine.impl.SardineImpl;
import com.google.common.net.UrlEscapers;
import fr.inrae.po2engine.exception.AlreadyExistException;
import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.model.*;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.utils.*;
import fr.inrae.po2engine.vocabsearch.Requete;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import javax.xml.namespace.QName;
import java.io.*;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CloudConnector {

    private static String login = PO2Properties.getProperty("nextCloud.login");
    private static String pwd = PO2Properties.getProperty("nextCloud.password");
    private static String protocol = PO2Properties.getProperty("nextCloud.protocol");
    private static String host = PO2Properties.getProperty("server.host");
    private static String path = PO2Properties.getProperty("nextCloud.path");
    private static Integer port = Integer.parseInt(PO2Properties.getProperty("nextCloud.port"));
    private static String cibleJson = PO2Properties.getProperty("nextCloud.cible");
    private static String platformJson = PO2Properties.getProperty("nextCloud.platform");

    private static String dataGroupJson = PO2Properties.getProperty("nextCloud.dataGroup");

    private static String webdavURI = path+"remote.php/webdav/";
//    private static CloseableHttpClient mainClient = null;
    private static HttpHost mainTarget = null;
    private static HttpClientContext mainContext = null;
    private static String loginUser = null;
    private static String passwordUser = null;
 //   private static String displayName = null;
    private static Boolean initUser = false;
    private static Sardine sardine = null;
    private static Sardine sardineListFile = null;
    private static Sardine sardineManager = null;
    private static BooleanProperty online = new SimpleBooleanProperty(false);
    private static Logger logger = LogManager.getLogger(CloudConnector.class);
    private static Boolean isInit = false;
//    private static SSLConnectionSocketFactory sslsf;
    private static HttpClientBuilder httpBuilder;

    private CloudConnector() {
    }

    public static void reset() {
        isInit = false;
    }

    public static String getDisplayLogin() {
        return loginUser;
    }
    private static void initProperty() {
        login = PO2Properties.getProperty("nextCloud.login");
        pwd = PO2Properties.getProperty("nextCloud.password");
        protocol = PO2Properties.getProperty("nextCloud.protocol");
        host = PO2Properties.getProperty("server.host");
        path = PO2Properties.getProperty("nextCloud.path");
        port = Integer.parseInt(PO2Properties.getProperty("nextCloud.port"));
        cibleJson = PO2Properties.getProperty("nextCloud.cible");
        platformJson = PO2Properties.getProperty("nextCloud.platform");
        dataGroupJson = PO2Properties.getProperty("nextCloud.dataGroup");
    }
    private static void init() {
        if(!isInit) {
            initProperty();
            try {
                SSLContextBuilder builder = new SSLContextBuilder();
                builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

                //TODO remove the noop param when quantum palaiseau is OK
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                        builder.build(), NoopHostnameVerifier.INSTANCE);
                httpBuilder = HttpClientBuilder.create().setSSLSocketFactory(sslsf);
                CredentialsProvider credsProvider = new BasicCredentialsProvider();
                credsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(login, pwd));

                mainTarget = new HttpHost(host, port, protocol);
                AuthCache authCache = new BasicAuthCache();
                BasicScheme basicAuth = new BasicScheme();
                authCache.put(mainTarget, basicAuth);

                mainContext = HttpClientContext.create();
                mainContext.setCredentialsProvider(credsProvider);
                mainContext.setAuthCache(authCache);
                sardineListFile = new SardineImpl(httpBuilder, PO2Properties.getProperty("nextCloud.listFile.login"), PO2Properties.getProperty("nextCloud.listFile.password"));

                sardineListFile.enablePreemptiveAuthentication(host);
                sardineManager = new SardineImpl(httpBuilder, login, pwd);
                sardineManager.enablePreemptiveAuthentication(host);

                isInit = true;
                checkOnline();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }

    }

    public static BooleanProperty onlineProperty() {
        return online;
    }

    public static void refreshPlatform() {
        init();
        try {
            if (online.getValue()) {
                HttpClient client = httpBuilder.build();
                HttpResponse response = client.execute(mainTarget, new HttpGet(path+"index.php/" + platformJson + "/download"), mainContext);
                if (response.getStatusLine().getStatusCode() == 200) {
                    InputStream configPlatform = response.getEntity().getContent();
                    Platform.init(configPlatform);
                    configPlatform.close();
                }
            }
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public static void refreshCible() {
        init();
        try {
            if (online.getValue()) {
                HttpClient client = httpBuilder.build();
                HttpResponse response = client.execute(mainTarget, new HttpGet(path+"index.php/" + cibleJson + "/download"), mainContext);
                if (response.getStatusLine().getStatusCode() == 200) {
                    InputStream configCible = response.getEntity().getContent();
                    Requete.init(configCible);
                    configCible.close();
                }
            }
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public static void refreshDatGroup() {
        init();
        try {
            if (online.getValue()) {
                HttpClient client = httpBuilder.build();
                HttpResponse response = client.execute(mainTarget, new HttpGet(path+"index.php/" + dataGroupJson + "/download"), mainContext);
                if (response.getStatusLine().getStatusCode() == 200) {
                    InputStream configDataGroup = response.getEntity().getContent();
                    String cdg;
                    try {
                        cdg = IOUtils.toString(configDataGroup, StandardCharsets.UTF_8);
                        JSONArray array = new JSONArray(cdg);
                        int n = array.length();
                        for (int i = 0; i < n; i++) {
                            JSONObject o = array.getJSONObject(i);
                            String groupName = o.getString("group-name");
                            JSONArray groupData = o.getJSONArray("group-data");
                            for(int j = 0; j < groupData.length(); j++) {
                                String ds = groupData.getString(j);
                                Data d = Datas.getData(ds);
                                if(d != null) {
                                    d.setGroup(groupName);
                                }
                            }
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                    configDataGroup.close();
                }
            }
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage(), e);
        }
    }

    public static void checkOnline() {
        init();
        if (!online.getValue()) {
            try {
                HttpClient client = httpBuilder.build();
//                HttpClient client = HttpClientBuilder.create().build();
                client.execute(mainTarget, new HttpGet(path + "index.php/" + cibleJson + "/download"), mainContext);
                online.setValue(true);
//                listOntology(null,null);
            } catch (IOException e) {
                logger.error(e.getClass() + ":" + e.getMessage(), e);
                online.setValue(false);
            }
        }
    }

    private static void initDisplayName(String login, String password) throws URISyntaxException, IOException {
        init();
        URIBuilder uri = new URIBuilder(path + "ocs/v1.php/cloud/users/"+login);
        uri.setParameter("format", "json");
        HttpGet request = new HttpGet(uri.build());
        request.addHeader("OCS-APIRequest", "true");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        credsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(login, password));

        CloseableHttpClient localClient = httpBuilder.setDefaultCredentialsProvider(credsProvider).build();
        HttpResponse response = localClient.execute(mainTarget, request, mainContext);
        String content = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());
        JSONObject shares = new JSONObject(content);
        JSONObject listElement = shares.getJSONObject("ocs").getJSONObject("data");
 //       displayName = listElement.getString("displayname");
    }

    private static Boolean testConnection(String login, String password) {
        initUser = false;
        init();
        checkOnline();
        if (online.get()) {
            Sardine sardineTemp = new SardineImpl(httpBuilder, login, password);
            sardineTemp.enablePreemptiveAuthentication(host);
            try {
                if (sardineTemp.exists(protocol + "://" + host + webdavURI)) {
                    logger.info("user connected : " + login);
                    initUser = true;
                    sardine = sardineTemp;
                    loginUser = login;
                    passwordUser = password;
       //             initDisplayName(login, password);
                }
            } catch (SardineException e) {
                if (e.getStatusCode() == 401) {
                    logger.info("bad login or password");
                }
            } catch (IOException e) {
                logger.error(e.getClass() + ":" + e.getMessage() + " - test connection", e);
            }
        }
        return initUser;
    }

    public static Boolean initUser(String login, String password) {
        return testConnection(login, password);
    }



    public static Boolean isInitUser() {
        return initUser;
    }

    public static void searchProperties(OntoData ontoData) throws IOException {
        Set<QName> mapFileID = new HashSet<>();
        QName fileID = new QName("http://owncloud.org/ns", "fileid", "oc");
        mapFileID.add(fileID);
        Set<QName> mapComment = new HashSet<>();
        QName message = new QName("http://owncloud.org/ns", "message", "oc");
        QName log = new QName("http://owncloud.org/ns", "log", "oc");
        mapComment.add(message);
        mapComment.add(log);

        List<DavResource> tryFileID = sardineListFile.propfind(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontoData.getName().get()), 0, mapFileID);
        for (DavResource io : tryFileID) {
            String id = io.getCustomProps().get("fileid");

            List<DavResource> tryComment = sardineListFile.propfind(protocol + "://" + host + path + "remote.php/dav/comments/files/" + UrlEscapers.urlPathSegmentEscaper().escape(id), 1, mapComment);
            for (DavResource io2 : tryComment) {

                String comm = io2.getCustomProps().get("message");
                String ll = io2.getCustomProps().get("log");

                if (comm != null && !comm.isEmpty() && !comm.equalsIgnoreCase("null")) {
                    ontoData.setDescription(comm);
                }
                if (ll != null && !ll.isEmpty() && !ll.equalsIgnoreCase("null")) {
                    ontoData.setOntoDataLog(ll);
                }
            }
        }
    }

    public static void searchDavFileId(OntoData ontoData) throws IOException {
        init();
        Set<QName> mapFileID = new HashSet<>();
        QName fileID = new QName("http://owncloud.org/ns", "fileid", "oc");
        mapFileID.add(fileID);

        try {
            List<DavResource> findFileID = sardineListFile.propfind(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontoData.getName().get()), 1, mapFileID);
            for (DavResource fileResource : findFileID) {

                if (ontoData instanceof Data && fileResource.toString().endsWith("zip")) {
                    ontoData.setDavFileID(fileResource.getCustomProps().get("fileid"));
                }

                if (ontoData instanceof Ontology && (fileResource.toString().endsWith("rdf") || fileResource.toString().endsWith("ttl"))) {
                    ontoData.setDavFileID(fileResource.getCustomProps().get("fileid"));
                }

            }
        } catch (SardineException se) {
            logger.error("error in response for " + ontoData.getName().getValue());
            se.printStackTrace();
        }
    }

    public static void listData(DoubleProperty doubleProgress, Double max) {
        init();
        try {
            if (online.get()) {
                Datas.getDatas().clear();
                URIBuilder uri = new URIBuilder(path + "ocs/v1.php/apps/files_sharing/api/v1/shares");
                uri.setParameter("path", "/PO2VocabManager/Data/");
                uri.setParameter("subfiles", "true");
                uri.setParameter("format", "json");
                HttpGet request = new HttpGet(uri.build());
                request.addHeader("OCS-APIRequest", "true");

                CredentialsProvider userCredsProvider = new BasicCredentialsProvider();
                userCredsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(login, pwd));

                HttpHost target = new HttpHost(host, port, protocol);
                AuthCache userAuthCache = new BasicAuthCache();
                userAuthCache.put(target, new BasicScheme());

                HttpClientContext context = HttpClientContext.create();
                context.setCredentialsProvider(userCredsProvider);
                context.setAuthCache(userAuthCache);

                HttpClient client = httpBuilder.build();

                HttpResponse response = client.execute(mainTarget, request, context);
                String content = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());

                JSONObject shares = new JSONObject(content);
                JSONArray listElement = shares.getJSONObject("ocs").getJSONArray("data");

                Set<QName> mapFileID = new HashSet<>();
                QName fileID = new QName("http://owncloud.org/ns", "fileid", "oc");
                mapFileID.add(fileID);
                Set<QName> mapComment = new HashSet<>();
                QName message = new QName("http://owncloud.org/ns", "message", "oc");
                QName log = new QName("http://owncloud.org/ns", "log", "oc");
                mapComment.add(message);
                mapComment.add(log);

                if (listElement != null && listElement.length() > 0) {
                    Double pro = null;
                    if(doubleProgress != null && max != null) {
                        pro = max / (double) listElement.length();
                    }
                    for (int i = 0; i < listElement.length(); i++) {
                        if (listElement.getJSONObject(i).getInt("share_type") == 3) {
                            Data data = Datas.addData(listElement.getJSONObject(i).getString("file_target").replaceAll("/", ""));
                            if (data != null) {

                                Integer d = listElement.getJSONObject(i).getInt("stime");
                                data.setCreationDate(new Date((long) d * 1000));
                                data.setPublic(listElement.getJSONObject(i).isNull("share_with"));
                                data.setDownloadLink(listElement.getJSONObject(i).getString("url") + "/download");
                                data.setLocal(false);
                            }
                        }
                        if(pro != null) {
                            doubleProgress.setValue(doubleProgress.getValue()+pro);
                        }
                    }
                }
                refreshDatGroup();
            }
            // on cherche les datas offline
            File local = new File(Tools.dataPath);
            File[] listLocalData = local.listFiles();
            if (listLocalData != null) {
                if(online.get()) {
                    for (int i = 0; i < listLocalData.length; i++) {
                        if (Datas.getData(listLocalData[i].getName()) == null) {
                            if(listLocalData[i].isFile()) {
                                listLocalData[i].delete();
                            } else {
                                FileUtils.deleteDirectory(listLocalData[i]);
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < listLocalData.length; i++) {
                        Data d = Datas.addData(listLocalData[i].getName());
                        if (d != null) {
                            d.setLocal(true);
                            logger.info("creation de : " + d.getName().get() + " local creer le " + d.getCreationDate() + " is private : " + d.isPublic());
                        }
                    }
                }
            } else {
                local.mkdirs();
            }
        } catch (URISyntaxException | IOException /*| ParserConfigurationException*/ | UnsupportedOperationException/* | SAXException*/ e) {

            logger.error(e.getClass() + ":" + e.getMessage() + " - list data", e);
        }
    }

    public static void listOntology(DoubleProperty doubleProgress, Double max) {
        init();
        try {
            if (online.get()) {
                Ontologies.getOntologies().clear();
                // on cherche les ontologies online
                // oo supprime les ontologies local qui ne sont pas online. (// en cas de renomage , suppression, ...)
                URIBuilder uri = new URIBuilder(path + "ocs/v1.php/apps/files_sharing/api/v1/shares");
                uri.setParameter("path", "/PO2VocabManager/Ontologies/");
                uri.setParameter("subfiles", "true");
                uri.setParameter("format", "json");
                HttpGet request = new HttpGet(uri.build());
                request.addHeader("OCS-APIRequest", "true");
                HttpClient client = httpBuilder.build();
                HttpResponse response = client.execute(mainTarget, request, mainContext);
                String content = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());


                JSONObject shares = new JSONObject(content);
                JSONArray listElement = shares.getJSONObject("ocs").getJSONArray("data");



                if (listElement != null && listElement.length() > 0) {
                    Double pro = null;
                    if(doubleProgress != null && max != null) {
                        pro = max / (double) listElement.length();
                    }
                    for (int i = 0; i < listElement.length(); i++) {
                        if (listElement.getJSONObject(i).getInt("share_type") == 3) {
                            Ontology o = Ontologies.addOntology(listElement.getJSONObject(i).getString("file_target").replaceAll("/", ""));
                            if (o != null) {

                                Integer d = listElement.getJSONObject(i).getInt("stime");
                                o.setCreationDate(new Date((long) d * 1000));
                                o.setPublic(listElement.getJSONObject(i).isNull("share_with"));
                                o.setDownloadLink(listElement.getJSONObject(i).getString("url") + "/download");
                                o.setLocal(false);
                                logger.info("creation de : " + o.getName().get() + " distante creer le " + o.getCreationDate() + " is private : " + !o.isPublic());
                            }
                        }
                        if(pro != null) {
                            doubleProgress.setValue(doubleProgress.getValue()+pro);
                        }
                    }
                }
            }

            // on cherche les ontologies offline
            File local = new File(Tools.ontoPath);
            File[] listLocalOnto = local.listFiles();
            if (listLocalOnto != null) {
                if (online.get()) {
                    for (int i = 0; i < listLocalOnto.length; i++) {
                        if (Ontologies.getOntology(listLocalOnto[i].getName()) == null) { // ontologie seulement local (pas à distance), on supprime
                            if(listLocalOnto[i].isFile()) {
                                listLocalOnto[i].delete();
                            } else {
                                FileUtils.deleteDirectory(listLocalOnto[i]);
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < listLocalOnto.length; i++) {
                        Ontology o = Ontologies.addOntology(listLocalOnto[i].getName());
                        if (o != null) {
                            o.setLocal(true);
                            logger.info("creation de : " + o.getName().get() + " local creer le " + o.getCreationDate() + " is private : " + o.isPublic());
                        }
                    }
                }
            } else {
                local.mkdirs();
            }

        } catch (URISyntaxException | IOException /*| ParserConfigurationException*/ | UnsupportedOperationException/* | SAXException*/ e) {

            logger.error(e.getClass() + ":" + e.getMessage() + " - list ontology", e);
        }
    }

    public static File getDavData(Data data) {
        init();
        File dataFile = null;
        if (isInitUser() && sardine != null) {
            try {
                URI uriRead = new URI(protocol, host, webdavURI + data.getName().get(), null);

                if (sardine.exists(uriRead.toString())) {
                    data.setRead(true);
                    // on test l'écriture
                    try {
                        URI uriWrite = new URI(protocol, host, webdavURI + data.getName().get() + "/test" + loginUser, null);
                        sardine.put(uriWrite.toString(), new byte[0]);
                        data.setWrite(true);
                        sardine.delete(uriWrite.toString());
                    } catch (SardineException se) {
                        data.setWrite(false);

                    }
                    List<DavResource> listDav = sardine.list(uriRead.toString(), 1);
                    File dataRoot = null;
                    for (DavResource resource : listDav) {
                        String name = resource.getPath().replace(webdavURI, "");

                        if (resource.isDirectory()) {
                            dataRoot = new File(Tools.dataPath + name);
                            FileUtils.deleteDirectory(dataRoot);
                            dataRoot.mkdirs();
                        } else {//resource.getContentType().equals("application/zip")
                            URI uriGetResource = new URI(protocol, host, resource.getPath(), null);
                            if (resource.getContentType().equals("application/zip")) {
                                data.setDavPath(resource.getPath());
                                InputStream is = sardine.get(uriGetResource.toString());
                                File f = File.createTempFile("ontoData", ".zip");
                                FileOutputStream fos = new FileOutputStream(f);
                                IOUtils.copy(is, fos);
                                fos.close();
                                try (ZipFile archive = new ZipFile(f)) {
                                    // on décompresse l'archive (oui on ecrase tout le contenu)
                                    for (Enumeration<ZipArchiveEntry> ite2 = archive.getEntriesInPhysicalOrder(); ite2.hasMoreElements(); ) {
                                        ZipArchiveEntry entry2 = ite2.nextElement();
                                        File curfile2 = new File(dataRoot, entry2.getName());
                                        if (!entry2.isDirectory()) {
                                            File parent = curfile2.getParentFile();
                                            if (!parent.exists()) {
                                                parent.mkdirs();
                                            }
                                            try {
                                                OutputStream out2 = new FileOutputStream(curfile2);
                                                org.apache.commons.compress.utils.IOUtils.copy(archive.getInputStream(entry2), out2);
                                                out2.close();
                                            } catch (FileNotFoundException fnf) {
                                                logger.error("Failed unzip file " + curfile2.getAbsolutePath());
                                            }
                                        } else {
                                            curfile2.mkdirs();
                                        }
                                    }
                                    dataFile = dataRoot;
                                    //    ontology.setDavPath(resource.getPath());
                                }
                            } else {
                                // not a zip file !!!
                            }

                        }
                    }
                    // l'utilisateur a access en lecture  via dav
                    logger.info("user can read");
                } else {
                    // l'utilisateur n'a pas access en lecture via dav
                    logger.info("user can't read");
                }
            } catch (IOException e) {
                logger.error(e.getClass() + ":" + e.getMessage() + " - download dav impossible", e);
            } catch (URISyntaxException e) {
                // ne doit jamais arriver !!!
                logger.error("Wrong URI !!" + e.getMessage());
            }

        }
        return dataFile;
    }

    public static File getDavOntology(Ontology ontology) {
        init();
        File ontoFile = null;
        if (isInitUser() && sardine != null) {
            try {
                if (sardine.exists(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontology.getName().get()))) {
                    ontology.setRead(true);
                    // on test l'écriture
                    try {
                        sardine.put(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontology.getName().get()) + "/test" + loginUser, new byte[0]);
                        ontology.setWrite(true);
                        sardine.delete(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontology.getName().get()) + "/test" + loginUser);
                    } catch (SardineException se) {
                        ontology.setWrite(false);

                    }
                    List<DavResource> listDav = sardine.list(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontology.getName().get()), 1);
                    for (DavResource resource : listDav) {
                        String name = resource.getPath().replace(webdavURI, "");

                        File file = new File(Tools.ontoPath + name);
                        if (resource.isDirectory()) {
                            FileUtils.deleteDirectory(file);
                            file.mkdirs();
                        } else {
                            InputStream is = sardine.get(protocol + "://" + host + "/" + UrlEscapers.urlPathSegmentEscaper().escape(resource.getPath()));
                            FileOutputStream fos = new FileOutputStream(file);
                            IOUtils.copy(is, fos);
                            fos.close();
                            if (FilenameUtils.getExtension(file.getPath()).equals("rdf")
                                    || FilenameUtils.getExtension(file.getPath()).equals("owl") || FilenameUtils.getExtension(file.getPath()).equals("ttl")) {
                                ontoFile = file;
                                ontology.setDavPath(resource.getPath());
                                logger.info("Dav path : "+resource.getPath());

                            }
                        }
                    }
                    // l'utilisateur a access en lecture
                    logger.info("user can read");
                } else {
                    // l'utilisateur n'a pas access en lecture
                    logger.info("user can't read");
                }
            } catch (IOException e) {
                logger.error(e.getClass() + ":" + e.getMessage() + " - download dav impossible", e);
            }

        }
        return ontoFile;
    }

    public static MutablePair<String, Date> getDavVersion(OntoData ontoData) throws URISyntaxException, IOException {
        init();
        MutablePair<String, Date> pair = new MutablePair<>(null, null);
        if(ontoData.getDavFileID().isEmpty()) {
            searchDavFileId(ontoData);
        }
        if(ontoData.getDavFileID() != null && !ontoData.getDavFileID().equals("")) {
            URI uriVersion = new URI(protocol + "://" + host + path + "remote.php/dav/versions/" + "listFiles" + "/versions/" + ontoData.getDavFileID());
           try {
                List<DavResource> versionResource = sardineListFile.list(uriVersion.toString(), 1, true);
                for (DavResource rr : versionResource) {
                    if (rr.getEtag() != null) {
                        if (pair.getRight() == null || pair.getRight().before(rr.getModified())) {
                            pair.setLeft(rr.getEtag());
                            pair.setRight(rr.getModified());
                        }
                    }
                }
           } catch (SocketException se) {
           }
        }
        return pair;
    }

    public static ZipFile getZipFile(OntoData ontoData) {
        init();
        ZipFile archive = null;
        CloseableHttpClient client = httpBuilder.build();
        try {
            URIBuilder uri = new URIBuilder(ontoData.getDownloadLink());
            HttpGet request = new HttpGet(uri.build());
            HttpResponse response = client.execute(request);
            InputStream fi = response.getEntity().getContent();
            File f = File.createTempFile("ontoData", ".zip");
            FileOutputStream fo = new FileOutputStream(f);
            IOUtils.copy(fi, fo);
            fo.close();
            archive = new ZipFile(f);

        } catch (URISyntaxException | IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage() + " - download zip impossible", e);
        }
        return archive;
    }

    //    public static ZipFile getZipOntology(Ontology ontology) {
//        ZipFile archive = null;
//        CloseableHttpClient client = HttpClients.custom().build();
//        try {
//            URIBuilder uri = new URIBuilder(ontology.getDownloadLink());
//            HttpGet request = new HttpGet(uri.build());
//            HttpResponse response = client.execute(request);
//            InputStream fi = response.getEntity().getContent();
//            File f = File.createTempFile("onto", ".zip");
//            FileOutputStream fo = new FileOutputStream(f);
//            IOUtils.copy(fi, fo);
//            fo.close();
//            archive = new ZipFile(f);
//
//        } catch (URISyntaxException | IOException e) {
//            logger.error(e.getClass() + ":" + e.getMessage() + " - download zip impossible", e);
//        }
//        return archive;
//    }
    public static Boolean update(Data data) {
        init();
        // zip du répertoire
        try {
            if(!data.isLocked()) {
                return false;
            }
            Tools.addProgress(ProgressPO2.SAVE, "Compressing files ...");
            File newZip = Tools.compressZipfile(data.getProjectFile());
            if (newZip.exists()) {
                logger.debug("zip file existe");
            } else {
                logger.debug("zip not exist " + newZip.getAbsolutePath());
            }
            // on onvoie le zip file protocole dav
            Tools.updateProgress(ProgressPO2.SAVE, "Uploading files ...", 0.33);
            byte[] dataToUpdate = IOUtils.toByteArray(new FileInputStream(newZip));
            sardine.put(protocol + "://" + host + "/" + UrlEscapers.urlPathSegmentEscaper().escape(data.getDavPath()), dataToUpdate);
            Tools.updateProgress(ProgressPO2.SAVE, "Checking files ...", 0.67);
            InputStream temp = sardine.get(protocol + "://" + host + "/" + UrlEscapers.urlPathSegmentEscaper().escape(data.getDavPath()));
            byte[] dataToTest = IOUtils.toByteArray(temp);
            temp.close();
            newZip.delete();
            Boolean egal = Arrays.equals(dataToUpdate, dataToTest);
            if(egal) {
                CloudConnector.addLog(data, "edited");
                try {
                    MutablePair<String, Date> versionDate = CloudConnector.getDavVersion(data);
                    if(versionDate.getLeft() != null) {
                        data.setCurrentVersionLoaded(versionDate.getLeft());
                    }
                } catch (URISyntaxException | IOException e) {
                    e.printStackTrace();
                }
            }
            Tools.updateProgress(ProgressPO2.SAVE,"done", 1.0 );
            Tools.delProgress(ProgressPO2.SAVE);
            return egal;

        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage() + " - update data failed", e);

        }
        Tools.updateProgress(ProgressPO2.SAVE,"done", 1.0 );
        Tools.delProgress(ProgressPO2.SAVE);
        return false;
    }




    public static void addPublishInfo(OntoData ontoData, String endpoint) {

    }

    public static void addLog(OntoData ontoData, String type) {
        init();
        try {
            LocalDateTime now = LocalDateTime.now();
            ontoData.addLog(type +" by " + loginUser + " on " + now.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            Set<QName> mapFileID = new HashSet<>();
            QName fileID = new QName("http://owncloud.org/ns", "fileid", "oc");
            mapFileID.add(fileID);
            Set<QName> mapComment = new HashSet<>();
            QName message = new QName("http://owncloud.org/ns", "message", "oc");
            QName log = new QName("http://owncloud.org/ns", "log", "oc");
            mapComment.add(message);
            mapComment.add(log);
            Map<QName, String> props = new HashMap<>();
            props.put(message, ontoData.getDescription());
            props.put(log, ontoData.getOntoDataLog());
            List<DavResource> tryFileID = sardineListFile.propfind(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape(ontoData.getName().get()), 0, mapFileID);
            for (DavResource io : tryFileID) {
                String id = io.getCustomProps().get("fileid");
                List<DavResource> tryComment = sardineListFile.propfind(protocol + "://" + host + path + "remote.php/dav/comments/files/" + UrlEscapers.urlPathSegmentEscaper().escape(id), 1, mapComment);
                for (DavResource io2 : tryComment) {
                    try {
                        logger.info("patching ");
                        sardineListFile.patch(protocol + "://" + host + io2.getHref().toString(), props);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {

        }
    }

    public static Boolean update(Ontology ontology) {
        init();
        try {
            if(!ontology.isLocked()) {
                return false;
            }
            Tools.addProgress(ProgressPO2.SAVE, "Uploading file ...");
            byte[] ontoToUpdate = IOUtils.toByteArray(new FileInputStream(ontology.getLocalFile()));
            sardine.put(protocol + "://" + host + "/" + UrlEscapers.urlPathSegmentEscaper().escape(ontology.getDavPath()), ontoToUpdate);
            Tools.updateProgress(ProgressPO2.SAVE,"Checking file ...", 0.50 );
            InputStream temp = sardine.get(protocol + "://" + host + "/" + UrlEscapers.urlPathSegmentEscaper().escape(ontology.getDavPath()));
            byte[] ontoToTest = IOUtils.toByteArray(temp);
            temp.close();
            Boolean egal = Arrays.equals(ontoToUpdate, ontoToTest);
            if(egal) {
                CloudConnector.addLog(ontology, "edited");
                try {
                    MutablePair<String, Date> versionDate = CloudConnector.getDavVersion(ontology);
                    if(versionDate.getLeft() != null) {
                        ontology.setCurrentVersionLoaded(versionDate.getLeft());
                    }
                } catch (URISyntaxException | IOException e) {
                    e.printStackTrace();
                }
            }
            Tools.updateProgress(ProgressPO2.SAVE,"done", 1.0 );
            Tools.delProgress(ProgressPO2.SAVE);

            return egal;
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage() + " - update ontology failed", e);

            Tools.updateProgress(ProgressPO2.SAVE,"error", 1.0 );
            Tools.delProgress(ProgressPO2.SAVE);
            return false;
        }
    }

    public static Boolean unlock(OntoData ontoData) {
        init();
        logger.debug("release lock");

        try {
            URI uriLock = new URI(protocol, host, webdavURI + ontoData.getName().get() + "/.lock", null);
            if (sardine.exists(uriLock.toString())) {
                sardine.delete(uriLock.toString());
                return true;
            }
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage() + " - unlock failed", e);
            return false;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static Boolean lock(OntoData ontoData) throws AlreayLockException, IOException, URISyntaxException {
        init();
        logger.debug("getting lock");

        try {
            URI uriLock = new URI(protocol, host, webdavURI + ontoData.getName().get() + "/.lock", null);
            if (ontoData.canWrite().get() && sardine.exists(uriLock.toString())) {
                logger.debug("lock already exist");
                // on regarde qui a le lock
                InputStream is = sardine.get(uriLock.toString());
                StringWriter w = new StringWriter();
                IOUtils.copy(is, w, StandardCharsets.UTF_8);
                if (w.toString().equals(loginUser)) {
                    logger.debug("this is my lock");
                    return true;
                } else {
                    Date d = sardine.list(uriLock.toString(), 0).stream().findFirst().map(DavResource::getModified).orElse(null);
                    throw new AlreayLockException(w.toString(), d);
                }
            } else {
                logger.debug("lock not exist");
                byte[] temp = loginUser.getBytes();
                logger.debug("create lock file : " + new String(temp));
                // NEVER USE PUT(InputStream) NOT WORKING IN DIJON !!!!
                sardine.put(uriLock.toString(), temp);
                logger.debug("I have the lock");
                return true;
            }
        } catch (IOException e) {
            logger.error(e.getClass() + ":" + e.getMessage() + " - Error getting lock", e);
            throw e;
        } catch (URISyntaxException e2) {
            logger.error(e2.getClass() + ":" + e2.getMessage() + " - Error getting lock", e2);
            throw e2;
        }
    }

    public static void createGroup() {
        init();
        try {
            URIBuilder uri = new URIBuilder(path + "ocs/v1.php/cloud/groups");
            uri.setParameter("groupid", "newgroup");
            HttpPost request = new HttpPost(uri.build());
            request.addHeader("OCS-APIRequest", "true");
            HttpClient client = httpBuilder.build();
            HttpResponse response = client.execute(mainTarget, request, mainContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Boolean removeProjectData(String dataName) {
        if(Datas.getDatas().isEmpty()) {
            CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
        }
        return removeProject(Datas.getData(dataName));
    }

    public static Boolean removeProjectOntology(String ontoName) {
        if(Ontologies.getOntologies().isEmpty()) {
            CloudConnector.listOntology(new SimpleDoubleProperty(0.0),0.5);
        }
        return removeProject(Ontologies.getOntology(ontoName));
    }
    public static Boolean removeProject(OntoData project) {
        if(project == null) {
            return false;
        }
        init();
        Boolean back = true;
        if (online.get() && isInitUser()) {
            String type = (project instanceof Data) ? "Data" : "Ontologies";
            if(project instanceof Data) {
                if(Datas.getDatas().isEmpty()) {
                    CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
                }
            }
            if(project instanceof Ontology) {
                if(Ontologies.getOntologies().isEmpty()) {
                    CloudConnector.listOntology(new SimpleDoubleProperty(0.0),0.5);
                }
            }

            String projectName = project.getName().get();
            String projectNameNormaliser = Tools.normalize(projectName);

            // chech write permission
            try {
                URI uriWrite = new URI(protocol, host, webdavURI + projectName + "/testRemove" + loginUser, null);
                sardine.put(uriWrite.toString(), new byte[0]);
                sardine.delete(uriWrite.toString());
            } catch (SardineException | URISyntaxException se) {
                // no write permission
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            // ok write permission
            try {
                // remove folder on cloud
                sardineManager.delete(protocol + "://" + host + webdavURI+ "PO2VocabManager/"+type+"/" + UrlEscapers.urlPathSegmentEscaper().escape(projectName));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            try {

                CredentialsProvider userCredsProvider = new BasicCredentialsProvider();
                userCredsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(loginUser, passwordUser));

                HttpHost target = new HttpHost(host, port, protocol);
                AuthCache userAuthCache = new BasicAuthCache();
                userAuthCache.put(target, new BasicScheme());


                URIBuilder uriGroup = new URIBuilder(protocol + "://" + host + "/po2-ws/deleteGroup");
                uriGroup.setParameter("groupID", projectNameNormaliser);
                HttpPost requestGroup = new HttpPost(uriGroup.build());


                HttpClientContext context = HttpClientContext.create();
                context.setCredentialsProvider(userCredsProvider);
                context.setAuthCache(userAuthCache);

                HttpClient client = httpBuilder.build();
                HttpResponse responseGroup = client.execute(
                        requestGroup, context);

                if(responseGroup.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    logger.error("Failed deleting ldap group " + projectNameNormaliser);
                    back = false;
                }

                if(project instanceof Data) {
                    CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
                }
                if(project instanceof Ontology) {
                    CloudConnector.listOntology(new SimpleDoubleProperty(0.0),0.5);
                }

            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
                back = false;
            }
        } else {
            back = false;
        }
        return back;
    }

    public static Ontology createNewOntology(String ontologyName, Boolean pub) throws AlreadyExistException, IOException, URISyntaxException, InterruptedException {
        if (online.get() && isInitUser()) {
            Tools.addProgress(ProgressPO2.CREATE, "Creation in progress");
            if (ontologyName == null || ontologyName.isEmpty()) {
                return null;
            }
            logger.info("creating new ontology : " + ontologyName);
            init();
            if (Ontologies.getOntologies().isEmpty()) {
                CloudConnector.listOntology(new SimpleDoubleProperty(0.0), 0.5);
            }
            String ontologyNameNormalised = Tools.normalize(ontologyName);
            if (Ontologies.getOntologies().containsKey(ontologyName) || Ontologies.getOntologies().containsKey(ontologyNameNormalised)) {
                logger.info("Ontology name  " + ontologyNameNormalised + " already used");
                Tools.delProgress(ProgressPO2.CREATE);
                throw new AlreadyExistException(ontologyName);
            }

            if (!sardineManager.exists(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Ontologies/template.ttl"))) {
                logger.error("ontology template not exist. Aborting");
                return null;
            }

            // create new folder
            sardineManager.createDirectory(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Ontologies/"+ontologyName));
            URIBuilder uri = new URIBuilder(path + "ocs/v1.php/apps/files_sharing/api/v1/shares");
            uri.setParameter("path", "PO2VocabManager/Ontologies/"+ontologyName);
            uri.setParameter("shareType", "3"); // public link
            uri.setParameter("publicUpload", "false");
            if(!pub) { // add a password
                uri.setParameter("password", "pas d'imp0r7anc3 dans le M0t de pass3 !!?");
            }
            HttpPost request = new HttpPost(uri.build());
            request.addHeader("OCS-APIRequest", "true");
            HttpClient client = httpBuilder.build();
            HttpResponse response = client.execute(mainTarget, request, mainContext);
            if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                logger.error("Failed creating shared folder " + ontologyName);
                return null;
            }

            // downloading the last template ttl
            InputStream is = sardineManager.get(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Ontologies/template.ttl"));
            // updating template
            String ontoContent = IOUtils.toString(is, StandardCharsets.UTF_8);
            ontoContent = ontoContent.replaceAll("\\{ontology_name\\}", ontologyNameNormalised);
            // uploading new content
            sardineManager.put(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Ontologies/" + ontologyName + "/" + ontologyNameNormalised + ".ttl"), ontoContent.getBytes());

            if(createLdapGroup(ontologyName, "Ontology")) {

                listOntology(new SimpleDoubleProperty(0.0),0.5);

                URI uriRead = new URI(protocol, host, webdavURI + ontologyName, null);

                int retry = 15;
                Tools.updateProgress(ProgressPO2.CREATE_PROJECT, "waiting for cloud response ...", 0.90);
                while(!sardine.exists(uriRead.toString()) && retry > 0) {
                    logger.debug("right no yet OK. Waiting 5 seconds");
                    TimeUnit.SECONDS.sleep(5);
                    retry--;
                }
                if(retry > 0) {
                    logger.debug("right OK.");
                } else {
                    logger.error("right not OK.");
                }
                Tools.delProgress(ProgressPO2.CREATE_PROJECT);
                Ontology newOnto = Ontologies.getOntology(ontologyName);
                if(newOnto != null) {
                    newOnto.load();
                    newOnto.setModified(false);
                    addLog(newOnto, "created");
                    return newOnto;
                } else {
                    logger.error("unknown error on ontology creation : " + ontologyName);
                    return null;
                }

            } else {
                logger.error("Failed creating new group " + ontologyName);
                return null;
            }
        }
        return null;
    }

    public static ProjectFile createNewProject(File projectFile, Boolean pub) throws AlreadyExistException {
        if(projectFile == null) {
            return null;
        }
        logger.info("creation new project from zip : " + projectFile.getName());
        init();
        if(Datas.getDatas().isEmpty()) {
            CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
        }
        String projectName = FilenameUtils.removeExtension(projectFile.getName());
        // ckeck projectFile can be loaded !
        String projet = Tools.normalize(projectName);
        if(Datas.getData(projet) != null || Datas.getData(projectName) != null) {
            logger.info("project " +projectName + " already exist");
            Tools.delProgress(ProgressPO2.CREATE);
            throw new AlreadyExistException(projectName);
        }
        Data d = Datas.addData(projectName);
        d.setLocal(true);
        ZipFile dataZip = null;
        try {
            dataZip = new ZipFile(projectFile);
            Data.unzip(dataZip, d.getName().get());
            if (d.load()) {
                Report report = DataTools.analyseFiles(d);
                if (report.success()) {
                    for (GeneralFile g : d.getListGeneralFile()) {
                        if (!d.analyseProcess(g)) {
                            return null;
                        } else {
                            g.unload();
                        }
                    }
                    d.init();
                    Datas.getDatas().remove(projectName);
                    logger.info("project zip is valid ");
                    // Project can be loaded localy.
                    return createNewProject(projectName, projectFile, pub);
                } else {
                    logger.error("unable to load project zip : " + projectName + " - " + projectFile.getAbsolutePath());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static ProjectFile createNewProject(String projectName, Boolean pub) throws AlreadyExistException {
        if(projectName != null && projectName.isEmpty()) {
            return null;
        }
        logger.info("creation new project from empty : " + projectName);
        init();
        return createNewProject(projectName, null, pub);
    }

    /**
     *
     * @param projectName
     * @param type Data or Ontology
     * @throws URISyntaxException
     */
    private static boolean createLdapGroup(String projectName, String type) throws URISyntaxException, IOException, InterruptedException {
        //Create 2 LDAP Group (projet_read & projet_write)
        Tools.addProgress(ProgressPO2.CREATE, "creating group");
        Tools.updateProgress(ProgressPO2.CREATE, 0.0);
        String project = Tools.normalize(projectName);
        CredentialsProvider userCredsProvider = new BasicCredentialsProvider();
        userCredsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(loginUser, passwordUser));

        HttpHost target = new HttpHost(host, port, protocol);
        AuthCache userAuthCache = new BasicAuthCache();
        userAuthCache.put(target, new BasicScheme());


        URIBuilder uriGroup = new URIBuilder(protocol + "://" + host + "/po2-ws/createGroup");
        uriGroup.setParameter("groupID", project);
        uriGroup.setParameter("type", type.toLowerCase());
        HttpPost requestGroup = new HttpPost(uriGroup.build());


        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(userCredsProvider);
        context.setAuthCache(userAuthCache);

        HttpClient client = httpBuilder.build();
        HttpResponse responseGroup = client.execute(requestGroup, context);



        if(responseGroup.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            logger.error("Failed creating ldap group " + project);
            Tools.delProgress(ProgressPO2.CREATE);
            return false;
        }
        Tools.updateProgress(ProgressPO2.CREATE, "Waiting for group propagation ... ", 0.5);

        // Checking new group (read & write) existing in nextcloud
        URIBuilder uriCheckGroup = new URIBuilder(protocol + "://"+host+path + "ocs/v1.php/cloud/groups");
        uriCheckGroup.setParameter("search", project+"_");
        uriCheckGroup.setParameter("format","json");

        HttpGet requestCheckGroup = new HttpGet(uriCheckGroup.build());
        requestCheckGroup.addHeader("OCS-APIRequest", "true");
        HttpClient clientCheck = httpBuilder.build();
        Boolean groupOK = false;
        Integer retry = 3;
        while (!groupOK) {
            HttpResponse responseCheck = clientCheck.execute(
                    requestCheckGroup, mainContext);
            if(responseCheck.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String resp = IOUtils.toString(responseCheck.getEntity().getContent(), StandardCharsets.UTF_8);
                JSONObject groups = new JSONObject(resp);
                JSONObject listElement = groups.getJSONObject("ocs").getJSONObject("data");
                JSONArray listGroups = listElement.getJSONArray("groups");
                if(listGroups.toString().contains(project+"_read") && listGroups.toString().contains(project+"_write")) {
                    groupOK = true;
                    logger.debug("new group find in nextcloud " + project);
                } else {
                    if(retry > 0) {
                        logger.debug("new group not find in nextcloud, retry in 60s " + project);
                        retry--;
                        TimeUnit.MINUTES.sleep(1);
                    } else {
                        logger.error("unable to find new groups in nextcloud for project :  " + project);
                        Tools.delProgress(ProgressPO2.CREATE);
                        return false;
                    }
                }
            }
        }

        Tools.updateProgress(ProgressPO2.CREATE, "group rights assignment ...", 0.75);
        if(type.equalsIgnoreCase("ontology")) {
            type = "Ontologies";
        }
        URIBuilder uriShareRead = new URIBuilder(protocol + "://"+host+path + "ocs/v1.php/apps/files_sharing/api/v1/shares");
        uriShareRead.setParameter("path", "PO2VocabManager/"+type+"/"+projectName);
        uriShareRead.setParameter("shareType", "1"); // group share
        uriShareRead.setParameter("shareWith", project+"_read");
        uriShareRead.setParameter("permissions", "1"); // read only

        HttpPost requestShareRead = new HttpPost(uriShareRead.build());
        requestShareRead.addHeader("OCS-APIRequest", "true");

        HttpClient clientRead = httpBuilder.build();
        HttpResponse responseShareRead = clientRead.execute(
                requestShareRead, mainContext);
        if(responseShareRead.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            logger.error("Failed creating shared folder read ldap group " + project);
            Tools.delProgress(ProgressPO2.CREATE);
            return false;
        }

        URIBuilder uriShareWrite = new URIBuilder(protocol + "://"+host+path + "ocs/v1.php/apps/files_sharing/api/v1/shares");
        uriShareWrite.setParameter("path", "PO2VocabManager/"+type+"/"+projectName);
        uriShareWrite.setParameter("shareType", "1"); // group share
        uriShareWrite.setParameter("shareWith", project+"_write");
        uriShareWrite.addParameter("permissions", "14"); // 2 : update, 4 : create, 8 : delete


        HttpPost requestShareWrite = new HttpPost(uriShareWrite.build());
        requestShareWrite.addHeader("OCS-APIRequest", "true");
        HttpResponse responseShareWrite = clientRead.execute(
                requestShareWrite, mainContext);

        if(responseShareWrite.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            logger.error("Failed creating shared folder write ldap group " + projectName);
            Tools.delProgress(ProgressPO2.CREATE);
            return false;
        }
        Tools.delProgress(ProgressPO2.CREATE);
        return true;
    }


    private static ProjectFile createNewProject(String projectName,File projectFile, Boolean pub) throws AlreadyExistException {
        logger.info("creation new project : " + projectName);
        projectName = projectName.substring(0,Math.min(100, projectName.length()));
        logger.info("project name limit size : " + projectName);
        init();
        if (online.get() && isInitUser()) {
            try {
                Tools.addProgress(ProgressPO2.CREATE_PROJECT, "creation in progress ..." );
                Tools.updateProgress(ProgressPO2.CREATE_PROJECT, 0.0);
                String projet = Tools.normalize(projectName);
                if(Datas.getDatas().isEmpty()) {
                    CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
                }
                if(Datas.getData(projet) != null || Datas.getData(projectName) != null) {
                    logger.info("project " +projectName + " already exist");
                    Tools.delProgress(ProgressPO2.CREATE_PROJECT);
                    throw new AlreadyExistException(projectName);
                }
                // create new folder
                sardineManager.createDirectory(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Data/"+projectName));
                URIBuilder uri = new URIBuilder(path + "ocs/v1.php/apps/files_sharing/api/v1/shares");
                uri.setParameter("path", "PO2VocabManager/Data/"+projectName);
                uri.setParameter("shareType", "3"); // public link
                uri.setParameter("publicUpload", "false");
                if(!pub) { // add a password
                    uri.setParameter("password", "pas d'imp0r7anc3 dans le M0t de pass3 !!?");
                }
                HttpPost request = new HttpPost(uri.build());
                request.addHeader("OCS-APIRequest", "true");
                HttpClient client = httpBuilder.build();
                HttpResponse response = client.execute(mainTarget, request, mainContext);
                if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    logger.error("Failed creating shared folder " + projectName);
                    return null;
                }
                // copy empty project

                if(projectFile != null) {
                    byte[] dataToUpdate = IOUtils.toByteArray(new FileInputStream(projectFile));
                    sardineManager.put(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Data/" + projectName + "/" + projet + ".zip"), dataToUpdate);
                } else {
                    sardineManager.copy(protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Data/empty_project_v2.zip"), protocol + "://" + host + webdavURI + UrlEscapers.urlPathSegmentEscaper().escape("PO2VocabManager/Data/" + projectName + "/" + projet + ".zip"));
                }


                if(createLdapGroup(projectName, "Data")) {

                    listData(new SimpleDoubleProperty(0.0),0.5);

                    URI uriRead = new URI(protocol, host, webdavURI + projectName, null);

                    int retry = 15;
                    Tools.updateProgress(ProgressPO2.CREATE_PROJECT, "waiting for cloud response ...", 0.90);
                    while(!sardine.exists(uriRead.toString()) && retry > 0) {
                        logger.debug("right no yet OK. Waiting 5 seconds");
                        TimeUnit.SECONDS.sleep(5);
                        retry--;
                    }
                    if(retry > 0) {
                        logger.debug("right OK.");
                    } else {
                        logger.error("right not OK.");
                    }
                    Tools.delProgress(ProgressPO2.CREATE_PROJECT);
                    Data newProj = Datas.getData(projectName);
                    if(newProj != null) {
                        newProj.load();
                        DataTools.analyseFiles(newProj);
                        newProj.setModified(false);
                        addLog(newProj, "created");
                        ProjectFile p = newProj.getProjectFile();
                        if (p != null) {
                            return p;
                        } else {
                            logger.error("unknown error on project creation : " + projectName);
                            return null;
                        }
                    } else {
                        logger.error("unknown error on project creation : " + projectName);
                        return null;
                    }

                } else {
                    logger.error("Failed creating new group " + projet);
                    return null;
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Tools.delProgress(ProgressPO2.CREATE);
        return null;
    }


}
