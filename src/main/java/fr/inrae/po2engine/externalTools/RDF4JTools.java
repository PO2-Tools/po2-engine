/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.externalTools;

import fr.inrae.po2engine.model.OntoData;
import fr.inrae.po2engine.utils.PO2Properties;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import org.apache.commons.io.IOUtils;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.util.ReplacingInputStream;
import org.eclipse.rdf4j.common.exception.ValidationException;
import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.base.InternedIRI;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDF4J;
import org.eclipse.rdf4j.model.vocabulary.RSX;
import org.eclipse.rdf4j.model.vocabulary.SHACL;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.query.UpdateExecutionException;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.config.RepositoryConfigSchema;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryInfo;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.shacl.ShaclSail;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.eclipse.rdf4j.model.util.Statements.statement;
import static org.eclipse.rdf4j.model.util.Values.iri;

public class RDF4JTools {

    private static String sonorus_login = PO2Properties.getProperty("triplestore.login");;
    private static String sonorus_pwd = PO2Properties.getProperty("triplestore.password");
    ////////// GRAPHDB /////////////////
    private static String serverURL = PO2Properties.getProperty("triplestore.protocole")+"://"+PO2Properties.getProperty("server.host")+":"+PO2Properties.getProperty("triplestore.port")+PO2Properties.getProperty("triplestore.path");
    public static String coreFilePath = "resources/PO2_core/default_config.ttl";
    private static Logger logger = LogManager.getLogger(RDF4JTools.class);
    private static HttpClientBuilder httpBuilder;

    ////////////////////////////////////
    /////////// RDF4J //////////////////
//    private static String serverURL = "http://localhost:8080/rdf4j-server";
//    public static String coreFilePath = "resources/PO2_core/default_rdf4j.ttl";
    ////////////////////////////////////

    private static void init() {
        serverURL = PO2Properties.getProperty("triplestore.protocole")+"://"+PO2Properties.getProperty("server.host")+":"+PO2Properties.getProperty("triplestore.port")+PO2Properties.getProperty("triplestore.path");
        if(httpBuilder == null) {
            SSLContextBuilder builder = new SSLContextBuilder();
            try {
                builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

                //TODO remove the noop param when quantum palaiseau is OK
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                        builder.build(), NoopHostnameVerifier.INSTANCE);
                httpBuilder = HttpClientBuilder.create().setSSLSocketFactory(sslsf);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }
    }
    public static void initRepositotyInfos(OntoData ontoData) {
        init();
        String ID = Tools.normalize(ontoData.getName().get());
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        try {
            RepositoryInfo ri = repositoryManager.getRepositoryInfo(ID);
            if(ri != null) {
                ontoData.setSemDescription(ri.getDescription());
                ontoData.setSemStatementSize(String.valueOf(repositoryManager.getRepository(ID).getConnection().size()));
            }
            HTTPRepository r = (HTTPRepository) repositoryManager.getRepository(ID);
            if(r != null) {
                ontoData.setSemURIRepository(r.getRepositoryURL());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        repositoryManager.shutDown();
    }
    public static String getRepository(String ID) {
        init();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.init();
        String name = "error";
        try {
            name = ((HTTPRepository) repositoryManager.getRepository(ID)).getRepositoryURL();
        } catch (Exception e) {
            e.printStackTrace();
        }
        repositoryManager.shutDown();
        return name;
    }

    /**
     * List all named graph for the repository
     * @param ID the repository to query
     * @return the list with all named graph
     */
    public static List<String> listNamedGraph(String ID) {
        init();
        List<String> back = new ArrayList<>();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        if(repositoryManager.hasRepositoryConfig(ID)) {
            logger.debug("listing all named graphs for repository " +ID);
            RepositoryConnection connTemp = repositoryManager.getRepository(ID).getConnection();
            connTemp.begin(ShaclSail.TransactionSettings.ValidationApproach.Bulk);
            try {
                connTemp.getContextIDs().forEach(r -> {
                    back.add(r.stringValue());
                });
                connTemp.close();
            } catch (RepositoryException exception) {
                logger.error("error during list named graph", exception);
                exception.printStackTrace();
            }
        }
        repositoryManager.shutDown();
        return back;
    }

    public static Report startShaclValidation(String ID) throws IOException {
        init();
        Report rapport = new Report();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        if(repositoryManager.hasRepositoryConfig(ID)) {
            // clearing shacl Graph before
            logger.debug("Repository "+ID+" exist. Starting Shacl Validation");
            RepositoryConnection connTemp = repositoryManager.getRepository(ID).getConnection();
            connTemp.begin(ShaclSail.TransactionSettings.ValidationApproach.Bulk);
            try {
                connTemp.commit();
            } catch (RepositoryException exception) {
                Throwable cause = exception.getCause();
                if (cause instanceof ValidationException) {
                    Model validationReportModel = ((ValidationException) cause).validationReportAsModel();
                    validationReportModel
                        .filter(null, RDF.TYPE, SHACL.VALIDATION_RESULT)
                            .forEach(s -> {
                                AtomicReference<String> message = new AtomicReference<>(), node = new AtomicReference<>(), path = new AtomicReference<>(), value = new AtomicReference<>();
                                AtomicReference<Value> severity = new AtomicReference<>();
                                validationReportModel.filter(s.getSubject(), SHACL.RESULT_MESSAGE, null).stream().findFirst().ifPresent(st -> message.set(st.getObject().stringValue()));
                                validationReportModel.filter(s.getSubject(), SHACL.FOCUS_NODE, null).stream().findFirst().ifPresent(st -> node.set(st.getObject().stringValue()));
                                validationReportModel.filter(s.getSubject(), SHACL.RESULT_PATH, null).stream().findFirst().ifPresent(st -> path.set(st.getObject().stringValue()));
                                validationReportModel.filter(s.getSubject(), SHACL.VALUE, null).stream().findFirst().ifPresent(st -> value.set(st.getObject().stringValue()));
                                validationReportModel.filter(s.getSubject(), SHACL.RESULT_SEVERITY, null).stream().findFirst().ifPresent(st -> severity.set(st.getObject()));
                                if(severity.get().equals(SHACL.VIOLATION)) {
                                    rapport.addError(message + "\n" +node + " - "+ path + " - " + value) ;
                                }
                                if(severity.get().equals(SHACL.WARNING)) {
                                    rapport.addWarning(message + "\n" +node + " - "+ path + " - " + value); ;
                                }
                                if(severity.get().equals(SHACL.INFO)) {
                                    rapport.addInfo(message + "\n" +node + " - "+ path + " - " + value); ;
                                }
                            });
                } else {
                    connTemp.close();
                    repositoryManager.shutDown();
                    logger.error("an error occured durant shacl validation", exception);
                    throw exception;
                }
            }
            connTemp.close();
        }
        repositoryManager.shutDown();
        return rapport;
    }

    public static void updateDataShaclGraph(String ID, InputStream stream, String shaclGraphExtension) throws IOException {
        updateShaclGraph(ID, "data", stream, shaclGraphExtension);
    }
    public static void updateOntologyShaclGraph(String ID, InputStream stream, String shaclGraphExtension) throws IOException {
        updateShaclGraph(ID, "onto", stream, shaclGraphExtension);
    }

    private static void updateShaclGraph(String ID, String ontoOrData, InputStream stream, String shaclGraphExtension) throws IOException {
        init();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        if(!repositoryManager.hasRepositoryConfig(ID)) {
            createOrClearRepository("Repository for project " + ID, ID);
        }
        if(repositoryManager.hasRepositoryConfig(ID)) {
            // clearing shacl Graph before
            logger.debug("Repository "+ID+" exist. Clearing shacl graph");
            RepositoryConnection connTemp = repositoryManager.getRepository(ID).getConnection();
            connTemp.begin(ShaclSail.TransactionSettings.ValidationApproach.Disabled);
            IRI shaclGraph = iri(RDF4J.NAMESPACE, "SHACLShapeGraph"+shaclGraphExtension);
            connTemp.clear(shaclGraph);
            logger.debug("Repository " + ID+ " : Updating shacl graph");
            connTemp.add(stream, RDFFormat.TURTLE, shaclGraph);
            if(ontoOrData.equalsIgnoreCase("data")) {
                // adding link datagraph with shapes only on data repository
                IRI namedGraph = iri("http://opendata.inrae.fr/PO2/ontology/",ID);
                if(shaclGraphExtension.equalsIgnoreCase("data")) {
                    // only one link with data from the data shapes
                    Statement st = statement(namedGraph, SHACL.SHAPES_GRAPH, shaclGraph, shaclGraph);
                    connTemp.add(st);
                }
                if(shaclGraphExtension.equalsIgnoreCase("onto")) {
                    // union link with rswx when shapes from onto
                    BNode bnode = connTemp.getValueFactory().createBNode();
                    connTemp.add(statement(bnode, RDF.TYPE, RSX.DataAndShapesGraphLink, shaclGraph));
                    connTemp.add(statement(bnode, RSX.shapesGraph, shaclGraph, shaclGraph));
                    connTemp.add(statement(bnode, RSX.dataGraph, iri("http://opendata.inrae.fr/PO2/ontology/", ID), shaclGraph));
                    connTemp.add(statement(bnode, RSX.dataGraph, iri("http://opendata.inrae.fr/PO2/data/", ID), shaclGraph));
                }
            }
            connTemp.commit();
            connTemp.close();
        }
        repositoryManager.shutDown();
    }

    public static void clearSHACLShapeGraph(String ID, String shaclGraphExtension) throws IOException {
        init();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        if(!repositoryManager.hasRepositoryConfig(ID)) {
            createOrClearRepository("Repository for project " + ID, ID);
        }
        logger.debug("Repository "+ID+" exist. Clearing shacl graph");
        RepositoryConnection connTemp = repositoryManager.getRepository(ID).getConnection();
        connTemp.begin(ShaclSail.TransactionSettings.ValidationApproach.Disabled);
        IRI shaclGraph = new InternedIRI(RDF4J.NAMESPACE, "SHACLShapeGraph"+shaclGraphExtension);
        connTemp.clear(shaclGraph);
        connTemp.commit();
        connTemp.close();

        repositoryManager.shutDown();
    }

    public static String createOrClearRepository(String label, String ID) throws IOException {
        init();
        Tools.addProgress( ProgressPO2.PUBLISH, "Creating repository");
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        // suppression si repo existe
        if(repositoryManager.hasRepositoryConfig(ID)) {
            Tools.updateProgress( ProgressPO2.PUBLISH, "Clearing existing repository", 0.3);
            logger.debug("Repository "+ID+" exist. Clearing...");
            RepositoryConnection connTemp = repositoryManager.getRepository(ID).getConnection();
            connTemp.begin(ShaclSail.TransactionSettings.ValidationApproach.Disabled); // for graphdb > 10.2
            connTemp.prepareUpdate("CLEAR ALL;").execute();
            connTemp.close();
        }
        else {
            Tools.updateProgress( ProgressPO2.PUBLISH, 0.3);
            logger.debug("Creating new Repository " + ID);
            TreeModel graph = new TreeModel();

            InputStream defaultconfig = RDF4JTools.class.getClassLoader().getResourceAsStream(coreFilePath);
            InputStream config = new ReplacingInputStream(new ReplacingInputStream(defaultconfig, "repoID", ID), "repoLabel", label);
            RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
            rdfParser.setRDFHandler(new StatementCollector(graph));
            rdfParser.parse(config, RepositoryConfigSchema.NAMESPACE);
            config.close();
            Resource repositoryNode = Models.subject(graph
                    .filter(null, RDF.TYPE, RepositoryConfigSchema.REPOSITORY))
                    .orElseThrow(() -> new RuntimeException(
                            "Oops, no <http://www.openrdf.org/config/repository#> subject found!"));
            RepositoryConfig repositoryConfig = RepositoryConfig.create(graph, repositoryNode);

            repositoryManager.addRepositoryConfig(repositoryConfig);
        }
        Tools.updateProgress( ProgressPO2.PUBLISH,  0.7);
        HTTPRepository repo = (HTTPRepository) repositoryManager.getRepository(ID);
        String repoName = repo.toString();

        // activating autocomplete on graphDB
        RepositoryConnection conn = repo.getConnection();
        conn.begin(ShaclSail.TransactionSettings.ValidationApproach.Disabled); // for graphdb > 10.2
        String queryString = "INSERT DATA { _:s  <http://www.ontotext.com/plugins/autocomplete#enabled> \"true\" . }";
        Update update = conn.prepareUpdate(queryString);
        try {
            update.execute();
        } catch (UpdateExecutionException ue) {
        }
        conn.close();

        repositoryManager.shutDown();
        Tools.updateProgress( ProgressPO2.PUBLISH,  1.0);
        Tools.delProgress(ProgressPO2.PUBLISH);
        return repoName;
    }

    public static boolean setFreeReadRepository(String repository, Boolean free) throws IOException, AuthenticationException{

        CloseableHttpClient httpclient = httpBuilder.build();
        HttpGet httpGet = new HttpGet(serverURL+"/rest/security/free-access");

        UsernamePasswordCredentials creds
                = new UsernamePasswordCredentials(sonorus_login, sonorus_pwd);


        httpGet.addHeader(new BasicScheme().authenticate(creds, httpGet, null));
        httpGet.setHeader("Accept", "*/*");
        httpGet.setHeader("Content-type", "application/json");

        CloseableHttpResponse responseGetEntity = httpclient.execute(httpGet);

        StringWriter stringResponse2 = new StringWriter();
        IOUtils.copy(responseGetEntity.getEntity().getContent(), stringResponse2, StandardCharsets.UTF_8);

        JSONObject jsonEntity = new JSONObject(stringResponse2.toString());
        if(free) {
            jsonEntity.getJSONArray("authorities").put("READ_REPO_" + repository);
        } else {
            int ind = jsonEntity.getJSONArray("authorities").toList().indexOf("READ_REPO_" + repository);
            if(ind >= 0) {
                jsonEntity.getJSONArray("authorities").remove(ind);
            }
        }

        HttpPost httpPost = new HttpPost(serverURL+"/rest/security/free-access");

        httpPost.setEntity(new StringEntity(jsonEntity.toString()));
        httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));
        httpPost.setHeader("Accept", "*/*");
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpclient.execute(httpPost);
        if(response.getStatusLine().getStatusCode() == 200) {
            return true;
        }
        return false;

    }

    public static void replaceGraph(String repositoryID, String dataNamedGraph, InputStream dataStream, String ontoNamedGraph, InputStream ontoStream, RDFFormat format) throws IOException {
        logger.debug("publishing in " + repositoryID);
        init();
        Tools.addProgress(ProgressPO2.PUBLISH, "Replacing new data in graph " + repositoryID);
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();

        if(!repositoryManager.hasRepositoryConfig(repositoryID)) {
            logger.debug("creating group repo " + repositoryID);
            TreeModel graph = new TreeModel();

            InputStream defaultconfig = RDF4JTools.class.getClassLoader().getResourceAsStream(coreFilePath);
            InputStream config = new ReplacingInputStream(new ReplacingInputStream(defaultconfig, "repoID", repositoryID), "repoLabel", "repository for group " +repositoryID );
            RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
            rdfParser.setRDFHandler(new StatementCollector(graph));
            rdfParser.parse(config, RepositoryConfigSchema.NAMESPACE);
            config.close();
            Resource repositoryNode = Models.subject(graph
                    .filter(null, RDF.TYPE, RepositoryConfigSchema.REPOSITORY))
                    .orElseThrow(() -> new RuntimeException(
                            "Oops, no <http://www.openrdf.org/config/repository#> subject found!"));
            RepositoryConfig repositoryConfig = RepositoryConfig.create(graph, repositoryNode);

            repositoryManager.addRepositoryConfig(repositoryConfig);
            logger.debug("creation done ");

        }
        Repository repository = repositoryManager.getRepository(repositoryID);
        repository.init();

        RepositoryConnection conn = repository.getConnection();
        conn.begin(ShaclSail.TransactionSettings.ValidationApproach.Disabled); // for graphdb > 10.2
        RepositoryResult<Resource> listContext = conn.getContextIDs();

        while (listContext.hasNext()) {
            Resource r = listContext.next();
            if(r.toString().equalsIgnoreCase(dataNamedGraph) || r.toString().equalsIgnoreCase(ontoNamedGraph)) {
                logger.debug("context already exist. Clearing context " + r.toString());
                conn.prepareUpdate("CLEAR GRAPH <" + r.toString()+">;").execute();
            }
        }
        Tools.updateProgress(ProgressPO2.PUBLISH, 0.5);
        logger.debug("publishing " + dataNamedGraph +" ... ");
        Resource dataContext = conn.getValueFactory().createIRI(dataNamedGraph);
        conn.add(dataStream, "", format, dataContext);
        logger.debug("publishing " + dataNamedGraph + " done");
        logger.debug("publishing " + ontoNamedGraph + " ... ");
        Resource ontoContext = conn.getValueFactory().createIRI(ontoNamedGraph);
        conn.add(ontoStream, "", format, ontoContext);
        logger.debug("publishing " + ontoNamedGraph + " done");
        conn.commit(); // for graphdb > 10.2
        conn.close();
        repository.shutDown();
        repositoryManager.shutDown();
        Tools.updateProgress(ProgressPO2.PUBLISH, 1.0);
        Tools.delProgress(ProgressPO2.PUBLISH);
    }

    public static void upload(String repositoryID, String namedGraph ,InputStream streamToUpload, RDFFormat format) throws IOException, RDFParseException {
        logger.debug("uploading data to " + namedGraph);
        Tools.addProgress(ProgressPO2.PUBLISH, "Uploading new Data in " + namedGraph);
        init();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        Repository repository = repositoryManager.getRepository(repositoryID);
        repository.init();

        RepositoryConnection conn = repository.getConnection();
        RepositoryResult<Resource> listContext = conn.getContextIDs();
        conn.begin(ShaclSail.TransactionSettings.ValidationApproach.Disabled); // for graphdb > 10.2
        Resource context = null;
        while (listContext.hasNext()) {
            Resource r = listContext.next();
            if(r.toString().equalsIgnoreCase(namedGraph)) {
                logger.debug("context exist. Clearing");
                conn.clear(r);
                context = r;
            }
        }
        Tools.updateProgress(ProgressPO2.PUBLISH, 0.5);
        if(context == null) {
            context = iri(namedGraph);
        }
        conn.add(streamToUpload, "", format, context);
        conn.commit(); // for graphdb > 10.2
        conn.close();
        repository.shutDown();
        repositoryManager.shutDown();
        Tools.updateProgress(ProgressPO2.PUBLISH, 1.0);
        Tools.delProgress(ProgressPO2.PUBLISH);
    }

    public static boolean shaclShapeExist(String repositoryID, String shaclGraphExtension) {
        return listNamedGraph(repositoryID).contains(RDF4J.NAMESPACE+"SHACLShapeGraph"+shaclGraphExtension);
    }

    public static void downloadShaclGraph(String repositoryID, OutputStream stream, String shaclGraphExtension) throws IOException {
        init();
        RemoteRepositoryManager repositoryManager  = new RemoteRepositoryManager(serverURL);
        repositoryManager.setHttpClient(httpBuilder.build());
        repositoryManager.setUsernameAndPassword(sonorus_login, sonorus_pwd);
        repositoryManager.init();
        if(repositoryManager.hasRepositoryConfig(repositoryID)) {

            logger.debug("Downloading " + repositoryID + " SHACL shape graph...");

            RDFWriter rdfWriter = Rio.createWriter(RDFFormat.TURTLE, stream);
            RepositoryConnection connTemp = repositoryManager.getRepository(repositoryID).getConnection();
            IRI shaclGraph = new InternedIRI(RDF4J.NAMESPACE, "SHACLShapeGraph"+shaclGraphExtension);
            connTemp.export(rdfWriter, shaclGraph);
            connTemp.close();

            logger.debug(repositoryID + " SHACL shape graph downloaded.");
        }
        else
            logger.debug("No repositiory " + repositoryID + " to download from.");

        repositoryManager.shutDown();
    }
}
