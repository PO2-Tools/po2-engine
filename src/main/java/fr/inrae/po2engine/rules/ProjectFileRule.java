/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.rules;

import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ProjectFileRule extends GenericRule {
    private String type = "";

    public String getType() {
        return this.type;
    }

    @Override
    public Boolean startTypeDetection(File file) {
        FileInputStream fis = null;
        Boolean cond1 = false;
        Boolean cond2 = false;
        try {
            fis = new FileInputStream(file);

            XSSFWorkbook workBook = new XSSFWorkbook(fis);
            XSSFSheet mySheet = workBook.getSheetAt(0);


            Row row0 = mySheet.getRow(0);
            Row row1 = mySheet.getRow(1);
            if(row0 != null) {
                Cell cell0 = row0.getCell(0);
                if(cell0 != null) {
                    if(cell0.getCellType().equals(CellType.STRING)) {
                        String val = cell0.getStringCellValue();
                        if(val.equalsIgnoreCase("Project")) {
                            cond1 = true;
                            this.type = "project";
                        }

                    }
                }
            }
            if(row1 != null) {
                Cell cell0 = row1.getCell(0);
                if(cell0 != null) {
                    if(cell0.getCellType().equals(CellType.STRING)) {
                        String val = cell0.getStringCellValue();
                        if(val.equalsIgnoreCase("project name")) {
                            cond2 = true;
                            this.type = "project";
                        }

                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (NotOfficeXmlFileException e) {
            // not an xlsx file !!!
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return (cond1 && cond2) ;
    }

    @Override
    public void startPartTypeDetection() {

    }

    @Override
    public void startMapping() {

    }


}
