/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.rules;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MatMetFileRule extends GenericRule {

    @Override
    public Boolean startTypeDetection(File file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);

            XSSFWorkbook workBook = new XSSFWorkbook(fis);
            XSSFSheet mySheet = workBook.getSheetAt(0);

            for (Row row : mySheet) {
                for (Cell cell : row) {
                    if(cell.getCellType().equals(CellType.STRING)) {
                        String val = cell.getStringCellValue();
                        if(StringUtils.containsIgnoreCase(val, "méthode") || StringUtils.containsIgnoreCase(val, "method") || StringUtils.containsIgnoreCase(val, "matériel") || StringUtils.containsIgnoreCase(val, "material")) {
                            Row r1 = mySheet.getRow(row.getRowNum()+1);
                            Row r2 = mySheet.getRow(row.getRowNum()+2);
                            if(r1 != null) {
                                Cell c1 = r1.getCell(cell.getColumnIndex());
                                if(c1 != null && c1.getCellType().equals(CellType.STRING)) {
                                    String val1 = c1.getStringCellValue();
                                    if (StringUtils.containsIgnoreCase(val1, "attribut") || StringUtils.containsIgnoreCase(val1, "caractéristiques") || StringUtils.containsIgnoreCase(val1, "caractéristique") || StringUtils.containsIgnoreCase(val1, "caracteristic") || StringUtils.containsIgnoreCase(val1, "caracteristique")) {
                                        return true;
                                    }
                                }
                            }

                            if (r2 != null) {
                                Cell c2 = r2.getCell(cell.getColumnIndex());
                                if (c2 != null && c2.getCellType().equals(CellType.STRING)) {
                                    String val2 = c2.getStringCellValue();

                                    if (StringUtils.containsIgnoreCase(val2, "attribut") || StringUtils.containsIgnoreCase(val2, "caractéristiques") || StringUtils.containsIgnoreCase(val2, "caractéristique") || StringUtils.containsIgnoreCase(val2, "caracteristic") || StringUtils.containsIgnoreCase(val2, "caracteristique")) {
                                        return true;
                                    }
                                }
                            }

                        }
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (NotOfficeXmlFileException e) {
            // not an xlsx file !!!
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void startPartTypeDetection() {

    }

    @Override
    public void startMapping() {

    }


}
