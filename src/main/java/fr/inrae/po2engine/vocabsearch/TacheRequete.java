/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.vocabsearch;

import javafx.util.Pair;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class TacheRequete implements Callable<Pair<Cible, List<Result>>> {

    private Cible cible;
    private List<Result> res;
    private Query query;
    private QueryExecution qexec;

    public TacheRequete(String requete, Cible cible) {
        this.cible = cible;
        res = new ArrayList<>();
        query = QueryFactory.create(requete);
    }

    @Override
    public Pair<Cible, List<Result>> call() throws Exception {
        Model model = null;
        ResultSet results = null;
//        Boolean stop = false;
//        List<String> addr = new ArrayList<>();
//        addr.add(cible.getAdresse());
//        for (String alt : cible.getAlternateAdress()) {
//            addr.add(alt);
//        }
//        while (!stop && !addr.isEmpty()) {
            try {

                if (cible.isRDF()) {
                    model = ModelFactory.createDefaultModel();
                    model.read(cible.getAdresse());

                    qexec = QueryExecutionFactory.create(query, model);
//                    addr.remove(0);
//                    addr.remove()
                } else if (cible.isEndPoint()) {
                    qexec = QueryExecutionFactory.sparqlService(cible.getAdresse(), query);
//                    addr.remove(0);
                } else if(cible.isTTL()) {
                    model = ModelFactory.createDefaultModel();
                    model.read(cible.getAdresse(), null, "TTL");

                    qexec = QueryExecutionFactory.create(query, model);
                }
                results = qexec.execSelect();

                ResultSetRewindable r = ResultSetFactory.copyResults(results);
                while (r.hasNext()) {
                    QuerySolution soln = r.nextSolution();
                    Result unRes = new Result(soln, cible.getName());
                    res.add(unRes);
                }
//                stop = true;
                cible.success();
                qexec.close();
            } catch (Exception e) {
                e.printStackTrace();
                cible.fail();
            }
//        }
        return new Pair<>(cible, res);
    }


}
