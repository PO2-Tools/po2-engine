/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.vocabsearch;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;

public class Requete implements Runnable {
    private static Logger logger = LogManager.getLogger(Requete.class);

    private static List<Cible> cibles = new ArrayList<>();
    private String queryStringE1 = "PREFIX vann:<http://purl.org/vocab/vann/>\n" +
            "PREFIX voaf:<http://purl.org/vocommons/voaf#>\n" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
            "PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>\n" +
            "PREFIX dbc:<http://dbpedia.org/resource/Category:>\n" +
            "SELECT DISTINCT ?class ?label ?comment\n";
    private String queryStringE2 = "WHERE{ {?class skos:prefLabel ?label} UNION {?class rdfs:label ?label} .\n" + //UNION {?class skosxl:literalForm ?label}
            "FILTER(!REGEX(STR(?class), \"^http://dbpedia.org/resource/Category:\")) \n" +
            "OPTIONAL{{?class rdfs:comment ?comment . FILTER ( lang(?label) = \"\" || langMatches( lang(?comment), \"en\" ))} UNION {?class skos:note ?comment . FILTER ( lang(?label) = \"\" || langMatches( lang(?comment), \"en\" ))} UNION {?class skos:definition ?comment . FILTER ( lang(?label) = \"\" || langMatches( lang(?comment), \"en\" ))} }.\n" + // UNION {?class skos:scopeNote ?comment} FILTER (langMatches( lang(?comment), \"en\" ) || langMatches( lang(?comment), \"\" ))
            "FILTER regex(?label, \"";
    private String queryStringE3 = "\", \"i\")\n"
            + "FILTER ( lang(?label) = \"\" || langMatches( lang(?label), \"en\" ) )   }\n"
            + "LIMIT 250";
    private String queryStringF1 = "PREFIX vann:<http://purl.org/vocab/vann/>\n" +
            "PREFIX voaf:<http://purl.org/vocommons/voaf#>\n" +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
            "PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#>\n" +
            "PREFIX dbc:<http://dbpedia.org/resource/Category:>\n" +
            "SELECT DISTINCT ?class ?label ?comment\n";
    private String queryStringF2 = "WHERE{ {?class skos:prefLabel ?label} UNION {?class rdfs:label ?label} .\n" + //UNION {?class skosxl:literalForm ?label}
            "FILTER(!REGEX(STR(?class), \"^http://dbpedia.org/resource/Category:\")) \n" +
            "OPTIONAL{{?class rdfs:comment ?comment . FILTER ( lang(?label) = \"\" || langMatches( lang(?comment), \"fr\" ))} UNION {?class skos:note ?comment . FILTER ( lang(?label) = \"\" || langMatches( lang(?comment), \"fr\" ))} UNION {?class skos:definition ?comment . FILTER ( lang(?label) = \"\" || langMatches( lang(?comment), \"fr\" ))} }.\n" + // UNION {?class rdfs:comment ?comment} // UNION {?class skos:note ?comment} UNION {?class skos:definition ?comment}
            "FILTER regex(?label, \"";
    private String queryStringF3 = "\", \"i\")\n"
            + "FILTER ( lang(?label) = \"\" || langMatches( lang(?label), \"fr\" ) )   }\n"
            + "LIMIT 250";
    private String requeteEnPart1;
    private String requeteEnPart2;
    private String requeteFrPart1;
    private String requeteFrPart2;
    private String requeteEn;
    private String requeteFr;
    private List<Callable<Pair<Cible, List<Result>>>> taches;
    private List<Cible> ciblesSelect = new ArrayList<Cible>();
    private ObservableList<Result> results = FXCollections.observableArrayList();
    private DoubleProperty completion = new SimpleDoubleProperty(0.0);
    private String phrase = "Requète à ";
    private String statut = "Pas de requete en cours";
    private ExecutorService executor;

    public Requete(String mot) {
        requeteEnPart2 = queryStringE2 + mot + queryStringE3;
        requeteEnPart1 = queryStringE1;
        requeteFrPart2 = queryStringF2 + mot + queryStringF3;
        requeteFrPart1 = queryStringF1;
        logger.debug(requeteEnPart1 + requeteEnPart2 + '\n');
//        cibles.add(DefaultCible.LOV);
//        cibles.add(DefaultCible.DBpedia_EN);
//        cibles.add(DefaultCible.DBpedia_FR);
//        cibles.add(DefaultCible.NALT);
//        cibles.add(DefaultCible.GACS);
//        cibles.add(DefaultCible.AgroVoc);
    }

    public static void init(InputStream config) {
        String cb;
        cibles = new ArrayList<Cible>();
        try {
            cb = IOUtils.toString(config, StandardCharsets.UTF_8);
//            cb = FileUtils.readFileToString(new File("cibles.txt"), StandardCharsets.UTF_8);

            JSONArray array = new JSONArray(cb);
            int n = array.length();
            for (int i = 0; i < n; i++) {
                JSONObject o = array.getJSONObject(i);
                String nom = o.getString("nom");
                String adresse = o.getString("adresse");
                String type = o.getString("type");
                String graph = o.optString("graph");
                JSONArray langues = o.getJSONArray("langues");
                String description = o.getString("description");

                Cible cible;
                if (graph.equals("")) {
                    if (langues.length() == 1) {
                        cible = new Cible(adresse, TypeCible.toType(type), nom, Language.toLangue(langues.getString(0)));
                    } else {
                        cible = new Cible(adresse, TypeCible.toType(type), nom);
                    }

                } else {
                    if (langues.length() == 1) {
                        cible = new Cible(adresse, TypeCible.toType(type), nom, graph, Language.toLangue(langues.getString(0)));
                    } else {
                        cible = new Cible(adresse, TypeCible.toType(type), nom, graph);
                    }
                }
                cible.setDescription(description);

                JSONArray alternate = o.optJSONArray("alternate");
                if (alternate != null) {
                    for (Iterator<Object> alt = alternate.iterator(); alt.hasNext(); ) {
                        String ad = (String) alt.next();
                        cible.addAlternateAdress(ad);
                    }
                }
                cibles.add(cible);
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        Collections.sort(cibles);
    }

    public static List<Cible> getDefautCibles() {
        return cibles;
    }

//    public void selectCibles(ArrayList<String> selected){
//        ciblesSelect = new ArrayList<Cible>();
//        for (String cs : selected){
//            for(Cible cb : cibles){
//                if (cs.equals(cb.getName())){
//                    ciblesSelect.add(cb);
//                }
//            }
//        }
//    }

    public void selectCibles(List<Cible> selected) {
        ciblesSelect = selected;
    }

    public void execute() {
        taches = new ArrayList<>();
        Iterator<Cible> it = ciblesSelect.iterator();
        statut = phrase + "0%.";
        while (it.hasNext()) {
            Cible cb = it.next();
            if (cb.isEN()) {
                if (cb.hasGraph()) {
                    requeteEn = requeteEnPart1 + "\n" + "FROM <" + cb.getGraph() + "> \n" + requeteEnPart2;
                } else {
                    requeteEn = requeteEnPart1 + requeteEnPart2;
                }
                Callable<Pair<Cible, List<Result>>> tacheEn = new TacheRequete(requeteEn, cb);
                taches.add(tacheEn);
            }
            if (cb.isFR()) {
                if (cb.hasGraph()) {
                    requeteFr = requeteFrPart1 + "\n" + "FROM <" + cb.getGraph() + "> \n" + requeteFrPart2;
                } else {
                    requeteFr = requeteFrPart1 + requeteFrPart2;
                }
                Callable<Pair<Cible, List<Result>>> tacheFr = new TacheRequete(requeteFr, cb);
                taches.add(tacheFr);
            }

        }
        int nbProcs = Runtime.getRuntime().availableProcessors();
        executor = Executors.newFixedThreadPool(nbProcs);
        resolve(executor, taches);

    }

    private void resolve(final ExecutorService executor, List<Callable<Pair<Cible, List<Result>>>> taches) {

        CompletionService<Pair<Cible, List<Result>>> completionService = new ExecutorCompletionService<>(executor);

        List<Future<Pair<Cible, List<Result>>>> futures = new ArrayList<>();

        Pair<Cible, List<Result>> res = null;

        try {
            for (Callable<Pair<Cible, List<Result>>> t : taches) {
                futures.add(completionService.submit(t));
            }
            for (int i = 0; i < taches.size(); i++) {
                try {

                    res = completionService.take().get();
                    completion.set(((i + 1) * 1.0 / taches.size()));
                    statut = phrase + completion + "%.";
                    if (res != null) {
                        addResults(res);
                    }
                } catch (ExecutionException ignore) {
                    System.err.println("erreur");
                    ignore.printStackTrace();
                }
            }
            statut = phrase + "100%.";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
        }
    }

    public List<Cible> getSelectCibles() {
        return ciblesSelect;
    }

    public void addResults(Pair<Cible, List<Result>> newResults) {
        newResults.getKey().setFinish();
        Iterator<Result> it = newResults.getValue().iterator();

        Result res;
        while (it.hasNext()) {
            res = it.next();
            results.add(res);
        }

    }

    public String[][] getStringResult() {
        String[][] res;
        Iterator<Result> it = results.iterator();
        if (it.hasNext()) {
            int i = 0;
            Result unRes;
            res = new String[results.size()][];
            while (it.hasNext()) {
                unRes = it.next();
                res[i] = unRes.getStringResult();
                i++;
            }
        } else {
            res = new String[0][3];
        }
        return res;
    }

    public String[][] getSortStringResult() {
        sortResult();
        String[][] res;
        Iterator<Result> it = results.iterator();
        if (it.hasNext()) {
            int i = 0;
            Result unRes;
            res = new String[results.size()][];
            while (it.hasNext()) {
                unRes = it.next();
                res[i] = unRes.getStringResult();
                i++;
            }
        } else {
            res = new String[0][3];
        }
        return res;
    }

    public String toString() {
        String res = "";
        Iterator<Result> it = results.iterator();

        Result unRes;
        while (it.hasNext()) {
            unRes = it.next();
            res = res + unRes.toString();
        }

        return res;
    }

    public String getStatut() {
        return statut;
    }

    public void sortResult() {
        Comparator<Result> comparator = new Comparator<Result>() {

            @Override
            public int compare(Result res1, Result res2) {
                int res = res1.getLabel().toString().toLowerCase().compareTo(res2.getLabel().toString().toLowerCase());
                return res;
            }

        };

        results.sort(comparator);

    }

    public DoubleProperty getCompletionProperty() {
        return this.completion;
    }

    public ObservableList<Result> getResultProperty() {
        return results;
    }

    public void sortSources() {
        Comparator<Result> comparator = new Comparator<Result>() {

            @Override
            public int compare(Result res1, Result res2) {
                int res = res1.getSource().toString().toLowerCase().compareTo(res2.getSource().toString().toLowerCase());
                if (res == 0) {
                    res = res1.getLabel().toString().toLowerCase().compareTo(res2.getLabel().toString().toLowerCase());
                }
                return res;
            }

        };

        results.sort(comparator);

    }

    @Override
    public void run() {
        execute();
    }

}
