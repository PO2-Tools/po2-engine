/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.vocabsearch;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.jena.query.QuerySolution;

public class Result {

    QuerySolution sol;
    StringProperty description = new SimpleStringProperty();
    StringProperty label = new SimpleStringProperty();
    StringProperty origine = new SimpleStringProperty();
    StringProperty classe = new SimpleStringProperty();
    BooleanProperty inOnto = new SimpleBooleanProperty(false);

    public Result(QuerySolution sol, String origine) {
        this.sol = sol;
        this.origine.set(origine);
        this.label.set(sol.get("label").toString().replaceAll("@.*", ""));
        String comment = "";
        if (sol.get("comment") != null) {
            comment = sol.get("comment").toString().replaceAll("@.*", "");
        }
        this.classe.set(sol.get("class").toString());
        this.description.set(comment);
    }

    public String[] getStringResult() {
        String comment = "";
        if (sol.get("comment") != null) {
            comment = sol.get("comment").toString();
        }
        String[] res = {sol.get("label").toString(), origine.get(), comment, sol.get("class").toString()};
        return res;
    }

    public String toString() {
        return (origine.get() + " : " + sol.toString() + "\n");
    }

    public String getLabel() {
        return sol.get("label").toString();
    }

    public String getSource() {
        return origine.get();
    }

    public StringProperty getLabelProperty() {
        return this.label;
    }

    public StringProperty getSourceProperty() {
        return this.origine;
    }

    public StringProperty getDescriptionProperty() {
        return this.description;
    }

    public StringProperty getClasseProperty() {
        return this.classe;
    }
}
