/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.vocabsearch;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Cible implements Comparable<Cible>{

    private String adresse = null;
    private Integer type = null;
    private String name = null;
    private String graph = null;
    private Boolean langFR = true;
    private Boolean langEN = true;
    private String description = null;
    private Boolean finishTemp = false;
    private DoubleProperty finish = new SimpleDoubleProperty(0);
    private StringProperty color = new SimpleStringProperty(" -fx-accent: limegreen;");
    private List<String> alternateAdresse = new ArrayList<>();
    private static Logger logger = LogManager.getLogger(Cible.class);

    private Cible() {
    }

    public Cible(String adresse, Integer type, String name) {
        this.adresse = adresse;
        this.type = type;
        this.name = name;
    }

    public Cible(String adresse, Integer type, String name, Integer lang) {
        this(adresse, type, name);
        this.langFR = (Language.FR == lang);
        this.langEN = (Language.EN == lang);
    }

    public Cible(String adresse, Integer type, String name, String graph) {
        this(adresse, type, name);
        this.graph = graph;
    }

    public Cible(String adresse, Integer type, String name, String graph, Integer lang) {
        this(adresse, type, name, graph);
        this.langFR = (Language.FR == lang);
        this.langEN = (Language.EN == lang);
    }

    public void init() {
        logger.debug("init cible " + this.getName());
        this.color.set(" -fx-accent: limegreen;");
        finishTemp = false;
        finish.set(0);
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isRDF() {
        return type == TypeCible.RDF;
    }
    public Boolean isTTL() {
        return type == TypeCible.TTL;
    }

    public Boolean isEndPoint() {
        return type == TypeCible.EndPoint;
    }

    public String getAdresse() {
        return adresse;
    }

    public Boolean isCKAN_API() {
        return type == TypeCible.CKAN_API;
    }

    public Boolean isTDB() {
        return type == TypeCible.TDB;
    }

    public String getName() {
        return name;
    }

    public String getGraph() {
        return graph;
    }

    public String getStringType() {
        if (type == 0) {
            return "RDF";
        } else if (type == 1) {
            return "EndPoint";
        } else if (type == 2) {
            return "EndPoint";
        } else if (type == 3) {
            return "CKAN_API";
        }
        return null;
    }

    public Boolean hasGraph() {
        return graph != null;
    }

    public Boolean isFR() {
        return langFR;
    }

    public Boolean isEN() {
        return langEN;
    }

    public void setFinish() {
        if (finishTemp) {
            finish.setValue(1.0);
        } else {
            finishTemp = true;
            if (!langFR || !langEN) {
                finish.setValue(1.0);
            } else {
                finish.setValue(0.5);
            }
        }
    }

    public void success() {
        this.color.set(" -fx-accent: limegreen;");
    }

    public void fail() {
        this.color.set(" -fx-accent: red;");
    }

    public StringProperty styleProperty() {
        return color;
    }

    public DoubleProperty isFinish() {
        return finish;
    }

    public void addAlternateAdress(String alternate) {
        alternateAdresse.add(alternate);
    }

    public List<String> getAlternateAdress() {
        return this.alternateAdresse;
    }

    @Override
    public int compareTo(Cible o) {
        return this.name.toLowerCase().compareTo(o.getName().toLowerCase());
    }
}
