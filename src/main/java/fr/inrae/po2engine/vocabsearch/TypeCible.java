/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.vocabsearch;

public class TypeCible {
    public static final int RDF = 0;
    public static final int EndPoint = 1;
    public static final int TDB = 2;
    public static final int CKAN_API = 3;
    public static final int TTL = 4;

    public static Integer toType(String str) {
        if (str.equals("RDF")) {
            return RDF;
        }
        if (str.equals("EndPoint")) {
            return EndPoint;
        }
        if (str.equals("TDB")) {
            return TDB;
        }
        if (str.equals("CKAN_API")) {
            return CKAN_API;
        }
        if (str.equals("TTL")) {
            return TTL;
        }
        return null;
    }
}
