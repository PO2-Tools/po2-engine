package fr.inrae.po2engine.exception;

public class TechnicalException extends Exception{

	public TechnicalException(String message) {
		super(message);
	}
}
