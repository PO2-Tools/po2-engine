package fr.inrae.po2engine.importExport;

import fr.inrae.po2engine.exception.TechnicalException;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class ImportDataNouveauFormat implements IImportUsingPO2Engine {

	enum Langue {
		FRANCAIS,
		ANGLAIS
	};
	enum IDMsg {
		CARACTERES_INTERDITS,
		PAS_DE_COMPONENT,
		PAS_DE_FEUILLE,
		ANCIEN_FORMAT,
		INTITULE_INATTENDU,
		INTITULE_MANQUANT,
		INTITULE_DUPLIQUE,
		INTITULE_XXX_EN_NB_INCOHERENT,
		CELLULES_XXX_DIFFERENTES,
		CELL_IS_EMPTY,
		CELL_IS_EMPTY_USING_DEFAULT,
		AU_MOINS_1MAT_OU_1MET,
		AU_PLUS_1MAT_OU_1MET,
		PAS_DE_MAT,
		AGENT_DEJA_INDIQUE,
		PAS_D_AGENT_TROUVE,
		CONTENU_XXX_INATTENDU_YYY_OU_ZZZ_ATTENDU,
		XXX_IMPLIQUE_YYY,
		XXX_INCOHERENT,
		XXX_INCOHERENT_POUR_YYY,
		XXX_INCOHERENT_POUR_YYY_ZZZ,
		XXX_INCOHERENT_POUR_YYY_ZZZ_TTT,
		XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU,
		XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV,
		XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV_WWW,
		XXX_INTROUVABLE_DANS_YYY,
		XXX_YYY_INTROUVABLE_DANS_ZZZ,
		XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT,
		XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU,
		XXX_YYY_ZZZ_TTT_UUU_INTROUVABLE_DANS_VVV,
		XXX_YYY_ZZZ_TTT_UUU_VVV_INTROUVABLE_DANS_WWW,
		COMPOSITION_DID_XXX_DEJA_EXISTANTE
		};
	protected static Langue langue = Langue.FRANCAIS; 
	protected static TreeMap<Langue, TreeMap<IDMsg, String>> dicoLangue_dicoIDMsg_Msg;

	protected static String nomFeuilleProjectProcess = "Project-Process";
	protected static String nomFeuilleMaterial = "Material";
	protected static String nomFeuilleMethod = "Method";
	protected static String nomFeuilleAgent = "Agents";
	protected static String nomFeuilleItinerary = "Itinerary";
	protected static String nomFeuilleStepControlParameters = "Step control parameters";
	protected static String nomFeuilleComposition = "Composition";
	protected static String nomFeuilleObservations = "Observations";
	protected static String nomFeuilleObservationControlParameters = "Observation control parameters";
	
	protected static ArrayList<GeneralFile> lstGeneralFile;
    protected static ArrayList<MaterialMethodPart> lstMaterialProjet;
    protected static ArrayList<MaterialMethodPart> lstMethodProjet;
    
    // Dico des objets ItineraryFile en fonction du numero correspondant
    protected static TreeMap<String, TreeMap<String, ItineraryFile>> dicoProcessName_dicoIndiceItin_ItineraryFile;

    // Dico des etapes en fonction des numeros d'etapes
    protected static TreeMap<String, TreeMap<String, StepFile>> dicoProcessName_dicoIndiceStep_StepFile;

    // Dico des compositions en fonction des noms des compositions
    protected static TreeMap<String, TreeMap<String, ComponentsDeComposition>> dicoProcessName_dicoCompositionId_Components;
    protected static TreeMap<String, TreeMap<String, String>> dicoProcessName_dicoCompositionId_CompositionType;
    protected static TreeMap<String, TreeMap<String, String>> dicoProcessName_dicoCompositionId_CompositionName;

    // Dico des observations en fonction des noms des observations. 3 dico differents car 3 cas possibles :
    // - Observations d'itineraires.
    // - Observations d'etapes.
    // - Observations de compositions.
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>> dicoProcessName_dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile;
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>> dicoProcessName_dicoIndiceStep_dicoObseravtionName_dicoTableNumber_ObsFile;
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>>> dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile;

    // Dico des numeros d'etapes precedents en fonction des etapes
    protected static TreeMap<String, TreeMap<String, TreeMap<String, ArrayList<String>>>> dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec;
    
    // Dico des compositions en fonction des noms des compositions
    protected static TreeMap<String, CompositionFile> dicoComponentsName_CompositionFile = new TreeMap<String, CompositionFile>();
    
    // Dico des infos (parametres de controle) des materiels et methodes
    protected static TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP;
    protected static TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP;

    // Dico des infos sur les materiels et methodes associes aux observations d'itineraires
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>> dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP ;
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>> dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP;

    // Dico des infos sur les materiels et methodes associes aux observations d'etapes
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>> dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP;
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>> dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP;

    // Dico des infos sur les materiels et methodes associes aux observations de compositions
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>>> dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP;
    protected static TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>>> dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP;

	protected static Map<IDMsg, Integer> dicoIDMsg_Nb;
	private static org.apache.poi.ss.usermodel.CellStyle csError;
	private static org.apache.poi.ss.usermodel.CellStyle csWarning;
	private static Report report;
    
    
    public ImportDataNouveauFormat() {
		// init all structure
		lstGeneralFile = new ArrayList<>();
		lstMaterialProjet = new ArrayList<>();
		lstMethodProjet = new ArrayList<>();

		dicoProcessName_dicoIndiceItin_ItineraryFile = new TreeMap<>();
	    dicoProcessName_dicoIndiceStep_StepFile = new TreeMap<>();


		dicoProcessName_dicoCompositionId_Components = new TreeMap<>();
		dicoProcessName_dicoCompositionId_CompositionType = new TreeMap<>();
		dicoProcessName_dicoCompositionId_CompositionName = new TreeMap<>();

	    dicoProcessName_dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile = new TreeMap<>();
	    dicoProcessName_dicoIndiceStep_dicoObseravtionName_dicoTableNumber_ObsFile = new TreeMap<>();
	    dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile = new TreeMap<>();

		
		dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec = new TreeMap<>();

		dicoComponentsName_CompositionFile = new TreeMap<>();

		dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP = new TreeMap<>();
		dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP = new TreeMap<>();

		dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP = new TreeMap<>();
		dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP = new TreeMap<>();

		dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP = new TreeMap<>();
		dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP = new TreeMap<>();

		dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP = new TreeMap<>();
		dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP = new TreeMap<>();


		TreeMap<IDMsg, String> dicoIDMsg_MsgFrancais = new TreeMap<>();
    	dicoIDMsg_MsgFrancais.put(IDMsg.CARACTERES_INTERDITS, "colonne 'XXX', caractère 'YYY' interdit.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.PAS_DE_COMPONENT, "Pas d'ingrédient pour la composition XXX.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.PAS_DE_FEUILLE, "pas de feuille nommee 'XXX'.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.ANCIEN_FORMAT, "fichier à l'ancien format (Voir feuille 'XXX').");
    	dicoIDMsg_MsgFrancais.put(IDMsg.INTITULE_INATTENDU, "intitule 'XXX' inattendu.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.INTITULE_MANQUANT, "intitule 'XXX' manquant.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.INTITULE_DUPLIQUE, "intitule 'XXX' duplique.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.INTITULE_XXX_EN_NB_INCOHERENT, "intitules 'XXX' en nombre incoherent.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.CELLULES_XXX_DIFFERENTES, "Les valeurs d'une même colonne 'XXX' doivent être identiques pour un même tableau complexe.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.CELL_IS_EMPTY, "cellule 'XXX' vide.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.CELL_IS_EMPTY_USING_DEFAULT, "'XXX' vide, utilisation d'une valeur par défaut.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.AU_MOINS_1MAT_OU_1MET, "'XXX' ou 'YYY' doit etre indique");
    	dicoIDMsg_MsgFrancais.put(IDMsg.AU_PLUS_1MAT_OU_1MET, "'XXX' et 'YYY' ne doivent pas être toutes les deux indiquées");
		dicoIDMsg_MsgFrancais.put(IDMsg.PAS_DE_MAT, "'XXX' pourrait etre indique");
		dicoIDMsg_MsgFrancais.put(IDMsg.AGENT_DEJA_INDIQUE, "Agent XXX déjà indiqué.");
		dicoIDMsg_MsgFrancais.put(IDMsg.PAS_D_AGENT_TROUVE, "agent 'XXX' pas trouvé dans la liste des agents");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_IMPLIQUE_YYY, "si 'XXX' est vide, 'YYY' ne devrait pas être remplie.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.CONTENU_XXX_INATTENDU_YYY_OU_ZZZ_ATTENDU, "");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT, "'XXX' incoherent par rapport aux lignes precedentes");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT_POUR_YYY, "");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, "'XXX' non cohérent par rapport à celui déjà défini plus haut pour ce meme couple ('YYY', 'ZZZ')");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT, "'XXX' non cohérent par rapport à celui déjà défini plus haut pour ce meme triplet ('YYY', 'ZZZ', 'TTT')");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU, "'XXX' incohérent pour le même n-uplet ('YYY', 'ZZZ', 'TTT', 'UUU') de la ligne précédente.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, "'XXX' incohérent pour le même n-uplet ('YYY', 'ZZZ', 'TTT', 'UUU', 'VVV') de la ligne précédente.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV_WWW, "'XXX' incohérent pour le même n-uplet ('YYY', 'ZZZ', 'TTT', 'UUU', 'VVV', 'WWW') de la ligne précédente.");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_INTROUVABLE_DANS_YYY, "'XXX' introuvable dans la feuille 'YYY'");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, "couple ('XXX', 'YYY') introuvable dans la feuille 'ZZZ'");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, "triplet ('XXX', 'YYY', 'ZZZ') introuvable dans la feuille 'TTT'");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, "quadruplet ('XXX', 'YYY', 'ZZZ', 'TTT') introuvable dans la feuille 'UUU'");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_YYY_ZZZ_TTT_UUU_INTROUVABLE_DANS_VVV, "quintuplet ('XXX', 'YYY', 'ZZZ', 'TTT', 'UUU') introuvable dans la feuille 'VVV'");
    	dicoIDMsg_MsgFrancais.put(IDMsg.XXX_YYY_ZZZ_TTT_UUU_VVV_INTROUVABLE_DANS_WWW, "n-uplet ('XXX', 'YYY', 'ZZZ', 'TTT', 'UUU', 'VVV') introuvable dans la feuille 'WWW'");
    	dicoIDMsg_MsgFrancais.put(IDMsg.COMPOSITION_DID_XXX_DEJA_EXISTANTE, "ID de composition 'XXX' déjà utilisé");
    	
    	TreeMap<IDMsg, String> dicoIDMsg_MsgAnglais = new TreeMap<IDMsg, String>();
    	dicoIDMsg_MsgAnglais.put(IDMsg.CARACTERES_INTERDITS, "column 'XXX', character 'YYY' forbidden.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.PAS_DE_COMPONENT, "No component for composition XXX.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.PAS_DE_FEUILLE, "No sheet named 'XXX'.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.ANCIEN_FORMAT, "previous file format (see sheet 'XXX').");
    	dicoIDMsg_MsgAnglais.put(IDMsg.INTITULE_INATTENDU, "'XXX' : unexpected title.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.INTITULE_MANQUANT, "'XXX' : missing title.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.INTITULE_DUPLIQUE, "'XXX' : duplicate title.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.INTITULE_XXX_EN_NB_INCOHERENT, "'XXX' title count inconsistent.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.CELLULES_XXX_DIFFERENTES, "Values in the same column 'XXX' must be equals in the same complexe table.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.CELL_IS_EMPTY, "'XXX' missing.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.CELL_IS_EMPTY_USING_DEFAULT, "'XXX' missing, using default");
    	dicoIDMsg_MsgAnglais.put(IDMsg.AU_MOINS_1MAT_OU_1MET, "'XXX' or 'YYY' must be filled");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_IMPLIQUE_YYY, "empty 'XXX' should implied empty 'YYY'.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.AU_PLUS_1MAT_OU_1MET, "Both 'XXX' and 'YYY' should not be filled");
		dicoIDMsg_MsgAnglais.put(IDMsg.PAS_DE_MAT, "'XXX' could be filled");
		dicoIDMsg_MsgAnglais.put(IDMsg.AGENT_DEJA_INDIQUE, "Agent XXX already introduced.");
		dicoIDMsg_MsgAnglais.put(IDMsg.PAS_D_AGENT_TROUVE, "agent 'XXX' not found in agents list");
    	dicoIDMsg_MsgAnglais.put(IDMsg.CONTENU_XXX_INATTENDU_YYY_OU_ZZZ_ATTENDU, "");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT, "'XXX' unconsistant compared to preceeding lines");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT_POUR_YYY, "");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, "'XXX' unconsistant for the same couple ('YYY', 'ZZZ') in preceeding lines");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT, "'XXX' unconsistant for the same triplet ('YYY', 'ZZZ', 'TTT') in preceeding lines");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU, "Line …, 'XXX' unconsistant for the same n-uplet ('YYY', 'ZZZ', 'TTT', 'UUU') on the preceeding line.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, "Line …, 'XXX' unconsistant for the same n-uplet ('YYY', 'ZZZ', 'TTT', 'UUU', 'VVV') on the preceeding line.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV_WWW, "Line …, 'XXX' unconsistant for the same n-uplet ('YYY', 'ZZZ', 'TTT', 'UUU', 'VVV', 'WWW') on the preceeding line.");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_INTROUVABLE_DANS_YYY, "'XXX' not found in sheet 'YYY'");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, "couple ('XXX', 'YYY') not found in sheet 'ZZZ'");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, "triplet ('XXX', 'YYY', 'ZZZ') not found in sheet 'TTT'");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, "quadruplet ('XXX', 'YYY', 'ZZZ', 'TTT') not found in sheet 'UUU'");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_YYY_ZZZ_TTT_UUU_INTROUVABLE_DANS_VVV, "quintuplet ('XXX', 'YYY', 'ZZZ', 'TTT', 'UUU') not found in sheet 'VVV'");
    	dicoIDMsg_MsgAnglais.put(IDMsg.XXX_YYY_ZZZ_TTT_UUU_VVV_INTROUVABLE_DANS_WWW, "n-uplet ('XXX', 'YYY', 'ZZZ', 'TTT', 'UUU', 'VVV) not found in sheet 'WWW'");
    	dicoIDMsg_MsgAnglais.put(IDMsg.COMPOSITION_DID_XXX_DEJA_EXISTANTE, "Composition ID 'XXX' already used");

		dicoLangue_dicoIDMsg_Msg = new TreeMap<>();
    	dicoLangue_dicoIDMsg_Msg.put(Langue.FRANCAIS, dicoIDMsg_MsgFrancais);
    	dicoLangue_dicoIDMsg_Msg.put(Langue.ANGLAIS, dicoIDMsg_MsgAnglais);

		// Init du nombre de messages d'erreurs et d'avertissements.
		dicoIDMsg_Nb = new HashMap<>();
		for (IDMsg id : IDMsg.values())
			dicoIDMsg_Nb.put(id, 0);
	}

	/**
	 * Import du fichier Excel dont le nom est passe en parametre dans PO2
	 * localement ou sur le serveur distant.
	 * @param projectFile
	 * @param importFile
	 * @return 
	 * @throws Exception
	 */
	public Report convertirFichier(ProjectFile projectFile, File importFile) throws Exception {
		report = new Report();

		// RaZ du decompte du nombre de messages d'erreurs et d'avertissements.
		for (IDMsg id : IDMsg.values())
			dicoIDMsg_Nb.put(id, 0);

		Tools.updateProgress(ProgressPO2.IMPORT, 0.0);
		Double pp = 1.0/10.0;
		// 1 seul fichier contient tout.
		try (InputStream inputStream = new FileInputStream(importFile))
		{
	        Workbook workbook = WorkbookFactory.create(inputStream);

			// Creation d'un style commun pour les cellules a surligner
			csError = workbook.createCellStyle();
			csError.setFillForegroundColor(org.apache.poi.ss.usermodel.IndexedColors.YELLOW.getIndex());
			csError.setFillPattern(org.apache.poi.ss.usermodel.FillPatternType.SOLID_FOREGROUND);
			csWarning = workbook.createCellStyle();
			csWarning.setFillForegroundColor(org.apache.poi.ss.usermodel.IndexedColors.ORANGE.getIndex());
			csWarning.setFillPattern(org.apache.poi.ss.usermodel.FillPatternType.SOLID_FOREGROUND);
	        
	        // Lecture des metadonnees sur le projet/process
			log.info("lireFeuilleProjetProcess...");
	        lireFeuilleProjetProcessSaufAgent(workbook, projectFile);
			Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);
			if (projectFile != null)
	        {
		        // Lecture et decodage de la feuille "Material"
		        boolean matSinonMet = true;
				log.info("lireFeuilleMatMet...");
		        lireFeuilleMatMet(workbook, projectFile, matSinonMet);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture et decodage de la feuille "Method"
		        matSinonMet = false;
				log.info("lireFeuilleMatMet...");
		        lireFeuilleMatMet(workbook, projectFile, matSinonMet);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture et decodage de la feuille "Agent"
				log.info("lireFeuilleAgent...");
		        lireFeuilleAgent(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture et decodage des infos "Agent" de la feuille "Projet-Process"
				log.info("lireFeuilleProjetProcess...");
		        lireFeuilleProjetProcessQueAgent(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture des compositions (avant les itineraires)
				log.info("lireFeuilleCompositions...");
				lireFeuilleCompositions(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture des itineraires
				log.info("lireFeuilleItineraire...");
				lireFeuilleItineraire(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture des parametres de controle
				log.info("lireFeuilleStepControlParams...");
				lireFeuilleStepControlParams(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture des parametres de controle
				log.info("lireFeuilleObservations...");
				lireFeuilleObservations(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

				// Lecture des parametres de controle
				log.info("lireFeuilleObservationControlParams...");
				lireFeuilleObservationControlParams(workbook, projectFile);
				Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);
			}

			// Sauvegarde du fichier temporaire (correspondant au fichier initial
			// + cellules posant probleme surlignees en jaunes (erreurs)
			// ou en orange (avertissements))
			File tempFile = File.createTempFile("helper",".xlsx");
			workbook.write(new FileOutputStream(tempFile));
			report.addInfo("{\"helper_file\": \""+tempFile.getAbsolutePath().replace("\\", "/")+"\"}");
			// MaJ du rapport
			for (IDMsg id : IDMsg.values())
			{
				Integer nb = dicoIDMsg_Nb.get(id);
				if (nb > 0)
					report.addInfo(nb.toString() + " messages type \"" + dicoLangue_dicoIDMsg_Msg.get(langue).get(id) + "\"");
			}
	        			
	    	// Fermeture du fichier
			workbook.close();
			inputStream.close();

			log.info("Sauvegarde terminée");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return report;
	}

	/**
	 * Lecture de la feuille du projet et ses process dans le fichier Excel passe en parametre.
	 * Les informations Contact et Funding ne sont pas lues ici car cela supposerait
	 * d'avoir deja lu les informations de la feuille Agents (informations sauvegardees dans l'objet ProjectFile 
	 * construit ici...)
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : L'objet dans lequel les donnees seront rangees
	 * @return : L'objet projet cree (ou retourne s'il existe deja)
	 * @throws Exception
	 */
	private static void lireFeuilleProjetProcessSaufAgent(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleProjectProcess;
		String intituleProjectName = "Project name";
		String intituleProjectDescription = "Project description";
		String intituleProjectExternalLink = "External link";
		String intituleProcessType = "Process type";
		String intituleProcessName = "Process name";
		String intituleProcessReplicate = "Process replicate";
		String intituleProcessStartDate = "Start date";
		String intituleProcessEndDate = "End date";
		String intituleProcessDescription = "Process description";
		
		// On n'ajoute pas les nouveaux champs "Process replicate", "Contact", "Funding"
		// a la liste pour permettre la lecture des fichiers aux formats precedents.
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleProjectName);
		lstIntitules.add(intituleProjectDescription);
		lstIntitules.add(intituleProjectExternalLink);
		lstIntitules.add(intituleProcessType);
		lstIntitules.add(intituleProcessName);
		lstIntitules.add(intituleProcessStartDate);
		lstIntitules.add(intituleProcessEndDate);
		lstIntitules.add(intituleProcessDescription);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();

		int i;
		
		try
		{			
			
	        Sheet sheet = workbook.getSheet(nomFeuille);			
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);

				// Recherche de l'indice de la colonne pour les nouveaux champs "process replicate", "Contact" et "Funding"
				// et affichage des champs manquants eventuellement.
				lstIntitules.clear();
				lstIntitules.add(intituleProcessReplicate);
				verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);
				
		    	// Lignes suivantes : les materiels et methodes.
		        for (i = 1; ok && i <= nbLignes; i++)
		        {
			    	Row row = sheet.getRow(i);
			    	String nomProjetComplet = getContenuCellule(row, i, intituleProjectName, dicoIntitule_ICol);
					String projectDescription = getContenuCellule(row, i, intituleProjectDescription, dicoIntitule_ICol);
					String projectExternalLink = getContenuCellule(row, i, intituleProjectExternalLink, dicoIntitule_ICol);
					String processType = getContenuCellule(row, i, intituleProcessType, dicoIntitule_ICol);
					String processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
					String processReplicate = getContenuCellule(row, i, intituleProcessReplicate, dicoIntitule_ICol);
					String processStartDate = getContenuCellule(row, i, intituleProcessStartDate, dicoIntitule_ICol, true);
					String processEndDate = getContenuCellule(row, i, intituleProcessEndDate, dicoIntitule_ICol, true);
					String processDescription = getContenuCellule(row, i, intituleProcessDescription, dicoIntitule_ICol);
					String projectName = ImportDataAtWeb.getNomCorrige(nomProjetComplet);

	        		// Verifications : Les fichiers aux formats precedents (sans Contact, ni Funding, ni Process Replicate)
					// sont quand meme acceptes (pas de verification pour ces 3 champs)
					boolean cont = true;
	        		if (projectName.isEmpty() 
						&& projectDescription.isEmpty() 
						&& projectExternalLink.isEmpty() 
						&& processType.isEmpty() 
						&& processName.isEmpty() 
						&& processStartDate.isEmpty() 
						&& processEndDate.isEmpty() 
						&& processDescription.isEmpty())
	        			cont = false;
	        		else
	        		{
	        			if (!verifier(!projectName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProjectName}, true, row, dicoIntitule_ICol))	cont = false;
	        			if (!verifier(!processType.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessType}, true, row, dicoIntitule_ICol))	cont = false;
	        			if (!verifier(!processName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessName}, true, row, dicoIntitule_ICol))	cont = false;
	        		}
	        		if (cont)
	        		{
	        			// Le nom de projet ne doit pas contenir certains caracteres
	        			List<String> listCaracteresInterdits = ImportDataAtWeb.listeCaracteresInterdits(nomProjetComplet);
	        			for (String car : listCaracteresInterdits)
	        				verifier(projectName.equals(nomProjetComplet), nomFeuille, i + 1, IDMsg.CARACTERES_INTERDITS, new String[] {intituleProjectName, car}, true, row, dicoIntitule_ICol);

	        			if (projectFile == null) {
							projectFile = ImportDataAtWeb.creerProjet(projectName);
						}
						projectFile.unbind();
						projectFile.setDescription(projectDescription);
						projectFile.setExternalLink(projectExternalLink);

						boolean processRepOK = !processReplicate.isEmpty();
	        			verifier(processRepOK, nomFeuille, i + 1, IDMsg.ANCIEN_FORMAT, new String[] {nomFeuille, intituleProcessReplicate}, true, row, dicoIntitule_ICol);						

						// Creation du process
						GeneralFile generalFile = ImportDataAtWeb.getGeneralFile(lstGeneralFile, processName);
						if (generalFile == null)
						{
							generalFile = new GeneralFile(projectFile);
							projectFile.getData().analyseProcess(generalFile);
//							    generalFile.constructData();									// construit le process entierement (step, observation, composition, ...)													// A quoi ça sert ? -> seul fichier M&M cree si commente
							generalFile.setTitle(processName);
							generalFile.setOntoType(processType);
							if (processRepOK) generalFile.setReplicate(processReplicate);
							if (!processStartDate.isEmpty()) generalFile.setStartDate(processStartDate);
							if (!processEndDate.isEmpty()) generalFile.setEndDate(processEndDate);
							if (!processDescription.isEmpty()) generalFile.setDescription(processDescription);

							// MaJ de la liste des projets
							lstGeneralFile.add(generalFile);
						}
	        		}				    					    	
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Lecture de la feuille des Materiels et Methodes
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * @param matSinonMet : Bouleen indiquant si la feuille a lire est celle des materiels
	 * ou celle des methodes.
	 * @throws Exception
	 */
	private static void lireFeuilleMatMet(Workbook workbook, ProjectFile projectFile, boolean matSinonMet) throws Exception {

		String nomFeuille = matSinonMet ? nomFeuilleMaterial : nomFeuilleMethod;
		String intituleMaterialNom = matSinonMet ? "Material name" : "Method name";
		String intituleMaterialType = matSinonMet ? "Material type" : "Method type";
		String intituleAttribut = "Attribute";
		String intituleObjectOfInterest = "Object of interest";
		String intituleValue = "Value";
		String intituleUnit = "Unit";
		String intituleComment = "Comment";
		
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleMaterialType);
		lstIntitules.add(intituleMaterialNom);
		lstIntitules.add(intituleAttribut);
		lstIntitules.add(intituleObjectOfInterest);
		lstIntitules.add(intituleValue);
		lstIntitules.add(intituleUnit);
		lstIntitules.add(intituleComment);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();

		int i;
		
		try
		{
	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);

		    	// Lignes suivantes : les materiels et methodes.
		        for (i = 1; ok && i <= nbLignes; i++)
		        {
		        	Row row = sheet.getRow(i);
			    	if (row != null)
			    	{
		        		String matMetNom = getContenuCellule(row, i, intituleMaterialNom, dicoIntitule_ICol); 
		        		String matMetType = getContenuCellule(row, i, intituleMaterialType, dicoIntitule_ICol); 
		        		String attribut = getContenuCellule(row, i, intituleAttribut, dicoIntitule_ICol);
		        		String objectOfInterest = getContenuCellule(row, i, intituleObjectOfInterest, dicoIntitule_ICol);
		        		String value = getContenuCellule(row, i, intituleValue, dicoIntitule_ICol);
		        		String unit = getContenuCellule(row, i, intituleUnit, dicoIntitule_ICol);
		        		String comment = getContenuCellule(row, i, intituleComment, dicoIntitule_ICol);
		        							    	
		        		// Verifications :
						boolean cont = true;
		        		if (matMetNom.isEmpty() && matMetType.isEmpty() && attribut.isEmpty() && objectOfInterest.isEmpty() && value.isEmpty() && unit.isEmpty() && comment.isEmpty())
		        			cont = false;
		        		else if (!verifier(!matMetNom.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleMaterialNom, nomFeuille}, true, row, dicoIntitule_ICol))	
	        				cont = false;
		        		if (cont)
		        		{
			    			// Creation du materiel s'il n'esxiste pas.
			    			MaterialMethodPart matMet = matSinonMet 
			    					? ImportDataAtWeb.getMaterialMethodPart(lstMaterialProjet, matMetNom)
			    					: ImportDataAtWeb.getMaterialMethodPart(lstMethodProjet, matMetNom); 
			    			if (matMet == null)
			    			{
			    				// Creation du nouveau materiel
						    	matMet = new MaterialMethodPart(projectFile, matSinonMet ? DataPartType.MATERIAL_RAW : DataPartType.METHOD_RAW);
						    	matMet.setOntoType(matMetType);
						        matMet.setId(matMetNom);
						        
						        // MaJ de la liste des materiels
						        if (matSinonMet)
						        	lstMaterialProjet.add(matMet);
						        else
						        	lstMethodProjet.add(matMet);
			    			}
			    			
			    			// Eventuellement, memorisation des precisions associees
			    			// Si seul un commentaire est ajoute, au lieu d'ajouter une caracteristique
			    			// le commentaire est attribue a la methode ou la materiel
			    			if (attribut.isEmpty() && objectOfInterest.isEmpty() && value.isEmpty() && unit.isEmpty())
			    				matMet.setComment(comment);
			    			else
			    				matMet.addCharacteristic(attribut, objectOfInterest, value, unit, comment);
		        		}
			    	}
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Lecture de la feuille des Agents
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * ou celle des methodes.
	 * @throws Exception
	 */
	private static void lireFeuilleAgent(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleAgent;
		String intituleOrg = "Organization";
		String intituleFName = "Family name";
		String intituleGName = "Given name";
		String intituleEMail = "eMail";
		
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleFName);
		lstIntitules.add(intituleOrg);
		lstIntitules.add(intituleGName);
		lstIntitules.add(intituleEMail);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();

		int i;
		
		try
		{
			// RaZ de la liste des agents actuelle
			projectFile.getListAgent().clear();

	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);

		    	// Lignes suivantes : les materiels et methodes.
		        for (i = 1; ok && i <= nbLignes; i++)
		        {
		        	Row row = sheet.getRow(i);
			    	if (row != null)
			    	{
		        		String org = getContenuCellule(row, i, intituleOrg, dicoIntitule_ICol); 
		        		String fName = getContenuCellule(row, i, intituleFName, dicoIntitule_ICol); 
		        		String gName = getContenuCellule(row, i, intituleGName, dicoIntitule_ICol);
		        		String eMail = getContenuCellule(row, i, intituleEMail, dicoIntitule_ICol);
		        							    	
		        		// Verifications :
						boolean cont = true;
		        		if (org.isEmpty() && fName.isEmpty() && gName.isEmpty() && eMail.isEmpty())
		        			cont = false;
		        		if (cont)
		        		{
							// Ajout de l'agent dans la liste s'il n'y est pas deja.
							String sAgent = agentExistant(projectFile, org, fName, gName, eMail);
							if (verifier(sAgent.isEmpty(), nomFeuille, -1, IDMsg.AGENT_DEJA_INDIQUE, new String[] {sAgent, intituleOrg, intituleFName, intituleGName, intituleEMail}, false, row, dicoIntitule_ICol))
								//projectFile.addAgent(org, fName, gName, eMail);
								projectFile.addAgent(org, fName, gName, eMail);
		        		}
			    	}
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void lireFeuilleProjetProcessQueAgent(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleProjectProcess;
		String intituleProcessName = "Process name";
		String intituleProjectContactName = "Contact name";
		String intituleProjectFunding = "Project funding";
		String intituleProjectContactMail = "Contact mail";
		String intituleContact = "Contact";
		String intituleFunding = "Funding";
		String sContact = "";
		String sFunding = "";
		
		ArrayList<String> lstIntitulesAncienFormat = new ArrayList<String> ();
		ArrayList<String> lstIntitulesNvFormat = new ArrayList<String> ();
		lstIntitulesAncienFormat.add(intituleProjectContactName);
		lstIntitulesAncienFormat.add(intituleProjectFunding);
		lstIntitulesAncienFormat.add(intituleProjectContactMail);
		lstIntitulesNvFormat.add(intituleContact);
		lstIntitulesNvFormat.add(intituleFunding);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();

		int i;
		
		try
		{			
			
	        Sheet sheet = workbook.getSheet(nomFeuille);			
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {
	        	// Verification des intitules sur la 1ere ligne : 2 cas
				// - Fichier au nouveau format
				// - Fichier a l'ancien format
		        int nbLignes = sheet.getLastRowNum();
				
				Row row = sheet.getLastRowNum() >= 0 ? sheet.getRow(0) : null;
				int nbCols = row != null ? row.getLastCellNum() : 0;
				for (int j = 0; j < nbCols; j++)
				{
					String buf = ImportDataAtWeb.lireCellule(row, 0, j);
					
					// Cas particulier des cellules vides
					if (buf.isEmpty());

					else if ((buf.equals(intituleProcessName) || lstIntitulesAncienFormat.indexOf(buf) >= 0 || lstIntitulesNvFormat.indexOf(buf) >= 0) && !dicoIntitule_ICol.containsKey(buf))
						dicoIntitule_ICol.put(buf, j);
				}
				
				// Le fichier est-il a l'ancien format, au nouveau format ?
				Boolean ancienFormat = true;
				for (String intitule : lstIntitulesAncienFormat)
					if (!dicoIntitule_ICol.containsKey(intitule))
						ancienFormat = false;
				Boolean nouveauFormat = true;
				for (String intitule : lstIntitulesNvFormat)
					if (!dicoIntitule_ICol.containsKey(intitule))
						nouveauFormat = false;
				
				// Message d'alerte si le fichier est a l'ancien format
       			verifier(!ancienFormat, nomFeuille, 1, IDMsg.ANCIEN_FORMAT, new String[] {nomFeuille, intituleProjectContactMail, intituleProjectContactName, intituleProjectFunding}, false, row, dicoIntitule_ICol);

		    	// Ligne suivantes : Contact et Funding.
				Boolean ok = ancienFormat || nouveauFormat;
		        for (i = 1; ok && i <= nbLignes; i++)
		        {
			    	row = sheet.getRow(i);
					String processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
					if (ancienFormat)
					{
						String projectContactName = getContenuCellule(row, i, intituleProjectContactName, dicoIntitule_ICol);
						String projectFunding = getContenuCellule(row, i, intituleProjectFunding, dicoIntitule_ICol);
						String projectContactMail = getContenuCellule(row, i, intituleProjectContactMail, dicoIntitule_ICol);
						
						HashMap<KeyWords, ComplexField> agent = new HashMap<KeyWords, ComplexField>();
						agent.put(ProjectAgentPart.agentOrganisationK, new ComplexField(""));
						agent.put(ProjectAgentPart.agentFamilyNameK, new ComplexField(projectContactName));
						agent.put(ProjectAgentPart.agentGivenNameK, new ComplexField(projectContactMail));
						sContact = Tools.agentToString(agent);
						
						agent = new HashMap<KeyWords, ComplexField>();
						agent.put(ProjectAgentPart.agentOrganisationK, new ComplexField(projectFunding));
						agent.put(ProjectAgentPart.agentFamilyNameK, new ComplexField(""));
						agent.put(ProjectAgentPart.agentGivenNameK, new ComplexField(""));
						sFunding = Tools.agentToString(agent);
					}
					else
					{
						sContact = getContenuCellule(row, i, intituleContact, dicoIntitule_ICol);
						sFunding = getContenuCellule(row, i, intituleFunding, dicoIntitule_ICol);
					}

	        		// Verifications :
	        		if (!processName.isEmpty() && (!sContact.isEmpty() || !sFunding.isEmpty()))
	        		{
						// Mise a jour du contact (vrai ou faux agent, en fonction des cas)
						HashMap<KeyWords, ComplexField> foundContact = projectFile.getAgent(sContact);
						if (verifier(foundContact != null, nomFeuille, -1, IDMsg.PAS_D_AGENT_TROUVE, new String[] {sContact, intituleContact}, true, row, dicoIntitule_ICol))
							projectFile.getContactProperty().setValue(foundContact);
						else
						{
							projectFile.unbind();
							projectFile.getContactProperty().setValue(Tools.createFakeAgentFromString(sContact));
						}

						// Mise a jour de la propriete Funding (vrai ou faux agent, en fonction des cas)
						HashMap<KeyWords, ComplexField> foundFunding = projectFile.getAgent(sFunding);
						if (verifier(foundFunding != null, nomFeuille, -1, IDMsg.PAS_D_AGENT_TROUVE, new String[] {sFunding, intituleFunding}, true, row, dicoIntitule_ICol))
							projectFile.getFundingProperty().setValue(foundFunding);
						else
							projectFile.getFundingProperty().setValue(Tools.createFakeAgentFromString(sFunding));
	        		}				    					    	
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Lecture de la feuille des itineraires
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * @throws Exception
	 */
	private static void lireFeuilleItineraire(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleItinerary;
		String intituleProcessName = "Process name";
		String intituleDate = "Step date";
		String intituleTime = "Step time";
		String intituleTimeDuration = "Step time duration";
		String intituleProductOfInterest = "Product of interest";
		String intituleItineraryName = "Itinerary name";
		String intituleItineraryNumber = "Itinerary number";
		String intituleStepNumber = "Step number";
		String intituleStepNextStep = "Next step";
		String intituleStepSubStep = "Substep";
		String intituleStepType = "Step type";
		String intituleStepName = "Step name";
		String intituleStepDescription = "Step description";
		String intituleMaterialName = "Material name";
		String intituleMethodName = "Method name";
		String intituleInputCompositionId = "Input composition id";
		String intituleOutputCompositionId = "Output composition id";
		String intituleStepOperator = "Step operator";
		String intituleStepScale = "Step scale";
		
		// On n'ajoute pas les nouveaux champs "Step scale" et "Step operator"
		// a la liste pour permettre la lecture des fichiers aux formats precedents.
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleProcessName);
		lstIntitules.add(intituleDate);
		lstIntitules.add(intituleTime);
		lstIntitules.add(intituleTimeDuration);
		lstIntitules.add(intituleProductOfInterest);
		lstIntitules.add(intituleItineraryName);
		lstIntitules.add(intituleItineraryNumber);
		lstIntitules.add(intituleStepNumber);
		lstIntitules.add(intituleStepNextStep);
		lstIntitules.add(intituleStepSubStep);
		lstIntitules.add(intituleStepType);
		lstIntitules.add(intituleStepName);
		lstIntitules.add(intituleStepDescription);
		lstIntitules.add(intituleMaterialName);
		lstIntitules.add(intituleMethodName);
		lstIntitules.add(intituleInputCompositionId);
		lstIntitules.add(intituleOutputCompositionId);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();

		// Dictionnaire en fonction du nom de process
		// du dictionnaire en fonction des numeros d'itineraires
		// du dictionnaire des numeros de StepFile suivants en fonction du numero de StepFile,
	    TreeMap<String, TreeMap<String, TreeMap<String, ArrayList<String>>>> 
	    	dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps 
	    	= new TreeMap<String, TreeMap<String,TreeMap<String,ArrayList<String>>>>(); 
	    TreeMap<String, TreeMap<String, TreeMap<String, ArrayList<String>>>> 
    	dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps 
    	= new TreeMap<String, TreeMap<String,TreeMap<String,ArrayList<String>>>>(); 

		Pattern pattern = Pattern.compile("::");
		
		int i;
		
		try
		{
	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);	        	

				// Recherche des indices des colonnes pour les nouveaux champs "Step scale" et "Step operator"
				// et affichage des champs manquants eventuellement.
				lstIntitules.clear();
				lstIntitules.add(intituleStepOperator);
				lstIntitules.add(intituleStepScale);
				verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);
		
		        // Lignes suivantes : Description des itineraires.
		        for (i = 1; ok && i <= nbLignes; i++)
		        {
			    	Row row = sheet.getRow(i);
	        		String processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
	        		String stepDate = getContenuCellule(row, i, intituleDate, dicoIntitule_ICol, true);
	        		String stepTime = getContenuCellule(row, i, intituleTime, dicoIntitule_ICol);
	        		String stepTimeDuration = getContenuCellule(row, i, intituleTimeDuration, dicoIntitule_ICol);
	        		String productOfInterest = getContenuCellule(row, i, intituleProductOfInterest, dicoIntitule_ICol);
	        		String itineraryName = getContenuCellule(row, i, intituleItineraryName, dicoIntitule_ICol);
	        		String itineraryNumber = getContenuCellule(row, i, intituleItineraryNumber, dicoIntitule_ICol);
	        		String stepNumber = getContenuCellule(row, i, intituleStepNumber, dicoIntitule_ICol);
	        		String stepNextSteps = getContenuCellule(row, i, intituleStepNextStep, dicoIntitule_ICol); 
	        		String stepSubsteps = getContenuCellule(row, i, intituleStepSubStep, dicoIntitule_ICol); 
	        		String stepName = getContenuCellule(row, i, intituleStepName, dicoIntitule_ICol);
	        		String stepType = getContenuCellule(row, i, intituleStepType, dicoIntitule_ICol);
	        		String stepDescription = getContenuCellule(row, i, intituleStepDescription, dicoIntitule_ICol);
	        		String materialName = getContenuCellule(row, i, intituleMaterialName, dicoIntitule_ICol);
	        		String methodName = getContenuCellule(row, i, intituleMethodName, dicoIntitule_ICol);
	        		String inputCompositionId = getContenuCellule(row, i, intituleInputCompositionId, dicoIntitule_ICol);
	        		String outputCompositionId = getContenuCellule(row, i, intituleOutputCompositionId, dicoIntitule_ICol);
					String stepAgent = getContenuCellule(row, i, intituleStepOperator, dicoIntitule_ICol);
					String stepScale = getContenuCellule(row, i, intituleStepScale, dicoIntitule_ICol);
	        							    	
	        		// Verifications :
	        		boolean cont = true;
	        		if (processName.isEmpty() && stepDate.isEmpty() && stepTime.isEmpty() && stepTimeDuration.isEmpty() && productOfInterest.isEmpty() && itineraryName.isEmpty() && itineraryNumber.isEmpty() && processName.isEmpty() && stepNumber.isEmpty() && stepNextSteps.isEmpty() && stepSubsteps.isEmpty() && stepName.isEmpty() && stepType.isEmpty() && stepDescription.isEmpty() && materialName.isEmpty() && methodName.isEmpty() && inputCompositionId.isEmpty() && outputCompositionId.isEmpty() && stepAgent.isEmpty() && stepScale.isEmpty())
	        			cont = false;
	        		else
	        		{
	        			if (!verifier(!processName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessName}, true, row, dicoIntitule_ICol))			cont = false;	        		
	        			if (!verifier(!itineraryNumber.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleItineraryNumber}, true, row, dicoIntitule_ICol))	cont = false;	        		
	        			if (!verifier(!stepNumber.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleStepNumber}, true, row, dicoIntitule_ICol))			cont = false;	        		
	        			if (!verifier(!stepType.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleStepType}, true, row, dicoIntitule_ICol))				cont = false;	        		
	        		}
	        		if (cont)
	        		{
			    		// Creation ou recup du process : 1 ou plusieurs par projet 
		    			GeneralFile generalFile = ImportDataAtWeb.getGeneralFile(lstGeneralFile, processName);
		    			if (verifier (generalFile != null, nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleProjectProcess}, true, row, dicoIntitule_ICol))
		        		{
		        			// Si le nom de l'itinéraire n'est pas indiqué, ce n'est pas bloquant
		        			// (on considerera donc que toutes les lignes pour lesquelles le nom
		        			// d'itineraire n'est pas renseigne correspond a ce meme itineraire)
		        			verifier(!itineraryName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleItineraryName}, false, row, dicoIntitule_ICol);
			        		
				    		// Recup ou creation de l'itineraire en fonction du nom indique
		        			// Le 05.04.23, prise en compte du numero d'itineraire a la place du nom
		        			// pour creer un nouvel itineraire.
				    		// En meme temps, MaJ du dico pour le lien entre le n° d'itineraire et l'itineraire
		        			ItineraryFile itineraryFile;
				    		if (!dicoProcessName_dicoIndiceItin_ItineraryFile.containsKey(processName))
				    			dicoProcessName_dicoIndiceItin_ItineraryFile.put(processName, new TreeMap<String, ItineraryFile>());
				    		TreeMap<String, ItineraryFile> dicoIndiceItin_ItineraryFile = dicoProcessName_dicoIndiceItin_ItineraryFile.get(processName);			    		
				    		if (dicoIndiceItin_ItineraryFile.containsKey(itineraryNumber))
				    		{
					    		// Si l'itineraire a deja ete cree, 
				    			// message d'alerte si le nom ne correspond pas
			        			itineraryFile = dicoProcessName_dicoIndiceItin_ItineraryFile
			        					.get(processName)
			        					.get(itineraryNumber);
			        			verifier(itineraryFile.getItineraryName().equals(itineraryName), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT, new String[] {itineraryName}, true, row, dicoIntitule_ICol);
				        		verifier(itineraryFile.getCProductsOfInterest().toString().equals(productOfInterest), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleProductOfInterest, intituleProcessName, intituleItineraryNumber}, false, row, dicoIntitule_ICol);
				    		}
				    		else				    		
				    		{
					    		// Si l'itineraire n'a pas encore ete cree
				    			// creation et MaJ du dico
			    		        itineraryFile = new ItineraryFile(generalFile);
			    		        itineraryFile.setItineraryName(itineraryName);
					    		itineraryFile.setProductsOfInterest(productOfInterest);
				    			dicoIndiceItin_ItineraryFile.put(itineraryNumber, itineraryFile);
				    		}

		        			// Verification de la coherence entre le nom de l'itineraire et le n° indique
				    		if (verifier(
				    				itineraryFile == dicoIndiceItin_ItineraryFile.get(itineraryNumber), 
				    				nomFeuille, 
				    				i + 1,
				    				IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ,
				    				new String[] {intituleItineraryName, intituleProcessName, intituleItineraryNumber},
									true))				    		
				    		{
						    	// Ajout (ou recup) d'une etape a l'itineraire
					        	//StepFile stepFile = ExportDataAtWeb.getOrCreateStepFile(itineraryFile, stepType, stepName, stepDate, stepTime, stepTimeDuration, stepDescription);
				    			StepFile stepFile = null;

					        	if (!dicoProcessName_dicoIndiceStep_StepFile.containsKey(processName))
					        		dicoProcessName_dicoIndiceStep_StepFile.put(processName, new TreeMap<String,StepFile>());
				        		TreeMap<String, StepFile> dicoIndiceStep_StepFile = dicoProcessName_dicoIndiceStep_StepFile.get(processName);
				        		if (!dicoIndiceStep_StepFile.containsKey(stepNumber))
				        		{
						        	//stepFile = ExportDataAtWeb.getOrCreateStepFile(itineraryFile, stepType, stepName, stepDate, stepTime, stepTimeDuration, stepDescription);
				    		        stepFile = new StepFile(stepType, generalFile);
				    		        stepFile.setId(stepName);
				    		        //stepFile.setId(stepName + " " + stepDescription);		// Juste pour dépanner LM
				    	    		stepFile.setDate(stepDate);
				    	    		stepFile.setTime(stepTime);
				    	    		stepFile.setDuration(stepTimeDuration);
				    	    		stepFile.setDescription(stepDescription);	

									// Association de l'agent operateur a l'etape. Un faux agent est cree
									// automatiquement si l'agent indique n'existe pas.
									if (!stepAgent.isEmpty())
									{
										stepFile.setAgents(stepAgent);
									}

									stepFile.setScale(stepScale);
									itineraryFile.addStep(stepFile);
				    		        
				        			dicoIndiceStep_StepFile.put(stepNumber, stepFile);
				        		}
				        		else
				        			stepFile = dicoIndiceStep_StepFile.get(stepNumber);

			        			// MaJ du dico pour gerer la liste des etapes suivantes
					        	if (!dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.containsKey(processName))
				        			dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.put(processName, new TreeMap<String, TreeMap<String,ArrayList<String>>>());
				        		TreeMap<String, TreeMap<String, ArrayList<String>>> dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps = dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.get(processName);
				        		if (!dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.containsKey(itineraryNumber))
				        			dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.put(itineraryNumber, new TreeMap<String, ArrayList<String>>());
				        		TreeMap<String, ArrayList<String>> dicoIndiceStep_ListIndiceNextSteps = dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.get(itineraryNumber);
				        		if (!dicoIndiceStep_ListIndiceNextSteps.containsKey(stepNumber))
				        			dicoIndiceStep_ListIndiceNextSteps.put(stepNumber, new ArrayList<String>());

			        			// MaJ du dico pour gerer la liste des etapes precedentes
				        		if (!dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.containsKey(processName))
				        			dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.put(processName, new TreeMap<String, TreeMap<String,ArrayList<String>>>());
				        		TreeMap<String, TreeMap<String, ArrayList<String>>> dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec = dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.get(processName); 
				        		if (!dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.containsKey(itineraryNumber))
				        			dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.put(itineraryNumber, new TreeMap<String, ArrayList<String>>());
				        		TreeMap<String, ArrayList<String>> dicoIndiceStep_LstIndicesStepPrec = dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.get(itineraryNumber);
				        		if (!dicoIndiceStep_LstIndicesStepPrec.containsKey(stepNumber))
				        			dicoIndiceStep_LstIndicesStepPrec.put(stepNumber, new ArrayList<String>());
					        	
				        		// Verifications (non bloquantes)
				        		verifier(stepFile.getId().equals(stepName), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleStepName, intituleProcessName, intituleStepNumber}, true, row, dicoIntitule_ICol);
				        		verifier(stepFile.getOntoType().equals(stepType), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleStepType, intituleProcessName, intituleStepNumber}, true, row, dicoIntitule_ICol);
				        		verifier(stepFile.getDate().equals(stepDate), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleDate, intituleProcessName, intituleStepNumber}, false, row, dicoIntitule_ICol);
				        		verifier(stepFile.getTime().equals(stepTime), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleTime, intituleProcessName, intituleStepNumber}, false, row, dicoIntitule_ICol);
				        		verifier(stepFile.getDuration().equals(stepTimeDuration), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleTimeDuration, intituleProcessName, intituleStepNumber}, false, row, dicoIntitule_ICol);
				        		verifier(stepFile.getDescription().equals(stepDescription), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleStepDescription, intituleProcessName, intituleStepNumber}, false, row, dicoIntitule_ICol);
				        		verifier(stepFile.getAgents().equals(stepAgent), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleStepOperator, intituleProcessName, intituleStepNumber}, false, row, dicoIntitule_ICol);
								verifier(stepFile.getCScale().getValue().getValue().equals(stepScale), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ, new String[] {intituleStepScale, intituleProcessName, intituleStepNumber}, false, row, dicoIntitule_ICol);
				        		
					        	// Eventuellement, ajout (ou recup) d'une etape pour chaque etape 
					        	// de la liste des etapes suivantes (sous la forme "...::...::...")
					        	// dans la colonne "Next step"
					        	String[] lstNumEtapes = stepNextSteps.trim().isEmpty()
					        			? null
					        			: pattern.split(stepNextSteps);
					        	if (lstNumEtapes != null)
					        	{
					        		// Memorisation de la liste des etapes suivantes chaque etape,
					        		// pour chaque process, n° itineraire, n° etape,
					        		// pour pouvoir creer plus tard les liens entre les etapes.
					        		if (!dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.containsKey(processName))
					        			dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.put(processName, new TreeMap<String, TreeMap<String,ArrayList<String>>>());
					        		if (!dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.containsKey(itineraryNumber))
					        			dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.put(itineraryNumber, new TreeMap<String, ArrayList<String>>());
					        		ArrayList<String> listIndiceNextSteps = dicoIndiceStep_ListIndiceNextSteps.get(stepNumber);
					        		if (lstNumEtapes != null)
					        			for (String num : lstNumEtapes)
					        				if (listIndiceNextSteps.indexOf(num) < 0)
					        					listIndiceNextSteps.add(num);					        			
					        		
					        		// MaJ de la liste des etapes precedentes pour chaque etape suivante
					        		// (pour gerer convenablement plus loin les cas ou des etapes isolees
					        		// n'ont pas d'etape suivante)
					        		if (!dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.containsKey(processName))
					        			dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.put(processName, new TreeMap<String, TreeMap<String,ArrayList<String>>>());
					        		if (!dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.containsKey(itineraryNumber))
					        			dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec.put(itineraryNumber, new TreeMap<String, ArrayList<String>>());
					        		for (String indiceNextStep : listIndiceNextSteps)
					        		{
						        		if (!dicoIndiceStep_LstIndicesStepPrec.containsKey(indiceNextStep))
						        			dicoIndiceStep_LstIndicesStepPrec.put(indiceNextStep, new ArrayList<String>());
						        		ArrayList<String> lstIndicesStepPrec = dicoIndiceStep_LstIndicesStepPrec.get(indiceNextStep);
						        		
						        		if (lstIndicesStepPrec == null)
						        			lstIndicesStepPrec = new ArrayList<String>();
						        		
						        		if (lstIndicesStepPrec.indexOf(stepNumber) < 0)
						        			lstIndicesStepPrec.add(stepNumber);
					        		}
					        	}				        	
						    					        		
					        	// Eventuellement, ajout (ou recup) d'une etape pour chaque etape 
					        	// de la liste des sous etapes (sous la forme "...::...::...")
					        	// dans la colonne "substep"
					        	lstNumEtapes = stepSubsteps.trim().isEmpty()
					        			? null
					        			: pattern.split(stepSubsteps);
					        	if (lstNumEtapes != null)
					        	{
					        		// Memorisation de la liste des sous etapes pour chaque etape,
					        		// pour chaque process, n° itineraire, n° etape,
					        		// pour pouvoir creer plus tard les liens entre les etapes.
						        	if (!dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.containsKey(processName))
						        		dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.put(processName, new TreeMap<String, TreeMap<String,ArrayList<String>>>());
					        		TreeMap<String, TreeMap<String, ArrayList<String>>> dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps = dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.get(processName);
					        		if (!dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.containsKey(itineraryNumber))
					        			dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.put(itineraryNumber, new TreeMap<String, ArrayList<String>>());
					        		TreeMap<String, ArrayList<String>> dicoIndiceStep_ListIndiceSubsteps = dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.get(itineraryNumber);
					        		if (!dicoIndiceStep_ListIndiceSubsteps.containsKey(stepNumber))
					        			dicoIndiceStep_ListIndiceSubsteps.put(stepNumber, new ArrayList<String>());
					        		ArrayList<String> listIndiceSubsteps = dicoIndiceStep_ListIndiceSubsteps.get(stepNumber);
					        		if (lstNumEtapes != null)
					        			for (String num : lstNumEtapes)
					        				if (listIndiceSubsteps.indexOf(num) < 0)
					        					listIndiceSubsteps.add(num);					        			
					        	}				        	
						    	
						    	// Recup du materiel dont le nom est indique.
				    			MaterialMethodPart matMet = null;
				    			if (verifier(!materialName.isEmpty(), nomFeuille, i+1, IDMsg.CELL_IS_EMPTY, new String[] {intituleMaterialName}, false, row, dicoIntitule_ICol))
				    			{
				    				matMet = ImportDataAtWeb.getMaterialMethodPart(lstMaterialProjet, materialName);
				    				verifier(matMet != null, nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleMaterialName, nomFeuilleMaterial}, true, row, dicoIntitule_ICol);
				    			}

			    				// Memorisation pour quand des parametres de controles seront ajoutes
	        					if (!dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP.containsKey(processName))
	        						dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>());
	        					TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoIndiceStep_dicoMaterialName_MatMetLP = dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP.get(processName);
	        					if (!dicoIndiceStep_dicoMaterialName_MatMetLP.containsKey(stepNumber))
	        						dicoIndiceStep_dicoMaterialName_MatMetLP.put(stepNumber, new TreeMap<String, MaterialMethodLinkPart>());
	        					TreeMap<String, MaterialMethodLinkPart> dicoMaterialName_MatMetLP = dicoIndiceStep_dicoMaterialName_MatMetLP.get(stepNumber);
				    			if (matMet != null)
				    			{
				    				// Ajout de l'association entre ce materiel ou cette methode et cette etape
				    				MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(matMet);
				    				stepFile.addMaterialMethod(lPart);
				    				
				    				// Memorisation pour quand des parametres de controles seront ajoutes
		        					if (!dicoMaterialName_MatMetLP.containsKey(materialName))
		        						dicoMaterialName_MatMetLP.put(materialName, lPart);
				    			}
						    	
						    	// Recup des methodes dont les nom sont indiques sous la forme
				    			// nom methode 1; nom methode 2; ...
					        	String[] lstMethodNames = methodName.trim().isEmpty()
					        			? null
					        			: pattern.split(methodName);
				        		if (lstMethodNames != null)
				        			for (String nom : lstMethodNames)
						    			if (!nom.isEmpty()) 
						    			{
							    			matMet = ImportDataAtWeb.getMaterialMethodPart(lstMethodProjet, nom);
							    			if (verifier(matMet != null, nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleMethodName, nomFeuilleMethod}, true, row, dicoIntitule_ICol))
							    			{
							    				// Ajout de la consigne pour ce materiel ou methode a cette etape
							    				MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(matMet);
							    				stepFile.addMaterialMethod(lPart);
							    				
							    				// Memorisation pour quand des parametres de controles seront ajoutes
					        					if (!dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP.containsKey(processName))
					        						dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>());
					        					TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoIndiceStep_dicoMethodName_MatMetLP = dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP.get(processName);
					        					if (!dicoIndiceStep_dicoMethodName_MatMetLP.containsKey(stepNumber))
					        						dicoIndiceStep_dicoMethodName_MatMetLP.put(stepNumber, new TreeMap<String, MaterialMethodLinkPart>());
					        					TreeMap<String, MaterialMethodLinkPart> dicoMethodName_MatMetLP = dicoIndiceStep_dicoMethodName_MatMetLP.get(stepNumber);
					        					if (!dicoMethodName_MatMetLP.containsKey(nom))
					        						dicoMethodName_MatMetLP.put(nom, lPart);
							    			}
						    			}
				        		

				        		
				        		
				        		
				        		
				        		
				        		
				        		
				        		
				        		
				        		// Ajout des compositions d'entree sortie pour chaque etape
				        		ajouterComposition(processName, intituleProcessName, intituleInputCompositionId, nomFeuille, stepNumber, inputCompositionId, stepFile, i + 1, true, row, dicoIntitule_ICol);
				        		ajouterComposition(processName, intituleProcessName, intituleOutputCompositionId, nomFeuille, stepNumber, outputCompositionId, stepFile, i + 1, false, row, dicoIntitule_ICol);
			    				
			    				
			    				
				    		}
		        		}
	        		}				    					    	
		        }
		        
		    	// Pour toutes les etapes de tous les itineraires de tous les projets,
		        // creation etapes
		        Iterator<Map.Entry<String, TreeMap<String, TreeMap<String, ArrayList<String>>>>> iteratorProcess = dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.entrySet().iterator();
		        while (iteratorProcess.hasNext())
		        {
		        	Map.Entry<String, TreeMap<String, TreeMap<String, ArrayList<String>>>> cplProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNestSteps = iteratorProcess.next();
		        	
		        	// Recup du nom du process 
		        	String processName = cplProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNestSteps.getKey();

	    			// Recup du dico en fonction des numeros d'itineraires
	    			// du dictionnaire des numeros de StepFile suivants en fonction du numero de StepFile,	    			
	    			TreeMap<String, TreeMap<String, ArrayList<String>>> dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps = cplProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceNestSteps.getValue();
	            	Iterator<Map.Entry<String, TreeMap<String, ArrayList<String>>>> iteratorItin = dicoIndiceItin_dicoIndiceStep_ListIndiceNextSteps.entrySet().iterator();
	            	while (iteratorItin.hasNext())
	            	{
	            		Map.Entry<String, TreeMap<String, ArrayList<String>>> cplIndiceItin_dicoIndiceStep_ListIndiceNextSteps = iteratorItin.next();

	            		// Recup de l'itineraire
	            		String itineraryNumber = cplIndiceItin_dicoIndiceStep_ListIndiceNextSteps.getKey();
						ItineraryFile itineraryFile = dicoProcessName_dicoIndiceItin_ItineraryFile
								.get(processName)
								.get(itineraryNumber);

	            		// Recup du dictionnaire des numeros de StepFile suivants en fonction du numero de StepFile,
						TreeMap<String, ArrayList<String>> dicoIndiceStep_ListIndiceNextSteps = cplIndiceItin_dicoIndiceStep_ListIndiceNextSteps.getValue();
		            	Iterator<Map.Entry<String, ArrayList<String>>> iteratorStep = dicoIndiceStep_ListIndiceNextSteps.entrySet().iterator();
		            	while (iteratorStep.hasNext())
		            	{
		            		Map.Entry<String, ArrayList<String>> cplIndiceStep_ListIndiceNextsteps = iteratorStep.next();
		            		
		            		// Recup de l'etape
		            		String stepNumber = cplIndiceStep_ListIndiceNextsteps.getKey();
		            		StepFile stepFile = dicoProcessName_dicoIndiceStep_StepFile.get(processName)
		            				.get(stepNumber);
		            		
				        	// Pour chaque etape suivante...
		            		ArrayList<String> listIndiceNextsteps = cplIndiceStep_ListIndiceNextsteps.getValue();
				        	if (listIndiceNextsteps.size() > 0)
			            		for (String stepNextstep : listIndiceNextsteps)
			            		{
				            		// Recup de l'etape suivante
						        	//StepFile stepNextStepFile = ExportDataAtWeb.getStepFile(itineraryFile, stepNextStep);
				            		StepFile stepNextstepFile = dicoProcessName_dicoIndiceStep_StepFile.get(processName)
				            				.get(stepNextstep);
				            		
		    						// Ajout d'un lien entre l'etape et celle qui suit.
		    						itineraryFile.addLinkItinerary(stepFile, stepNextstepFile);
			            		}
							
							// Si l'etape n'a pas d'etape suivante ni d'etape precedente,
				        	// le lien avec l'itineraire doit quand meme etre fait.
				        	else if (dicoProcessName_dicoIndiceItin_dicoIndiceStep_LstIndicesStepPrec
			        				.get(processName)
			        				.get(itineraryNumber)
			        				.get(stepNumber)
			        				.size() == 0)
	    						itineraryFile.addLinkItinerary(stepFile, null);
		            	}
	            	}
		        }

		        
		    	// Pour toutes les etapes de tous les itineraires de tous les projets,
		        // creation sous etapes
		        iteratorProcess = dicoProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.entrySet().iterator();
		        while (iteratorProcess.hasNext())
		        {
		        	Map.Entry<String, TreeMap<String, TreeMap<String, ArrayList<String>>>> cplProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps = iteratorProcess.next();
		        	
		        	// Recup du nom du process 
		        	String processName = cplProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.getKey();

	    			// Recup du dico en fonction des numeros d'itineraires
	    			// du dictionnaire des numeros de StepFile suivants en fonction du numero de StepFile,	    			
	    			TreeMap<String, TreeMap<String, ArrayList<String>>> dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps = cplProcessName_dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.getValue();
	            	Iterator<Map.Entry<String, TreeMap<String, ArrayList<String>>>> iteratorItin = dicoIndiceItin_dicoIndiceStep_ListIndiceSubsteps.entrySet().iterator();
	            	while (iteratorItin.hasNext())
	            	{
	            		Map.Entry<String, TreeMap<String, ArrayList<String>>> cplIndiceItin_dicoIndiceStep_ListIndiceSubsteps = iteratorItin.next();

	            		// Recup du dictionnaire des numeros de StepFile suivants en fonction du numero de StepFile,
						TreeMap<String, ArrayList<String>> dicoIndiceStep_ListIndiceSubsteps = cplIndiceItin_dicoIndiceStep_ListIndiceSubsteps.getValue();
		            	Iterator<Map.Entry<String, ArrayList<String>>> iteratorStep = dicoIndiceStep_ListIndiceSubsteps.entrySet().iterator();
		            	while (iteratorStep.hasNext())
		            	{
		            		Map.Entry<String, ArrayList<String>> cplIndiceStep_ListIndiceSubsteps = iteratorStep.next();
		            		
		            		// Recup de l'etape
		            		String stepNumber = cplIndiceStep_ListIndiceSubsteps.getKey();
		            		StepFile stepFile = dicoProcessName_dicoIndiceStep_StepFile.get(processName)
		            				.get(stepNumber);
		            		
				        	// Pour chaque etape suivante...
		            		ArrayList<String> listIndiceSubsteps = cplIndiceStep_ListIndiceSubsteps.getValue();
				        	if (listIndiceSubsteps.size() > 0)
			            		for (String stepSubstep : listIndiceSubsteps)
			            		{
				            		// Recup de l'etape suivante
						        	//StepFile stepNextStepFile = ExportDataAtWeb.getStepFile(itineraryFile, stepNextStep);
				            		StepFile stepSubstepFile = dicoProcessName_dicoIndiceStep_StepFile.get(processName)
				            				.get(stepSubstep);
				            		
		    						// Ajout d'un lien entre l'etape et celle qui suit.
		    						stepFile.addSubStep(stepSubstepFile);
			            		}
		            	}
	            	}
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Lecture de la feuille des parametres de controle des etapes
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * @throws Exception
	 */
	private static void lireFeuilleStepControlParams(Workbook workbook, ProjectFile projectFile) throws Exception {

		String processName;
		String stepNumber;
		String stepName;				// Remarque : Pas de verification de la coherence par rapport au numero d'etape
		String materialName;
		String methodName;

		String nomFeuille = nomFeuilleStepControlParameters;
		String intituleProcessName = "Process name";
		String intituleStepNumber = "Step number";
		String intituleStepName = "Step name";
		String intituleMaterialName = "Material name";
		String intituleMethodName = "Method name";
		String intituleAttribut = "Attribute";
		String intituleObjectOfInterest = "Object of interest";
		String intituleValue = "Value";
		String intituleUnit = "Unit";
		String intituleComment = "Comment";
		
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleProcessName);
		lstIntitules.add(intituleStepNumber);
		lstIntitules.add(intituleStepName);
		lstIntitules.add(intituleMaterialName);
		lstIntitules.add(intituleMethodName);
		lstIntitules.add(intituleAttribut);
		lstIntitules.add(intituleObjectOfInterest);
		lstIntitules.add(intituleValue);
		lstIntitules.add(intituleUnit);
		lstIntitules.add(intituleComment);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();

		int i;

		try
		{
	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, false))
	        {	        
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);

		        // Lignes suivantes : Description des itineraires.
		        for (i = 1; ok && i <= nbLignes; i++)
		        {
			    	Row row = sheet.getRow(i);
	        		processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
	        		stepNumber = getContenuCellule(row, i, intituleStepNumber, dicoIntitule_ICol);
	        		stepName = getContenuCellule(row, i, intituleStepName, dicoIntitule_ICol);
	        		materialName = getContenuCellule(row, i, intituleMaterialName, dicoIntitule_ICol);
	        		methodName = getContenuCellule(row, i, intituleMethodName, dicoIntitule_ICol);
	        							    	
	        		// Verifications :
    				MaterialMethodLinkPart lPart = null;
    				boolean cont = true;
	        		if (processName.isEmpty() && stepNumber.isEmpty() && materialName.isEmpty() && methodName.isEmpty())
	        			cont = false;
	        		else
	        		{
	        			if (!verifier(!processName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessName}, true, row, dicoIntitule_ICol))			cont = false;	        		
	        			if (!verifier(!stepNumber.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleStepNumber}, true, row, dicoIntitule_ICol))			cont = false;	        		
	        			if (!verifier(!materialName.isEmpty() || !methodName.isEmpty(), nomFeuille, i + 1, IDMsg.AU_MOINS_1MAT_OU_1MET, new String[] {intituleMaterialName, intituleMethodName}, true, row, dicoIntitule_ICol)) cont = false;
	        			if (!verifier(materialName.isEmpty() || methodName.isEmpty(), nomFeuille, i + 1, IDMsg.AU_PLUS_1MAT_OU_1MET, new String[] {intituleMaterialName, intituleMethodName}, true, row, dicoIntitule_ICol))	cont = false;
	        		}

	        		if (!cont);

					// Cas ou les precisions portent sur un materiel
					else if (!materialName.isEmpty())
	        		{
	        			if (!verifier(dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP
	        					.containsKey(processName),
	        					nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
						else if (!verifier(dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP
								.get(processName)
								.containsKey(stepNumber),
								nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleStepNumber, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
		        		// Verification (non bloquantes, mais on ne fait rien quand meme).
	    				// Le dico dicoProcessName_dicoIndiceStep_StepFile
	    				// et dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP
	    				// ont ete construits en meme temps, donc si c'est OK pour le 2eme,
	    				// c'est aussi OK pour le 1er.	        	
						else if (!verifier(dicoProcessName_dicoIndiceStep_StepFile
								.get(processName)
								.get(stepNumber)
								.getId().equals(stepName),
								nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleStepName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
							
		        		else if (verifier(dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP
								.get(processName)
								.get(stepNumber)
								.containsKey(materialName),
								nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleMaterialName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol))									

							// Recup de l'association (entre le materiel et l'etape) faite lors de la lecture de l'itineraire
							// Inutile de refaire toutes les verifs, car certaines d'entre elles sont communes avec l'autre dico
							lPart = dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP
									.get(processName)
									.get(stepNumber)
									.get(materialName);		        		
	        		}

					// Cas ou les precisions portent sur une methode
					else if (!methodName.isEmpty())
	        		{
	        			if (!verifier(dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP
	        					.containsKey(processName),
								nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleMethodName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
						else if (!verifier(dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP
								.get(processName)
								.containsKey(stepNumber),
								nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleMethodName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
		        		// Verification (non bloquantes, mais on ne fait rien quand meme).
	    				// Le dico dicoProcessName_dicoIndiceStep_StepFile
	    				// et dicoProcessName_dicoIndiceStep_dicoMaterialName_MatMetLP
	    				// ont ete construits en meme temps, donc si c'est OK pour le 2eme,
	    				// c'est aussi OK pour le 1er.	        	
						else if (!verifier(dicoProcessName_dicoIndiceStep_StepFile
								.get(processName)
								.get(stepNumber)
								.getId().equals(stepName),
								nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleStepName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
							
		        		else if (verifier(dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP
									.get(processName)
									.get(stepNumber)
									.containsKey(methodName),
									nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleMethodName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol))									

							// Recup de l'association (entre le materiel et l'etape) faite lors de la lecture de l'itineraire
							// Inutile de refaire toutes les verifs, car certaines d'entre elles sont communes avec l'autre dico
							lPart = dicoProcessName_dicoIndiceStep_dicoMethodName_MatMetLP
									.get(processName)
									.get(stepNumber)
									.get(methodName);
	        		}
	        		
	        		// Ajout des informations.
	        		if (lPart != null)
	        		{
    					
		        		String attribut = getContenuCellule(row, i, intituleAttribut, dicoIntitule_ICol);
		        		String objectOfInterest = getContenuCellule(row, i, intituleObjectOfInterest, dicoIntitule_ICol);
		        		String value = getContenuCellule(row, i, intituleValue, dicoIntitule_ICol);
		        		String unit = getContenuCellule(row, i, intituleUnit, dicoIntitule_ICol);
		        		String comment = getContenuCellule(row, i, intituleComment, dicoIntitule_ICol);

		        		// Ajout du deyail pour ce materiel ou cette methode a cette etape
    					lPart.addCharacteristic(
    							attribut,
    							objectOfInterest, 
    							value,
    							unit,
    							comment);
	        			
	        		}
    			}
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Lecture de la feuille des compositions
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * @throws Exception
	 */
	private static void lireFeuilleCompositions(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleComposition;
		String intituleProcessName = "Process name";
		String intituleCompositionId = "Composition id";
		String intituleCompositionName = "Composition name";
		String intituleCompositionType = "Composition type";
		String intituleComponentType = "Component type";
		String intituleAttribut = "Attribute";
		String intituleValue = "Value";
		String intituleUnit = "Unit";
		String intituleComment = "Comment";

		String processName = "", processNamePrec;
		String compositionId = "", compositionIdPrec;
		String compositionName = "", compositionNamePrec;
		String compositionType = "", compositionTypePrec;
		String componentType;
		String attribut;
		String value;
		String unit;
		String comment;
		
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleProcessName);
		lstIntitules.add(intituleCompositionId);
		lstIntitules.add(intituleCompositionName);
		lstIntitules.add(intituleCompositionType);
		//lstIntitules.add(intituleComponentType);
		//lstIntitules.add(intituleAttribut);
		//lstIntitules.add(intituleValue);
		//lstIntitules.add(intituleUnit);
		//lstIntitules.add(intituleComment);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();
		ComponentsDeComposition componentsPrec = new ComponentsDeComposition(); 
		
		int i;

		try
		{
	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {	        
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);

				// Pour rester compatible avec l'ancien format (avec ingredients), au cas ou
				lstIntitules.add(intituleComponentType);
				lstIntitules.add(intituleAttribut);
				lstIntitules.add(intituleValue);
				lstIntitules.add(intituleUnit);
				lstIntitules.add(intituleComment);
				verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, false);

		    	// Lignes suivantes : Description des itineraires.
		        for (i = 1; ok && i <= nbLignes + 1; i++)
		        {
					Row rowP = sheet.getRow(i - 1); 
			    	Row row = sheet.getRow(i);
			    	processNamePrec = processName;
	        		processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
	        		compositionIdPrec = compositionId;
	        		compositionId = getContenuCellule(row, i, intituleCompositionId, dicoIntitule_ICol);
	        		compositionNamePrec = compositionName;
	        		compositionName = getContenuCellule(row, i, intituleCompositionName, dicoIntitule_ICol);
	        		compositionTypePrec = compositionType;
	        		compositionType = getContenuCellule(row, i, intituleCompositionType, dicoIntitule_ICol);
	        		componentType = getContenuCellule(row, i, intituleComponentType, dicoIntitule_ICol);
	        		attribut = getContenuCellule(row, i, intituleAttribut, dicoIntitule_ICol);
	        		value = getContenuCellule(row, i, intituleValue, dicoIntitule_ICol);
	        		unit = getContenuCellule(row, i, intituleUnit, dicoIntitule_ICol);
	        		comment = getContenuCellule(row, i, intituleComment, dicoIntitule_ICol);

	        		boolean cont = true;
	        		if (i > nbLignes
		        				|| (i > 1 && (!processNamePrec.equals(processName)
		        						|| !compositionIdPrec.equals(compositionId)
				        				|| !compositionNamePrec.equals(compositionName))))
    				{	    				
		        		if (processNamePrec.isEmpty() && compositionIdPrec.isEmpty() && compositionNamePrec.isEmpty() && compositionTypePrec.isEmpty())
		        			cont = false;
		        		else
		        		{
		        			if (!verifier(!processNamePrec.isEmpty(), nomFeuille, i, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessName}, true, rowP, dicoIntitule_ICol))			cont = false;	        		
		        			//if (!verifier(!compositionTypePrec.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleCompositionType}))	cont = false;	        		
		        			if (!verifier(!compositionIdPrec.isEmpty() || !compositionNamePrec.isEmpty(), nomFeuille, i, IDMsg.CELL_IS_EMPTY, new String[] {intituleCompositionId}, true, rowP, dicoIntitule_ICol))	cont = false;	        		
		        		}
	    				if (cont)
	    				{
	    					// Avertissement si "composition id" est vide : dans ce cas on utilise le champ indique dans "composition name"
	    					// (le cas ou les 2 champs sont vides n'est pas possible a ce niveau : voir test juste au dessus).
	    					if (!verifier(!compositionIdPrec.isEmpty(), nomFeuille, i, IDMsg.CELL_IS_EMPTY_USING_DEFAULT, new String[] {intituleCompositionId}, false, rowP, dicoIntitule_ICol))
	    						compositionIdPrec = compositionNamePrec;
	    					
	    					// Plus d'indication si pas d'ingredient.
		        			//verifier(componentsPrec.getLstComponents().size() > 0, nomFeuille, i, IDMsg.PAS_DE_COMPONENT, new String[] {compositionNamePrec});

				    		// Verification de l'existence du process 
			    			GeneralFile generalFile = ImportDataAtWeb.getGeneralFile(lstGeneralFile, processNamePrec);
			    			if (verifier (generalFile != null, nomFeuille, i, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleProjectProcess}, true, rowP, dicoIntitule_ICol))
			        		{
		    					// Memorisation du dico des compositions, toutes etapes confondues
		    					// Remarque : le dernier dico est fonction du nom (arbitraire, mais unique)
		    					// de la (ou les) liste(s) des ingredients associes aux noms de compositions
		    					// (2 compositions differentes peuvent avoir le meme nom, la difference est faite
		    					// sur la liste des ingredients)
		    					if (!dicoProcessName_dicoCompositionId_Components.containsKey(processNamePrec))
		    						dicoProcessName_dicoCompositionId_Components.put(processNamePrec, new TreeMap<String, ComponentsDeComposition>());
		    					TreeMap<String, ComponentsDeComposition> dicoCompositionId_Components = dicoProcessName_dicoCompositionId_Components.get(processNamePrec);
		    					if (verifier(!dicoCompositionId_Components.containsKey(compositionIdPrec), nomFeuille, i, IDMsg.COMPOSITION_DID_XXX_DEJA_EXISTANTE, new String[] {compositionIdPrec}, true, rowP, dicoIntitule_ICol))
		    						dicoCompositionId_Components.put(compositionIdPrec, componentsPrec); 
		        
		    					// Memorisation du type de la composition.
		    					if (!dicoProcessName_dicoCompositionId_CompositionType.containsKey(processNamePrec))
		    						dicoProcessName_dicoCompositionId_CompositionType.put(processNamePrec, new TreeMap<String, String>());
		    					TreeMap<String, String> dicoCompositionId_CompositionType = dicoProcessName_dicoCompositionId_CompositionType.get(processNamePrec);
		    					if (!dicoCompositionId_CompositionType.containsKey(compositionIdPrec))
		    						dicoCompositionId_CompositionType.put(compositionIdPrec, compositionTypePrec);
		    					
		    					// Memorisation du nom de la composition.
		    					if (!dicoProcessName_dicoCompositionId_CompositionName.containsKey(processNamePrec))
		    						dicoProcessName_dicoCompositionId_CompositionName.put(processNamePrec, new TreeMap<String, String>());
		    					TreeMap<String, String> dicoCompositionId_CompositionName = dicoProcessName_dicoCompositionId_CompositionName.get(processNamePrec);
		    					if (!dicoCompositionId_CompositionName.containsKey(compositionIdPrec))
		    						dicoCompositionId_CompositionName.put(compositionIdPrec, compositionNamePrec);
			        		}
	    					
	        			    // Creation d'une nouvelle liste d'ingredients, pour la prochaine fois
	    					componentsPrec = new ComponentsDeComposition();
	    				}
    				}
	        		
	        		// Pour toutes les lignes, si pas de probleme, memorisation des ingredients...
	        		if (processName.isEmpty() && compositionId.isEmpty() && compositionName.isEmpty() && compositionType.isEmpty())
	        			cont = false;
	        		if (cont)
	        		{
		        		// Plus de verifications pour les erreurs sans consequence...
		        		//verifier(!componentType.isEmpty(), nomFeuille, i+1, IDMsg.CELL_IS_EMPTY, new String[] {intituleComponentType});
		        		//verifier(!value.isEmpty(), nomFeuille, i+1, IDMsg.CELL_IS_EMPTY, new String[] {intituleValue});
		        		
		        		// Ajout de l'ingredient, si pas d'erreur grave.
						// Plus de message dans ce cas.
		        		if (!attribut.isEmpty())	        		
	        				componentsPrec.addComponent(componentType, attribut, value, unit, comment);
	        		}
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Lecture de la feuille des observations
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * @throws Exception
	 */
	private static void lireFeuilleObservations(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleObservations;

		// La gestion des tableaux complexes ne peut se faire qu'apres avoir tout lu les donnees
		// de tout le tableau.
		String processName = "", processNamePrec;
		String itineraryNumber = "", itineraryNumberPrec;
		String stepNumber = "", stepNumberPrec;
		String compositionId = "", compositionIdPrec;
		String observationName = "", observationNamePrec;
		String tableNumber = "", tableNumberPrec;
    	String stepDate = "";
		String stepTime = "";
		String stepTimeDuration = "";
		String obsDescription = "";
		String materialName = "";
		String methodName = "";
		String observationOperator = "";
		String observationScale = "";
		String tableType = "", tableTypePrec;
		
		String intituleDate = "Date";
		String intituleTime = "Time";
		String intituleTimeDuration = "Time duration";
		String intituleDescription = "Description";
		String intituleProcessName = "Process name";
		String intituleItineraryNumber = "Itinerary number";
		String intituleStepNumber = "Step number";
		//String intituleStepName = "Step name";
		String intituleCompositionId = "Composition id";
		String intituleObservationName = "Observation name";
		String intituleMaterialName = "Material name";
		String intituleMethodName = "Method name";
		String intituleTableNumber = "Table number";
		String intituleAttribut = "Attribute";
		String intituleObjectOfInterest = "Object of interest";
		String intituleValue = "Value";
		String intituleUnit = "Unit";
		String intituleComment = "Comment";
		String intituleObservationOperator = "Observation operator";
		String intituleObservationScale = "Observation scale";
		String intituleTableType = "Table type";
		ArrayList<String> lstIntitulesMultiples = new ArrayList<String>();
		lstIntitulesMultiples.add(intituleAttribut);
		lstIntitulesMultiples.add(intituleObjectOfInterest);
		lstIntitulesMultiples.add(intituleValue);
		lstIntitulesMultiples.add(intituleUnit);
		lstIntitulesMultiples.add(intituleComment);
		
		// On n'ajoute pas les nouveaux champs "Observation operator" et "Observation scale"
		// a la liste pour permettre la lecture des fichiers aux formats precedents.
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleDate);
		lstIntitules.add(intituleTime);
		lstIntitules.add(intituleTimeDuration);
		lstIntitules.add(intituleDescription);
		lstIntitules.add(intituleProcessName);
		lstIntitules.add(intituleItineraryNumber);
		lstIntitules.add(intituleStepNumber);
		//lstIntitules = lstIntitules.append(intituleStepName);
		lstIntitules.add(intituleCompositionId);
		lstIntitules.add(intituleObservationName);
		lstIntitules.add(intituleMaterialName);
		lstIntitules.add(intituleMethodName);
		lstIntitules.add(intituleTableNumber);
		lstIntitules.add(intituleTableType);
		lstIntitules.add(intituleAttribut);
		lstIntitules.add(intituleObjectOfInterest);
		lstIntitules.add(intituleValue);
		lstIntitules.add(intituleUnit);
		lstIntitules.add(intituleComment);
		
		TreeMap<String, ArrayList<Integer>> dicoIntitule_LstICol = new TreeMap<String, ArrayList<Integer>>();
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();
		
		// Dico des etapes en fonction des numeros d'etapes
    	TreeMap<String, ArrayList<String>> dicoProcessName_listIndiceStepTraites = new TreeMap<String, ArrayList<String>>();

		// Dico des caracteristiques devant etre fixes (date, heure, ...) en fonction des observations.
		TreeMap<String, HashMap<String, String>> dicoCleObs_dicoInituleVal = new TreeMap<>();

		int i, j, jDeb;
		int nbLignes, nbCols;
			
		try
		{
	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, true))
	        {	        
	        	// Verification des intitules sur la 1ere ligne
		        nbLignes = sheet.getLastRowNum();
		        nbCols = verifierTitresMultiples(sheet, nomFeuille, lstIntitules, lstIntitulesMultiples, dicoIntitule_ICol, dicoIntitule_LstICol);

				// Recherche des indices des colonnes pour les nouveaux champs "Observation operator" et "Observation scale"
				// et affichage des champs manquants eventuellement.
				lstIntitules.clear();
				lstIntitules.add(intituleObservationOperator);
				lstIntitules.add(intituleObservationScale);
				verifierTitresMultiples(sheet, nomFeuille, lstIntitules, lstIntitulesMultiples, dicoIntitule_ICol, dicoIntitule_LstICol);
		
	        	// Creation des objets pour memoriser les quintuplets (attribut, objet, value, unit, comment)
	        	// pour chaque tableau d'observation
	    		boolean tabAttributValide[] = new boolean[nbCols];
	    		ArrayList<ArrayList<String>> lstLstAttribut = new ArrayList<ArrayList<String>>(nbCols);
	    		ArrayList<ArrayList<String>> lstLstObjectOfInterest = new ArrayList<ArrayList<String>>(nbCols);
	    		ArrayList<ArrayList<String>> lstLstValue = new ArrayList<ArrayList<String>>(nbCols);
	    		ArrayList<ArrayList<String>> lstLstUnit = new ArrayList<ArrayList<String>>(nbCols);
	    		ArrayList<ArrayList<String>> lstLstComment = new ArrayList<ArrayList<String>>(nbCols);
	    		lstLstAttribut = new ArrayList<ArrayList<String>>(nbCols);
	    		for (j = 0; j < nbCols; j++)
	    		{
	    			tabAttributValide[j] = false;
	    			lstLstAttribut.add(new ArrayList<String>());
	    			lstLstObjectOfInterest.add(new ArrayList<String>());
	    			lstLstValue.add(new ArrayList<String>());
	    			lstLstUnit.add(new ArrayList<String>());
	    			lstLstComment.add(new ArrayList<String>());
	    		}
        	
		    	// Lignes suivantes : Description des itineraires.
	    		boolean ok = nbCols > 0;
				ObservationFile obs = null;		    	
		        for (i = 1; ok && i <= nbLignes + 1; i++)
		        {
			    	Row row = sheet.getRow(i);		    	
			    	
	        		// Lecture des valeurs non cle
	        		stepDate = getContenuCellule(row, i, intituleDate, dicoIntitule_ICol);
	        		stepTime = getContenuCellule(row, i, intituleTime, dicoIntitule_ICol);
	        		stepTimeDuration = getContenuCellule(row, i, intituleTimeDuration, dicoIntitule_ICol);
	        		obsDescription = getContenuCellule(row, i, intituleDescription, dicoIntitule_ICol);
	        		materialName = getContenuCellule(row, i, intituleMaterialName, dicoIntitule_ICol);
	        		methodName = getContenuCellule(row, i, intituleMethodName, dicoIntitule_ICol);
					observationOperator = getContenuCellule(row, i, intituleObservationOperator, dicoIntitule_ICol);
					observationScale = getContenuCellule(row, i, intituleObservationScale, dicoIntitule_ICol);

	        		// Lecture des valeurs cle permettant de savoir si le tableau de donnees a change
			    	processNamePrec = processName; 
	        		processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
	        		itineraryNumberPrec = itineraryNumber;
	        		itineraryNumber = getContenuCellule(row, i, intituleItineraryNumber, dicoIntitule_ICol);
	        		stepNumberPrec = stepNumber; 
	        		stepNumber = getContenuCellule(row, i, intituleStepNumber, dicoIntitule_ICol);
	        		compositionIdPrec = compositionId;
	        		compositionId = decoderCompositionId(getContenuCellule(row, i, intituleCompositionId, dicoIntitule_ICol));
	        		observationNamePrec = observationName;
	        		observationName = getContenuCellule(row, i, intituleObservationName, dicoIntitule_ICol);
	        		tableNumberPrec = tableNumber;
	        		tableNumber = getContenuCellule(row, i, intituleTableNumber, dicoIntitule_ICol);
					if (tableNumber.isEmpty()) tableNumber = "1";
					tableTypePrec = tableType;
	        		tableType = getContenuCellule(row, i, intituleTableType, dicoIntitule_ICol);

					// La derniere ligne, ou en cas de changement dans les variables cles.
  	        		// Ajout des details sur l'observation
				  	if (obs != null &&
					(i > nbLignes
					|| (i > 1 && (!processNamePrec.equals(processName)
							  || !itineraryNumberPrec.equals(itineraryNumber)
							  || !stepNumberPrec.equals(stepNumber)
							  || !compositionIdPrec.equals(compositionId)
							  || !observationNamePrec.equals(observationName)
							  || !tableNumberPrec.equals(tableNumber)
							  || !tableTypePrec.equals(tableType)))))
					{
						// Determination de la dimension du tableau a creer (1D ou 2D)
						// en fonction du nb d'attributs valides	        			
						int nb = 0;
						for (j = 0; j < nbCols; j++)
							if (tabAttributValide[j])
								nb++;
						
						// Creation d'un tableau 1D
						if (nb == 1)
						{
							TablePart tp = obs.createObsData(true);
							if (tableTypePrec.toLowerCase().equals("calc data"))
								tp.setType(DataPartType.CALC_DATA);
							SimpleTable st = (SimpleTable)tp.getTable();
							
							// Recherche de l'indice de la colonne remplie
							j = 0;
							while (j < nbCols && !tabAttributValide[j])
								j++;

							// Chaque elements de la liste attribut correspond a une nouvelle ligne dans le tableau.
							nb = lstLstAttribut.get(j).size();
							for (int k = 0; k < nb; k++)
							{
								String attribut = lstLstAttribut.get(j).get(k);
								String objectOfInterest = lstLstObjectOfInterest.get(j).get(k);
								String value = lstLstValue.get(j).get(k);
								String unit = lstLstUnit.get(j).get(k);
								String comment = lstLstComment.get(j).get(k);
								st.addObservationLine(attribut, objectOfInterest, value, unit, comment);
							}
						}
						
						// Creation d'un tableau 2D
						else if (nb > 1)
						{
							TablePart tp = obs.createObsData(false);
							if (tableTypePrec.toLowerCase().equals("calc data"))
								tp.setType(DataPartType.CALC_DATA);
							ComplexTable ct = (ComplexTable)tp.getTable();
							TreeMap<Integer, Map<ComplexField, ComplexField>> dicoILigne_ComplexLine = new TreeMap<Integer, Map<ComplexField,ComplexField>>();

							// Recherche de l'indice de 1ere la colonne remplie
							jDeb = 0;
							while (jDeb < nbCols && !tabAttributValide[jDeb])
								jDeb++;
							
							// Verifications : Les noms des attributs, objets, unites commentaires
							// doivent etre egaux pour chaque ligne
							j = jDeb;
							while (j < nbCols)
							{
								if (tabAttributValide[j])
								{								
									int k = 0;
									nb = lstLstAttribut.get(j).size();
									String attribut = lstLstAttribut.get(j).get(k);
									String objectOfInterest = lstLstObjectOfInterest.get(j).get(k);
									String unit = lstLstUnit.get(j).get(k);
									String comment = lstLstComment.get(j).get(k);
									for (k = 1; k < nb; k++)
									{
										Row rowPP = sheet.getRow(i - nb + k);
										verifier(attribut.equals(lstLstAttribut.get(j).get(k)), nomFeuille, i, IDMsg.CELLULES_XXX_DIFFERENTES, new String[] {intituleAttribut}, true, rowPP, dicoIntitule_LstICol, j);
										verifier(objectOfInterest.equals(lstLstObjectOfInterest.get(j).get(k)), nomFeuille, i, IDMsg.CELLULES_XXX_DIFFERENTES, new String[] {intituleObjectOfInterest}, true, rowPP, dicoIntitule_LstICol, j);
										verifier(unit.equals(lstLstUnit.get(j).get(k)), nomFeuille, i, IDMsg.CELLULES_XXX_DIFFERENTES, new String[] {intituleUnit}, true, rowPP, dicoIntitule_LstICol, j);
										verifier(comment.equals(lstLstComment.get(j).get(k)), nomFeuille, i, IDMsg.CELLULES_XXX_DIFFERENTES, new String[] {intituleComment}, true, rowPP, dicoIntitule_LstICol, j);
									}

									// Creation de la colonne (intitule
									ComplexField cfColInitule = ct.addColumn(attribut, objectOfInterest, unit, comment);
									
									// Chaque valeur est une ligne dans le tableau
									for (k = 0; k < nb; k++)
									{
										String value = lstLstValue.get(j).get(k);
										
										// La ligne est creee pour la 1ere colonne
										// et recuperee pour les autres.
										Map<ComplexField, ComplexField> complexLine;
										if (j == jDeb)
										{			        			    		
											complexLine = ct.addLine();
											dicoILigne_ComplexLine.put(k, complexLine);
										}
										else
											complexLine = dicoILigne_ComplexLine.get(k);			        			    		
										complexLine.put(cfColInitule, new ComplexField(value));			        			    	
									}
								}
								
								j++;
							}
						}
						
						// Nouveau tableau de donnees => RaZ des drapeaux, listes, ...
						for (j = 0; j < nbCols; j++)
						{
							tabAttributValide[j] = false;
							lstLstAttribut.get(j).clear();
							lstLstObjectOfInterest.get(j).clear();
							lstLstValue.get(j).clear();
							lstLstUnit.get(j).clear();
							lstLstComment.get(j).clear();
						}
						obs = null;
					}
  



	        		// Pour chaque ligne de donnees.
					if (!processName.isEmpty() || !itineraryNumber.isEmpty() || !stepNumber.isEmpty() || !compositionId.isEmpty() || !observationName.isEmpty())
    				{
	        			// Lecture du materiel
	        			TreeMap<String, MaterialMethodPart> dicoMatDObsName_MMP = new TreeMap<String, MaterialMethodPart>();
	        			if (verifier(!materialName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleMaterialName}, false, row, dicoIntitule_ICol))
	        		    {
			    			//material = ImportDataAtWeb.getMaterialMethodPart(lstMaterialProjet, materialNamePrec);
	        		    	dicoMatDObsName_MMP = getDicoMatMetName_MMLP(lstMaterialProjet, materialName);
			    			
							// Pas de materiel trouve ?
							verifier(dicoMatDObsName_MMP.size() > 0, nomFeuille, i + 1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleMaterialName, nomFeuilleMaterial}, true, row, dicoIntitule_ICol);

			    			// Le materiel peut aussi etre une liste de materiels sous la forme id1::id2::...
			    			// Chaque id de materiel est donc verifie
	        		    	for (Map.Entry<String, MaterialMethodPart> cplMatDObsName_MMP : dicoMatDObsName_MMP.entrySet())
	        		    		verifier(cplMatDObsName_MMP.getValue() != null, nomFeuille, i + 1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleMaterialName, nomFeuilleMaterial}, true, row, dicoIntitule_ICol);
	        		    }
	        			
	        		    // Lecture de la methode
	        			TreeMap<String, MaterialMethodPart> dicoMetDObsName_MMP = new TreeMap<String, MaterialMethodPart>();
	        		    if (!methodName.isEmpty())
	        		    {
	        		    	//method = ImportDataAtWeb.getMaterialMethodPart(lstMethodProjet, methodNamePrec);
	        		    	dicoMetDObsName_MMP = getDicoMatMetName_MMLP(lstMethodProjet, methodName);

			    			// La methode peut aussi etre une liste de methodes sous la forme id1::id2::...
			    			// Chaque id de methode est donc verifiee
	        		    	for (Map.Entry<String, MaterialMethodPart> cplMetDObsName_MMP : dicoMetDObsName_MMP.entrySet())
	        		    		verifier(cplMetDObsName_MMP.getValue() != null, nomFeuille, i + 1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleMethodName, nomFeuilleMethod}, true, row, dicoIntitule_ICol);
	        		    }
	        		    
	        			// Creation ou recup de l'observation : 3 cas possibles
    	        		// -1- Etape non indiquee => observation d'itineraire.
    	        		if (stepNumber.isEmpty())
    	        		{
            				// Verifications... Numero d'itineraire necessaire
            				if (verifier(!itineraryNumber.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleItineraryNumber}, true, row, dicoIntitule_ICol))
            				{
	            				// Verifications... Problemes sans consequence
	    	        			verifier(compositionId.isEmpty(), nomFeuille, i + 1, IDMsg.XXX_IMPLIQUE_YYY, new String[] {intituleStepNumber, intituleCompositionId}, false, row, dicoIntitule_ICol);
	    	        			
	    	        			// Verifications...
	    	        			if (!verifier(dicoProcessName_dicoIndiceItin_ItineraryFile.containsKey(processName),
	    	        					nomFeuille, i + 1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
	    	        			else if (!verifier(dicoProcessName_dicoIndiceItin_ItineraryFile.get(processName).containsKey(itineraryNumber),
	    	        					nomFeuille, i + 1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleItineraryNumber, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
	    	        			else
	    	        			{
	                				// Recup ou creation de l'observation
	                				if (!dicoProcessName_dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile.containsKey(processName))
	                					dicoProcessName_dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>());
	                				TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>> dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile = dicoProcessName_dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile.get(processName);
	                				if (!dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile.containsKey(itineraryNumber))
	                					dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile.put(itineraryNumber, new TreeMap<String, TreeMap<String, ObservationFile>>());
	                				TreeMap<String, TreeMap<String, ObservationFile>> dicoObservationName_dicoTableNumber_ObsFile = dicoIndiceItin_dicoObservationName_dicoTableNumber_ObsFile.get(itineraryNumber);
	                				if (!dicoObservationName_dicoTableNumber_ObsFile.containsKey(observationName))
										dicoObservationName_dicoTableNumber_ObsFile.put(observationName, new TreeMap<String, ObservationFile>());
									TreeMap<String, ObservationFile> dicoTableNumber_ObsFile = dicoObservationName_dicoTableNumber_ObsFile.get(observationName);
									if (dicoTableNumber_ObsFile.containsKey(tableNumber))
	                					obs = dicoTableNumber_ObsFile.get(tableNumber);
									else if (dicoTableNumber_ObsFile.size() > 0) 
									{
										// 1 meme observations pour plusieurs tableaux.
										obs = dicoTableNumber_ObsFile.entrySet().iterator().next().getValue();
		    	        				dicoTableNumber_ObsFile.put(tableNumber, obs);

										// Memorisation des infos fixes pour une meme observation
										// (pour verification eventuelle plus tard, par exemple lors de la prise en compte d'une autre table)
										HashMap<String, String> dicoInituleVal = new HashMap<String, String>();
										String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
										dicoInituleVal.put(intituleDate, stepDate);
										dicoInituleVal.put(intituleTime, stepTime);
										dicoInituleVal.put(intituleTimeDuration, stepTimeDuration);
										dicoInituleVal.put(intituleDescription, obsDescription);
										dicoInituleVal.put(intituleObservationOperator, observationOperator);
										dicoInituleVal.put(intituleObservationScale, observationScale);
										dicoInituleVal.put(intituleMaterialName, materialName);
										dicoInituleVal.put(intituleMethodName, methodName);
										dicoInituleVal.put(intituleTableType, tableType);
										dicoCleObs_dicoInituleVal.put(cleObs, dicoInituleVal);
									}
	                				else
	                				{   
	                					// Creation de l'observation d'itineraire 
		    	        				ItineraryFile itineraryFile = dicoProcessName_dicoIndiceItin_ItineraryFile.get(processName).get(itineraryNumber);
		    	        				obs = new ObservationFile(itineraryFile);
										obs.setDate(stepDate);
    		        					obs.setHour(stepTime);
										obs.setDuree(stepTimeDuration);
										obs.setId(observationName);
										//setObservationOperator(projectFile, obs, observationOperator, nomFeuille, intituleObservationOperator, row, dicoIntitule_ICol);
										//if (!observationOperator.isEmpty())
										obs.setAgents(observationOperator);									
										obs.setScale(observationScale);
										obs.setDescription(obsDescription);
		    	        				dicoTableNumber_ObsFile.put(tableNumber, obs);

										// Memorisation des infos fixes pour une meme observation
										// (pour verification eventuelle plus tard, par exemple lors de la prise en compte d'une autre table)
										HashMap<String, String> dicoInituleVal = new HashMap<String, String>();
										String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
										dicoInituleVal.put(intituleDate, stepDate);
										dicoInituleVal.put(intituleTime, stepTime);
										dicoInituleVal.put(intituleTimeDuration, stepTimeDuration);
										dicoInituleVal.put(intituleDescription, obsDescription);
										dicoInituleVal.put(intituleObservationOperator, observationOperator);
										dicoInituleVal.put(intituleObservationScale, observationScale);
										dicoInituleVal.put(intituleMaterialName, materialName);
										dicoInituleVal.put(intituleMethodName, methodName);
										dicoInituleVal.put(intituleTableType, tableType);
										dicoCleObs_dicoInituleVal.put(cleObs, dicoInituleVal);
	
			    	        		    // Creation du lien avec le materiel
			    	        		    //if (material != null)
		    	        				if (dicoMatDObsName_MMP.size() > 0)
		    		        		    	for (Map.Entry<String, MaterialMethodPart> cplMatDObsName_MMP : dicoMatDObsName_MMP.entrySet())
				    	        		    {
		    		        		    		String matName = cplMatDObsName_MMP.getKey();
		    		        		    		MaterialMethodPart mmp = cplMatDObsName_MMP.getValue();
		    		        		    		
							    				// Memorisation pour quand des parametres de controles seront ajoutes
					            				if (!dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(processName))
					            					dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String,TreeMap<String,MaterialMethodLinkPart>>>());
					            				TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP = dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.get(processName);
					            				if (!dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(itineraryNumber))
					            					dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.put(itineraryNumber, new TreeMap<String, TreeMap<String,MaterialMethodLinkPart>>());
					            				TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoObservationName_dicoMaterialName_MatMetLP = dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.get(itineraryNumber);
					            				if (!dicoObservationName_dicoMaterialName_MatMetLP.containsKey(observationName))
					            					dicoObservationName_dicoMaterialName_MatMetLP.put(observationName, new TreeMap<String, MaterialMethodLinkPart>());
					            				TreeMap<String, MaterialMethodLinkPart> dicoMaterialName_MatMetLP = dicoObservationName_dicoMaterialName_MatMetLP.get(observationName);
					            				if (!dicoMaterialName_MatMetLP.containsKey(matName))
					            				{
								    				// Ajout de l'association entre ce materiel ou cette methode et cette observation
								    				MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(mmp);
								    				obs.addMaterialMethod(lPart);
								    				dicoMaterialName_MatMetLP.put(matName, lPart);
					            				}
				    	        		    }
			    	        		    
			    	        		    // Creation du lien avec la methode
			    	        		    //if (method != null)
		    	        				if (dicoMetDObsName_MMP.size() > 0)
		    		        		    	for (Map.Entry<String, MaterialMethodPart> cplMetDObsName_MMP : dicoMetDObsName_MMP.entrySet())
							    			{					    			
		    		        		    		String metName = cplMetDObsName_MMP.getKey();
		    		        		    		MaterialMethodPart mmp = cplMetDObsName_MMP.getValue();
			    		        		    		
							    				// Memorisation pour quand des parametres de controles seront ajoutes
					            				if (!dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.containsKey(processName))
					            					dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String,TreeMap<String,MaterialMethodLinkPart>>>());
					            				TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP = dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.get(processName);
					            				if (!dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.containsKey(itineraryNumber))
					            					dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.put(itineraryNumber, new TreeMap<String, TreeMap<String,MaterialMethodLinkPart>>());
					            				TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoObservationName_dicoMethodName_MatMetLP = dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.get(itineraryNumber);
					            				if (!dicoObservationName_dicoMethodName_MatMetLP.containsKey(observationName))
					            					dicoObservationName_dicoMethodName_MatMetLP.put(observationName, new TreeMap<String, MaterialMethodLinkPart>());
					            				TreeMap<String, MaterialMethodLinkPart> dicoMethodName_MatMetLP = dicoObservationName_dicoMethodName_MatMetLP.get(observationName);
					            				if (!dicoMethodName_MatMetLP.containsKey(metName))
					            				{
						    	        		    MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(mmp);
						    	        		    obs.addMaterialMethod(lPart);
						    	        		    dicoMethodName_MatMetLP.put(metName, lPart);
					            				}
							    			}
	                				}
	    	        			}
            				}
    	        		}

    	        		// Si l'etape est indiquee, verification qu'elle existe
    	        		else if (!verifier(dicoProcessName_dicoIndiceStep_StepFile.containsKey(processName),
	        					nomFeuille, i + 1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
    	        		else if (!verifier(dicoProcessName_dicoIndiceStep_StepFile.get(processName).containsKey(stepNumber),
	        					nomFeuille, i + 1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleStepNumber, nomFeuilleItinerary}, true, row, dicoIntitule_ICol));
            			else
    	        		{					
							// Prise en compte de la ligne uniquement si l'etape n'est pas une etape
							// clonee ayant deja ete traitee.
							if (!dicoProcessName_listIndiceStepTraites.containsKey(processName))
								dicoProcessName_listIndiceStepTraites.put(processName, new ArrayList<>());
							if (!dicoProcessName_listIndiceStepTraites.get(processName).contains(stepNumber))
							{
								// Quand le numero d'etape change, MaJ de la liste des etapes deja traitees
								// pour eviter de traiter plusieurs fois une meme etape clonee.
								if (!stepNumber.equals(stepNumber))
									dicoProcessName_listIndiceStepTraites.get(processName).add(stepNumber);

								// Recup de l'etape
								StepFile stepFile = dicoProcessName_dicoIndiceStep_StepFile
										.get(processName)
										.get(stepNumber);

								
								// -2- Etape indiquee, mais pas composition => observation d'etape
								if (compositionId.isEmpty())
								{
									// Creation ou recup de l'observation
									if (!dicoProcessName_dicoIndiceStep_dicoObseravtionName_dicoTableNumber_ObsFile.containsKey(processName))
										dicoProcessName_dicoIndiceStep_dicoObseravtionName_dicoTableNumber_ObsFile.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>());
									TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>> dicoIndiceStep_dicoObservationName_dicoTableNumber_ObsFile = dicoProcessName_dicoIndiceStep_dicoObseravtionName_dicoTableNumber_ObsFile.get(processName);
									if (!dicoIndiceStep_dicoObservationName_dicoTableNumber_ObsFile.containsKey(stepNumber))
										dicoIndiceStep_dicoObservationName_dicoTableNumber_ObsFile.put(stepNumber, new TreeMap<String, TreeMap<String, ObservationFile>>());
									TreeMap<String, TreeMap<String, ObservationFile>> dicoObservationName_dicoTableNumber_ObsFile = dicoIndiceStep_dicoObservationName_dicoTableNumber_ObsFile.get(stepNumber);
									if (!dicoObservationName_dicoTableNumber_ObsFile.containsKey(observationName))
										dicoObservationName_dicoTableNumber_ObsFile.put(observationName, new TreeMap<String, ObservationFile>());
									TreeMap<String, ObservationFile> dicoTableNumber_ObsFile = dicoObservationName_dicoTableNumber_ObsFile.get(observationName);
									if (dicoTableNumber_ObsFile.containsKey(tableNumber))
										obs = dicoTableNumber_ObsFile.get(tableNumber);
									else if (dicoTableNumber_ObsFile.size() > 0) 
									{
										// 1 meme observations pour plusieurs tableaux.
										obs = dicoTableNumber_ObsFile.entrySet().iterator().next().getValue();
										dicoTableNumber_ObsFile.put(tableNumber, obs);

										// Memorisation des infos fixes pour une meme observation
										// (pour verification eventuelle plus tard, par exemple lors de la prise en compte d'une autre table)
										HashMap<String, String> dicoInituleVal = new HashMap<String, String>();
										String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
										dicoInituleVal.put(intituleDate, stepDate);
										dicoInituleVal.put(intituleTime, stepTime);
										dicoInituleVal.put(intituleTimeDuration, stepTimeDuration);
										dicoInituleVal.put(intituleDescription, obsDescription);
										dicoInituleVal.put(intituleObservationOperator, observationOperator);
										dicoInituleVal.put(intituleObservationScale, observationScale);
										dicoInituleVal.put(intituleMaterialName, materialName);
										dicoInituleVal.put(intituleMethodName, methodName);
										dicoInituleVal.put(intituleTableType, tableType);
										dicoCleObs_dicoInituleVal.put(cleObs, dicoInituleVal);
									}
									else
									{
										// Creation de l'observation
										obs = new ObservationFile(stepFile);
										obs.setDate(stepDate);
    		        					obs.setHour(stepTime);
										obs.setDuree(stepTimeDuration);
										obs.setId(observationName);
										//setObservationOperator(projectFile, obs, observationOperator, nomFeuille, intituleObservationOperator, row, dicoIntitule_ICol);
										//if (!observationOperator.isEmpty())
											obs.setAgents(observationOperator);
										obs.setScale(observationScale);
										obs.setDescription(obsDescription);
										dicoTableNumber_ObsFile.put(tableNumber, obs);

										// Memorisation des infos fixes pour une meme observation
										// (pour verification eventuelle plus tard, par exemple lors de la prise en compte d'une autre table)
										HashMap<String, String> dicoInituleVal = new HashMap<String, String>();
										String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
										dicoInituleVal.put(intituleDate, stepDate);
										dicoInituleVal.put(intituleTime, stepTime);
										dicoInituleVal.put(intituleTimeDuration, stepTimeDuration);
										dicoInituleVal.put(intituleDescription, obsDescription);
										dicoInituleVal.put(intituleObservationOperator, observationOperator);
										dicoInituleVal.put(intituleObservationScale, observationScale);
										dicoInituleVal.put(intituleMaterialName, materialName);
										dicoInituleVal.put(intituleMethodName, methodName);
										dicoInituleVal.put(intituleTableType, tableType);
										dicoCleObs_dicoInituleVal.put(cleObs, dicoInituleVal);

										// Observation d'etape
										obs.setFoi("step");
										
										// Creation du lien avec le materiel
										//if (material != null)
										if (dicoMatDObsName_MMP.size() > 0)
											for (Map.Entry<String, MaterialMethodPart> cplMatDObsName_MMP : dicoMatDObsName_MMP.entrySet())
											{
												String matName = cplMatDObsName_MMP.getKey();
												MaterialMethodPart mmp = cplMatDObsName_MMP.getValue();
												
												// Memorisation pour quand des parametres de controles seront ajoutes
												if (!dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(processName))
													dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>());
												TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP = dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.get(processName);
												if (!dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(stepNumber))
													dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.put(stepNumber, new TreeMap<String, TreeMap<String,MaterialMethodLinkPart>>());
												TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoObservationName_dicoMaterialName_MatMetLP = dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.get(stepNumber);
												if (!dicoObservationName_dicoMaterialName_MatMetLP.containsKey(observationName))
													dicoObservationName_dicoMaterialName_MatMetLP.put(observationName, new TreeMap<String, MaterialMethodLinkPart>());
												TreeMap<String, MaterialMethodLinkPart> dicoMaterialName_MatMetLP = dicoObservationName_dicoMaterialName_MatMetLP.get(observationName);
												if (!dicoMaterialName_MatMetLP.containsKey(matName))
												{
													MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(mmp);
													obs.addMaterialMethod(lPart);
													dicoMaterialName_MatMetLP.put(matName, lPart);
												}
											}
										
										// Creation du lien avec la methode
										//if (method != null)
										if (dicoMetDObsName_MMP.size() > 0)
											for (Map.Entry<String, MaterialMethodPart> cplMetDObsName_MMP : dicoMetDObsName_MMP.entrySet())
											{
												String metName = cplMetDObsName_MMP.getKey();
												MaterialMethodPart mmp = cplMetDObsName_MMP.getValue();
												
												// Memorisation pour quand des parametres de controles seront ajoutes
												if (!dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.containsKey(processName))
													dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>());
												TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP = dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.get(processName);
												if (!dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.containsKey(stepNumber))
													dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.put(stepNumber, new TreeMap<String, TreeMap<String,MaterialMethodLinkPart>>());
												TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoObservationName_dicoMethodName_MatMetLP = dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.get(stepNumber);
												if (!dicoObservationName_dicoMethodName_MatMetLP.containsKey(observationName))
													dicoObservationName_dicoMethodName_MatMetLP.put(observationName, new TreeMap<String, MaterialMethodLinkPart>());
												TreeMap<String, MaterialMethodLinkPart> dicoMethodName_MatMetLP = dicoObservationName_dicoMethodName_MatMetLP.get(observationName);
												if (!dicoMethodName_MatMetLP.containsKey(metName))
												{
													MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(mmp);
													obs.addMaterialMethod(lPart);
													dicoMethodName_MatMetLP.put(metName, lPart);
												}
											}
									}
								}
								
								// -3- Etape et composition indiquees => observation de composition
								else
								{
									// Creation ou recup de l'observation
									if (!dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile.containsKey(processName))
										dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>>());
									TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>> dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile = dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile.get(processName);
									if (!dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.containsKey(stepNumber))
										dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.put(stepNumber, new TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>());
									TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>> dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile = dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.get(stepNumber);
									if (!dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.containsKey(compositionId))
										dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.put(compositionId, new TreeMap<String, TreeMap<String, ObservationFile>>());
									TreeMap<String, TreeMap<String, ObservationFile>> dicoObservationName_dicoTableNumber_ObsFile = dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.get(compositionId);
									if (!dicoObservationName_dicoTableNumber_ObsFile.containsKey(observationName))
										dicoObservationName_dicoTableNumber_ObsFile.put(observationName, new TreeMap<String, ObservationFile>());
									TreeMap<String, ObservationFile> dicoTableNumber_ObsFile = dicoObservationName_dicoTableNumber_ObsFile.get(observationName);
									if (dicoTableNumber_ObsFile.containsKey(tableNumber))
										obs = dicoTableNumber_ObsFile.get(tableNumber);
									else if (dicoTableNumber_ObsFile.size() > 0) 
									{
										// 1 meme observations pour plusieurs tableaux.
										obs = dicoTableNumber_ObsFile.entrySet().iterator().next().getValue();
										dicoTableNumber_ObsFile.put(tableNumber, obs);

										// Memorisation des infos fixes pour une meme observation
										// (pour verification eventuelle plus tard, par exemple lors de la prise en compte d'une autre table)
										HashMap<String, String> dicoInituleVal = new HashMap<String, String>();
										String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
										dicoInituleVal.put(intituleDate, stepDate);
										dicoInituleVal.put(intituleTime, stepTime);
										dicoInituleVal.put(intituleTimeDuration, stepTimeDuration);
										dicoInituleVal.put(intituleDescription, obsDescription);
										dicoInituleVal.put(intituleObservationOperator, observationOperator);
										dicoInituleVal.put(intituleObservationScale, observationScale);
										dicoInituleVal.put(intituleMaterialName, materialName);
										dicoInituleVal.put(intituleMethodName, methodName);
										dicoInituleVal.put(intituleTableType, tableType);
										dicoCleObs_dicoInituleVal.put(cleObs, dicoInituleVal);
									}
									else
									{
										if (!verifier(dicoProcessName_dicoCompositionId_Components.containsKey(processName),
											nomFeuille, i + 1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleComposition}, true, row, dicoIntitule_ICol));
										else if (!verifier(dicoProcessName_dicoCompositionId_Components.get(processName).containsKey(compositionId),
											nomFeuille, i + 1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleCompositionId, nomFeuilleComposition}, true, row, dicoIntitule_ICol));
										else
										{
											String nomListeComponents = dicoProcessName_dicoCompositionId_Components
													.get(processName)
													.get(compositionId)
													.toString();
											String compositionFileName = dicoComponentsName_CompositionFile
													.get(nomListeComponents)
													.getFileName();
										
											// Creation de l'observation
											obs = new ObservationFile(stepFile);
											obs.setDate(stepDate);
											obs.setHour(stepTime);
											obs.setDuree(stepTimeDuration);
											obs.setId(observationName);
											//setObservationOperator(projectFile, obs, observationOperator, nomFeuille, intituleObservationOperator, row, dicoIntitule_ICol);
											//if (!observationOperator.isEmpty())
												obs.setAgents(observationOperator);
											obs.setScale(observationScale);
											obs.setDescription(obsDescription);
											dicoTableNumber_ObsFile.put(tableNumber, obs);

											// Memorisation des infos fixes pour une meme observation
											// (pour verification eventuelle plus tard, par exemple lors de la prise en compte d'une autre table)
											HashMap<String, String> dicoInituleVal = new HashMap<String, String>();
											String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
											dicoInituleVal.put(intituleDate, stepDate);
											dicoInituleVal.put(intituleTime, stepTime);
											dicoInituleVal.put(intituleTimeDuration, stepTimeDuration);
											dicoInituleVal.put(intituleDescription, obsDescription);
											dicoInituleVal.put(intituleObservationOperator, observationOperator);
											dicoInituleVal.put(intituleObservationScale, observationScale);
											dicoInituleVal.put(intituleMaterialName, materialName);
											dicoInituleVal.put(intituleMethodName, methodName);
											dicoInituleVal.put(intituleTableType, tableType);
											dicoCleObs_dicoInituleVal.put(cleObs, dicoInituleVal);
	
											// Observation de composition
											obs.setFoi(compositionFileName);
											
											// Creation du lien avec le materiel
											//if (material != null)
											if (dicoMatDObsName_MMP.size() > 0)
												for (Map.Entry<String, MaterialMethodPart> cplMatDObsName_MMP : dicoMatDObsName_MMP.entrySet())
												{
													String matName = cplMatDObsName_MMP.getKey();
													MaterialMethodPart mmp = cplMatDObsName_MMP.getValue();
													
													// Memorisation pour quand des parametres de controles seront ajoutes
													if (!dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(processName))
														dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>>());
													TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>> dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP = dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(processName);
													if (!dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(stepNumber))
														dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.put(stepNumber, new TreeMap<String, TreeMap<String,TreeMap<String,MaterialMethodLinkPart>>>());
													TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP = dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(stepNumber);
													if (!dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(compositionId))
														dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.put(compositionId, new TreeMap<String, TreeMap<String,MaterialMethodLinkPart>>());
													TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoObservationName_dicoMaterialName_MatMetLP = dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(compositionId);
													if (!dicoObservationName_dicoMaterialName_MatMetLP.containsKey(observationName))
														dicoObservationName_dicoMaterialName_MatMetLP.put(observationName, new TreeMap<String, MaterialMethodLinkPart>());
													TreeMap<String, MaterialMethodLinkPart> dicoMaterialName_MatMetLP = dicoObservationName_dicoMaterialName_MatMetLP.get(observationName);
													if (!dicoMaterialName_MatMetLP.containsKey(matName))
													{
														MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(mmp);
														obs.addMaterialMethod(lPart);
														dicoMaterialName_MatMetLP.put(matName, lPart);
													}
												}
											
											// Creation du lien avec la methode
											//if (method != null)
											if (dicoMetDObsName_MMP.size() > 0)
												for (Map.Entry<String, MaterialMethodPart> cplMetDObsName_MMP : dicoMetDObsName_MMP.entrySet())
												{
													String metName = cplMetDObsName_MMP.getKey();
													MaterialMethodPart mmp = cplMetDObsName_MMP.getValue();
													
													// Memorisation pour quand des parametres de controles seront ajoutes
													if (!dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.containsKey(processName))
														dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>>());
													TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>>> dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP = dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(processName);
													if (!dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.containsKey(stepNumber))
														dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.put(stepNumber, new TreeMap<String, TreeMap<String,TreeMap<String,MaterialMethodLinkPart>>>());
													TreeMap<String, TreeMap<String, TreeMap<String, MaterialMethodLinkPart>>> dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP = dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(stepNumber);
													if (!dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.containsKey(compositionId))
														dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.put(compositionId, new TreeMap<String, TreeMap<String,MaterialMethodLinkPart>>());
													TreeMap<String, TreeMap<String, MaterialMethodLinkPart>> dicoObservationName_dicoMethodName_MatMetLP = dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(compositionId);
													if (!dicoObservationName_dicoMethodName_MatMetLP.containsKey(observationName))
														dicoObservationName_dicoMethodName_MatMetLP.put(observationName, new TreeMap<String, MaterialMethodLinkPart>());
													TreeMap<String, MaterialMethodLinkPart> dicoMethodName_MatMetLP = dicoObservationName_dicoMethodName_MatMetLP.get(observationName);
													if (!dicoMethodName_MatMetLP.containsKey(metName))
													{
														MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(mmp);
														obs.addMaterialMethod(lPart);
														dicoMethodName_MatMetLP.put(metName, lPart);
													}
												}
										}
									}
								}
							}
    	        		}    	        		
					
						// Si une observation a pu etre trouvee ou cree
						if (obs != null)
						{							
							// Verification que les infos censees etre communes le sont bien.
							// Rappel : Les lignes de donnees d'une meme observation dans Excel sont supposees se suivrent.
							String cleObs = processName + itineraryNumber + stepNumber + compositionId + observationName + tableNumber;
							if (dicoCleObs_dicoInituleVal.containsKey(cleObs))
							{
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleDate).equals(stepDate), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleDate, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleTime).equals(stepTime), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleTime, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleTimeDuration).equals(stepTimeDuration), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleTimeDuration, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleDescription).equals(obsDescription), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleDescription, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleMaterialName).equals(materialName), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleMaterialName, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleMethodName).equals(methodName), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleMethodName, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleObservationOperator).equals(observationOperator), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleObservationOperator, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleObservationScale).equals(observationScale), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleObservationScale, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
								verifier(dicoCleObs_dicoInituleVal.get(cleObs).get(intituleTableType).equals(tableType), nomFeuille, i + 1, IDMsg.XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV, new String[] {intituleTableType, intituleProcessName, intituleItineraryNumber, intituleStepNumber, intituleCompositionId, intituleObservationName}, true, row, dicoIntitule_ICol);
							}

							// Pour toutes les lignes, memorisation des attributs, valeurs, etc... 
							ArrayList<String> lstAttribut = getContenusCellules(row, i, intituleAttribut, dicoIntitule_LstICol);
							ArrayList<String> lstObjectOfInterest = getContenusCellules(row, i, intituleObjectOfInterest, dicoIntitule_LstICol);
							ArrayList<String> lstValue = getContenusCellules(row, i, intituleValue, dicoIntitule_LstICol);
							ArrayList<String> lstUnit = getContenusCellules(row, i, intituleUnit, dicoIntitule_LstICol);
							ArrayList<String> lstComment = getContenusCellules(row, i, intituleComment, dicoIntitule_LstICol);
														
							if (verifier(!processName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessName}, true, row, dicoIntitule_ICol))
							{
								if (verifier(dicoProcessName_dicoIndiceStep_StepFile.containsKey(processName),
										nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleItinerary}, true, row, dicoIntitule_ICol))
								{
									// Infos manquantes pas traumatisantes
									verifier(!observationName.isEmpty(), nomFeuille, i+1, IDMsg.CELL_IS_EMPTY, new String[] {intituleObservationName}, false, row, dicoIntitule_ICol);
			
									// A cette etape, memorisation de chaque quintuplet (attribut, valeur, objet, unites, commentaire)
									// sur la ligne et de sa validite
									for (j = 0; j < nbCols; j++)
									{
										String attribut = lstAttribut.get(j);
										tabAttributValide[j] = !attribut.isEmpty();
										lstLstAttribut.get(j).add(attribut);
										lstLstObjectOfInterest.get(j).add(lstObjectOfInterest.get(j));
										lstLstValue.get(j).add(lstValue.get(j));
										lstLstUnit.get(j).add(lstUnit.get(j));
										lstLstComment.get(j).add(lstComment.get(j));
									}
								}	        				
							}
						}
					}
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Lecture de la feuille des parametres de controle des observations
	 * dans le fichier Excel passe en parametre, et creation de tous les objets
	 * PO2 necessaires associes a l'objet ProjectFile passe en parametre
	 * @param workbook : Le classeur Excel d'ou les donnees sont lues
	 * @param projectFile : Le projet auquel les donnees sont rattachees
	 * @throws Exception
	 */
	private static void lireFeuilleObservationControlParams(Workbook workbook, ProjectFile projectFile) throws Exception {

		String nomFeuille = nomFeuilleObservationControlParameters;

		// La gestion des tableaux complexes ne peut se faire qu'apres avoir tout lu les donnees
		// de tout le tableau.
		String processName;
		String itineraryNumber;
		String stepNumber;
		String compositionId;
		String observationName;
		String materialName;
		String methodName;
		
		String intituleProcessName = "Process name";
		String intituleItineraryNumber = "Itinerary number";
		String intituleStepNumber = "Step number";
		String intituleCompositionId = "Composition id";
		String intituleObservationName = "Observation name";
		String intituleMaterialName = "Material name";
		String intituleMethodName = "Method name";
		String intituleAttribut = "Attribute";
		String intituleObjectOfInterest = "Object of interest";
		String intituleValue = "Value";
		String intituleUnit = "Unit";
		String intituleComment = "Comment";
		
		ArrayList<String> lstIntitules = new ArrayList<String> ();
		lstIntitules.add(intituleProcessName);
		lstIntitules.add(intituleItineraryNumber);
		lstIntitules.add(intituleStepNumber);
		lstIntitules.add(intituleCompositionId);
		lstIntitules.add(intituleObservationName);
		lstIntitules.add(intituleMaterialName);
		lstIntitules.add(intituleMethodName);
		lstIntitules.add(intituleAttribut);
		lstIntitules.add(intituleObjectOfInterest);
		lstIntitules.add(intituleValue);
		lstIntitules.add(intituleUnit);
		lstIntitules.add(intituleComment);
		
		TreeMap<String, Integer> dicoIntitule_ICol = new TreeMap<String, Integer>();
		
		int i;
		
		try
		{
	        Sheet sheet = workbook.getSheet(nomFeuille);
	        if (verifier(sheet != null, nomFeuille, -1, IDMsg.PAS_DE_FEUILLE, new String[] {nomFeuille}, false))
	        {	        
	        	// Verification des intitules sur la 1ere ligne
		        int nbLignes = sheet.getLastRowNum();
		        boolean ok = verifierTitresSImples(sheet, nomFeuille, lstIntitules, dicoIntitule_ICol, true);

		    	// Lignes suivantes : Description des itineraires.
		        for (i = 1; ok && i <= nbLignes + 1; i++)
		        {
			    	Row row = sheet.getRow(i);		    	
			    	
	        		// Lecture des valeurs cle permettant de savoir si le tableau de donnees a change
	        		processName = getContenuCellule(row, i, intituleProcessName, dicoIntitule_ICol);
	        		itineraryNumber = getContenuCellule(row, i, intituleItineraryNumber, dicoIntitule_ICol);
	        		stepNumber = getContenuCellule(row, i, intituleStepNumber, dicoIntitule_ICol);
	        		compositionId = decoderCompositionId(getContenuCellule(row, i, intituleCompositionId, dicoIntitule_ICol));
	        		observationName = getContenuCellule(row, i, intituleObservationName, dicoIntitule_ICol);
	        		materialName = getContenuCellule(row, i, intituleMaterialName, dicoIntitule_ICol);
	        		methodName = getContenuCellule(row, i, intituleMethodName, dicoIntitule_ICol);
			    	
        			MaterialMethodLinkPart lPart = null;

        			// Verifications
	        		boolean cont = true;
	        		if (processName.isEmpty() && /*itineraryNumber.isEmpty() &&*/ stepNumber.isEmpty() && compositionId.isEmpty() && observationName.isEmpty() && materialName.isEmpty() && methodName.isEmpty())
	        			cont = false;
	        		else
	        		{
	        			if (!verifier(!processName.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleProcessName}, true, row, dicoIntitule_ICol))			cont = false;	        		
	        			//if (!verifier(!itineraryNumber.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleItineraryNumber}))	cont = false;	        		
	        			if (!verifier(!materialName.isEmpty() || !methodName.isEmpty(), nomFeuille, i + 1, IDMsg.AU_MOINS_1MAT_OU_1MET, new String[] {intituleMaterialName, intituleMethodName}, true, row, dicoIntitule_ICol)) cont = false;
	        			if (!verifier(materialName.isEmpty() || methodName.isEmpty(), nomFeuille, i + 1, IDMsg.AU_PLUS_1MAT_OU_1MET, new String[] {intituleMaterialName, intituleMethodName}, true, row, dicoIntitule_ICol))	cont = false;
	        		}

	        		// Creation de l'observation : 3 cas possibles
	        		// -1- Etape non indiquee => observation d'itineraire.
        			if (!cont);        			
        			else if (stepNumber.isEmpty())
	        		{
        				// Verifications... Numero d'itineraire necessaire
        				if (!verifier(!itineraryNumber.isEmpty(), nomFeuille, i + 1, IDMsg.CELL_IS_EMPTY, new String[] {intituleItineraryNumber}, true, row, dicoIntitule_ICol))	cont = false;
        				if (cont)
        				{
		        			// Verifications... Problemes sans consequence
		        			verifier(compositionId.isEmpty(), nomFeuille, i+1, IDMsg.XXX_IMPLIQUE_YYY, new String[] {intituleStepNumber, intituleCompositionId}, false, row, dicoIntitule_ICol);
		        			
		        			// Recup de l'association entre l'observation et la materiel
	        				if (!materialName.isEmpty())
	        				{
	            				if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	            				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).containsKey(itineraryNumber), nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleItineraryNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	            				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(itineraryNumber).containsKey(observationName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleItineraryNumber, intituleObservationName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	            				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(itineraryNumber).get(observationName).containsKey(materialName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleItineraryNumber, intituleObservationName, intituleMaterialName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	            				else
				    				lPart = dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMaterialName_MatMetLP
				    						.get(processName)
				    						.get(itineraryNumber)
				    						.get(observationName)
				    						.get(materialName);
	        				}
	        				else if (methodName.isEmpty());
	        				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	        				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.get(processName).containsKey(itineraryNumber), nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleItineraryNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	        				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(itineraryNumber).containsKey(observationName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleItineraryNumber, intituleObservationName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	        				else if (!verifier(dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(itineraryNumber).get(observationName).containsKey(methodName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleItineraryNumber, intituleObservationName, intituleMethodName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	        				else
			    				lPart = dicoProcessName_dicoIndiceItin_dicoObservationName_dicoMethodName_MatMetLP
			    						.get(processName)
			    						.get(itineraryNumber)
			    						.get(observationName)
			    						.get(methodName);
        				}
	        		}
	        		
	        		// Si l'etape est indiquee, verification qu'elle existe
	        		else if (!verifier(dicoProcessName_dicoIndiceStep_StepFile.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
	        		else if (!verifier(dicoProcessName_dicoIndiceStep_StepFile.get(processName).containsKey(stepNumber), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleItineraryNumber, intituleStepNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
        			else
        			{
        				// -2- Si la composition n'est pas indiquee, il s'agit d'une observation d'etape
            			if (compositionId.isEmpty())
            			{
    	        			// Recup de l'association entre l'observation et la materiel
            				if (!materialName.isEmpty())
            				{
                				if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
                				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).containsKey(stepNumber), nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleStepNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
                				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(stepNumber).containsKey(observationName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleObservationName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
                				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(stepNumber).get(observationName).containsKey(materialName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleStepNumber, intituleObservationName, intituleMaterialName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
                				else		                				
    			    				lPart = dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMaterialName_MatMetLP
    			    						.get(processName)
    			    						.get(stepNumber)
    			    						.get(observationName)
    			    						.get(materialName);
            				}
            				else if (methodName.isEmpty());
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.get(processName).containsKey(stepNumber), nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleStepNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(stepNumber).containsKey(observationName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleObservationName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(stepNumber).get(observationName).containsKey(materialName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleStepNumber, intituleObservationName, intituleMaterialName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else		                				
			    				lPart = dicoProcessName_dicoIndiceStep_dicoObservationName_dicoMethodName_MatMetLP
			    						.get(processName)
			    						.get(stepNumber)
			    						.get(observationName)
			    						.get(materialName);
            			}
            			
        				// -3- Si la composition est indiquee, il s'agit d'une observation de composition
            			else if (!materialName.isEmpty())
            			{
            				// Recup de l'association entre l'observation et la materiel
            				if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).containsKey(stepNumber), nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleStepNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(stepNumber).containsKey(compositionId), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleCompositionId, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(stepNumber).get(compositionId).containsKey(observationName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleStepNumber, intituleCompositionId, intituleObservationName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP.get(processName).get(stepNumber).get(compositionId).get(observationName).containsKey(materialName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_UUU_INTROUVABLE_DANS_VVV, new String[] {intituleProcessName, intituleStepNumber, intituleCompositionId, intituleObservationName, intituleMaterialName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else
			    				lPart = dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMaterialName_MatMetLP
			    						.get(processName)
			    						.get(stepNumber)
			    						.get(compositionId)
			    						.get(observationName)
			    						.get(materialName);
            			}
            			else if (!methodName.isEmpty())
            			{
    	        			// Recup de l'association entre l'observation et la materiel
            				if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.containsKey(processName), nomFeuille, i+1, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(processName).containsKey(stepNumber), nomFeuille, i+1, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {intituleProcessName, intituleStepNumber, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(stepNumber).containsKey(compositionId), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT, new String[] {intituleProcessName, intituleStepNumber, intituleCompositionId, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(stepNumber).get(compositionId).containsKey(observationName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleStepNumber, intituleCompositionId, intituleObservationName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else if (!verifier(dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP.get(processName).get(stepNumber).get(compositionId).get(observationName).containsKey(methodName), nomFeuille, i+1, IDMsg.XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU, new String[] {intituleProcessName, intituleStepNumber, intituleCompositionId, intituleObservationName, intituleMethodName, nomFeuilleObservations}, true, row, dicoIntitule_ICol));
            				else
			    				lPart = dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoMethodName_MatMetLP
			    						.get(processName)
			    						.get(stepNumber)
			    						.get(compositionId)
			    						.get(observationName)
			    						.get(methodName);
            			}
    				}
	        		
	        		// Ajout des details sur l'observation
	        		if (lPart != null)
	        		{
		        		String attribut = getContenuCellule(row, i, intituleAttribut, dicoIntitule_ICol);
		        		String objectOfInterest = getContenuCellule(row, i, intituleObjectOfInterest, dicoIntitule_ICol);
		        		String value = getContenuCellule(row, i, intituleValue, dicoIntitule_ICol);
		        		String unit = getContenuCellule(row, i, intituleUnit, dicoIntitule_ICol);
		        		String comment = getContenuCellule(row, i, intituleComment, dicoIntitule_ICol);
	        			
    					lPart.addCharacteristic(attribut, objectOfInterest, value, unit, comment);
	        		}
		        }
	        }
	    							         	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TechnicalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Pour chaque id de domposition decode a partir du parametre compositionsIds,
	 * creation d'un objet CompositionFile ou recuperation s'il existe deja,
	 * et association a l'etape passee en parametre. 
	 * @param processName : Le nom du process concerne
	 * @param intituleProcessName : (pour les messages d'erreur) l'intitule correspondant dans la feuille Excel
	 * @param intituleCompositionId : Intitule pour la composition dans la feuille Excel
	 * @param nomFeuille : (pour les messages d'erreur) le nom de la feuille Excel
	 * @param stepNumber : Le numero de l'etape concernee
	 * @param compositionsIds : L'ID ou les ID des compositions sous la forme ID1::ID2::...::IDN 
	 * @param tableNumber : 
	 * @param stepFile : L'objet auquel devra etre associee la composition
	 * @param i : (pour les messages d'erreur) le n° de ligne concernee dans le fichier Excel
	 * @param sens : Drapeau indiquant si la composition est d'entree ou de sortie
	 * @param row : Ligne correspondante à i dans la feuille Excel
	 * @param dicoIntitule_ICol : Dico des intitules sur la 1ere ligne des feuilles Excel
	 */
	private static void ajouterComposition (
			String processName, 
			String intituleProcessName, 
			String intituleCompositionId,
			String nomFeuille, 
			//String itineraryNumber,
			String stepNumber,
			String compositionsIds,
			StepFile stepFile,
			int i,
			boolean sens,
			Row row,
			TreeMap<String, Integer> dicoIntitule_ICol) {

		if (verifier(dicoProcessName_dicoCompositionId_Components.containsKey(processName),
				nomFeuille, i, IDMsg.XXX_INTROUVABLE_DANS_YYY, new String[] {intituleProcessName, nomFeuilleProjectProcess}, true, row, dicoIntitule_ICol))
    
			if (!compositionsIds.isEmpty())
    		{
    			// Decodage du nom sous la forme nom1::nom2::nom3...
    			Pattern pattern = Pattern.compile("::");
	        	String[] lstIdCompo = compositionsIds.isEmpty()
	        			? null
	        			: pattern.split(compositionsIds);
	        	
	        	// Pour chaque nom de composition
        		if (lstIdCompo != null)

        			for (String compositionId : lstIdCompo)
		    		
        				if (!compositionId.isEmpty()) 

							// Remarque : ajout d'un element dans le tableau passe en parametre
							// juste pour pouvoir mettre la cellule correspondante en surbrillance
		    				if (verifier(dicoProcessName_dicoCompositionId_Components.get(processName).containsKey(compositionId),
		    	    				nomFeuille, i, IDMsg.XXX_YYY_INTROUVABLE_DANS_ZZZ, new String[] {processName, compositionId, nomFeuilleComposition, intituleCompositionId}, true, row, dicoIntitule_ICol))
		    				{
	        	
		    					ComponentsDeComposition components = dicoProcessName_dicoCompositionId_Components
	        						.get(processName)
	        						.get(compositionId);
		    					if (components != null)
		    					{
			    				
		    						String nomListeComponents = components.toString();
			    					CompositionFile compositionFile = null;
		    						if (!dicoComponentsName_CompositionFile.containsKey(nomListeComponents))
		    						{
		    							// A ce niveau, pas de probleme avec le dico dicoProcessName_dicoCompositionId_Components
		    							// donc pas de probleme non plus avec dicoProcessName_dicoComposition_CompositionType
		    							// et dicoProcessName_dicoCompositionId_CompositionName
		    							String compositionType = dicoProcessName_dicoCompositionId_CompositionType
			    							.get(processName)
			    							.get(compositionId);
		    							String compositionName = dicoProcessName_dicoCompositionId_CompositionName
			    							.get(processName)
			    							.get(compositionId);
			        		
				    		        	// Creation d'une nouvelle composition, avec sa liste d'ingredients
				    					compositionFile = new CompositionFile(stepFile, sens);
				    					compositionFile.setCompositionID(compositionName);
				    					compositionFile.setCompositionType(compositionType);
				    					dicoComponentsName_CompositionFile.put(nomListeComponents, compositionFile);
										for (ComponentDeComposition component : components.getLstComponents())
										{
											// Depuis le ..., la liste d'ingredients correspond
											// a autant d'observations que d'ingredients.
					    					//compositionFile.addComponent(component.getAttribute()
				    						//	, component.getType()
				    						//	, component.getValue()
				    						//	, component.getUnit()
				    						//	, component.getComment());
											ajouterObservationALaPlaceDeIngredientDeComposition(
												processName, 
												stepNumber,
												compositionId,
												compositionName,
												"1",
												stepFile,
												component);

										}
		    						}
		    						else
		    						{
				    			    
										// Association entre l'etape et la composition. 
				    					compositionFile = dicoComponentsName_CompositionFile.get(nomListeComponents);
										if (compositionFile != null)
											stepFile.getCompositionFile().put(compositionFile, sens);
		    						}			    				
		    					}
		    				}    		
    		}
	}

	/**
	 * Cette methode a ete ajoutee pour la compatibilite avec les fichiers 
	 * a l'ancien format (ceux dans lesquels
	 * des ingredients peuvent etre decrits dans la feuille Composition).
	 * La methode remplace les ingredients des compositions par des observations
	 * comme indique ici :
	 * https://forgemia.inra.fr/PO2-Tools/po2manager/-/issues/44
	 * @param processName : Le nom du process concerne
	 * @param stepNumber : Le numero de l'etape concernee
	 * @param compositionId
	 * @param compositionName
	 * @param tableNumber
	 * @param stepFile
	 * @param component
	 */
	private static void ajouterObservationALaPlaceDeIngredientDeComposition(
			String processName, 
			String stepNumber,
			String compositionId,
			String compositionName,
			String tableNumber,
			StepFile stepFile,
			ComponentDeComposition component)	{

		// Autant d'observations sont construites que d'ingredients.
		ObservationFile obs = null;
		String attribut = component.getAttribute();
		String objectOfInterest = component.getType();
		String value = component.getValue();
		String unit = component.getUnit();
		String comment = component.getComment();
		String observationName = "Composition in " + objectOfInterest + " for " + compositionName;

		// Creation ou recup de l'observation
		if (!dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile.containsKey(processName))
			dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile.put(processName, new TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>>());
		TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>> dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile = dicoProcessName_dicoIndiceStep_dicoCompositionId_dicoObservationName_dicoTableNumber_ObsFile.get(processName);
		if (!dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.containsKey(stepNumber))
			dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.put(stepNumber, new TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>>());
		TreeMap<String, TreeMap<String, TreeMap<String, ObservationFile>>> dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile = dicoIndiceStep_dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.get(stepNumber);
		if (!dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.containsKey(compositionId))
			dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.put(compositionId, new TreeMap<String, TreeMap<String, ObservationFile>>());
		TreeMap<String, TreeMap<String, ObservationFile>> dicoObservationName_dicoTableNumber_ObsFile = dicoCompositionId_dicoObseravtionName_dicoTableNumber_ObsFile.get(compositionId);
		if (dicoObservationName_dicoTableNumber_ObsFile.containsKey(tableNumber))
			dicoObservationName_dicoTableNumber_ObsFile.put(tableNumber, new TreeMap<String, ObservationFile>());
		TreeMap<String, ObservationFile> dicoTableNumber_ObsFile = dicoObservationName_dicoTableNumber_ObsFile.get(tableNumber);
		if (dicoTableNumber_ObsFile.containsKey(observationName))
			obs = dicoTableNumber_ObsFile.get(observationName);
		else
		{
			// Plus de message suite aux verifications
			if (dicoProcessName_dicoCompositionId_Components.containsKey(processName))
				if (dicoProcessName_dicoCompositionId_Components.get(processName).containsKey(compositionId))
				{
					String nomListeComponents = dicoProcessName_dicoCompositionId_Components
							.get(processName)
							.get(compositionId)
							.toString();
					String compositionFileName = dicoComponentsName_CompositionFile
							.get(nomListeComponents)
							.getFileName();
				
					// Creation de l'observation
					obs = new ObservationFile(stepFile);
					dicoTableNumber_ObsFile.put(observationName, obs);

					// Observation de composition
					obs.setFoi(compositionFileName);
					
					// Pas de materiel associe.
					
					// Pas de methode associee.
				}
		}

		// Ajout des details sur l'observation
		if (obs != null)
		{
			// Pas d'info sur la date, l'heure, la duree, juste le nom
			obs.setId(observationName);

			// Creation d'un tableau 1D
			TablePart tp = obs.createObsData(true);
			SimpleTable st = (SimpleTable)tp.getTable();
			
			// 1 seule ligne dans le tableau
			st.addObservationLine(attribut, objectOfInterest, value, unit, comment);
		}
	}

	/**
	 * Verification si tous les intitules de la liste passee en parametre
	 * sont uniques et seuls presents sur la 1ere ligne de la feuille.
	 * En cas de probleme, log d'un message d'erreur.
	 * Retourne true, sauf en cas de probleme grave.
	 * Met egalement a jour le dico des indices des colonnes en fonction des intitules 
	 * @param sheet : La feuille d'interet
	 * @param nomFeuille : Le nom de la feuille d'interet (pour les messages d'erreur)
	 * @param lstIntitules : La liste des intitules attendus.
	 * @param dicoIntitule_ICol : Le dico des indices des colonnes en fonction des intitules.
	 * @return : TRUE en cas de message bloquant
	 * @throws Exception
	 */
	private static boolean verifierTitresSImples(
			Sheet sheet, 
			String nomFeuille, 
			ArrayList<String> lstIntitules, 
			TreeMap<String, Integer> dicoIntitule_ICol,
			boolean avecMessageAlerte) throws Exception {

		int j;
		int nbCols;
		boolean cont = true;
        
		if (sheet != null)
		{
	        // 1ere ligne = intitules
        	Row row = sheet.getLastRowNum() >= 0 ? sheet.getRow(0) : null;
        	nbCols = row != null ? row.getLastCellNum() : 0;
        	for (j = 0; j < nbCols; j++)
        	{
        		String buf = ImportDataAtWeb.lireCellule(row, 0, j);
        		
        		// Cas particulier des cellules vides
        		if (buf.isEmpty());

				else if (avecMessageAlerte)
				{        		
					// Intitule inattendu : pas grave...
					if (!verifier(lstIntitules.indexOf(buf) >= 0, nomFeuille, -1, IDMsg.INTITULE_INATTENDU, new String[] {buf}, false, row, dicoIntitule_ICol));
					
					// Intitule en double : pas grave, on ne tient compte que du 1er.
					else if (!verifier(!dicoIntitule_ICol.containsKey(buf), nomFeuille, -1, IDMsg.INTITULE_DUPLIQUE, new String[] {buf}, false, row, dicoIntitule_ICol));
					
					// Memorisation de l'indice de la colonne 
					else
						dicoIntitule_ICol.put(buf, j);
				}

				// Option sans message d'alerte
        		else if (lstIntitules.indexOf(buf) >= 0 && !dicoIntitule_ICol.containsKey(buf))
					dicoIntitule_ICol.put(buf, j);
        	}
        	
        	// Verification que tous les intitules attendus ont bien ete lus
        	// Si certains intitules sont manquants, on considere que c'est grave.
			if (avecMessageAlerte)
				for (String intitule : lstIntitules)
					if (!verifier(dicoIntitule_ICol.containsKey(intitule), nomFeuille, -1, IDMsg.INTITULE_MANQUANT, new String[] {intitule}, true, row, dicoIntitule_ICol))
						cont = false;
        	
        }
		
		return cont;
	}

	/**
	 * Verification de la coherence des intitules sur la 1ere ligne de la feuille
	 * Excel passee en parametre : Affichage de messages d'erreur dans le log si
	 * des intitules sont manquants, inattendus, ou en double alors qu'ils ne devraient pas.
	 * En meme temps, construction d'1 dictionnaire des indices des colonnes en fonction
	 * des intitules simples, et d'1 dictionnaire de la liste des indices des colonnes
	 * en fonction des intitules lorsque plusieurs colonnes de meme intitule sont attendues.
	 * @param sheet : La feuille verifiee
	 * @param nomFeuille : Le nom de la feuille verifiee (pour les messages d'erreur)
	 * @param lstIntitules : La liste des intitules attendus.
	 * @param lstIntitulesMultiples : La liste des intitules attendus pouvant apparaitre sur plusieurs colonnes.
	 * @param dicoIntitule_ICol : Le dico mis a jour des indices des colonnes en fonction des intitules.
	 * @param dicoIntitule_LstICol : Le dico mis a jour de la liste des indices des colonnes en fonction des intitules. 
	 * @return : Nombre de fois ou des intitules pouvant apparaitre dans plusieurs colonnes
	 * apparaissent effectivement sur la 1ere ligne de la feuille Excel. 
	 * @throws Exception
	 */
	private static int verifierTitresMultiples(
			Sheet sheet, 
			String nomFeuille, 
			ArrayList<String> lstIntitules, 
			ArrayList<String> lstIntitulesMultiples,
			TreeMap<String, Integer> dicoIntitule_ICol,
			TreeMap<String, ArrayList<Integer>> dicoIntitule_LstICol) throws Exception {

		int j;
		int nbCols = 0;
		boolean cont = true;
        
		if (sheet != null)
		{
	        // 1ere ligne = intitules
        	Row row = sheet.getLastRowNum() > 0 ? sheet.getRow(0) : null;
        	nbCols = row != null ? row.getLastCellNum() : 0;
        	for (j = 0; j < nbCols; j++)
        	{
        		String buf = ImportDataAtWeb.lireCellule(row, 0, j);
        		
        		// Cellules vides non prises en compte. 
        		if (buf.isEmpty());
        		
        		// Intitule inattendu : pas grave...
        		else if (!verifier(lstIntitules.indexOf(buf) >= 0, nomFeuille, -1, IDMsg.INTITULE_INATTENDU, new String[] {buf}, false, row, dicoIntitule_ICol));
        		
        		// Intitule en double : pas grave, on ne tient compte que du 1er.
        		else if (!verifier(!dicoIntitule_ICol.containsKey(buf) || lstIntitulesMultiples.indexOf(buf) >= 0, nomFeuille, -1, IDMsg.INTITULE_DUPLIQUE, new String[] {buf}, false, row, dicoIntitule_ICol));
        		
        		// Cas des intitules ne devant apparaitre qu'1 seule fois
        		else if (lstIntitulesMultiples.indexOf(buf) < 0)
        			dicoIntitule_ICol.put(buf, j);
        		
        		// Cas des intiules pouvant apparaitre plusieurs fois mais pas encore "vu"
        		else if (!dicoIntitule_LstICol.containsKey(buf))
        		{
        			ArrayList<Integer> lst = new ArrayList<Integer>();
        			lst.add(j);
        			dicoIntitule_LstICol.put(buf, lst);
        		}

        		// Cas des intiules pouvant apparaitre plusieurs fois et deja "vus"
        		else
        		{
        			ArrayList<Integer> lst = dicoIntitule_LstICol.get(buf);
        			lst.add(j);
        		}
        	}
        	
        	// Verification que tous les intitules attendus ont bien ete lus
        	// Si certains intitules sont manquants, on considere que c'est grave.
        	for (String intitule : lstIntitules)
        		if (!verifier(dicoIntitule_ICol.containsKey(intitule) || dicoIntitule_LstICol.containsKey(intitule), nomFeuille, -1, IDMsg.INTITULE_MANQUANT, new String[] {intitule}, true, row, dicoIntitule_ICol))
        			cont = false;
        	
        	// Verification que les intitules "multiples" sont presents au moins 1 fois et en meme quantite
        	boolean prem = true;
        	for (String intitule : lstIntitulesMultiples)
        	{
        		if (!verifier(dicoIntitule_LstICol.containsKey(intitule), nomFeuille, -1, IDMsg.INTITULE_MANQUANT, new String[] {intitule}, true, row, dicoIntitule_ICol))
        			cont = false;
        		else if (prem)
        		{
        			nbCols = dicoIntitule_LstICol.get(intitule).size();
        			if (!verifier(nbCols > 0, nomFeuille, -1, IDMsg.INTITULE_MANQUANT, new String[] {intitule}, true, row, dicoIntitule_ICol))
	        			cont = false;
        			prem = false;
        		}
        		else if (!verifier(nbCols == dicoIntitule_LstICol.get(intitule).size(), nomFeuille, -1, IDMsg.INTITULE_XXX_EN_NB_INCOHERENT, new String[] {intitule}, true, row, dicoIntitule_ICol))
        			cont = false;
        	}
		}
		
		if (!cont)
			nbCols = 0;
		
		return nbCols;
	}

	/**
	 * Affichage d'un message dans le log en fonction du bouleen passe en parametre.
	 * Le message indique est construit a l'aide de tous les autres parametres.
	 * @param testOK : Drapeau indiquant si un message doit etre genere
	 * @param nomFeuille : Nom de la feuille Excel auquel l'erreur est rattachee
	 * @param iLigne : Numero de ligne sur la feuille Excel auquel l'erreur est rattachee
	 * @param idMsg : ID du message a afficher
	 * @param tabIntitule : Tableau de tous les messages
	 * @param grave : 0 = avertissement, 1 = erreur
	 * @return : Le drapeau passe en parametre
	 */
	private static boolean verifier(
			boolean testOK,
			String nomFeuille, 
			int iLigne, 
			IDMsg idMsg, 
			String[] tabIntitule,
			boolean grave) {
		
		// Des choses a faire uniquement en cas de probleme.
		if (!testOK)
		{
			
			// Juste au cas ou...
			if (nomFeuille.isBlank()) nomFeuille = "";
			
			int nb = tabIntitule != null ? tabIntitule.length : 0;
			
			String intitule1 = nb > 0 ? tabIntitule[0] : ""; 
			String intitule2 = nb > 1 ? tabIntitule[1] : ""; 
			String intitule3 = nb > 2 ? tabIntitule[2] : ""; 
			String intitule4 = nb > 3 ? tabIntitule[3] : ""; 
			String intitule5 = nb > 4 ? tabIntitule[4] : ""; 
			String intitule6 = nb > 5 ? tabIntitule[5] : ""; 
			String intitule7 = nb > 6 ? tabIntitule[6] : ""; 
			
			String msg = "Feuille '" + nomFeuille + "'";
			if (iLigne >= 0)
				msg += ", ligne " + iLigne;
			
			switch (idMsg) 
			{
				case PAS_DE_COMPONENT : 
				case PAS_DE_FEUILLE :
				case ANCIEN_FORMAT :
				case INTITULE_INATTENDU :
				case INTITULE_DUPLIQUE :
				case INTITULE_MANQUANT :
				case CELL_IS_EMPTY :
				case CELL_IS_EMPTY_USING_DEFAULT :
				case AGENT_DEJA_INDIQUE :
				case PAS_D_AGENT_TROUVE :
				case INTITULE_XXX_EN_NB_INCOHERENT :
				case CELLULES_XXX_DIFFERENTES :
				case XXX_INCOHERENT :
				case COMPOSITION_DID_XXX_DEJA_EXISTANTE :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1);
					break;
					
				case CARACTERES_INTERDITS :
				case AU_MOINS_1MAT_OU_1MET :
				case AU_PLUS_1MAT_OU_1MET :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2);
					break;
				
				case XXX_INCOHERENT_POUR_YYY :
				case XXX_INTROUVABLE_DANS_YYY :
				case XXX_IMPLIQUE_YYY :

					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2);
					break;
				
				case XXX_INCOHERENT_POUR_YYY_ZZZ :
				case XXX_YYY_INTROUVABLE_DANS_ZZZ :
				case CONTENU_XXX_INATTENDU_YYY_OU_ZZZ_ATTENDU :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2)
							.replace("ZZZ", intitule3);
					break;
				
				case XXX_INCOHERENT_POUR_YYY_ZZZ_TTT :
				case XXX_YYY_ZZZ_INTROUVABLE_DANS_TTT :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2)
							.replace("ZZZ", intitule3)
							.replace("TTT", intitule4);
					break;
					
				case XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU :
				case XXX_YYY_ZZZ_TTT_INTROUVABLE_DANS_UUU :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2)
							.replace("ZZZ", intitule3)
							.replace("TTT", intitule4)
							.replace("UUU", intitule5);
					break;	

				case XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV :
				case XXX_YYY_ZZZ_TTT_UUU_INTROUVABLE_DANS_VVV :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2)
							.replace("ZZZ", intitule3)
							.replace("TTT", intitule4)
							.replace("UUU", intitule5)
							.replace("VVV", intitule6);
					break;	

				case XXX_INCOHERENT_POUR_YYY_ZZZ_TTT_UUU_VVV_WWW :
				case XXX_YYY_ZZZ_TTT_UUU_VVV_INTROUVABLE_DANS_WWW :
					msg += ", " + dicoLangue_dicoIDMsg_Msg.get(langue).get(idMsg)
							.replace("XXX", intitule1)
							.replace("YYY", intitule2)
							.replace("ZZZ", intitule3)
							.replace("TTT", intitule4)
							.replace("UUU", intitule5)
							.replace("VVV", intitule6)
							.replace("WWW", intitule7);
					break;
									
			}
			if (grave)
				report.addError(msg);
			else
				report.addWarning(msg);
			dicoIDMsg_Nb.put(idMsg, dicoIDMsg_Nb.get(idMsg) + 1);
			log.info(msg);
			
		}
		
		return testOK;
	}

	/**
	 * Affichage d'un message dans le log en fonction du bouleen passe en parametre.
	 * Le message indique est construit a l'aide de tous les autres parametres.
	 * Le classeur ouvert dont la ligne est passee en parametre est modifie
	 * de telle sorte que le fond des cellules qui posent probleme soit surligne.
	 * @param testOK : Drapeau indiquant si un message doit etre genere
	 * @param nomFeuille : Nom de la feuille Excel auquel l'erreur est rattachee
	 * @param iLigne : Numero de ligne sur la feuille Excel auquel l'erreur est rattachee
	 * @param idMsg : ID du message a afficher
	 * @param tabIntitule : Tableau de tous les messages
	 * @param grave : 0 = avertissement, 1 = erreur
	 * @param row : Ligne correspondante à iLigne dans la feuille Excel
	 * @param dicoIntitule_ICol : Dico des intitules sur la 1ere ligne des feuilles Excel
	 * @return : Le drapeau passe en parametre
	 */
	private static boolean verifier(
			boolean testOK,
			String nomFeuille, 
			int iLigne, 
			IDMsg idMsg, 
			String[] tabIntitule,
			boolean grave,
			Row row,
			TreeMap<String, Integer> dicoIntitule_ICol) {

		verifier(testOK, nomFeuille, iLigne, idMsg, tabIntitule, grave);
		if (!testOK)
		{
			// Recherche de l'emplacement des cellules correspondant eventuellement
			// aux intitules passes en parametre
			for(String intitule : tabIntitule)
			{
				if (dicoIntitule_ICol.containsKey(intitule))
				{
					int iCol = dicoIntitule_ICol.get(intitule);
					if (row != null && iCol >= 0)
					{
						org.apache.poi.ss.usermodel.Cell cell = row.getCell(iCol);
						if (cell == null)
							cell = row.createCell(iCol);
						if (cell != null)
							cell.setCellStyle(grave ? csError : csWarning);
					}	
				}
			}
		}
		
		return testOK;
	}

	/**
	 * Affichage d'un message dans le log en fonction du bouleen passe en parametre.
	 * Le message indique est construit a l'aide de tous les autres parametres.
	 * Le classeur ouvert dont la ligne est passee en parametre est modifie
	 * de telle sorte que le fond des cellules qui posent probleme soit surligne.
	 * @param testOK : Drapeau indiquant si un message doit etre genere
	 * @param nomFeuille : Nom de la feuille Excel auquel l'erreur est rattachee
	 * @param iLigne : Numero de ligne sur la feuille Excel auquel l'erreur est rattachee
	 * @param idMsg : ID du message a afficher
	 * @param tabIntitule : Tableau de tous les messages
	 * @param grave : 0 = avertissement, 1 = erreur
	 * @param row : Ligne correspondante à iLigne dans la feuille Excel
	 * @param dicoIntitule_LstICol : Dico des liste des indices des colonnes des tableaux complexes 
	 * sur la 1ere ligne des feuilles Excel, en fonction des intitules.
	 * @return : Le drapeau passe en parametre
	 */
	private static boolean verifier(
			boolean testOK,
			String nomFeuille, 
			int iLigne, 
			IDMsg idMsg, 
			String[] tabIntitule,
			boolean grave,
			Row row,
			TreeMap<String, ArrayList<Integer>> dicoIntitule_LstICol,
			int kLstICol) {

		verifier(testOK, nomFeuille, iLigne, idMsg, tabIntitule, grave);
		if (!testOK)
		{
			// Recherche de l'emplacement des cellules correspondant eventuellement
			// aux intitules passes en parametre
			for(String intitule : tabIntitule)
			{
				if (dicoIntitule_LstICol.containsKey(intitule))
					if (dicoIntitule_LstICol.get(intitule).size() > kLstICol)
					{
						int iCol = dicoIntitule_LstICol.get(intitule).get(kLstICol);
						if (row != null && iCol >= 0)
						{
							org.apache.poi.ss.usermodel.Cell cell = row.getCell(iCol);
							if (cell == null)
								cell = row.createCell(iCol);
							if (cell != null)
								cell.setCellStyle(grave ? csError : csWarning);							
						}	
					}
			}
		}
		
		return testOK;
	}
	
	/**
	 * Retourne le contenu de la cellule dont la ligne et l'indice de la colonne
	 * sont passes en parametre.
	 * @param row : La ligne sur laquelle la cellule est lue
	 * @param iLigne : L'indice de la ligne (pour les messages d'erreur)
	 * @param intitule : L'intitule de la colonne concernee
	 * @param dicoIntitule_ICol : Dictionnaire des indices des colonnes en fonction des intitules 
	 * @param date : Bouleen indiquant si la cellule est une date
	 * @return : une chaine de caractere representant le contenu de la cellule
	 * @throws Exception
	 */
	protected static String getContenuCellule (Row row, int iLigne, String intitule, TreeMap<String, Integer> dicoIntitule_ICol, boolean date) throws Exception {

		return dicoIntitule_ICol.containsKey(intitule) 
				? ImportDataAtWeb.lireCellule(row, iLigne, dicoIntitule_ICol.get(intitule), date)
				: "";
	}

	/**
	 * Retourne le contenu de la cellule dont la ligne et l'indice de la colonne
	 * sont passes en parametre.
	 * La cellule est supposee ne pas etre une date.
	 * @param row : La ligne sur laquelle la cellule est lue
	 * @param iLigne : L'indice de la ligne (pour les messages d'erreur)
	 * @param intitule : L'intitule de la colonne concernee
	 * @param dicoIntitule_ICol : Dictionnaire des indices des colonnes en fonction des intitules 
	 * @return : Chaine de caractere representant le contenu de la cellule
	 * @throws Exception
	 */
	protected static String getContenuCellule (Row row, int iLigne, String intitule, TreeMap<String, Integer> dicoIntitule_ICol) throws Exception {

		return getContenuCellule (row, iLigne, intitule, dicoIntitule_ICol, false);
	}

	/**
	 * Construit une liste du contenu des cellules dont la ligne et les indices des colonnes
	 * sont passes en parametre.
	 * @param row : La ligne sur laquelle la cellule est lue
	 * @param iLigne : L'indice de la ligne (pour les messages d'erreur)
	 * @param intitule : L'intitule commun a toutes les colonnes a lire
	 * @param dicoIntitule_ListeICol : Dictionnaire de la liste des indices des colonnes en fonction des intitules
	 * @return : Liste de chaînes de caractères correspondantes aux contenus des cellules
	 * @throws Exception
	 */
	protected static ArrayList<String> getContenusCellules (Row row, int iLigne, String intitule, TreeMap<String, ArrayList<Integer>> dicoIntitule_ListeICol) throws Exception {

		ArrayList<String> lst = new ArrayList<String>();
		
		if (dicoIntitule_ListeICol.containsKey(intitule))
		{
			for (Integer j : dicoIntitule_ListeICol.get(intitule))
        		lst.add(ImportDataAtWeb.lireCellule(row, iLigne, j));

			/*
	        Iterator<Map.Entry<String, ArrayList<Integer>>> iterator = dicoIntitule_ListeICol.entrySet().iterator();
			while (iterator.hasNext())
			{
	        	ArrayList<Integer> lstICol = iterator.next().getValue();
	        	for (Integer j : lstICol)
	        		lst.add(ExportDataAtWeb.lireCellule(row, iLigne, j));
			}
			*/
		}
		
		return lst;
	}
	

	/**
	 * Decodage de la chaine passee en parametre supposee etre sous la forme
	 * id1 ou id1::id2::...
	 * La derniere chaine de caractere de la liste est retournee. 
	 * @param compositionId
	 * @return
	 */
	protected static String decoderCompositionId(String compositionId) {

    	String[] listeCompositionId = compositionId.split("::");
    	int nb = listeCompositionId.length;  
    	if (nb > 1)
    		compositionId = listeCompositionId[nb - 1];
    	
    	return compositionId;
	}

	/**
	 * Creation de la liste des id des materiels a partir du nom passe en parametre sous
	 * la forme "nom_materiel1::nom_materiel2::..."
	 * @param lstMatMetProjet
	 * @param matMetNames
	 * @return
	 */
	private static ArrayList<MaterialMethodPart> getListMaterialMethodPart(
			ArrayList<MaterialMethodPart> lstMatMetProjet, 
			String matMetNames) throws Exception {
		Pattern pattern = Pattern.compile("::");
    	String[] listeMatMetName = pattern.split(matMetNames);

    	ArrayList<MaterialMethodPart> lstMatMetLoc = new ArrayList<MaterialMethodPart>();

		try
		{
			// Verifications
			if (lstMatMetProjet == null)
				throw new TechnicalException("La liste ne doit pas etre nulle");
					
			// Recherche de l'objet correspondant (dont le nom de projet correspond) dans la liste
			// pour chaque nom de la liste
			for (String matMetName : listeMatMetName)
			{
				boolean trouve = false;
				Iterator<MaterialMethodPart> iterator = lstMatMetProjet.iterator();
				while (!trouve && iterator.hasNext())
				{
					MaterialMethodPart mmp = iterator.next();
					String id = mmp.getID().getValue().get();
					if (id.equals(matMetName))
					{
						trouve = true;
						lstMatMetLoc.add(mmp);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			log.error("getListMaterialMethodPart : Erreur due a " + e.getMessage());
			throw new TechnicalException(e.getMessage());
		}	
		
		return lstMatMetLoc;
	}

	/**
	 * Creation du dico des materiels ou methodes en fonction des id decodes
	 * a partir du nom passe en parametre sous la forme "nom_materiel1::nom_materiel2::..."
	 * @param lstMMProjet
	 * @param matMetNames
	 * @return
	 */
	private static TreeMap<String, MaterialMethodPart> getDicoMatMetName_MMLP(
			ArrayList<MaterialMethodPart> lstMMProjet, String matMetNames)  throws Exception {
		Pattern pattern = Pattern.compile("::");
    	String[] listeMatMetName = pattern.split(matMetNames);

    	TreeMap<String, MaterialMethodPart> dicoMatMetName_MMLP = new TreeMap<String, MaterialMethodPart>();

		try
		{
			// Verifications
			if (lstMMProjet == null)
				throw new TechnicalException("La liste ne doit pas etre nulle");
					
			// Recherche de l'objet correspondant (dont le nom de projet correspond) dans la liste
			// pour chaque nom de la liste
			for (String matMetName : listeMatMetName)
			{
				boolean trouve = false;
				Iterator<MaterialMethodPart> iterator = lstMMProjet.iterator();
				while (!trouve && iterator.hasNext())
				{
					MaterialMethodPart mmp = iterator.next();
					String id = mmp.getID().getValue().get();
					if (id.equals(matMetName))
					{
						trouve = true;
						dicoMatMetName_MMLP.put(id,  mmp);
					}
				}
			}
		} 
		catch (Exception e) 
		{
			log.error("getListMaterialMethodPart : Erreur due a " + e.getMessage());
			throw new TechnicalException(e.getMessage());
		}	
		
		return dicoMatMetName_MMLP;

	}

	/**
	 * Verification qu'un agent caracterise par les parametres (org, fName, gName, eMail)
	 * passes en parametre n'existe pas deja dans la liste des agents du projet passe
	 * en parametre.
	 * Si oui la chaine de caractere assossiee a cet agent est retournee.
	 * Si non une chaine vie dest retournee.
	 * @param projectFile : Le projet dans lequel l'agent est recherche
	 * @param org : Organization
	 * @param fName : Familiy name
	 * @param gName : Given name
	 * @param eMail : eMail
	 * @return
	 */
	private static String agentExistant(ProjectFile projectFile, String org, String fName, String gName, String eMail) {

		// On verifie si un meme agent a deja ete ajoute
		HashMap<KeyWords, ComplexField> fAgent = Tools.createFakeAgentFromString("");
		fAgent.get(ProjectAgentPart.agentOrganisationK).setValue(org);
		fAgent.get(ProjectAgentPart.agentFamilyNameK).setValue(fName);
		fAgent.get(ProjectAgentPart.agentGivenNameK).setValue(gName);
		fAgent.get(ProjectAgentPart.agentMailK).setValue(eMail);

		return (projectFile.getAgent(Tools.agentToString(fAgent)) != null) ? Tools.agentToString(fAgent) : "";
	}

	/**
	 * Mise a jour de la propriete Agent pour l'objet ObservationFile passe en parametre
	 * a partir de la chaine de caractere representant l'agent passee en parametre.
	 * @param projectFile : Le projet concerne
	 * @param obs : L'observation concernee
	 * @param observationOperatorPrec : La chaine de caractere representant l'agent
	 * @param nomFeuille : Nom de la feuille Excel dans laquelle la chaine de caracteres representant l'agent sont lues
	 * @param intituleObservationOperator : Intitule de la colonne Excel dans laquelle la chaine de caracteres representant l'agent sont lues
	 * @param row : Feuille Excel dans laquelle la chaine de caracteres representant l'agent sont lues
	 * @param dicoIntitule_ICol : Dico des indices des colonnes dans Excel en fonction des noms des colonnes.
	 */
	private static void setObservationOperator(ProjectFile projectFile, 
		ObservationFile obs,
		String observationOperatorPrec, 
		String nomFeuille, 
		String intituleObservationOperator, 
		Row row, 
		TreeMap<String, Integer> dicoIntitule_ICol) {

		if (!observationOperatorPrec.isEmpty())
		{
			obs.setAgents(observationOperatorPrec);
		}
	}
}
