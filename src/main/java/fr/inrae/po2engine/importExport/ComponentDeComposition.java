package fr.inrae.po2engine.importExport;

public class ComponentDeComposition {
	
	private String type;
	private String attribute;
	private String value;
	private String unit;
	private String comment;

	public ComponentDeComposition() {
		this.type = "";
		this.attribute = "";
		this.value = "";
		this.unit = "";
		this.comment = "";
	}

	public ComponentDeComposition(String type, String attribute, String value, String unit, String comment) {
		this.type = type != null ? type.trim() : "";
		this.attribute = attribute != null ? attribute.trim() : "";
		this.value = value != null ? value.trim() : "";
		this.unit = unit != null ? unit.trim() : "";
		this.comment = comment != null ? comment.trim() : "";
	}

    public String getType() { return this.type; }	
    public String getAttribute() { return this.attribute; }	
    public String getValue() { return this.value; }	
    public String getUnit() { return this.unit; }	
    public String getComment() { return this.comment; }	
}
