package fr.inrae.po2engine.importExport;

import fr.inrae.po2engine.model.dataModel.ProjectFile;
import fr.inrae.po2engine.utils.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.TreeMap;

public interface IImportUsingPO2Engine {
	Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getName());

	// 	Dictionnaire des noms des unites corrects en fonction des noms incorrects.
	TreeMap<String, String> dicoNomUnitesIncorrect_Correct = new TreeMap<>();

	Report convertirFichier(ProjectFile projectFile, File importFile) throws Exception;
}
