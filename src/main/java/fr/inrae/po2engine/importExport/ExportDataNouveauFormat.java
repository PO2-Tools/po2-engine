package fr.inrae.po2engine.importExport;

import fr.inrae.po2engine.exception.TechnicalException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.OntoData;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.DataTools;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class ExportDataNouveauFormat {

	private static Logger log = LogManager.getLogger(ExportDataNouveauFormat.class);

	enum IDIntitule {
    	PROJECT_NAME,
    	PROJECT_CONTACT,
    	PROJECT_FUNDING,
    	PROJECT_DESCRIPTION,
    	EXTERNAL_LINK,
        PROCESS_TYPE,
        PROCESS_NAME,
		PROCESS_REPLICATE,
        START_DATE,
        END_DATE,
        PROCESS_DESCRIPTION,
        
        MM_TYPE,
        MM_NAME,
        ATTRIBUTE,
        ATTR_OOI,
        ATTR_VALUE,
        ATTR_UNIT,
        ATTR_COMMENT,
        
		AGENT_ORG,
		AGENT_F_NAME,
		AGENT_G_NAME,
		AGENT_EMAIL,

        TIME,
        TIME_DUR,
        ITI_POI,
        ITI_NAME,
        ITI_NUMBER,
        STEP_AGENT,
        STEP_SCALE,
        STEP_NUMBER,
        STEP_NEXT_STEP,
        STEP_SUB_STEP,
        STEP_TYPE,
        STEP_NAME,
        STEP_DESCR,
        MATERIAL_NAME,
        METHOD_NAME,
        
        COMPOSITION_ID,
        COMPOSITION_NAME,
        COMPOSITION_TYPE,
        COMPONENT_TYPE,
        
        COMPOSITION_IN_OUT,
        INPUT_COMPOSITION_ID,
        OUTPUT_COMPOSITION_ID,
        OBSERVATION_AGENT,
		OBSERVATION_SCALE,
        OBSERVATION_NAME,
        OBSERVATION_DESCR,
        TABLE_NUMBER,
        TABLE_TYPE
	}
	
	// Pour gerer des numeros simples pour les etapes et les compositions,
	// et faire la correspondance entre les id et les objets
	LinkedHashMap<String, LinkedHashMap<String, String>> dicoProcessNum_dicoStepFileNum_NumExcel;
	LinkedHashMap<String, LinkedHashMap<String, CompositionFile>> dicoProcessName_dicoCompositionFileId_CompositionFile;
	LinkedHashMap<String, ArrayList<String>> dicoProcessName_listCompositionFileId;

	public ExportDataNouveauFormat() {
		dicoProcessNum_dicoStepFileNum_NumExcel = new LinkedHashMap<>();
		dicoProcessName_dicoCompositionFileId_CompositionFile = new LinkedHashMap<>();
		dicoProcessName_listCompositionFileId = new LinkedHashMap<>();
	}

	/**
	 * Version specifique PB (ponctuelle)
	 * Export dans plusieurs fichiers Excel du projet PO²Manager 
	 * dont le nom est indique en parametre.
	 * Export specifique pour 1 projet de PB dans lequel les donnees
	 * de plusieurs projets ont ete introduites dans un meme projet
	 * (pour pouvoir beneficier de la copie d'itineraires).
	 * Pour s'y retrouver dans les fichiers Excel a creer, les noms des itineraires
	 * sont supposes avoir ete introduits sous la forme "nom_de_projet--nom_d_itineraire"
	 * @param nomProjet : Nom du projet PO²Manager a exporter
	 * @throws Exception
	 */
	public void lireEtDecouper(String nomProjet) throws Exception {

		log.info("Debut lecture...");

		CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
    	Map<String, OntoData> dico = Datas.getDatas();
    	int lng = dico.size();
    	log.info(((Integer)lng).toString() + " projet(s).");

    	// Parcours de la liste des projets sur le serveur si on est connecte au reseau,
    	// ou localement sinon.
    	Iterator<Map.Entry<String, OntoData>> entryIterator = dico.entrySet().iterator();
    	lng = 0;
		while(entryIterator.hasNext()) {
			Map.Entry<String, OntoData> element = entryIterator.next();	
			String nom = element.getKey();
			
			// Recherche du projet dont le nom correspond.
			if (nom.equals(nomProjet))
			{				
				// Lecture du projet, et verouillage (necessaire ?)
		    	Data data = Datas.getData(nomProjet);
		    	data.load();
		        DataTools.analyseFiles(data);		

		    	// Creation du fichier Excel d'export complet.
		    	CreerFichier(data);
			}
			lng++;
    	}
		log.info("Fin lecture.");
	}

	/**
	 * Creation du fichier Excel d'export a partir du nom du fichier Excel
	 * et des donnees du projet passes en parametre.
	 * Version specifique PB (ponctuelle) pour laquelle plusieurs fichiers
	 * d'export sont crees en fonction des infos trouvees dans les
	 * noms des itineraires (supposes sous la forme <nom projet>--<nom itineraire>)
	 * @param data : Le jeu de donnees a exporter dans le fichier
	 */
	private void CreerFichier(Data data) {
		
	    // Recuperation de la liste des differents projet
		// a partir des noms de tous les itineraires.
	    ArrayList<String> listNomsProjets = new ArrayList<String>();
	    DecoderItinerary(data, listNomsProjets);    
		
		// Creation du fichier Excel d'export pour chaque nom de projet de la liste
	    for (String nomProjet : listNomsProjets) 
	    {
	    	int longueur = nomProjet.length();
	    	if (longueur > 20)
	    		longueur = 20;
	    	String nomFic = nomProjet.substring(0, longueur);
			XSSFWorkbook wb = new XSSFWorkbook();
			try (OutputStream fileOut = new FileOutputStream("excelfiles" + File.separator + nomFic + ".xlsx")) {
				
				// Creation de la feuille "Project-Process"
				creerFeuilleProjectProcess(wb, data, nomProjet);
				
				// Creation des feuilles "Material" et "Method"
				creerFeuilleMatMet(wb, data, true, nomProjet);
				creerFeuilleMatMet(wb, data, false, nomProjet);

				// Creation des feuilles "Agents"
				creerFeuilleAgents(wb, data, nomProjet);

				// Creation de la feuille pour les itineraires
				creerFeuilleItinerary(wb, data, nomProjet);
				
				// Creation de la feuille pour les parametres de controles			
				creerFeuilleStepControlParameters(wb, data, nomProjet);
				
				// Creation de la feuille pour les compositions
				creerFeuilleCompositions(wb, data, nomProjet);
				
				// Creation de la feuille pour les observations
				creerFeuilleObservations(wb, data, nomProjet);
				
				// Creation de la feuille pour les parametres de controle des observations
				creerFeuilleObservationsControlParameters(wb, data, nomProjet);
				
				wb.write(fileOut);
				wb.close();
				
				fileOut.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * ATTENTION : Specifique pour PB (export d'un projet en le decoupant dans plusieurs
	 * fichiers Excel en fonction des infos indiquees dans les noms des itineraires
	 * supposes sous la forme "nom_projet--nom_itineraire")
	 * @param data : Jeu de donnees d'ou les infos sont lues
	 * @param listNomsProjets : Liste (mise a jour dans la methode) de tous les noms
	 * de projets decodes a partir des noms de tous les itineraires (sous la forme
	 * <nom projet>--<nom itineraire> trouves dans le jeu de donnee. 
	 */
	private void DecoderItinerary(Data data, ArrayList<String> listNomsProjets) {
		try {

			Pattern pattern = Pattern.compile("--");

			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size(); k++)
	    	{
				// Infos relatives au process
	    		GeneralFile generalFile = listGeneralFile.get(k);
	    		data.analyseProcess(generalFile);

				// Pour chaque itineraire du process,
				List<ItineraryFile> listItineraryFile = generalFile.getItinerary();
				int nbItin = listItineraryFile.size();
				for (int ii = 0; ii < nbItin; ii++)
				{
					// Decodage du nom de l'itineraire sous la forme "nom_projet--nom_itineraire"
					ItineraryFile itineraryFile = listItineraryFile.get(ii);
					String itineraryName = itineraryFile.getItineraryName();
		        	String[] lstItems = itineraryName.trim().isEmpty()
		        			? null
		        			: pattern.split(itineraryName);
		        	if (lstItems.length != 2)
						throw new TechnicalException("Nom d'itineraire au format inattendu");
		        	
		        	// Creation du projet s'il n'existe pas deja
		        	String nomProjet = lstItems[0];
		        	if (!listNomsProjets.contains(nomProjet))
		        		listNomsProjets.add(nomProjet);
				}
	    	}
			
			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Export dans un fichier Excel du projet PO²Manager 
	 * dont le nom est indique en parametre.
	 * @param project : Le projet a exporter
	 */
	public File lire(ProjectFile project) {
		Tools.addProgress(ProgressPO2.IMPORT, "Exporting project");
		Tools.updateProgress(ProgressPO2.IMPORT, 0.0);
		log.info("Starting exporting project " + project.getNameProperty().get());

		Data data = project.getData();
		if(!data.isLoaded().get()) {
			data.load();
			DataTools.analyseFiles(data);
		}
		ArrayList<GeneralFile> listProcessToUnload = data.getListGeneralFile().stream().filter(gf -> !gf.isLoaded()).map(gf -> {
			data.analyseProcess(gf);
			return gf;
		}).collect(Collectors.toCollection(ArrayList::new));
		int longueur = Math.min(20, project.getNameProperty().getValue().length());
		String nomFic =  project.getNameProperty().getValue().substring(0, longueur);
		File exportFile = CreerFichier(nomFic, data);
		log.info("Exporting project " + project.getNameProperty().get() + " done");
		listProcessToUnload.forEach(GeneralFile::unload);
		Tools.delProgress(ProgressPO2.IMPORT);
		return exportFile;
	}

	/**
	 * Creation du fichier Excel d'export a partir du nom du fichier Excel
	 * et des donnees du projet passes en parametre.
	 * @param nomFic : Nom du fichier Excel (sans extension) a creer
	 * @param data : Le jeu de donnees a exporter dans le fichier
	 */
	private File CreerFichier(String nomFic, Data data) {
		Tools.updateProgress(ProgressPO2.IMPORT, 0.0);
		Double pp = 1.0/8.0;

		XSSFWorkbook wb = new XSSFWorkbook();

		// Creation de la feuille "Project-Process"
		creerFeuilleProjectProcess(wb, data);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		// Creation des feuilles "Material" et "Method"
		creerFeuilleMatMet(wb, data, true);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		creerFeuilleMatMet(wb, data, false);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		// Creation de la feuille "Agents"
		creerFeuilleAgents(wb, data, true);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);
		
		// Creation de la feuille pour les itineraires
		creerFeuilleItinerary(wb, data);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		// Creation de la feuille pour les parametres de controles
		creerFeuilleStepControlParameters(wb, data);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		// Creation de la feuille pour les compositions
		creerFeuilleCompositions(wb, data);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		// Creation de la feuille pour les observations
		creerFeuilleObservations(wb, data);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		// Creation de la feuille pour les parametres de controle des observations
		creerFeuilleObservationsControlParameters(wb, data);
		Tools.updateProgress(ProgressPO2.IMPORT, Tools.getProgress(ProgressPO2.IMPORT).getProgress() +pp);

		try {
			File tempFile = File.createTempFile(nomFic,".xlsx");
			FileOutputStream fout = new FileOutputStream(tempFile);
			wb.write(fout);
			wb.close();
			fout.close();
			return tempFile;
		} catch (IOException e) {
			log.error("export failed", e);
		}
		return null;
	}

	/**
	 * Creation de la feuille "Observation control parameters"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */
	private void creerFeuilleObservationsControlParameters(XSSFWorkbook wb, Data data) {

		creerFeuilleObservationsControlParameters(wb, data, "");
		
	}

	/**
	 * Creation de la feuille "Observation control parameters"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleObservationsControlParameters(XSSFWorkbook wb, Data data, String nomProjet) {

		try {

			log.info("creerFeuilleObservationsControlParameters...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_NAME, "Process name");
			dicoIDIntitule_Intitule.put(IDIntitule.ITI_NUMBER, "Itinerary number");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NUMBER, "Step number");
			dicoIDIntitule_Intitule.put(IDIntitule.COMPOSITION_ID, "Composition id");
			dicoIDIntitule_Intitule.put(IDIntitule.OBSERVATION_NAME, "Observation name");
	        dicoIDIntitule_Intitule.put(IDIntitule.MATERIAL_NAME, "Material name");
	        dicoIDIntitule_Intitule.put(IDIntitule.METHOD_NAME, "Method name");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTRIBUTE, "Attribute");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_OOI, "Object of interest");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_VALUE, "Value");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_UNIT, "Unit");
		    dicoIDIntitule_Intitule.put(IDIntitule.ATTR_COMMENT, "Comment");
		    
			String nomFeuille = "Observation control parameters";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
			
			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout d'1 ligne pour le projet
			ajouterLignesObservationControlParameters(sheet, dicoIDIntitule_ICol, data, nomProjet);

			log.info("fin creerFeuilleObservationsControlParameters");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Ajout de toutes les lignes relatives aux parametres de controle des observations
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesObservationControlParameters(Sheet sheet,
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data, String nomProjet) {

		try {

			String motCleName = "name";
			String motCleMatType = "material type";
			String motCleMetType = "method type";
			String motCleMeasuringInstrument = "measuring instrument";
			String motCleProcessingEquipment = "processing equipment";
			List<String> listMotsCleReserves = new ArrayList<String>();
			listMotsCleReserves.add(motCleName);
			listMotsCleReserves.add(motCleMatType);
			listMotsCleReserves.add(motCleMetType);
			listMotsCleReserves.add(motCleMeasuringInstrument);
			listMotsCleReserves.add(motCleProcessingEquipment);

			// Liste des couples (numero etape, id composition) deja traites
			TreeMap<String, ArrayList<String>> dicoNumEtape_ListIdCompo = new TreeMap<String, ArrayList<String>>();

			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;
			
			// Indicatreur de reenseignement deja donne
			boolean dejaTraite;

			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size(); k++)
	    	{
				// Infos relatives au process
	    		GeneralFile generalFile = listGeneralFile.get(k);    		
	    		String processName = generalFile.processNameProperty().getValue();
				
	    		// Recup du dico entre la signature des etapes et le n° correspondant dans Excel.
				String processNum = generalFile.toString(); 
				if (!dicoProcessNum_dicoStepFileNum_NumExcel.containsKey(processNum))
					dicoProcessNum_dicoStepFileNum_NumExcel.put(processNum, new LinkedHashMap<String, String>());
				LinkedHashMap<String, String> dicoStepFileNum_NumExcel = 
						dicoProcessNum_dicoStepFileNum_NumExcel.get(processNum);

				// Pour chaque itineraire du process,
				List<ItineraryFile> listItineraryFile = generalFile.getItinerary();
				int nbItin = listItineraryFile.size();
				List<ObservationFile> listToutesObservationFile = new ArrayList<ObservationFile>();
				for (int ii = 0; ii < nbItin; ii++)
				{
					// Infos relatives a l'itineraire
					ItineraryFile itineraryFile = listItineraryFile.get(ii);					
					if (itineraryFile.getItineraryName().startsWith(nomProjet))
					{
						String itiNumber = ((Integer)(ii + 1)).toString();
						
						// Creation de la liste des observations :
						// Ajout des observations d'itineraires
						List<ObservationFile> listObservationFile = new ArrayList<ObservationFile>();
						listObservationFile.addAll(itineraryFile.getListObservation());
	
						// Creation de la liste des observations, suite :
						// Ajout des observations d'etapes et de compositions
						// en prenant bien soin de ne pas rajouter de lignes
						// pour les observations d'etapes clonees.
						for (StepFile stepFile : itineraryFile.getListStep()) {
							for (ObservationFile of : stepFile.getObservationFiles()) {
								if (listToutesObservationFile.indexOf(of) < 0) {
									listObservationFile.add(of);
									listToutesObservationFile.add(of);
								}
							}
						}
						
						// Pour chaque observation,
						for (ObservationFile observationFile : listObservationFile)
						{
							String obsName = observationFile.getCID().toString();
							String compositionId = observationFile.getCFoi().toString();
							String stepNumber = "";
							
							// Bien qu'1 seul FOI ne soit possible, pour des raisons historiques,
							// la methode getCFoi() opeut renvoyer une liste sous la forme
							// id1::id2::.... Dans ce cas seule la derniere id de la liste
							// doit etre conservee
							compositionId = decoderCompositionId(compositionId);

							// Init du drapeau indiquant si l'observation a deja ete traitee
							dejaTraite = false;
							
							// Observation d'itineraire.
							StepFile stepFile = observationFile.getStepFile();
							if (stepFile == null);
							
							// Observation d'etape ou de composition
							else 
							{
								// Pas d'indication sur l'itineraire car une meme etape
								// peut etre dans plusieurs itineraires
								itiNumber = "";
								
								String stepFileNum = stepFile.getFileName(); 
								stepNumber = dicoStepFileNum_NumExcel.get(stepFileNum);
	
								// Observation d'etape.
								if (compositionId.equals("step"))
									compositionId = "";
								
								// Sinon, observation de composition
								
								// MaJ du drapeau indiquant si l'observation a deja ete ajoutee,
								// et du dictionnaire des 
								if (!dicoNumEtape_ListIdCompo.containsKey(stepNumber))
									dicoNumEtape_ListIdCompo.put(stepNumber, new ArrayList<String>());
								ArrayList<String> listIdCompo = dicoNumEtape_ListIdCompo.get(stepNumber); 
								if (listIdCompo.contains(compositionId))
									dejaTraite = true;
								else
									listIdCompo.add(compositionId);
							}
							
							// Pas besoin de recommencer pour des etapes deja traitees
							//if (!dejaTraite) // ERREUR car ce test implique 1 seule observation par composition => Suppression
							{
								// Simplification de l'id composition
								compositionId = simplifierCompositionId(processName, compositionId);

								// Pour chaque Materiel et chaque Methode,
								List<MaterialMethodLinkPart> listMatMet = observationFile.getListMaterialMethodRO();
								for (MaterialMethodLinkPart mmlp : listMatMet)
								{
									ConsignPart consignePart = mmlp.getConsignPart();
									MaterialMethodPart mmp = mmlp.getMaterialMethodPartPart();
									String nom = mmp.getID().toString();
									KeyWords lpType = mmp.getPartType();
									String nomMat = lpType.equals(DataPartType.MATERIAL_RAW) ? nom : "";
									String nomMet = lpType.equals(DataPartType.METHOD_RAW) ? nom : "";
		
									// Liste des attributs des Materiels.						
									// Verification s'il y a des attributs de materiels ou methodes
									// a indiquer (autre que les attributs indiques au niveau du projet)
									SimpleTable st = consignePart.getDataTable();
									List<Map<KeyWords, ComplexField>> listLignes = st.getContent();
									int nbLignes = listLignes.size();
		
						    		// Pour chaque attribut de materiel ou methode indique.
									// Recup des infos specifiques utilisateur
						    		for (int kk = 0; kk < nbLignes; kk++)
						    		{		    
							    		Map<KeyWords, ComplexField> ligne = listLignes.get(kk); 
							    		SimpleStringProperty attribut = ligne.get(SimpleTable.caractK).getValue();
							    		SimpleStringProperty obj = ligne.get(SimpleTable.ObjK).getValue();
							    		SimpleStringProperty val = ligne.get(SimpleTable.valK).getValue();
							    		SimpleStringProperty unit = ligne.get(SimpleTable.unitK).getValue();
							    		SimpleStringProperty comment = ligne.get(SimpleTable.commentK).getValue();
							    		
										if (!listMotsCleReserves.contains(attribut.getValue().toString()))
							    		{
											// 1 ligne par info specifique utilisateur (la 1ere est deja creee)
											Row row = sheet.createRow(i++);
											
											// Infos relatives au process repetees
											j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
											Cell cell = row.createCell(j);
											cell.setCellValue(processName);
					
											j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(itiNumber);
											
											j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(stepNumber);
											
											// Infos repetees relatives a la composition
											j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_ID);
											cell = row.createCell(j);
											cell.setCellValue(compositionId);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_NAME);
											cell = row.createCell(j);
											cell.setCellValue(obsName);
							    			
											// Infos generales relatives aux Materiels et Methodes repetees
											j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
											cell = row.createCell(j);
											cell.setCellValue(nomMat);
								
											j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
											cell = row.createCell(j);
											cell.setCellValue(nomMet);
								
											// Infos specifiques utilisateur
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTRIBUTE);
											cell = row.createCell(j);
											cell.setCellValue(attribut.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_OOI);
											cell = row.createCell(j);
											cell.setCellValue(obj.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_VALUE);
											cell = row.createCell(j);
											cell.setCellValue(val.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_UNIT);
											cell = row.createCell(j);
											cell.setCellValue(unit.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_COMMENT);
											cell = row.createCell(j);
											cell.setCellValue(comment.getValue().toString());
							    		}
						    		}
								}
							}
						}
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Creation de la feuille "Observations"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */
	private void creerFeuilleObservations(XSSFWorkbook wb, Data data) {

		creerFeuilleObservations(wb, data, "");
		
	}

	/**
	 * Creation de la feuille "Observations"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleObservations(XSSFWorkbook wb, Data data, String nomProjet) {

		try {

			log.info("creerFeuilleObservations...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, String> dicoIDIntituleVar_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_NAME, "Process name");
			dicoIDIntitule_Intitule.put(IDIntitule.ITI_NUMBER, "Itinerary number");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NUMBER, "Step number");
			dicoIDIntitule_Intitule.put(IDIntitule.COMPOSITION_ID, "Composition id");
			dicoIDIntitule_Intitule.put(IDIntitule.OBSERVATION_AGENT, "Observation operator");
			dicoIDIntitule_Intitule.put(IDIntitule.OBSERVATION_NAME, "Observation name");
			dicoIDIntitule_Intitule.put(IDIntitule.OBSERVATION_SCALE, "Observation scale");
		    dicoIDIntitule_Intitule.put(IDIntitule.START_DATE, "Date");
			dicoIDIntitule_Intitule.put(IDIntitule.TIME, "Time");
			dicoIDIntitule_Intitule.put(IDIntitule.TIME_DUR, "Time duration");
			dicoIDIntitule_Intitule.put(IDIntitule.OBSERVATION_DESCR, "Description");
	        dicoIDIntitule_Intitule.put(IDIntitule.MATERIAL_NAME, "Material name");
	        dicoIDIntitule_Intitule.put(IDIntitule.METHOD_NAME, "Method name");
			dicoIDIntitule_Intitule.put(IDIntitule.TABLE_NUMBER, "Table number");
			dicoIDIntitule_Intitule.put(IDIntitule.TABLE_TYPE, "Table type");

			dicoIDIntituleVar_Intitule.put(IDIntitule.ATTRIBUTE, "Attribute");
			dicoIDIntituleVar_Intitule.put(IDIntitule.ATTR_OOI, "Object of interest");
			dicoIDIntituleVar_Intitule.put(IDIntitule.ATTR_VALUE, "Value");
			dicoIDIntituleVar_Intitule.put(IDIntitule.ATTR_UNIT, "Unit");
		    dicoIDIntituleVar_Intitule.put(IDIntitule.ATTR_COMMENT, "Comment");
		    dicoIDIntitule_Intitule.putAll(dicoIDIntituleVar_Intitule);

			String nomFeuille = "Observations";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
						
			// Ajout d'1 ligne pour les intitules "fixes".
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);

			// Ajout d'1 ligne pour le projet			
			int nbIntitulesSuppl = ajouterLignesObservations(sheet, dicoIDIntitule_ICol, data, nomProjet);
						
			// La ligne des intitules est completee
			completerLigneIntitules(sheet, dicoIDIntitule_Intitule.size(), nbIntitulesSuppl, dicoIDIntituleVar_Intitule);

			log.info("fin creerFeuilleObservations");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Ajout de toutes les lignes relatives aux donnees de compositions
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 * @return : Nombre d'intitules supplementaires a ajouter dans la 1ere ligne
	 * des intitules de la feuille (car ce nombre est variable en fonction des observations)
	 */
	private int ajouterLignesObservations(XSSFSheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data, String nomProjet) {

		int nbIntitulesSuppl = 0;

		try {

			// Liste des couples (numero etape, id composition) deja traites
			TreeMap<String, ArrayList<String>> dicoNumEtape_ListIdCompo = new TreeMap<String, ArrayList<String>>();

			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			// Indicatreur de reenseignement deja donne
			boolean dejaTraite;
			
			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size(); k++)
	    	{
	    		// Drapeau indiquant le 1er itineraire
				boolean premItin = true;

				// Ajout d'1 ligne.
				Row row = sheet.createRow(i++);

				// Infos relatives au process
	    		GeneralFile generalFile = listGeneralFile.get(k);    		
	    		String processName = generalFile.processNameProperty().getValue();
				
	    		// Recup du dico entre la signature des etapes et le n° correspondant dans Excel.
				String processNum = generalFile.toString(); 
				if (!dicoProcessNum_dicoStepFileNum_NumExcel.containsKey(processNum))
					dicoProcessNum_dicoStepFileNum_NumExcel.put(processNum, new LinkedHashMap<String, String>());
				LinkedHashMap<String, String> dicoStepFileNum_NumExcel = 
						dicoProcessNum_dicoStepFileNum_NumExcel.get(processNum);

	    		// Cas particulier si pas d'itineraire associe au process
				List<ItineraryFile> listItineraryFile = generalFile.getItinerary();
				int nbItin = listItineraryFile.size();
				if (nbItin == 0)
				{
					j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
					Cell cell = row.createCell(j);
					cell.setCellValue(processName);
				}
				
				// Pour chaque itineraire du process,
				List<ObservationFile> listToutesObservationFile = new ArrayList<ObservationFile>();
				for (int ii = 0; ii < nbItin; ii++)
				{
					ItineraryFile itineraryFile = listItineraryFile.get(ii);					
					String itiName = itineraryFile.getItineraryName();
					
					if (itiName.startsWith(nomProjet))
					{	
						// 1 ligne par itineraire (la 1ere est deja creee)
						if (premItin)
							premItin = false;
						else
							row = sheet.createRow(i++);
	
						// Infos relatives a l'itineraire
						String itiNumber = ((Integer)(ii + 1)).toString();

						// Creation de la liste des observations :
						// Ajout des observations d'itineraires
						List<ObservationFile> listObservationFile = new ArrayList<ObservationFile>();
						listObservationFile.addAll(itineraryFile.getListObservation());
	
						// Creation de la liste des observations, suite :
						// Ajout des observations d'etapes et de compositions
						// en prenant bien soin de ne pas rajouter de lignes
						// pour les observations d'etapes clonees.
						for (StepFile stepFile : itineraryFile.getListStep()) {
							for (ObservationFile of : stepFile.getObservationFiles()) {
								if (listToutesObservationFile.indexOf(of) < 0) {
									listObservationFile.add(of);
									listToutesObservationFile.add(of);
								}
							}
						}

						// Pour chaque observation,
						int oo = 0;
						for (ObservationFile observationFile : listObservationFile)
						{
							// 1 ligne par ingredient (la 1ere est deja creee)
							if (oo++ > 0)
								row = sheet.createRow(i++);
	
				    		String obsAgent = observationFile.getAgents();
							String obsScale = observationFile.getCScale().toString();
							String obsTime = observationFile.getCHeure().toString();
							String obsDate = observationFile.getCDate().toString();
							String obsTimeDuration = observationFile.getCDuree().toString();
							String obsDescr = observationFile.getCDescrition().toString();
							String obsName = observationFile.getCID().toString();
							List<MaterialMethodLinkPart> listMatMet = observationFile.getListMaterialMethodRO();
							String nomMat = construireNomMatet(listMatMet, DataPartType.MATERIAL_RAW);
							String nomMet = construireNomMatet(listMatMet, DataPartType.METHOD_RAW);
							String compositionId = decoderCompositionId(observationFile.getCFoi().toString());
							String stepNumber = "";

							// Init du drapeau indiquant si l'observation a deja ete traitee
							dejaTraite = false;
							
							// Observation d'itineraire.
							StepFile stepFile = observationFile.getStepFile();
							if (stepFile == null);
							
							// Observation d'etape ou de composition
							else 
							{
								// Pas d'indication sur l'itineraire car une meme etape
								// peut etre dans plusieurs itineraires
								itiNumber = "";
								
								String stepFileNum = stepFile.getFileName(); 
								stepNumber = dicoStepFileNum_NumExcel.get(stepFileNum);
	
								// Observation d'etape.
								if (compositionId.equals("step"))
									compositionId = "";
	
								// Sinon, observation de composition
								
								// MaJ du drapeau indiquant si l'observation a deja ete ajoutee,
								// et du dictionnaire des 
								if (!dicoNumEtape_ListIdCompo.containsKey(stepNumber))
									dicoNumEtape_ListIdCompo.put(stepNumber, new ArrayList<String>());
								ArrayList<String> listIdCompo = dicoNumEtape_ListIdCompo.get(stepNumber); 
								if (listIdCompo.contains(compositionId))
									dejaTraite = true;
								else
									listIdCompo.add(compositionId);
							}
								
							// Pas besoin de recommencer pour des etapes deja traitees -> ?
							//if (!dejaTraite) // ERREUR car ce test implique 1 seule observation par composition => Suppression
							{
								// Simplification de l'id composition
								compositionId = simplifierCompositionId(processName, compositionId);

								// Cas particulier si pas de tableau de donnee
								List<TablePart> listTablepart = observationFile.getContentList();
								int nbTP = listTablepart.size();
								if (nbTP == 0)
								{
									// Infos relatives au process repetees
									j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
									Cell cell = row.createCell(j);
									cell.setCellValue(processName);
			
									j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
									cell = row.createCell(j);
									cell.setCellValue(itiNumber);
		
									// Infos repetees relatives aux etapes
									j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
									cell = row.createCell(j);
									cell.setCellValue(stepNumber);
									
									// Infos repetees relatives a la composition
									j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_ID);
									cell = row.createCell(j);
									cell.setCellValue(compositionId);
									
									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_AGENT);
									cell = row.createCell(j);
									cell.setCellValue(obsAgent);
									
									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_SCALE);
									cell = row.createCell(j);
									cell.setCellValue(obsScale);

									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_NAME);
									cell = row.createCell(j);
									cell.setCellValue(obsName);
					    			
									// Infos generales relatives aux Materiels et Methodes repetees
									j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
									cell = row.createCell(j);
									cell.setCellValue(nomMat);
						
									j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
									cell = row.createCell(j);
									cell.setCellValue(nomMet);
									
									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.START_DATE);
									cell = row.createCell(j);
									cell.setCellValue(obsDate);
									
									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.TIME);
									cell = row.createCell(j);
									cell.setCellValue(obsTime);
									
									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.TIME_DUR);
									cell = row.createCell(j);
									cell.setCellValue(obsTimeDuration);
									
									// Infos relatives a l'observation
									j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_DESCR);
									cell = row.createCell(j);
									cell.setCellValue(obsDescr);
								}
								else
								{
									// Pour chaque table de donnee
									boolean prem = true;
									for (int tt = 0; tt < nbTP; tt++)
									{
										TablePart tp = listTablepart.get(tt);	
										GenericTable gt = tp.getTable();
										String tableType = tp.getPartType().equals(DataPartType.RAW_DATA) ? "raw data" : "calc data";
										int nbLignesS = 0;
										int nbLignesC = 0;
										List<Map<KeyWords, ComplexField>> listLignesS = null;
										List<Map<ComplexField, ComplexField>> listLignesC = null;
							    		//LinkedHashMap<String, Integer> dicoIntitule_ICol = new LinkedHashMap<String, Integer>();
																		
										// Recherche du nombre de tables de donnees 1D et 2D.
										if (gt instanceof SimpleTable)
										{									
									        SimpleTable st = (SimpleTable) gt;
											listLignesS = st.getContent();
											nbLignesS = listLignesS.size();
										}
										else if (gt instanceof ComplexTable)
										{
									        ComplexTable ct = (ComplexTable) gt;
											listLignesC = ct.getContent();
											nbLignesC = listLignesC.size();
		
											List<ComplexField> listIntitules = ct.getTableHeader();
											Integer nbIntitules = listIntitules.size() - 2;
											if (nbIntitules > nbIntitulesSuppl)
												nbIntitulesSuppl = nbIntitules;
										}									
										
										// Cas particulier si pas de ligne dans la table de donnees.
							    		if (nbLignesS == 0 && nbLignesC == 0)
							    		{
											// Infos relatives au process repetees
											j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
											Cell cell = row.createCell(j);
											cell.setCellValue(processName);
					
											j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(itiNumber);
		
											// Infos repetees relatives aux etapes
											j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(stepNumber);
											
											// Infos repetees relatives a la composition
											j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_ID);
											cell = row.createCell(j);
											cell.setCellValue(compositionId);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_AGENT);
											cell = row.createCell(j);
											cell.setCellValue(obsAgent);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_SCALE);
											cell = row.createCell(j);
											cell.setCellValue(obsScale);

											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_NAME);
											cell = row.createCell(j);
											cell.setCellValue(obsName);
							    			
											// Infos generales relatives aux Materiels et Methodes repetees
											j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
											cell = row.createCell(j);
											cell.setCellValue(nomMat);
								
											j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
											cell = row.createCell(j);
											cell.setCellValue(nomMet);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.START_DATE);
											cell = row.createCell(j);
											cell.setCellValue(obsDate);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.TIME);
											cell = row.createCell(j);
											cell.setCellValue(obsTime);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.TIME_DUR);
											cell = row.createCell(j);
											cell.setCellValue(obsTimeDuration);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_DESCR);
											cell = row.createCell(j);
											cell.setCellValue(obsDescr);
											
											// Infos relatives a la table : numero.
											j = dicoIDIntitule_ICol.get(IDIntitule.TABLE_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(((Integer)(tt + 1)).toString());
											
											// Infos relatives a la table : donnees brutes oucalculees
											j = dicoIDIntitule_ICol.get(IDIntitule.TABLE_TYPE);
											cell = row.createCell(j);
											cell.setCellValue(tableType);
							    		}
							    	
										// Cas d'un tableau 1D. Donnees de la table 
							    		for (int kk = 0; kk < nbLignesS; kk++)
							    		{		    
											// 1 ligne par ingredient (la 1ere est deja creee)
											if (prem)
												prem = false;
											else
												row = sheet.createRow(i++);
		
											Map<KeyWords, ComplexField> ligne = listLignesS.get(kk); 
								    		SimpleStringProperty attribut = ligne.get(SimpleTable.caractK).getValue();
								    		SimpleStringProperty componentType = ligne.get(SimpleTable.ObjK).getValue();
								    		String val = ligne.get(SimpleTable.valK).getValuesAsString();
								    		SimpleStringProperty unit = ligne.get(SimpleTable.unitK).getValue();
								    		SimpleStringProperty comment = ligne.get(SimpleTable.commentK).getValue();
		
											// Infos relatives au process repetees
											j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
											Cell cell = row.createCell(j);
											cell.setCellValue(processName);
					
											j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(itiNumber);
		
											// Infos repetees relatives aux etapes
											j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(stepNumber);
											
											// Infos repetees relatives a la composition
											j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_ID);
											cell = row.createCell(j);
											cell.setCellValue(compositionId);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_AGENT);
											cell = row.createCell(j);
											cell.setCellValue(obsAgent);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_SCALE);
											cell = row.createCell(j);
											cell.setCellValue(obsScale);

											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_NAME);
											cell = row.createCell(j);
											cell.setCellValue(obsName);
							    			
											// Infos generales relatives aux Materiels et Methodes repetees
											j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
											cell = row.createCell(j);
											cell.setCellValue(nomMat);
								
											j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
											cell = row.createCell(j);
											cell.setCellValue(nomMet);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.START_DATE);
											cell = row.createCell(j);
											cell.setCellValue(obsDate);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.TIME);
											cell = row.createCell(j);
											cell.setCellValue(obsTime);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.TIME_DUR);
											cell = row.createCell(j);
											cell.setCellValue(obsTimeDuration);
											
											// Infos relatives a l'observation
											j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_DESCR);
											cell = row.createCell(j);
											cell.setCellValue(obsDescr);
											
											// Infos relatives a la table : numero
											j = dicoIDIntitule_ICol.get(IDIntitule.TABLE_NUMBER);
											cell = row.createCell(j);
											cell.setCellValue(((Integer)(tt + 1)).toString());
											
											// Infos relatives a la table : donnees brutes oucalculees
											j = dicoIDIntitule_ICol.get(IDIntitule.TABLE_TYPE);
											cell = row.createCell(j);
											cell.setCellValue(tableType);
		
											// Infos specifiques utilisateur
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTRIBUTE);
											cell = row.createCell(j);
											cell.setCellValue(attribut.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_OOI);
											cell = row.createCell(j);
											cell.setCellValue(componentType.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_VALUE);
											cell = row.createCell(j);
											cell.setCellValue(val);
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_UNIT);
											cell = row.createCell(j);
											cell.setCellValue(unit.getValue().toString());
		
											j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_COMMENT);
											cell = row.createCell(j);
											cell.setCellValue(comment.getValue().toString());
							    		}
										
							    	
										// Cas d'un tableau 2D. Donnees de la table 
										// Chaque table comporte plusieurs lignes
							    		for (int kk = 0; kk < nbLignesC; kk++)
							    		{		    
											// 1 ligne par ingredient (la 1ere est deja creee)
											if (prem)
												prem = false;
											else
												row = sheet.createRow(i++);
											
											// Chaque ligne comporte plusieurs colonnes
											Map<ComplexField, ComplexField> ligne = listLignesC.get(kk); 
											int cc = 0;
											for (Map.Entry<ComplexField, ComplexField> element : ligne.entrySet())
											{
												ComplexField x = element.getKey();
												ComplexField y = element.getValue();
		
												List<String> listObj = x.getListObject();
									    		String[] tabObj = listObj.toArray(new String[0]);
									    		String ooi = java.util.Arrays.stream(tabObj).collect(java.util.stream.Collectors.joining("::"));
												
									    		String attribut = x.toString();
									    		if (!attribut.equals("#") && !attribut.equals("action"))
									    		{
										    		String val = y.toString();
										    		String unit = x.getUnit().getValue();
										    		String comment = x.getTooltip().getValue();
										    		
													// Infos relatives au process repetees
													j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
													Cell cell = row.createCell(j);
													cell.setCellValue(processName);
							
													j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
													cell = row.createCell(j);
													cell.setCellValue(itiNumber);
		
													// Infos repetees relatives aux etapes
													j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
													cell = row.createCell(j);
													cell.setCellValue(stepNumber);
													
													// Infos repetees relatives a la composition
													j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_ID);
													cell = row.createCell(j);
													cell.setCellValue(compositionId);
													
													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_AGENT);
													cell = row.createCell(j);
													cell.setCellValue(obsAgent);
													
													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_SCALE);
													cell = row.createCell(j);
													cell.setCellValue(obsScale);

													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_NAME);
													cell = row.createCell(j);
													cell.setCellValue(obsName);
									    			
													// Infos generales relatives aux Materiels et Methodes repetees
													j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
													cell = row.createCell(j);
													cell.setCellValue(nomMat);
										
													j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
													cell = row.createCell(j);
													cell.setCellValue(nomMet);
													
													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.START_DATE);
													cell = row.createCell(j);
													cell.setCellValue(obsDate);
													
													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.TIME);
													cell = row.createCell(j);
													cell.setCellValue(obsTime);
													
													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.TIME_DUR);
													cell = row.createCell(j);
													cell.setCellValue(obsTimeDuration);
													
													// Infos relatives a l'observation
													j = dicoIDIntitule_ICol.get(IDIntitule.OBSERVATION_DESCR);
													cell = row.createCell(j);
													cell.setCellValue(obsDescr);
													
													// Infos relatives a la table : numero
													j = dicoIDIntitule_ICol.get(IDIntitule.TABLE_NUMBER);
													cell = row.createCell(j);
													cell.setCellValue(((Integer)(tt + 1)).toString());
													
													// Infos relatives a la table : donnees brutes oucalculees
													j = dicoIDIntitule_ICol.get(IDIntitule.TABLE_TYPE);
													cell = row.createCell(j);
													cell.setCellValue(tableType);
		
													// Infos specifiques utilisateur
													j = dicoIDIntitule_ICol.get(IDIntitule.ATTRIBUTE) + cc*5;
													cell = row.createCell(j);
													cell.setCellValue(attribut);
		
													j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_OOI) + cc*5;
													cell = row.createCell(j);
													cell.setCellValue(ooi);
		
													j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_VALUE) + cc*5;
													cell = row.createCell(j);
													cell.setCellValue(val);
		
													j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_UNIT) + cc*5;
													cell = row.createCell(j);
													cell.setCellValue(unit);
		
													j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_COMMENT) + cc*5;
													cell = row.createCell(j);
													cell.setCellValue(comment);
		
													cc++;
									    		}
											}
							    		}
									}
								}
							}
						}
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return nbIntitulesSuppl - 1;
	}

	/**
	 * Creation de la feuille "Composition"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */
	private void creerFeuilleCompositions(XSSFWorkbook wb, Data data) {

		creerFeuilleCompositions(wb, data, "");
		
	}

	/**
	 * Creation de la feuille "Composition"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleCompositions(XSSFWorkbook wb, Data data, String nomProjet) {

		try {


			log.info("creerFeuilleCompositions...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_NAME, "Process name");
			dicoIDIntitule_Intitule.put(IDIntitule.COMPOSITION_ID, "Composition id");
			dicoIDIntitule_Intitule.put(IDIntitule.COMPOSITION_NAME, "Composition name");
			dicoIDIntitule_Intitule.put(IDIntitule.COMPOSITION_TYPE, "Composition type");
		    
			String nomFeuille = "Composition";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
			
			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout d'1 ligne pour le projet
			ajouterLignesComposition(sheet, dicoIDIntitule_ICol, data, nomProjet);

			log.info("fin creerFeuilleCompositions");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Ajout de toutes les lignes relatives aux donnees de compositions
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesComposition(XSSFSheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data, String nomProjet) {

		try {

			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			// Pour chaque process du projet
	    	for (Map.Entry<String, LinkedHashMap<String, CompositionFile>> cplProcessName_dicoCompositionFileId_CompositionFile : dicoProcessName_dicoCompositionFileId_CompositionFile.entrySet())
	    	{
				// Infos relatives au process
	    		String processName = cplProcessName_dicoCompositionFileId_CompositionFile.getKey();
	    		LinkedHashMap<String, CompositionFile> dicoCompositionFileId_CompositionFile = cplProcessName_dicoCompositionFileId_CompositionFile.getValue();
	    		
	    		// Pour chaque Composition du process
	    		for (Map.Entry<String, CompositionFile> cplCompositionFileId_CompositionFile : dicoCompositionFileId_CompositionFile.entrySet())
	    		{
	    			CompositionFile cf = cplCompositionFileId_CompositionFile.getValue();
	    			if (compositionUtiliseeDansProjet(data, cf, nomProjet))
	    			{	    			
			    		// Ajout d'1 ligne.
						Row row = sheet.createRow(i++);
	
						String compositionId = cplCompositionFileId_CompositionFile.getKey();
						String compositionName = cf.getCompositionID().toString();
						String compositionType = cf.getCompositionType();
							    
						// Simplification de l'id composition
						compositionId = simplifierCompositionId(processName, compositionId);

							// Infos relatives au process repetees
							j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
							Cell cell = row.createCell(j);
							cell.setCellValue(processName);
	
							// Infos repetees relatives aux compositions
							j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_ID);
							cell = row.createCell(j);
							cell.setCellValue(compositionId);
	
							// Infos repetees relatives aux compositions
							j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_NAME);
							cell = row.createCell(j);
							cell.setCellValue(compositionName);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.COMPOSITION_TYPE);
							cell = row.createCell(j);
							cell.setCellValue(compositionType);
	    			}
				}
	    	}
			
			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * ATTENTION : Specifique pour PB (export d'un projet en le decoupant dans plusieurs
	 * fichiers Excel en fonction des infos indiquees dans les noms des itineraires
	 * supposes sous la forme "nom_projet--nom_itineraire")
	 * Verification que la composition indiquee en parametre est utilisee dans le projet 
	 * dont le nom est passe en parametre
	 * @param data
	 * @param nomProjet
	 * @return
	 */
	private boolean compositionUtiliseeDansProjet(Data data, CompositionFile cf, String nomProjet) {
		
		boolean trouve = false;
		try {

			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size() && !trouve; k++)
	    	{
				// Pour chaque itineraire du process,
	    		GeneralFile generalFile = listGeneralFile.get(k);    		
				List<ItineraryFile> listItineraryFile = generalFile.getItinerary();
				int nbItin = listItineraryFile.size();
				for (int ii = 0; ii < nbItin && !trouve; ii++)
				{
					ItineraryFile itineraryFile = listItineraryFile.get(ii);					
					String itiName = itineraryFile.getItineraryName();
					
					if (itiName.startsWith(nomProjet))
					{	
						// Pour chaque etape de l'itineraire,
						List<StepFile> listStepFile = itineraryFile.getListStep();
						int nbStep = listStepFile.size();
						for (int ss = 0; ss < nbStep && !trouve; ss++)
						{
							// Infos relatives aux etapes
							StepFile stepFile = listStepFile.get(ss);
							HashMap<CompositionFile, Boolean> dicoCompositionFile_Sens = stepFile.getCompositionFile();
					        Iterator<Map.Entry<CompositionFile, Boolean>> iterator = dicoCompositionFile_Sens.entrySet().iterator();
					        while (iterator.hasNext() && !trouve)
					        {
					        	// Ajout des intitules dans l'ordre du dico
					        	Map.Entry<CompositionFile, Boolean> cplCompositionFile_Sens = iterator.next();
							
								CompositionFile compositionFile = cplCompositionFile_Sens.getKey();
								if (compositionFile.equals(cf))
									trouve = true;
							}
						}
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return trouve;
	}
	
	/**
	 * Creation de la feuille "Step control parameters"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */	
	private void creerFeuilleStepControlParameters(XSSFWorkbook wb, Data data) {

		creerFeuilleStepControlParameters(wb, data, "");
		
	}

	/**
	 * Creation de la feuille "Step control parameters"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleStepControlParameters(XSSFWorkbook wb, Data data, String nomProjet) {

		try {

			log.info("creerFeuilleStepControlParameters...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_NAME, "Process name");
			//dicoIDIntitule_Intitule.put(IDIntitule.ITI_NUMBER, "Itinerary number");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NUMBER, "Step number");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NAME, "Step name");
	        dicoIDIntitule_Intitule.put(IDIntitule.MATERIAL_NAME, "Material name");
	        dicoIDIntitule_Intitule.put(IDIntitule.METHOD_NAME, "Method name");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTRIBUTE, "Attribute");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_OOI, "Object of interest");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_VALUE, "Value");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_UNIT, "Unit");
		    dicoIDIntitule_Intitule.put(IDIntitule.ATTR_COMMENT, "Comment");
		    
			String nomFeuille = "Step control parameters";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
			
			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout des lignes de donnees.
			ajouterLignesStepControlParameters(sheet, dicoIDIntitule_ICol, data, nomProjet);

			log.info("fin creerFeuilleStepControlParameters");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Ajout de toutes les lignes relatives aux parametres de controle pour les etapes
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesStepControlParameters(XSSFSheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol,
			Data data, String nomProjet) {
		
		// Rien d'indique si pas d'indication sur le materiel ou la methode
		try {

			String motCleName = "name";
			String motCleMatType = "material type";
			String motCleMetType = "method type";
			String motCleMeasuringInstrument = "measuring instrument";
			String motCleProcessingEquipment = "processing equipment";
			List<String> listMotsCleReserves = new ArrayList<String>();
			listMotsCleReserves.add(motCleName);
			listMotsCleReserves.add(motCleMatType);
			listMotsCleReserves.add(motCleMetType);
			listMotsCleReserves.add(motCleMeasuringInstrument);
			listMotsCleReserves.add(motCleProcessingEquipment);

			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size(); k++)
	    	{
				// Infos relatives au process
	    		GeneralFile generalFile = listGeneralFile.get(k);    		
	    		String processName = generalFile.processNameProperty().getValue();

	    		// Recup du dico entre la signature des etapes et le n° correspondant dans Excel.
				String processNum = generalFile.toString(); 
				if (!dicoProcessNum_dicoStepFileNum_NumExcel.containsKey(processNum))
					dicoProcessNum_dicoStepFileNum_NumExcel.put(processNum, new LinkedHashMap<String, String>());
				LinkedHashMap<String, String> dicoStepFileNum_NumExcel = 
						dicoProcessNum_dicoStepFileNum_NumExcel.get(processNum);
 
				// La construction du fichier ne tient plus compte des itineraires
				// car des memes etapes peuvent faire partie de plusieurs itineraires.
				// On ne parcourt plus la liste des itineraires, mais la liste de
				// toutes les etapes du process :
				for (StepFile sf : generalFile.getListStep())
				{
					// Infos relatives aux etapes
					String stepFileNum = sf.getFileName(); 
					String stepNumber = dicoStepFileNum_NumExcel.get(stepFileNum);
					String stepName = sf.getId();

					// Pour chaque Materiel et chaque Methode,
					List<MaterialMethodLinkPart> listMatMet = sf.getListMaterialMethodRO();
					int nbMatMet = listMatMet.size();
					for (int mm = 0; mm < nbMatMet; mm++)
					{
						MaterialMethodLinkPart mmlp = listMatMet.get(mm);
						ConsignPart consignePart = mmlp.getConsignPart();
						MaterialMethodPart mmp = mmlp.getMaterialMethodPartPart();
						String nom = mmp.getID().toString();
						KeyWords lpType = mmp.getPartType();
						String nomMat = lpType.equals(DataPartType.MATERIAL_RAW) ? nom : "";
						String nomMet = lpType.equals(DataPartType.METHOD_RAW) ? nom : "";

						// Liste des attributs des Materiels.						
						// Verification s'il y a des attributs de materiels ou methodes
						// a indiquer (autre que les attributs indiques au niveau du projet)
						SimpleTable st = consignePart.getDataTable();
						List<Map<KeyWords, ComplexField>> listLignes = st.getContent();
						int nbLignes = listLignes.size();

						// Recup des infos specifiques utilisateur
			    		for (int kk = 0; kk < nbLignes; kk++)
			    		{		    
				    		Map<KeyWords, ComplexField> ligne = listLignes.get(kk); 
				    		SimpleStringProperty attribut = ligne.get(SimpleTable.caractK).getValue();
				    		SimpleStringProperty obj = ligne.get(SimpleTable.ObjK).getValue();
				    		SimpleStringProperty val = ligne.get(SimpleTable.valK).getValue();
				    		SimpleStringProperty unit = ligne.get(SimpleTable.unitK).getValue();
				    		SimpleStringProperty comment = ligne.get(SimpleTable.commentK).getValue();
				    		
							if (!listMotsCleReserves.contains(attribut.getValue().toString()))
				    		{
					    		// Ajout d'1 ligne.
								Row row = sheet.createRow(i++);
								
								// Infos relatives au process repetees
								j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
								Cell cell = row.createCell(j);
								cell.setCellValue(processName);
		
								j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
								cell = row.createCell(j);
								cell.setCellValue(stepNumber);
								
								j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NAME);
								cell = row.createCell(j);
								cell.setCellValue(stepName);
				    			
								// Infos generales relatives aux Materiels et Methodes repetees
								j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
								cell = row.createCell(j);
								cell.setCellValue(nomMat);
					
								j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
								cell = row.createCell(j);
								cell.setCellValue(nomMet);
					
								// Infos specifiques utilisateur
								j = dicoIDIntitule_ICol.get(IDIntitule.ATTRIBUTE);
								cell = row.createCell(j);
								cell.setCellValue(attribut.getValue().toString());

								j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_OOI);
								cell = row.createCell(j);
								cell.setCellValue(obj.getValue().toString());

								j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_VALUE);
								cell = row.createCell(j);
								cell.setCellValue(val.getValue().toString());

								j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_UNIT);
								cell = row.createCell(j);
								cell.setCellValue(unit.getValue().toString());

								j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_COMMENT);
								cell = row.createCell(j);
								cell.setCellValue(comment.getValue().toString());
				    		}
			    		}
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Creation de la feuille "Itinerary"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */
	private void creerFeuilleItinerary(XSSFWorkbook wb, Data data) {

		creerFeuilleItinerary(wb, data, "");
		
	}

	/**
	 * Creation de la feuille "Itinerary"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleItinerary(XSSFWorkbook wb, Data data, String nomProjet) {

		try {

			log.info("creerFeuilleItinerary...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_NAME, "Process name");
			dicoIDIntitule_Intitule.put(IDIntitule.ITI_NAME, "Itinerary name");
			dicoIDIntitule_Intitule.put(IDIntitule.ITI_NUMBER, "Itinerary number");
			dicoIDIntitule_Intitule.put(IDIntitule.ITI_POI, "Product of interest");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_AGENT, "Step operator");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_SCALE, "Step scale");
			dicoIDIntitule_Intitule.put(IDIntitule.START_DATE, "Step date");
			dicoIDIntitule_Intitule.put(IDIntitule.TIME, "Step time");
			dicoIDIntitule_Intitule.put(IDIntitule.TIME_DUR, "Step time duration");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NUMBER, "Step number");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NEXT_STEP, "Next step");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_SUB_STEP, "Substep");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_TYPE, "Step type");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_NAME, "Step name");
			dicoIDIntitule_Intitule.put(IDIntitule.STEP_DESCR, "Step description");
	        dicoIDIntitule_Intitule.put(IDIntitule.MATERIAL_NAME, "Material name");
	        dicoIDIntitule_Intitule.put(IDIntitule.METHOD_NAME, "Method name");
	        dicoIDIntitule_Intitule.put(IDIntitule.INPUT_COMPOSITION_ID, "Input composition id");
	        dicoIDIntitule_Intitule.put(IDIntitule.OUTPUT_COMPOSITION_ID, "Output composition id");
		    
			String nomFeuille = "Itinerary";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
			
			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout d'1 ligne pour le projet
			// et MaJ de la liste des compositions.
			dicoProcessName_dicoCompositionFileId_CompositionFile.clear();
			dicoProcessName_listCompositionFileId.clear();
			ajouterLignesItinerary(sheet, dicoIDIntitule_ICol, data, nomProjet);

			log.info("fin creerFeuilleItinerary");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Ajout de toutes les lignes relatives aux donnees d'itineraires
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesItinerary(XSSFSheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data, String nomProjet) {
		
		try {

			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size(); k++)
	    	{
	    		// Drapeau indiquant le 1er itineraire
				boolean premItin = true;

				// Ajout d'1 ligne.
				Row row = sheet.createRow(i++);

				// Infos relatives au process
	    		GeneralFile generalFile = listGeneralFile.get(k);    		
	    		String processName = generalFile.processNameProperty().getValue();

	    		// Cas particulier si pas d'itineraire associe au process
				List<ItineraryFile> listItineraryFile = generalFile.getItinerary();
				int nbItin = listItineraryFile.size();
				if (nbItin == 0)
				{
					j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
					Cell cell = row.createCell(j);
					cell.setCellValue(processName);
				}
				
				// Attribution d'un n° a toutes les etapes du process (pour pouvoir
				// faire la liste des etapes suivantes chaque etape dans la suite).
				String processNum = generalFile.toString(); 
				if (!dicoProcessNum_dicoStepFileNum_NumExcel.containsKey(processNum))
					dicoProcessNum_dicoStepFileNum_NumExcel.put(processNum, new LinkedHashMap<String, String>());
				LinkedHashMap<String, String> dicoStepFileNum_NumExcel = 
						dicoProcessNum_dicoStepFileNum_NumExcel.get(processNum);
				int ss = 0;
				if (dicoStepFileNum_NumExcel.size() == 0) {
					for (StepFile sf : generalFile.getListStep()) {
						dicoStepFileNum_NumExcel.put(sf.getFileName(), String.valueOf(++ss));
					}
				}
				
				// Pour chaque itineraire du process,
				for (int ii = 0; ii < nbItin; ii++)
				{
					ItineraryFile itineraryFile = listItineraryFile.get(ii);					
					String itiName = itineraryFile.getItineraryName();
					
					if (itiName.startsWith(nomProjet))
					{	
						// 1 ligne par itineraire (la 1ere est deja creee)
						if (premItin)
							premItin = false;
						else
							row = sheet.createRow(i++);
	
						// Infos relatives au process repetees
						j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
						Cell cell = row.createCell(j);
						cell.setCellValue(processName);
	
						// Infos relatives a l'itineraire
						String poi = itineraryFile.getCProductsOfInterest().toString();
						String itiNumber = ((Integer)(ii + 1)).toString();
						
						// Cas particulier si pas d'etape associee a l'itineraire
						List<StepFile> listStepFile = itineraryFile.getListStep();
						int nbStep = listStepFile.size();
						if (nbStep == 0)
						{
							j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NAME);
							cell = row.createCell(j);
							cell.setCellValue(itiName);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
							cell = row.createCell(j);
							cell.setCellValue(itiNumber);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.ITI_POI);
							cell = row.createCell(j);
							cell.setCellValue(poi);
						}
						
						// Pour chaque etape de l'itineraire,
						for (ss = 0; ss < nbStep; ss++)
						{
							// 1 ligne par etape (la 1ere est deja creee)
							if (ss > 0)
								row = sheet.createRow(i++);
							
							// Infos relatives au process repetees
							j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
							cell = row.createCell(j);
							cell.setCellValue(processName);
	
							// Infos relatives a l'itineraire repetees
							j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NAME);
							cell = row.createCell(j);
							cell.setCellValue(itiName);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.ITI_NUMBER);
							cell = row.createCell(j);
							cell.setCellValue(itiNumber);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.ITI_POI);
							cell = row.createCell(j);
							cell.setCellValue(poi);
	
							// Infos relatives aux etapes
							StepFile stepFile = listStepFile.get(ss);
							String stepOperator = stepFile.getAgents();
							String stepScale = stepFile.getCScale().toString();
							String stepDate = stepFile.getDate();
							String stepTime = stepFile.getTime();
							String stepTimeDuration = stepFile.getDuration();
							String stepFileNum = stepFile.getFileName(); 
							String stepNumber = dicoStepFileNum_NumExcel.get(stepFileNum);
							String stepType = stepFile.getOntoType();
							String stepName = stepFile.getId();
							String stepDescription = stepFile.getDescription();
							List<MaterialMethodLinkPart> listMatMet = stepFile.getListMaterialMethodRO();
							String nomMat = construireNomMatet(listMatMet, DataPartType.MATERIAL_RAW);
							String nomMet = construireNomMatet(listMatMet, DataPartType.METHOD_RAW);
							HashMap<CompositionFile, Boolean> dicoCompositionFile_Sens = stepFile.getCompositionFile();
							String inputCompositionId = construireCompositionsIds(processName, dicoCompositionFile_Sens, true);
							String outputCompositionId = construireCompositionsIds(processName, dicoCompositionFile_Sens, false);
							String nextSteps = "";
							String substeps = "";
							
							// Construction du contenu de la cellule indiquant les etapes suivant chaque etape.
							// En meme temps construction de la liste des sous etapes.
							List<
								Pair<
									Pair<ComplexField, ObjectProperty<StepFile>>, 
									Pair<ComplexField, ObjectProperty<StepFile>>
									>
								>
								listLienEtapes = itineraryFile.getItinerary();
							for (Pair<
									Pair<ComplexField, ObjectProperty<StepFile>>, 
									Pair<ComplexField, ObjectProperty<StepFile>>
									> cplLienEtapes : listLienEtapes)
							{
								Pair<ComplexField, ObjectProperty<StepFile>>
								etp1 = cplLienEtapes.getKey();
								
								// S'il n'y a qu'1 etape dans l'itineraire, j'espere qu'elle est bien
								// dans la cle et pas la valeur... A VERIFIER
								
								ObjectProperty<StepFile> etp1OPSF = etp1 != null ? etp1.getValue() : null;
								StepFile sf1 = etp1OPSF != null ? etp1OPSF.get() : null;
								String nomStepFile1 = sf1 != null ? sf1.getFileName() : "";
								if (nomStepFile1.equals(stepFileNum))
								{
									Pair<ComplexField, ObjectProperty<StepFile>>
									etp2 = cplLienEtapes.getValue();
	
									ObjectProperty<StepFile> etp2OPSF = etp2 != null ? etp2.getValue() : null;
									StepFile sf2 = etp2OPSF != null ? etp2OPSF.get() : null;						
									String nomStepFile2 = sf2 != null ? sf2.getFileName() : "";
									if (!nomStepFile2.isEmpty())
									{
										// Construction de la chaine indiquant la liste d'etapes suivantes
										if (!nextSteps.isEmpty())
											nextSteps += "::";
										nextSteps += dicoStepFileNum_NumExcel.get(nomStepFile2);
									}
									
									// Construction de la liste des sous etapes.
									// Remarque : la chaine est construite en 1 fois,
									// donc des que la liste a ete lue inutile de le refaire
									if (substeps.isEmpty())
									{
										Set<StepFile> lstSS = sf1.getSubStep();
										for (var sbs : lstSS)
										{
											String nomSubStepFile = sbs != null ? sbs.getFileName() : "";
											if (!substeps.isEmpty())
												substeps += "::";
											substeps += dicoStepFileNum_NumExcel.get(nomSubStepFile);
										}
									}
								}
							}

							// Simplification des id de composition
							inputCompositionId = simplifierCompositionId(processName, inputCompositionId);
							outputCompositionId = simplifierCompositionId(processName, outputCompositionId);
							
							// Remplissage des lignes
							j = dicoIDIntitule_ICol.get(IDIntitule.START_DATE);
							cell = row.createCell(j, CellType.STRING);
							cell.setCellValue(stepDate);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.TIME);
							cell = row.createCell(j);
							cell.setCellValue(stepTime);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.TIME_DUR);
							cell = row.createCell(j);
							cell.setCellValue(stepTimeDuration);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_AGENT);
							cell = row.createCell(j);
							cell.setCellValue(stepOperator);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_SCALE);
							cell = row.createCell(j);
							cell.setCellValue(stepScale);

							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NUMBER);
							cell = row.createCell(j);
							cell.setCellValue(stepNumber);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NEXT_STEP);
							cell = row.createCell(j);
							cell.setCellValue(nextSteps);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_SUB_STEP);
							cell = row.createCell(j);
							cell.setCellValue(substeps);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_TYPE);
							cell = row.createCell(j);
							cell.setCellValue(stepType);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_NAME);
							cell = row.createCell(j);
							cell.setCellValue(stepName);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.STEP_DESCR);
							cell = row.createCell(j);
							cell.setCellValue(stepDescription);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.MATERIAL_NAME);
							cell = row.createCell(j);
							cell.setCellValue(nomMat);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.METHOD_NAME);
							cell = row.createCell(j);
							cell.setCellValue(nomMet);
												
							j = dicoIDIntitule_ICol.get(IDIntitule.INPUT_COMPOSITION_ID);
							cell = row.createCell(j);
							cell.setCellValue(inputCompositionId);
							
							j = dicoIDIntitule_ICol.get(IDIntitule.OUTPUT_COMPOSITION_ID);
							cell = row.createCell(j);
							cell.setCellValue(outputCompositionId);
						}
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}


	/**
	 * Creation de la feuille "Agents"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */
	private void creerFeuilleAgents(XSSFWorkbook wb, Data data, boolean matSinonMet) {
		
		creerFeuilleAgents(wb, data, "");

	}

	/**
	 * Creation de la feuille "Agents"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleAgents(XSSFWorkbook wb, Data data, String nomProjet) {
		
		try {

			log.info("creerFeuilleAgents...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
			dicoIDIntitule_Intitule.put(IDIntitule.AGENT_ORG, "Organization");
			dicoIDIntitule_Intitule.put(IDIntitule.AGENT_F_NAME, "Family name");
			dicoIDIntitule_Intitule.put(IDIntitule.AGENT_G_NAME, "Given name");
			dicoIDIntitule_Intitule.put(IDIntitule.AGENT_EMAIL, "eMail");
		    
			String nomFeuille = "Agents";
			XSSFSheet sheet = Tools.createSheet(wb, nomFeuille);

			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout d'1 ligne pour le projet
			ajouterLignesAGENTS(sheet, dicoIDIntitule_ICol, data, nomProjet);

			log.info("fin creerFeuilleAgents");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	/**
	 * Ajout de toutes les lignes relatives aux agents a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesAGENTS(Sheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data, String nomProjet) {

		try {

			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			// Plusieurs materiels ou methodes possibles
			ProjectFile projectFile = data.getProjectFile();
			List<HashMap<KeyWords, ComplexField>> listAgents = projectFile.getListAgent();
	    	log.info(projectFile.getCName().toString() + ". Nombre d'agents': " + ((Integer)listAgents.size()).toString());		    	
	    	for (int k = 0; k < listAgents.size(); k++)
	    	{
	    		// Pour chaque agent
				HashMap<KeyWords, ComplexField> agent = listAgents.get(k);
				String org = agent.get(ProjectAgentPart.agentOrganisationK).getValue().getValue();
				String fName = agent.get(ProjectAgentPart.agentFamilyNameK).getValue().getValue();
				String gName = agent.get(ProjectAgentPart.agentGivenNameK).getValue().getValue();
				String eMail = agent.get(ProjectAgentPart.agentMailK).getValue().getValue();

				// Tous les agents sont exportes, qu'ils soient utilises ou non dans les process
				Row row = sheet.createRow(i++);

				j = dicoIDIntitule_ICol.get(IDIntitule.AGENT_ORG);
				Cell cell = row.createCell(j);
				cell.setCellValue(org);
		
				j = dicoIDIntitule_ICol.get(IDIntitule.AGENT_F_NAME);
				cell = row.createCell(j);
				cell.setCellValue(fName);
		
				j = dicoIDIntitule_ICol.get(IDIntitule.AGENT_G_NAME);
				cell = row.createCell(j);
				cell.setCellValue(gName);
		
				j = dicoIDIntitule_ICol.get(IDIntitule.AGENT_EMAIL);
				cell = row.createCell(j);
				cell.setCellValue(eMail);
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Creation de la feuille "Material" ou "Method" en fonction du choix indique
	 * dans le parametre matSinonMet
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param matSinonMet : Drapeau indiquant si la feuille "Material" ou "Method" est creee.
	 */
	private void creerFeuilleMatMet(XSSFWorkbook wb, Data data, boolean matSinonMet) {
		
		creerFeuilleMatMet(wb, data, matSinonMet, "");

	}

	/**
	 * Creation de la feuille "Material" ou "Method" en fonction du choix indique
	 * dans le parametre matSinonMet
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param matSinonMet : Drapeau indiquant si la feuille "Material" ou "Method" est creee.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleMatMet(XSSFWorkbook wb, Data data, boolean matSinonMet, String nomProjet) {
		
		try {

			log.info("creerFeuilleMatMet...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
			dicoIDIntitule_Intitule.put(IDIntitule.MM_NAME, matSinonMet ? "Material name" : "Method name");
			dicoIDIntitule_Intitule.put(IDIntitule.MM_TYPE, matSinonMet ? "Material type" : "Method type");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTRIBUTE, "Attribute");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_OOI, "Object of interest");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_VALUE, "Value");
			dicoIDIntitule_Intitule.put(IDIntitule.ATTR_UNIT, "Unit");
		    dicoIDIntitule_Intitule.put(IDIntitule.ATTR_COMMENT, "Comment");
		    
			String nomFeuille = matSinonMet ? "Material" : "Method";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
			
			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout d'1 ligne pour le projet
			ajouterLignesMATMET(sheet, dicoIDIntitule_ICol, data, matSinonMet, nomProjet);

			log.info("fin creerFeuilleMatMet");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	/**
	 * Ajout de toutes les lignes relatives aux materiels ou methodes
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 * @param matSinonMet : Drapeau indiquant si la feuille concerne les materiels ou les methodes
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesMATMET(XSSFSheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data,
			boolean matSinonMet, String nomProjet) {

		try {

			String motCleName = "name";
			String motCleType = matSinonMet ? "material type" : "method type";
			String motCleMeasuringInstrument = "measuring instrument";
			String motCleProcessingEquipment = "processing equipment";
			List<String> listMotsCleReserves = new ArrayList<String>();
			listMotsCleReserves.add(motCleName);
			listMotsCleReserves.add(motCleType);
			listMotsCleReserves.add(motCleMeasuringInstrument);
			listMotsCleReserves.add(motCleProcessingEquipment);
			
			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			// Plusieurs materiels ou methodes possibles
			ProjectFile projectFile = data.getProjectFile();
			MaterialsMethodsFile matsMetsFile = projectFile.getData().getMaterialMethodFile();
			List<MaterialMethodPart> list = matSinonMet
					? matsMetsFile.getMaterialPart()
					: matsMetsFile.getMethodPart();
	    	log.info(projectFile.getCName().toString() + ". Nombre de " + (matSinonMet ? "matériels: " : "méthodes: ") + ((Integer)list.size()).toString());		    	
	    	for (int k = 0; k < list.size(); k++)
	    	{
	    		// Pour chaque materiel ou methode, plusieurs attributs possibles.
	    		// Les infos generales sont repetees autant de fois que d'attributs
	    		// Remarque : la facon de gerer les attributs est differente de celle de SD :
	    		// Pour SD pas de difference entre les "infos generales" dont on parle ici
	    		// et les autres attributs.
				MaterialMethodPart matMetPart = list.get(k);

				// Tous les M sont exportes, qu'ils soient utilises ou non dans les process
				Row row = sheet.createRow(i++);
				boolean prem = true;

				// Infos "generales"
				SimpleTable st = matMetPart.getDataTable();
				String matMetName = st.getComplexeValue(motCleName).toString();
				String type = st.getComplexeValue(motCleType).toString();

				// Verification qu'il y a des attributs autres que les generaux.
				List<Map<KeyWords, ComplexField>> listLignes = st.getContent();
				int nbLignes = listLignes.size();
				boolean pasDInfoSuppl = true;
				int kk = 0;
				while (pasDInfoSuppl && kk < nbLignes)
				{		    
					Map<KeyWords, ComplexField> ligne = listLignes.get(kk); 
					SimpleStringProperty attribut = ligne.get(SimpleTable.caractK).getValue();
					if (listMotsCleReserves.contains(attribut.getValue().toString()))
						kk++;
					else
						pasDInfoSuppl = false;					
				}
			
				// Cas particulier si pas d'attribut : qu'1 seule ligne a afficher.
				if (pasDInfoSuppl)
				{
					j = dicoIDIntitule_ICol.get(IDIntitule.MM_NAME);
					Cell cell = row.createCell(j);
					cell.setCellValue(matMetName);
			
					j = dicoIDIntitule_ICol.get(IDIntitule.MM_TYPE);
					cell = row.createCell(j);
					cell.setCellValue(type);
				}
				
				// Recup des infos specifiques utilisateur
				for (kk = 0; kk < nbLignes; kk++)
				{		    
					Map<KeyWords, ComplexField> ligne =listLignes.get(kk); 
					SimpleStringProperty attribut = ligne.get(SimpleTable.caractK).getValue();
					SimpleStringProperty obj = ligne.get(SimpleTable.ObjK).getValue();
					SimpleStringProperty val = ligne.get(SimpleTable.valK).getValue();
					SimpleStringProperty unit = ligne.get(SimpleTable.unitK).getValue();
					SimpleStringProperty comment = ligne.get(SimpleTable.commentK).getValue();
					
					if (!listMotsCleReserves.contains(attribut.getValue().toString()))
					{
						// 1 ligne par info specifique utilisateur (la 1ere est deja creee)
						if (prem)
							prem = false;
						else
							row = sheet.createRow(i++);
						
						// Infos generales repetees
						j = dicoIDIntitule_ICol.get(IDIntitule.MM_TYPE);
						Cell cell = row.createCell(j);
						cell.setCellValue(type);

						j = dicoIDIntitule_ICol.get(IDIntitule.MM_NAME);
						cell = row.createCell(j);
						cell.setCellValue(matMetName);
			
						// Infos specifiques utilisateur
						j = dicoIDIntitule_ICol.get(IDIntitule.ATTRIBUTE);
						cell = row.createCell(j);
						cell.setCellValue(attribut.getValue().toString());

						j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_OOI);
						cell = row.createCell(j);
						cell.setCellValue(obj.getValue().toString());

						j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_VALUE);
						cell = row.createCell(j);
						cell.setCellValue(val.getValue().toString());

						j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_UNIT);
						cell = row.createCell(j);
						cell.setCellValue(unit.getValue().toString());

						j = dicoIDIntitule_ICol.get(IDIntitule.ATTR_COMMENT);
						cell = row.createCell(j);
						cell.setCellValue(comment.getValue().toString());
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * ATTENTION : Specifique pour PB (export d'un projet en le decoupant dans plusieurs
	 * fichiers Excel en fonction des infos indiquees dans les noms des itineraires
	 * supposes sous la forme "nom_projet--nom_itineraire")
	 * Verification que la composition indiquee en parametre est utilisee dans le projet 
	 * dont le nom est passe en parametre
	 * @param data
	 * @param matMetPart
	 * @param nomProjet
	 * @return
	 */
	private boolean matMetUtiliseDansProjet(Data data, MaterialMethodPart matMetPart, String nomProjet) {
		
		boolean trouve = false;
		try {

			// Pour chaque process du projet
			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = projectFile.getData().getListGeneralFile();
	    	for (int k = 0; k < listGeneralFile.size() && !trouve; k++)
	    	{
				// Pour chaque itineraire du process,
	    		GeneralFile generalFile = listGeneralFile.get(k);    		
				List<ItineraryFile> listItineraryFile = generalFile.getItinerary();
				int nbItin = listItineraryFile.size();
				List<ObservationFile> listToutesObservationFile = new ArrayList<ObservationFile>();
				for (int ii = 0; ii < nbItin && !trouve; ii++)
				{
					ItineraryFile itineraryFile = listItineraryFile.get(ii);					
					String itiName = itineraryFile.getItineraryName();
					
					if (itiName.startsWith(nomProjet))
					{					
						// Verification si la mat ou met n'est pas utilise dans une etape de l'itineraire,
						List<StepFile> listStepFile = itineraryFile.getListStep();
						int nbStep = listStepFile.size();
						for (int ss = 0; ss < nbStep && !trouve; ss++)
						{
							StepFile stepFile = listStepFile.get(ss);
							List<MaterialMethodLinkPart> listMatMet = stepFile.getListMaterialMethodRO();
					        Iterator<MaterialMethodLinkPart> iterator = listMatMet.iterator();
					        while (iterator.hasNext() && !trouve)
					        	if (iterator.next().getMaterialMethodPartPart().equals(matMetPart))
									trouve = true;
						}
						
						// Verification si la mat ou met n'est pas utilise dans une observation
						if (!trouve)
						{
							// Construction de la liste des observations :
							// Observations d'itineraires + Observations d'etapes et de compositions
							// en prenant bien soin de ne pas compter plusieurs fois
							// les observations d'etapes clonees.
						List<ObservationFile> listObservationFile = new ArrayList<ObservationFile>();
							listObservationFile.addAll(itineraryFile.getListObservation());
							for (StepFile stepFile : itineraryFile.getListStep()) {
								for (ObservationFile of : stepFile.getObservationFiles()) {
									if (listToutesObservationFile.indexOf(of) < 0) {
										listObservationFile.add(of);
										listToutesObservationFile.add(of);
									}
								}
							}

							// Verification
							int nbObs = listObservationFile.size();
							for (int oo = 0; oo < nbObs && !trouve; oo++)
							{
								ObservationFile observationFile = listObservationFile.get(oo);
								List<MaterialMethodLinkPart> listMatMet = observationFile.getListMaterialMethodRO();
						        Iterator<MaterialMethodLinkPart> iterator = listMatMet.iterator();
						        while (iterator.hasNext() && !trouve)
						        	if (iterator.next().getMaterialMethodPartPart().equals(matMetPart))
										trouve = true;
							}
						}
					}
				}
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return trouve;
	}
	
	/**
	 * Creation de la feuille "Project-process"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 */
	private void creerFeuilleProjectProcess(XSSFWorkbook wb, Data data) {

		creerFeuilleProjectProcess(wb, data, "");

	}

	/**
	 * Creation de la feuille "Project-process"
	 * @param wb : Le fichier Excel dans lequel la feuille est ajoute
	 * @param data : Le jeu de donnees PO2 source.
	 * @param nomProjet : Nom du projet dont les donnees sont extraites dans le jeu
	 * (specifique pour le depannage de PB. Non utilise en routine)
	 */
	private void creerFeuilleProjectProcess(XSSFWorkbook wb, Data data, String nomProjet) {

		try {

			log.info("creerFeuilleProjectProcess...");

			LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule = new LinkedHashMap<IDIntitule, String>();
			LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol = new LinkedHashMap<IDIntitule, Integer>();
			
			dicoIDIntitule_Intitule.put(IDIntitule.PROJECT_NAME, "Project name");
			dicoIDIntitule_Intitule.put(IDIntitule.PROJECT_CONTACT, "Contact");
			dicoIDIntitule_Intitule.put(IDIntitule.PROJECT_FUNDING, "Funding");
			dicoIDIntitule_Intitule.put(IDIntitule.PROJECT_DESCRIPTION, "Project description");
			dicoIDIntitule_Intitule.put(IDIntitule.EXTERNAL_LINK, "External link");
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_TYPE, "Process type");
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_NAME, "Process name");
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_REPLICATE, "Process replicate");
		    dicoIDIntitule_Intitule.put(IDIntitule.START_DATE, "Start date");
		    dicoIDIntitule_Intitule.put(IDIntitule.END_DATE, "End date");
		    dicoIDIntitule_Intitule.put(IDIntitule.PROCESS_DESCRIPTION, "Process description");
		    
			String nomFeuille = "Project-Process";
			XSSFSheet sheet = Tools.createSheet(wb,nomFeuille);
			
			// Ajout d'1 ligne pour les intitules
			ajouterLigneIntitules(sheet, dicoIDIntitule_Intitule, dicoIDIntitule_ICol);
			
			// Ajout d'1 ligne pour le projet
			ajouterLignesProjetProcess(sheet, dicoIDIntitule_ICol, data, nomProjet);

			log.info("fin creerFeuilleProjectProcess");
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Ajout de toutes les lignes relatives au projet et ses process
	 * a partir de l'objet Data passe en parametre
	 * dans la feuille passee en parametre
	 * ATTENTION : Le parametre nomProjet a ete ajoute uniquement pour depanner PB
	 * (export d'un projet dans plusieurs fichiers Excel comme s'il s'agissait de
	 * plusieurs projets dont les noms sont supposes etre indiques dans les noms
	 * des itineraires sous la forme "nomProjet--nomItineraire).
	 * En temps normal, ce parametre doit etre = "" 
	 * @param sheet : La feuille Excel completee des donnees a partir de la 2eme ligne
	 * @param dicoIDIntitule_ICol : Le dictionnaire des indices des colonnes ou inserer
	 * les donnees en fonction de l'ID des intitules
	 * @param data : L'objet PO2 qui contient toutes les donnees du projet
	 * @param nomProjet : Le nom du projet pour lequel les donnees doivent etre ajoutees.
	 * Toutes les donnees sont ajoutees si = ""
	 */
	private void ajouterLignesProjetProcess(XSSFSheet sheet, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol, Data data, String nomProjet) {

		try {
			
			// Ajout a partir de la 2eme ligne de la feuille.
			int i = 1, j;

			ProjectFile projectFile = data.getProjectFile();
			List<GeneralFile> listGeneralFile = data.getListGeneralFile();
	    	log.info(projectFile.getCName().toString() + ". Nombre de process: " + ((Integer)listGeneralFile.size()).toString());		    	
	    	for (int k = 0; k < listGeneralFile.size(); k++)
	    	{
	    		// Les infos du projet sont repetees autant de fois que de process.
				Row row = sheet.createRow(i++);

				// Infos relatives au projet.
				j = dicoIDIntitule_ICol.get(IDIntitule.PROJECT_NAME);
				Cell cell = row.createCell(j);
				//String projectName = nomProjet.isBlank() ? projectFile.getCName().toString() : nomProjet;  
				String projectName = nomProjet.isBlank() ? projectFile.getNameProperty().get() : nomProjet;
				cell.setCellValue(projectName);
	
				j = dicoIDIntitule_ICol.get(IDIntitule.PROJECT_CONTACT);
				cell = row.createCell(j);
//				cell.setCellValue(projectFile.getContactProperty().get().get(ProjectAgentPart.agentFamilyNameK).getValue().getValue());
				cell.setCellValue(Tools.agentToString(projectFile.getContactProperty().getValue()));
//				cell.setCellValue(projectFile.getContactName());
	
				j = dicoIDIntitule_ICol.get(IDIntitule.PROJECT_FUNDING);
				cell = row.createCell(j);
				cell.setCellValue(Tools.agentToString(projectFile.getFundingProperty().getValue()));
	
				//j = dicoIDIntitule_ICol.get(IDIntitule.CONTACT_MAIL);
				//cell = row.createCell(j);
				//cell.setCellValue(projectFile.getContactProperty().get().get(ProjectAgentPart.agentMailK).getValue().getValue());
//				cell.setCellValue(projectFile.getContactMail());

				j = dicoIDIntitule_ICol.get(IDIntitule.PROJECT_DESCRIPTION);
				cell = row.createCell(j);
				cell.setCellValue(projectFile.getCDescription().toString());
	
				j = dicoIDIntitule_ICol.get(IDIntitule.EXTERNAL_LINK);
				cell = row.createCell(j);
				//cell.setCellValue(projectFile.getCExternalLinks().toString());
				cell.setCellValue(String.join("::", projectFile.getExternalLinks()));

				// Infos relatives aux process du projet
				// L'appel a analyseProcess() construit tous les objets associes au process.
				// Mais si on l'appelle plusieurs fois de suite, le nb d'objets itineraires
				// est multiplie d'autant.
				// Ici pas besoin de s'occuper de ca car l'appel a deja ete fait avant.
	    		GeneralFile generalFile = listGeneralFile.get(k);
	    		//if (nomProjet.isBlank())
	    		//	data.analyseProcess(generalFile);
	
	    		j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_TYPE);
				cell = row.createCell(j);
				cell.setCellValue(generalFile.getOntoType());

				j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_NAME);
				cell = row.createCell(j);
				cell.setCellValue(generalFile.getCTitle().toString());

				j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_REPLICATE);
				cell = row.createCell(j);
				cell.setCellValue(generalFile.getReplicateAsString());

				j = dicoIDIntitule_ICol.get(IDIntitule.START_DATE);
				cell = row.createCell(j);
				cell.setCellValue(generalFile.getCStartDate().toString());

				j = dicoIDIntitule_ICol.get(IDIntitule.END_DATE);
				cell = row.createCell(j);
				cell.setCellValue(generalFile.getCEndDate().toString());

				j = dicoIDIntitule_ICol.get(IDIntitule.PROCESS_DESCRIPTION);
				cell = row.createCell(j);
				cell.setCellValue(generalFile.getCDescription().toString());
	    	}

			log.info("2eme ligne de donnees : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * - Construction d'une liste d'id d'objets CompositionFile sous la forme idCF1::idCF2::...
	 * pour le sens (input ou output) indique en parametre.
	 * - Mise a jour du dictionnaire, en fonction du nom de process
	 * du dictionnaire des objets CompositionFile en fonction de leur id.
	 * Ce dictionnaire contient donc toutes les compositions du projet.
	 * Ce dictionnaire est utile pour faire les liste des compositionFile lors de la construction
	 * de la feuille composition.
	 * @param processName : Nom du process
	 * @param dicoCompositionFile_Sens : dictionnaire du sens des compositionFile en fonction des compositionFile
	 * utilise pour construire la liste retournee, et pour completer le dico
	 * @param inputSinonOutput : sens des compositions dont on veut la liste
	 * @return : chaine d'id d'objets CompositionFile sous la forme idCF1::idCF2::... correspondant
	 * au dictionnaire et au sens passes en parametre.
	 */
	private String construireCompositionsIds (
			String processName,
			HashMap<CompositionFile, Boolean> dicoCompositionFile_Sens, 
			Boolean inputSinonOutput) {

		String compositionsIds = "";
		
		try {
			for (Map.Entry<CompositionFile, Boolean> cplCompositionFile_Sens : dicoCompositionFile_Sens.entrySet())
			{
				CompositionFile compositionFile = cplCompositionFile_Sens.getKey();
				Boolean sens = cplCompositionFile_Sens.getValue();
				//int lng = "fr.inrae.po2engine.model.dataModel.CompositionFile@".length();
				String compositionFileId = compositionFile.getFileName();
				//if (compositionFileId.length() > lng)
				//	compositionFileId = compositionFileId.substring(lng); 

				// Cette methode n'est pas la seule appelee pour generer un id de composition :
				// Par exemple la methode ObservationFile.getCFoi() est parfois aussi appelee.
				// Pour rester coherent avec cette methode, un id sous la meme forme est construit,
				// et la simplification de l'id est faite (plus tard) dans simplifierCompositionId ()
				
				if (sens == inputSinonOutput)
				{
					// Si plusieurs materiels ou methodes, separation par "::"
					if (compositionsIds.isEmpty())
						compositionsIds = compositionFileId;
					else
						compositionsIds = compositionsIds + "::" + compositionFileId;
				}
			
				if (!dicoProcessName_dicoCompositionFileId_CompositionFile.containsKey(processName))
					dicoProcessName_dicoCompositionFileId_CompositionFile.put(processName, new LinkedHashMap<String, CompositionFile>());
				LinkedHashMap<String, CompositionFile> dicoCompositionFileId_CompositionFile = dicoProcessName_dicoCompositionFileId_CompositionFile.get(processName);
				if (!dicoCompositionFileId_CompositionFile.containsKey(compositionFileId))
					dicoCompositionFileId_CompositionFile.put(compositionFileId, compositionFile);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return compositionsIds;
	}

	/**
	 * Retourne une chaine construite a partir des materiels ou methodes de la liste
	 * passee en parametre.
	 * @param listMatMet : Liste d'objets a partir desquels les noms des materiels
	 * ou methodes sont obtenus.
	 * @param matOuMet : Drapeau indiquant s'il s'agit d'un materiel ou d'une methode 
	 * @return : chaine construite sous la forme mat1::mat2::...::matN (ou idem met)
	 */
	private String construireNomMatet(List<MaterialMethodLinkPart> listMatMet, KeyWords matOuMet) {

		String nomMatOuMet = "";
		
		try {
			for (MaterialMethodLinkPart mmlp : listMatMet)
			{
				MaterialMethodPart mmp = mmlp.getMaterialMethodPartPart();
				String nom = mmp.getID().toString();
				KeyWords lpType = mmp.getPartType();
				if (lpType.equals(matOuMet))
				{
					// Si plusieurs materiels ou methodes, separation par "::"
					if (nomMatOuMet.isEmpty())
						nomMatOuMet = nom;
					else
						nomMatOuMet = nomMatOuMet + "::" + nom;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return nomMatOuMet;
	}

	/**
	 * Ajout d'1 ligne d'intitules au debut de la feuille Excel passee en parametre.
	 * La liste des intitules a ajouter est extraite du dictionnaire (des intitules
	 * en fonction de leur ID) passe en parametre.
	 * Le dictionnaire de l'indice des colonnes en fonction de l'ID est mis a jour. 
	 * @param sheet : La feuille sur laquelle la ligne est ajoutee
	 * @param dicoIDIntitule_Intitule : Le dictionnaire d'ou est extraite
	 * la liste des intitules a ajouter
	 * @param dicoIDIntitule_ICol : Le dictionnaire mis à jour des indices des colonnes
	 * des intitules en fonction de leur ID.
	 */
	private void ajouterLigneIntitules(XSSFSheet sheet, LinkedHashMap<IDIntitule, String> dicoIDIntitule_Intitule, LinkedHashMap<IDIntitule, Integer> dicoIDIntitule_ICol) {

		try {
			
			// Ajout sur la 1ere ligne de la feuille
			Row row = sheet.createRow(0);
			
			// Pour chaque couple du dico des intitules en fonction des id,
			int j = 0;
	        Iterator<Map.Entry<IDIntitule, String>> iterator = dicoIDIntitule_Intitule.entrySet().iterator();
	        while (iterator.hasNext())
	        {
	        	// Ajout des intitules dans l'ordre du dico
	        	Map.Entry<IDIntitule, String> cplIDIntitule_Intitule = iterator.next();
	        	IDIntitule id = cplIDIntitule_Intitule.getKey();
	        	String intitule = cplIDIntitule_Intitule.getValue();
				Cell cell = row.createCell(j);
				cell.setCellValue(intitule);
				
				// MaJ du dictionnaire des indices des colonnes en fonction des id des intitules.
				dicoIDIntitule_ICol.put(id, j++);
			}
			log.info("1ere ligne intitule : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Ajout d'intitules manquants sur la 1ere ligne de la feuille Excel
	 * passee en parametre.
	 * @param sheet : La feuille sur laquelle la 1ere ligne est completee
	 * @param nbIntitules : Nombre d'intitules initialement sur la 1ere ligne
	 * @param nbIntitulesSuppl : Nombre d'intitules supplementaires a ajouter
	 * @param dicoIDIntituleVar_Intitule : Le dictionnaire d'ou est extraite
	 * la liste des intitules a ajouter 
	 */
	private void completerLigneIntitules(XSSFSheet sheet, int nbIntitules, int nbIntitulesSuppl,
			LinkedHashMap<IDIntitule, String> dicoIDIntituleVar_Intitule) {

		try {
			
			// La 1ere ligne de la feuille est completee
			Row row = sheet.getRow(0);
			
			// Pour chaque couple du dico des intitules en fonction des id,
			int j = nbIntitules;
			for (int cc = 0; cc < nbIntitulesSuppl; cc++)
				for (Map.Entry<IDIntitule, String> cplIDIntitule_Intitule : dicoIDIntituleVar_Intitule.entrySet())
		        {
		        	// Ajout des intitules dans l'ordre du dico
		        	String intitule = cplIDIntitule_Intitule.getValue();
					Cell cell = row.createCell(j++);
					cell.setCellValue(intitule);
				}
			log.info("1ere ligne intitule : OK.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Decodage de la chaine passee en parametre supposee etre sous la forme
	 * id1 ou id1::id2::...
	 * La derniere chaine de caractere de la liste est retournee. 
	 * @param compositionId
	 * @return
	 */
	private String decoderCompositionId(String compositionId) {

    	String[] listeCompositionId = compositionId.split("::");
    	int nb = listeCompositionId.length;  
    	if (nb > 1)
    		compositionId = listeCompositionId[nb - 1];
    	
    	return compositionId;
	}

	/**
	 * Simplification du code de la composition dont l'id (compliquee) est passee en parametre.
	 * L'id en parametre est supposee sous la forme "composition_xxxxxxxxxxxxx::composition_yyyyyyyyyyyy..." ou egale a "step".
	 * Lorsque l'id vaut "step", elle n'est pas transformee.
	 * @param processName
	 * @param compositionId
	 * @return
	 */
	private String simplifierCompositionId(String processName, String compositionId) {
		
		if (compositionId != "step" && !compositionId.isBlank())
		{
			if (!dicoProcessName_listCompositionFileId.containsKey(processName))
				dicoProcessName_listCompositionFileId.put(processName, new ArrayList<String> ());
			
	    	String[] listeCompositionId = compositionId.split("::");
	    	compositionId = "";
	    	int nb = listeCompositionId.length;  
	    	for (int k = 0; k < nb; k++)
	    	{
	    		String id = listeCompositionId[k];
				if (!dicoProcessName_listCompositionFileId.get(processName).contains(id))
					dicoProcessName_listCompositionFileId.get(processName).add(id);
				id = ((Integer)(1 + dicoProcessName_listCompositionFileId.get(processName).indexOf(id))).toString();					
				if (compositionId.isEmpty())
					compositionId = id;
				else
					compositionId = compositionId + "::" + id;

	    	}
		}
		
		return compositionId;
	}
}


// Exemples de code avec les expressions lambda :
/* 
// Correspondance entre le nom des compositions de l'etape et leur ID unique
// (dans l'hypothese ou plusieurs compositions de meme nom ne peuvent
// pas avoir le meme nom dans une etape, ce qui n'est pas vrai dans PO2Manager)
LinkedHashMap<String, String> dicoCompositionFileName_CompositionFileId = 
		stepFile.getCompositionFile()
		.entrySet()
		.stream()
		.collect(java.util.stream.Collectors.toMap(
				entry -> entry.getKey().getCompositionID(), 										
				entry -> entry.getKey().getFileName(),//toString(),
                (a, b) -> { throw new AssertionError(); },
                LinkedHashMap::new));

						LinkedHashMap<CompositionFile, Boolean> dicoIngrTrie = 
								dicoIngr.entrySet()
								.stream()
								.sorted(
										(t2, t1) -> {
											return t2.getKey().getCompositionID().compareTo(t1.getKey().getCompositionID());
											
								            //if (t2.getKey().getCompositionID().compareTo(t1.getKey().getCompositionID()) != 0)
								            //    return t2.getKey().getCompositionID().compareTo(t1.getKey().getCompositionID());
								            //else if (t2.getValue() && !t1.getValue())
								            //    return +1;
								            // else if (!t2.getValue() && t1.getValue())
								            //    return -1;
								            // else
								            //    return 0;							            
										})
								.collect(java.util.stream.Collectors.toMap(
										Map.Entry::getKey, 
										Map.Entry::getValue,
						                (a, b) -> { throw new AssertionError(); },
						                LinkedHashMap::new));
*/
