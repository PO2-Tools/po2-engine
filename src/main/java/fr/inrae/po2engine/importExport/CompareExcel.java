package fr.inrae.po2engine.importExport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import fr.inrae.po2engine.exception.TechnicalException;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;

public class CompareExcel {

    TreeMap<String, ArrayList<String>> dicoNomFeuille_listLignes = new TreeMap<String, ArrayList<String>>();

    public static TreeMap<String, ArrayList<String>> lireFichierExcel(File file) {

        TreeMap<String, ArrayList<String>> dicoNomFeuille_listLignes = new TreeMap<String, ArrayList<String>>();
        StringBuilder sb;
        
		try (InputStream stream = new FileInputStream(file))
		{
	        Workbook workbook = WorkbookFactory.create(stream);
            int nbFeuilles = workbook.getNumberOfSheets();

            // Parcours des feuilles
            for (int iSheet = 0; iSheet < nbFeuilles; iSheet++) {
                Sheet sheet = workbook.getSheetAt(iSheet);
                String nomFeuille = sheet.getSheetName();
                dicoNomFeuille_listLignes.put(nomFeuille, new ArrayList<>());

                // Parcours des lignes
                int iDebLigne = sheet.getFirstRowNum();
                int iFinLigne = sheet.getLastRowNum();
                for (int iLigne = iDebLigne; iLigne <= iFinLigne; iLigne++) {
                    Row row = sheet.getRow(iLigne);
                    if (row != null) {
                        int iDebCol = row.getFirstCellNum();
                        int iFinCol = row.getLastCellNum();

                        // Parcours des colonnes.
                        sb = new StringBuilder();
                        for (int iCol = iDebCol; iCol < iFinCol; iCol++) {
                            Cell cell = row.getCell(iCol);
                            if (cell != null) {
                                switch (cell.getCellType()) {
                                case BLANK:
                                    sb.append("\t");
                                    break;

                                case BOOLEAN:
                                    sb.append(String.format("%d\t", cell.getBooleanCellValue()));
                                    break;

                                case ERROR:
                                    sb.append(String.format("%d", cell.getErrorCellValue()));
                                    break;

                                case NUMERIC:
                                    sb.append(String.format("%f", cell.getNumericCellValue()));
                                    break;

                                case STRING:
                                    sb.append(String.format("%s", cell.getStringCellValue()));
                                    break;

                                case FORMULA:
                                default:
                                    sb.append(String.format("%s", cell.getCellFormula()));
                                }
                            }
                        }
                        dicoNomFeuille_listLignes.get(nomFeuille).add(sb.toString());
                    }
                }
            }
                            
	        stream.close();

        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return dicoNomFeuille_listLignes;
    }

    public static Report compareFiles(File file1, File file2) {
        Report ret = new Report();
        StringBuilder sb = new StringBuilder();
        boolean cont = true;

        TreeMap<String, ArrayList<String>> dico1NomFeuille_listLignes = lireFichierExcel(file1);
        TreeMap<String, ArrayList<String>> dico2NomFeuille_listLignes = lireFichierExcel(file2);

        int nbFeuilles = dico1NomFeuille_listLignes.size();

        if (nbFeuilles != dico2NomFeuille_listLignes.size()) {
            sb.append("Nombre de feuilles différent");
            cont = false;
        }
        
        // Parcours des feuilles
        for (Map.Entry<String, ArrayList<String>> element : dico1NomFeuille_listLignes.entrySet()) {

            String nomFeuille = element.getKey();
            if (!dico2NomFeuille_listLignes.containsKey(nomFeuille)) {
                sb.append(String.format("Feuille %s pas trouvée dans 1 des 2 fichiers", nomFeuille));
                cont = false;
                break;
            }
            else {
                ArrayList<String> list1Lignes = dico1NomFeuille_listLignes.get(nomFeuille); 
                ArrayList<String> list2Lignes = dico2NomFeuille_listLignes.get(nomFeuille); 

                for (String ligne : list1Lignes) {

                    if (!list2Lignes.contains(ligne)) {
                        sb.append(String.format("ligne \"%s\" pas trouvée dans 1 des 2 fichiers", ligne));
                        cont = false;
                        break;
                    }        
                }
            }
        }

        if (!cont)
            ret.addError(sb.toString());

        return ret;
    }

}
