package fr.inrae.po2engine.importExport;

import fr.inrae.po2engine.exception.TechnicalException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.OntoData;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.MaterialMethodPart;
import fr.inrae.po2engine.utils.Report;
import javafx.beans.property.SimpleDoubleProperty;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImportDataAtWeb implements IImportUsingPO2Engine {
	
	public static LinkedHashMap<String, String> dicoCaracNOK_CaracOK = new LinkedHashMap<String, String>() {{
		put("é", "e");
		put("\'", "");
		put(",", "");
		put(".", "");
		put("·", "");
		put("\\", " ");
		put("/", " ");
		put("(", "_");
		put(")", "_");
		put(":", "");
		put(";",  "");
		put("µ", "u");
		put("μ", "u");
		put("°", "_");
		put("%", "_");
	}};
	

	public ImportDataAtWeb() {
		// Initialisation du dictionnaire pour corriger les unites incorrectes.
		dicoNomUnitesIncorrect_Correct.put("Micrometer", "um");
		dicoNomUnitesIncorrect_Correct.put("µm", "um");
		dicoNomUnitesIncorrect_Correct.put("One", "1");
		dicoNomUnitesIncorrect_Correct.put("°C", "Cel");
		
		// Dictionnaire permettant de contourner le probleme des longs Title
		// dans les noms de projets (qui peuvent poser probleme au moment
		// de la creation des chemins dans Windows)
	}


	protected static ProjectFile creerProjet(String nomProjet) throws Exception {

		ProjectFile projectFile = null;


			log.info("Avant listData...");

			// Suppression du projet du meme nom, s'il existe
			try {

				CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
		    	Map<String, OntoData> dico = Datas.getDatas();
		    	int lng = dico.size();
		    	log.info(((Integer)lng).toString() + " projet(s).");

		    	// Parcours de la liste des projets sur le serveur si on est connecte au reseau,
		    	// ou localement sinon.
		    	Iterator<Map.Entry<String, OntoData>> entryIterator = dico.entrySet().iterator();
		    	lng = 0;
		    	boolean trouve = false;
				while(!trouve && entryIterator.hasNext())
				{
					Map.Entry<String, OntoData> element = entryIterator.next();
					String nom = element.getKey();

					// Recherche du projet dont le nom correspond.
					if (nom.equals(nomProjet))
						trouve = true;
				}
				if (trouve)
				{
			    	Data data = Datas.getData(nomProjet);
			    	if (data != null)
			    	{
			    		 //data.load();
			    		//DataTools.analyseFiles(data);
			    		data.lockMode();
			    		CloudConnector.removeProject(data);
			    		data.unlockMode();
			    	}
					//CloudConnector.removeProject(nomProjet);
				}



		    	//Data data = Datas.getData(nomProjet);
		    	//if (data != null)
		    	{
		    		//data.load();
		    		//DataTools.analyseFiles(data);
		    		//data.lockMode();
		    		//CloudConnector.removeProject(nomProjet);
		    		//data.unlockMode();
		    	}
			}
			catch (Exception e)
			{
				throw new TechnicalException("creerProjet :" + e.getMessage());
			}


	    	// modif du 30.09.22
			projectFile = CloudConnector.createNewProject(nomProjet, true);

	    	Data data = projectFile.getData();
	        int k = 0;
	        boolean ok = false;
	        String msg = "";
	        do
	        {
		        try {
		        	data.lockMode();
		        	ok = true;
		        }
				catch (Exception e)
				{
					k++;
					ok = false;
					msg = e.getMessage();
					log.info("lockmode, essai " + ((Integer)k).toString());
				}
	        } while (!ok && k < 100);

	        if (!ok)
	        	throw new TechnicalException("lireCelluleDate :" + msg);

			log.info("Apres gData...");


		return projectFile;
		/*
		// Nouveau jeu de données.
		Data data = recupData (nomProjet, localSinonDistant);
	    data.load();
	    DataTools.analyseFiles(data);
	    if (!localSinonDistant)
	    {
	    	data.lockMode();

	        // Suppression du projet "default ()" cree automatiquement mais qui ne serta rien.
	        //GeneralFile first = data.getListGeneralFile().stream().findFirst().orElse(null);
	        //if(first != null)
	        //    first.removeFile();
	    }

	    // Nouveau projet
	    ProjectFile projectFile = data.getProjectFile();
		*/
	}

	/**
	 * Recherche de l'objet de type MaterialMethodPart dans la liste passee en parametre
	 * et caracterise par la chaine de caracteres passee en parametre.
	 * Retourne null si rien n'est trouve.
	 * @param lstMatMetProjet
	 * @param idMatMet
	 * @return
	 * @throws Exception
	 */
	protected static MaterialMethodPart getMaterialMethodPart(ArrayList<MaterialMethodPart> lstMatMetProjet, String idMatMet) throws Exception {

		MaterialMethodPart matMetPart = null;

		try
		{
			// Verifications
			if (lstMatMetProjet == null)
				throw new TechnicalException("La liste ne doit pas etre nulle");
			else if (idMatMet == null || idMatMet.isEmpty())
				throw new TechnicalException("Le nom du materiel ou de la methode n'est pas valide");
					
			// Recherche de l'objet correspondant (dont le nom de projet correspond) dans la liste
			boolean trouve = false;
			Iterator<MaterialMethodPart> iterator = lstMatMetProjet.iterator();
			while (!trouve && iterator.hasNext())
			{
				MaterialMethodPart mmp = iterator.next();
				String id = mmp.getID().getValue().get();
				if (id.equals(idMatMet))
				{
					trouve = true;
					matMetPart = mmp;
				}
			}
		} 
		catch (Exception e) 
		{
			log.error("GetGeneralFile : Erreur due a " + e.getMessage());
			throw new TechnicalException(e.getMessage());
		}	
		
		return matMetPart;
	}



	/**
	 * Lecture du contenu de la cellule passee en parametre.
	 * Les autres parametres (ligne, colonne) sont justes la en cas d'exception
	 * pour donner des infos.
	 * @param row
	 * @param iLigne
	 * @param iCol
	 * @return
	 * @throws Exception
	 */
	protected static String lireCellule (Row row, int iLigne, int iCol) throws Exception {

		return lireCellule (row, iLigne, iCol, false);
	}

	/**
	 * Retourne une chaine correspondant au contenu de la cellule dont la ligne et l'indice
	 * de la colonne sont passes en parametre.
	 * @param row : La ligne dans laquelle la donnee est lue
	 * @param iLigne : Pour les messages d'erreur, le n° de ligne correspondant
	 * @param iCol : L'indice de la colonne pour laquelle la donnee doit etre lue
	 * @param date : Drapeau indiquant si la cellule contient une date
	 * @return : Chaine de caractere correspondant au contenu d'une cellule Excel.
	 * @throws Exception
	 */
	protected static String lireCellule (Row row, int iLigne, int iCol, boolean date) throws Exception {

		String ret = "";

		Cell cell = iCol >= 0 
				? (row != null ? row.getCell(iCol) : null) 
				: null;
		if (cell != null)
		{
			// Pour les formules, voir :
			// https://stackoverflow.com/questions/22992758/how-to-get-the-formula-cell-valuedata-using-apache-poi-3-1
			CellType cellType = cell.getCellType();
			if (cellType == CellType.FORMULA)
				cellType = cell.getCachedFormulaResultType();
	        
			if (cellType == CellType.NUMERIC)
			{
				// Pour la conversion reel->String la plus courte possible.
				//NumberFormat nf = NumberFormat.getNumberInstance();
				//nf.setGroupingUsed(false);
				////nf.setMaximumFractionDigits(2);

				// Pour que les nombre en notation scientifique soient correctement convertis
				// car le nf.format() ne marche pas dans ce cas.
				DataFormatter formatter = new DataFormatter(java.util.Locale.FRANCE);
				//ret = nf.format(cell.getNumericCellValue());
				ret = formatter.formatCellValue(cell);
				ret = ret.replace(',', '.');

				// Cas particulier si l'utilisateur veut mettre le resultat sous forme de date.
				if (date)
				{
					//HSSFDateUtil.isCellDateFormatted(cell);
					String style = cell.getCellStyle().getDataFormatString();
					if (!style.equals("General"))
					{
						Date javaDate = DateUtil.getJavaDate(Double.parseDouble(ret));
						ret = new SimpleDateFormat("yyyy-MM-dd").format(javaDate);
					}
				}
			}
	        else if (cellType == CellType.STRING)
	            ret = cell.getStringCellValue().trim();
	        else if (cellType == CellType.BOOLEAN)
	            ret = String.valueOf(cell.getBooleanCellValue());
	        else if (cellType == CellType.BLANK);
	        else if (cellType == CellType.ERROR);
	        else 
	        	throw new TechnicalException("Ligne " + iLigne + ", colonne \"" + iCol + "\", type de cellule " + cellType + " inconnu");
		}
		
		return ret;
	}
	
	/**
	 * Remplacement des caractères spéciaux du dictionnaire dicoCaracNOK_CaracOK
	 * par ceux indiques dans le meme dictionnaire.
	 * @param nom
	 * @return
	 */
	protected static String getNomCorrige(String nom) {
		
		for (Map.Entry<String, String> cplCaracNOK_CaracOK : dicoCaracNOK_CaracOK.entrySet())
			nom = nom.replace(cplCaracNOK_CaracOK.getKey(), cplCaracNOK_CaracOK.getValue());
		nom = nom.trim();

		return nom;
	}
	
	/**
	 * Retourne la liste des caracteres speciaux trouves dans la chaine
	 * passee en parametre.
	 * @param nom
	 * @return
	 */
	protected static List<String> listeCaracteresInterdits(String nom) {
		
		List<String> listeRet = new java.util.ArrayList<String>();
		for (String car : dicoCaracNOK_CaracOK.keySet())
		{
			int k = nom.indexOf(car); 
			if (k >= 0 && listeRet.indexOf(car) < 0)
				listeRet.add(car);
		}

		return listeRet;
	}


	/**
	 * Recherche de l'objet de type GeneralFile dans la liste passee en parametre
	 * et caracterise par la chaine de caracteres passee en parametre.
	 * Retourne null si rien n'est trouve.
	 * @param lstGeneralFile
	 * @param processName
	 * @return
	 * @throws Exception
	 */
	protected static GeneralFile getGeneralFile(ArrayList<GeneralFile> lstGeneralFile, String processName) throws Exception {

		GeneralFile generalFile = null;

		try
		{
			// Verifications
			if (lstGeneralFile == null)
				throw new TechnicalException("La liste ne doit pas etre nulle");
			else if (processName == null || processName.isEmpty())
				throw new TechnicalException("Le nom du projet n'est pas valide");
					
			// Recherche de l'objet correspondant (dont le nom de projet correspond) dans la liste
			processName = processName.trim();
			boolean trouve = false;
			Iterator<GeneralFile> iterator = lstGeneralFile.iterator();
			while (!trouve && iterator.hasNext())
			{
				GeneralFile gf = iterator.next();
				String title = gf.getCTitle().toString();
				if (title.equals(processName))
				{
					trouve = true;
					generalFile = gf;
				}
			}
		} 
		catch (Exception e) 
		{
			log.error("GetGeneralFile : Erreur due a " + e.getMessage());
			throw new TechnicalException(e.getMessage());
		}	
		
		return generalFile;
	}


	@Override
	public Report convertirFichier(ProjectFile projectFile, File importFile) throws Exception {
		return null;
	}
}
