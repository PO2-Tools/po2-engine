package fr.inrae.po2engine.importExport;

import java.util.ArrayList;

public class ComponentsDeComposition {

	private ArrayList<ComponentDeComposition> lstComponents;
	
	public ComponentsDeComposition() {
		lstComponents = new ArrayList<ComponentDeComposition>();
	}

	//@Override
	//public int hashCode() {
	//	final int prime = 31;
	//	int result = 1;
	//	result = prime * result + ((lstComponents == null) ? 0 : lstComponents.hashCode());
	//	return result;
	//}

	public int indexOf(ComponentDeComposition component)
	{
		int ret = -1;
		if (component == null)
			ret = -1;
		else if (lstComponents == null)
			ret = -1;
		else if (lstComponents.size() == 0)
			ret = -1;
		else
		{
			int k = 0, nb = lstComponents.size();
			boolean cont = true;
			while (cont && k < nb)
			{
				ComponentDeComposition ingr = lstComponents.get(k);
				if (ingr.getType().equals(component.getType())
					&& ingr.getAttribute().equals(component.getAttribute())
					&& ingr.getValue().equals(component.getValue())
					&& ingr.getUnit().equals(component.getUnit())
					&& ingr.getComment().equals(component.getComment()))
				{
					ret = k;
					cont = false;
				}
				else
					k++;
			}
			
		}
		
		return ret;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		else if (obj == null)
			return false;
		else if (getClass() != obj.getClass())
			return false;
		
		ComponentsDeComposition other = (ComponentsDeComposition) obj;
		if (lstComponents == null && other.lstComponents != null) 
			return false;
		else if (lstComponents != null && other.lstComponents == null)
			return false;
		else if (lstComponents == null && other.lstComponents == null);
		else if (lstComponents.size() != other.lstComponents.size())
			return false;
		else
		{
			int k = 0, nb = lstComponents.size();
			boolean cont = true;
			while (cont && k < nb)
				if (other.indexOf(lstComponents.get(k)) < 0)
					cont = false;
				else
					k++;
			
			return cont;
		}
		
		return true;
	}

	public ArrayList<ComponentDeComposition> getLstComponents() {
		return lstComponents;
	}

	public void addComponent(String type, String attribute, String value, String unit, String comment)
	{
		ComponentDeComposition componentDeComposition = new ComponentDeComposition(type, attribute, value, unit, comment); 
		lstComponents.add(componentDeComposition);
	}
}
