/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

import fr.inrae.po2engine.exception.*;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.model.*;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.DataPartType;
import fr.inrae.po2engine.utils.DataTools;
import fr.inrae.po2engine.utils.PO2Properties;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.SimpleDoubleProperty;

import org.apache.commons.io.FileUtils;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.File;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LocalTest {

    @org.junit.jupiter.api.Test
    @Order(1)
    void init() throws InvalidObjectException, URISyntaxException {

        // creation d'un nouveau projet
        // !! si le projet existe déjà sur le disque, alors il ne sera pas supprime mais complete

        ProjectFile projectFile = new ProjectFile("nouveau projet");
        HashMap<KeyWords, ComplexField> inraeAgent = projectFile.addAgent("INRAE", "", "", "");
        HashMap<KeyWords, ComplexField> contactAgent =  projectFile.addAgent("INRAE", "nom du contact", "prénom du contact", "contact@inrae.fr");
        projectFile.getFundingProperty().set(inraeAgent);
        projectFile.getContactProperty().set(contactAgent);
        projectFile.setDescription("short description of the project");
        projectFile.addExternalLink("http://external link/if/available");
        projectFile.addExternalLink("http://another/external link/if/available");


        // ajout d'un nouveau materiel au projet
        MaterialMethodPart materiel1 = new MaterialMethodPart(projectFile, DataPartType.MATERIAL_RAW);
        materiel1.setId("pompe n1");
        materiel1.setOntoType("pompe"); // -> vocabulaire de l'ontologie prefLabel@EN
        materiel1.addCharacteristic("masse", "", "5", "kg", "");


        // Ajout d'un nouveau process au projet
        GeneralFile process1 = new GeneralFile(projectFile);
        process1.setOntoType("Transformation Process"); // -> vocabulaire de l'ontologie prefLabel@EN
        process1.setStartDate("11/08/2021");
        process1.setEndDate("15/11/2021");
        process1.setTitle("my new Process");
        process1.setDescription("short description for this process");

        // Ajout d'un nouvel itinéraire au process 1
        ItineraryFile itineraryFile = new ItineraryFile(process1);
        itineraryFile.setItineraryName("itinerary 1 name");
        itineraryFile.addProductsOfInterest("milk"); // --> vocabulaire de l'ontologie prefLabel@EN

        // Ajout d'un nouvel itinéraire au process 1
        ItineraryFile itineraryFile2 = new ItineraryFile(process1);
        itineraryFile2.setItineraryName("itinerary 2 name");
        itineraryFile2.addProductsOfInterest("raw milk"); // --> vocabulaire de l'ontologie prefLabel@EN


        // Création d'une observation d'itinéraire de type ICV
        ObservationFile itiObs = new ObservationFile(itineraryFile);
        itiObs.setDate("21/08/2021");
        itiObs.setHour("23:10");
        itiObs.setDuree("24:00");
        itiObs.setScale("scale");
        itiObs.setId("observation name");

        // creation d'un lien entre observation et materiel. (observation faite avec le materiel)
        MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(materiel1);
        lPart.addCharacteristic("volume", "", "1600", "L/h", "");
        itiObs.addMaterialMethod(lPart);

        TablePart simpleObs = itiObs.createObsData(true);
        SimpleTable st = (SimpleTable) simpleObs.getTable();
        st.addObservationLine("temp", "water::milk", "10", "Cel", "this is a comment" );
        st.addObservationLine("temp", "cheese", "150", "[degF]", "this is a comment on the second line");

        // new Complexe table obs
        TablePart partComplexe = itiObs.createObsData(false);
        ComplexTable ct = (ComplexTable) partComplexe.getTable();

        // il faut creer les colonnes (contrairement aux simpleTable)
        ComplexField cTemp = ct.addColumn("temp","water::milk","Cel","this is a comment");
        ComplexField cVol = ct.addColumn("volume","","m3","this is a comment");

        Map<ComplexField, ComplexField> lineComplexe = ct.addLine();
        lineComplexe.put(cTemp, new ComplexField("10"));
        lineComplexe.put(cVol, new ComplexField("[[15;1]]"));

        Map<ComplexField, ComplexField> lineComplexe2 = ct.addLine();
        lineComplexe2.put(cTemp, new ComplexField("13"));
        lineComplexe2.put(cVol, new ComplexField("[[17;2]]"));

        // Création d'une étape de type transport
        StepFile step1 = new StepFile("transport", process1);
        step1.setId("transport didier");
        //step1.setType("transport");

        // Création d'une étape de type pompage
        StepFile step2 = new StepFile("pompage", process1);
        step2.setId("pompage 1");
        step2.addMaterialMethod(lPart);

        // ajout des étapes step1 et step2 à l'itinéraire avec enchainement temporel. step1 précéde step2
        itineraryFile.addLinkItinerary(step1, step2);

        // ajout d'une observation sur l'étape step1
        ObservationFile stepObs = new ObservationFile(step1);
        stepObs.setId("observation name");
        stepObs.setFoi("itinerary");


        // ajout d'une composition input sur l'étape step1
        CompositionFile cInput = new CompositionFile(step1, true);
        cInput.addComponent("volume", "milk", "10", "l", "a comment");

        stepObs.setFoi(cInput.getFileName());

        // creatation et ajout d'une composition output sur l'étape step1
        CompositionFile cOutput = new CompositionFile(step1, false);

        // ajout de la composition cOutput (step1) comme input de step2
        step2.addCompositionFile(cOutput, true);

        // sauvegarde du projet sur le disque
        try {
            projectFile.saveData(false); // sauvegarde local uniquement
            File zip = Tools.compressZipfile(projectFile);
            // zip a envoyer au webservice pour enregistrement sur le cloud + semantisation + publication
            System.out.println(zip.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @org.junit.jupiter.api.Test
    @Order(2)
    void filePathTest() throws InvalidObjectException, URISyntaxException {

        // creation d'un nouveau projet
        String projectName = "nouveau projet";
        Data d = new Data(projectName);
        d.load();
        DataTools.analyseFiles(d); // n'analyse que superficiellement (detection des generals Files et du materiel et methode seulement)
        ProjectFile projectFile = d.getProjectFile();

        Assertions.assertTrue(projectFile.getFolderPath().equals(Tools.dataPath + projectName + File.separator));

        // Ajout d'un nouveau process au projet
        GeneralFile general1 = new GeneralFile(projectFile);
        general1.constructData(); // contruit le process entierement (step, observation, composition, ...)
        general1.setStartDate("11/08/2021");
        general1.setEndDate("15/11/2021");
        general1.setTitle("my new Process");
        Assertions.assertTrue(general1.getFolderPath().equals(projectFile.getFolderPath() + general1.getFileName()+File.separator));
        Assertions.assertTrue(general1.getFilePath().equals(projectFile.getFolderPath() + general1.getFileName()+File.separator + general1.getFileName()+".xlsx"));



        // Ajout d'un nouvel itinéraire au process general1
        ItineraryFile itineraryFile = new ItineraryFile(general1);
        itineraryFile.setItineraryName("nom de l'itinéraire");

        // Création d'une observation d'itinéraire de type ICV
        ObservationFile itiObs = new ObservationFile(itineraryFile);
        Assertions.assertTrue(itiObs.getFolderPath().equals(general1.getFolderPath()+"observation"+File.separator));
        Assertions.assertTrue(itiObs.getFilePath().equals(general1.getFolderPath()+"observation"+File.separator + itiObs.getFileName()+".xlsx"));

        String stepType = "step 1";

        StepFile step = new StepFile(stepType, general1);
        itineraryFile.addLinkItinerary(step, null);
        Assertions.assertTrue(step.getFolderPath().equals(general1.getFolderPath()+Tools.normalize(stepType)+File.separator));
        Assertions.assertTrue(step.getFilePath().equals(step.getFolderPath()+step.getFileName()+".xlsx"));

        CompositionFile cInput = new CompositionFile(step, true);
        Assertions.assertTrue(cInput.getFolderPath().equals(step.getFolderPath() + "composition-conduite" + File.separator));
        Assertions.assertTrue(cInput.getFilePath().equals(step.getFolderPath() + "composition-conduite" + File.separator + cInput.getFileName()+".xlsx"));

        ObservationFile stepObs = new ObservationFile(step);
        stepObs.setId("nom de l'obs");
        Assertions.assertTrue(stepObs.getFolderPath().equals(step.getFolderPath()+"observation"+File.separator));
        Assertions.assertTrue(stepObs.getFilePath().equals(step.getFolderPath()+"observation"+File.separator + stepObs.getFileName()+".xlsx"));

    }



    @org.junit.jupiter.api.Test
    @Order(2)
    void remove() throws InvalidObjectException, URISyntaxException {

            Boolean userInit = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
            Boolean remove = CloudConnector.removeProjectData("nouveau projet");
            System.out.println(remove);


    }

    @org.junit.jupiter.api.Test
    @Order(2)
    void tt() throws InvalidObjectException, URISyntaxException {
        File newProj = new File("/home/stephane/nouveau projet.zip");
        try {
            Boolean userInit = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
            ProjectFile pf = CloudConnector.createNewProject(newProj, true); // creation depuis un zip existant
            if(pf != null) {
                pf.getData().lockMode();
                CloudConnector.listOntology(new SimpleDoubleProperty(0.0),0.5);
                Ontology onto = Ontologies.getOntology("transform");
                onto.load();
                JSONObject rapport = pf.getData().semantize(null, onto);
                Boolean publish = pf.getData().publish(rapport);
                System.out.println(rapport);
                if(publish) {
                    System.out.println("success");

                }
            }
        } catch (AlreadyExistException e) {
            e.printStackTrace();
        } catch (CantWriteException e) {
            e.printStackTrace();
        } catch (NotLoginException e) {
            e.printStackTrace();
        } catch (AlreayLockException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LocalLockException e) {
            e.printStackTrace();
        }

    }


    @org.junit.jupiter.api.Test
    @Order(3)
    void exportProject() throws IOException, LocalLockException, CantWriteException, NotLoginException, URISyntaxException {
        try {
            // Test juste pour publier des jeux sans passer par PO2Manager.

            String projectName = "data_test";
            Boolean ok = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
            Assert.assertTrue(ok);


            CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
            Data data = Datas.getData(projectName);
            Assert.assertNotNull(data);
            data.load();
            DataTools.analyseFiles(data);
            File exportFile = data.getProjectFile().exportProject();
            File destinationFile = new File("/tmp/exportProject-" + projectName+".xlsx");
            FileUtils.copyFile(exportFile, destinationFile);

        } catch (IOException | RDFParseException e) {
            e.printStackTrace();
        }
    }


    void testPublish() throws IOException, LocalLockException, CantWriteException, NotLoginException, URISyntaxException {
        try {
            // Test juste pour publier des jeux sans passer par PO2Manager.

            String ontoName = "transformON";
            String projectName = "data_test";
            Boolean ok = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
            Assert.assertTrue(ok);

            CloudConnector.listOntology(null, 1.0);
            Ontology onto = Ontologies.getOntology(ontoName);
            ok = onto.load();
            Assert.assertTrue(ok);

            CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
            Data data = Datas.getData(projectName);
            Assert.assertNotNull(data);
            //data.load();

            data.lockMode();
            data.setModified(false);
            JSONObject listMessages = data.semantize(null, onto);
            Assert.assertTrue(listMessages.optJSONArray("error").length() == 0);
            //String finalID = Tools.normalize(data.getProjectFile().getNameProperty().getValue());
            ok = data.publish(listMessages);
            Assert.assertTrue(ok);
            //String repoName = RDF4JTools.getRepository(finalID);
            data.unlockMode();
            System.out.println(listMessages);

        } catch (AlreayLockException e) {
            e.printStackTrace();
        } catch (IOException | RDFParseException e) {
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    @Order(6)
    void testRDF4JToolsSHACLValidation() throws CantWriteException, NotLoginException, AlreayLockException, IOException, URISyntaxException, LocalLockException {
    try {        
        String dataName = "data_test";
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(resOK);

        RDF4JTools.updateDataShaclGraph(Tools.normalize(dataName), getClass().getClassLoader().getResourceAsStream("test_shapes_success.ttl"), "Data");
        Report r = RDF4JTools.startShaclValidation(Tools.normalize(dataName));
        Assert.assertTrue(r.success());
        System.out.println("Erreurs :");
        System.out.println(r.prettyPrintError());
        System.out.println("Fin des messages");

        RDF4JTools.updateDataShaclGraph(Tools.normalize(dataName), getClass().getClassLoader().getResourceAsStream("test_shapes_failed.ttl"), "Data");
        r = RDF4JTools.startShaclValidation(Tools.normalize(dataName));
        Assert.assertFalse(r.success());
        System.out.println("Erreurs :");
        System.out.println(r.prettyPrintError());
        System.out.println("Fin des messages");

        } catch (IOException | RDFParseException e) {
            Tools.delProgress(ProgressPO2.PUBLISH);
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    @Order(7)
    void testDataSHACLValidation() throws CantWriteException, NotLoginException, AlreayLockException, IOException, URISyntaxException, LocalLockException {
    try {        
        String projectName = "data_test";
        Boolean ok = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(ok);

        CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
        Data data = Datas.getData(projectName);

        // Test n°1 : No SHACL shape graph
        RDF4JTools.clearSHACLShapeGraph(projectName, "Data");
        RDF4JTools.clearSHACLShapeGraph(projectName, "Onto");
//        Report r = RDF4JTools.startShaclValidation(Tools.normalize(projectName));
//        Assert.assertTrue(r.success());

        // Test n°2 : No validation error
//        RDF4JTools.updateDataShaclGraph(Tools.normalize(projectName), getClass().getClassLoader().getResourceAsStream("test_shapes_success.ttl"), "Data");
//        r = RDF4JTools.startShaclValidation(Tools.normalize(projectName));
//        Assert.assertTrue(r.success());

        // Test n°3 : Validations errors
      //  RDF4JTools.updateDataShaclGraph(Tools.normalize(projectName), getClass().getClassLoader().getResourceAsStream("test_shapes_failed.ttl"), "Data");
      //  r = RDF4JTools.startShaclValidation(Tools.normalize(projectName));

      //  RDF4JTools.clearSHACLShapeGraph(projectName, "Data");
      //  RDF4JTools.clearSHACLShapeGraph(projectName, "Onto");

      //  r = RDF4JTools.startShaclValidation(Tools.normalize(projectName));
      //  Assert.assertTrue(r.success());

        // Test n°2 : No validation error
      //  RDF4JTools.updateDataShaclGraph(Tools.normalize(projectName), getClass().getClassLoader().getResourceAsStream("test_shapes_success.ttl"), "Onto");
      //  r = RDF4JTools.startShaclValidation(Tools.normalize(projectName));
      //  Assert.assertTrue(r.success());

        // Test n°3 : Validations errors
        RDF4JTools.updateDataShaclGraph(Tools.normalize(projectName), getClass().getClassLoader().getResourceAsStream("test_failed_onto.ttl"), "Onto");
        Report r = RDF4JTools.startShaclValidation(Tools.normalize(projectName));
        Assert.assertFalse(r.success());
        System.out.println(r.prettyPrintError());

        } catch (IOException | RDFParseException e) {
            Tools.delProgress(ProgressPO2.PUBLISH);
            e.printStackTrace();
        }
    }

    @org.junit.jupiter.api.Test
    @Order(11)
    void testListNamedGraph() throws IOException {
        List<String> output = RDF4JTools.listNamedGraph("data_test");
        System.out.println(output);
    }
    @org.junit.jupiter.api.Test
    @Order(10)
    void testSHACLDataOntoValidation() throws IOException {
        String dataRepositoryID = "Training guillemin";
        String ontoRepositoryID = "TransformON";
        String dataName = Tools.normalize(dataRepositoryID);
        String ontoName = Tools.normalize(ontoRepositoryID);
        //String nomFic = "test_shapes_failed.ttl";

        //File schaclConstraintsFile = new File(RDF4JTools.class.getClassLoader().getResource(nomFic).getFile());
        //InputStream stream = new FileInputStream(schaclConstraintsFile);
        //RDF4JTools.updateShaclGraph(dataName, stream);

        Report r = RDF4JTools.startShaclValidation(dataName);
        Assert.assertFalse(r.success());
        System.out.println("Erreurs :");
        System.out.println(r.prettyPrintError());
        System.out.println("Fin des messages");
    }

}
