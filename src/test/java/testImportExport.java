/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

 
 import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
 import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.security.KeyManagementException;
 import java.security.KeyStoreException;
 import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer;
 import org.junit.jupiter.api.Order;
 import org.junit.jupiter.api.TestMethodOrder;
 
 import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONException;
import org.json.JSONObject;

import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.exception.CantWriteException;
import fr.inrae.po2engine.exception.LocalLockException;
import fr.inrae.po2engine.exception.NotLoginException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.importExport.CompareExcel;
//import fr.inrae.po2engine.importExport.CompareExcel;
import fr.inrae.po2engine.importExport.ExportDataNouveauFormat;
 import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.OntoData;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
 import fr.inrae.po2engine.utils.DataTools;
 import fr.inrae.po2engine.utils.PO2Properties;
import fr.inrae.po2engine.utils.Report;
import fr.inrae.po2engine.utils.Tools;
import javafx.beans.property.SimpleDoubleProperty;
import fr.inrae.po2engine.model.Data;
 
 @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
 public class testImportExport {

     @org.junit.jupiter.api.Test
     @Order(1)
     void importExport() throws IOException {
 
        // Import d'un jeu de donnees, puis export et comparaison
        // du contenu des fichiers initial et final.
        String nomProjet = "Training guillemin";
        String nomFic = nomProjet + "Nv.xlsx";
        nomFic = "FLEGME_modifie.xlsx";
        nomFic = "Evagrain_PO2_HG.xlsx";
        nomFic = "Training guilleminFLEGME.xlsx";
        nomFic = "Evagrain_PO2 - restreint.xlsx";
        nomFic = "TANGO1_HG._Complete.xlsx";
        nomFic = "Training guillemin.xlsx";
        nomFic = "FLEGME 21 01 25 avant import._Complete_modfie.xlsx";
        ClassLoader classLoader = getClass().getClassLoader();
        File srcFile = new File(URLDecoder.decode(classLoader.getResource(nomFic).getFile(), "UTF-8"));
        Assert.assertNotNull(srcFile);

        Boolean ok = CloudConnector.initUser(PO2Properties.getProperty("ldap.hg.login"), PO2Properties.getProperty("ldap.hg.password"));
        Assert.assertTrue(ok);

        CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);
        Data data = Datas.getData(nomProjet);
        Assert.assertNotNull(data);

        data.load();
        try {
            ok = data.lockMode();
        } catch (AlreayLockException | LocalLockException | CantWriteException | NotLoginException | IOException
                | URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ok = false;
        }
        Assert.assertTrue(ok);
         
        DataTools.analyseFiles(data);
        ProjectFile projectFile = data.getProjectFile();          
        Report report = null;
        try {
            report = projectFile.importProject(srcFile);
    
            ok = true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ok = false;
        }        
        Assert.assertTrue(ok);
        ok = report.success();

        // Telechargement du fichier Excel d'aide
        String helperPath = "";
        for(Iterator<String> si = report.getInfos().iterator(); si.hasNext();)
        {
            String infos = si.next();
            try {
                JSONObject jo = new JSONObject(infos);
                if (jo.keySet().contains("helper_file")) {
                    helperPath = jo.getString("helper_file");
                    si.remove();
                }
            } catch (JSONException e) {
                // not a json string
            }
        }
        if(!helperPath.isEmpty()) {
            File hf = new File(helperPath);
            File helpFile = new File("d:/atrier/help.xlsx");
             if (helpFile != null) {
                FileUtils.copyFile(hf, helpFile);
            }
        }

        Assert.assertTrue(projectFile.saveData(false));
        Assert.assertTrue(CloudConnector.update(data));
        Assert.assertTrue(data.unlockMode());
        
        File destFile = projectFile.exportProject();
        Assert.assertNotNull(destFile);
        File fic = new File("d:/atrier/export.xlsx");
        FileUtils.copyFile(destFile, fic);

        report = CompareExcel.compareFiles(srcFile, destFile);
        Assert.assertTrue(report.success());
    }
}
 