/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

import fr.inrae.po2engine.exception.AlreadyExistException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.ComplexTable;
import fr.inrae.po2engine.model.partModel.SimpleTable;
import fr.inrae.po2engine.model.partModel.TablePart;
import fr.inrae.po2engine.utils.DataTools;
import fr.inrae.po2engine.utils.PO2Properties;
import fr.inrae.po2engine.utils.Tools;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.File;
import java.io.InvalidObjectException;
import java.util.Map;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test {
    String projectName = "unitTestProject";
    static GeneralFile general1;
    static ItineraryFile itineraryFile;
    static ProjectFile projectFile;



    @org.junit.jupiter.api.Test
    @Order(2)
    void createNewProject() {
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        ProjectFile projectFile = null;
        try {
            projectFile = CloudConnector.createNewProject(projectName, true);
        } catch (AlreadyExistException e) {
            e.printStackTrace();
        }
        Assertions.assertNotNull(projectFile);
        Assertions.assertNotNull(Datas.getData(projectName));
    }


    @org.junit.jupiter.api.Test
    @Order(3)
    void loadProject() throws InvalidObjectException {
        Data data = Datas.getData(projectName);
        data.load();

        Assertions.assertTrue(Datas.getData(projectName).isLoaded().get());

        DataTools.analyseFiles(data);

        projectFile = data.getProjectFile();
        Assertions.assertTrue(projectFile.getFolderPath().equals(Tools.dataPath + projectName + File.separator));

        // new Process
        general1 = new GeneralFile(projectFile);
        Assertions.assertTrue(general1.getFolderPath().equals(projectFile.getFolderPath() + general1.getFileName()+File.separator));
        Assertions.assertTrue(general1.getFilePath().equals(projectFile.getFolderPath() + general1.getFileName()+File.separator + general1.getFileName()+".xlsx"));


    }

    @org.junit.jupiter.api.Test
    @Order(4)
    void addItinerary() throws InvalidObjectException {
        itineraryFile = new ItineraryFile(general1);
        Assertions.assertTrue(general1.getItinerary().size() == 1);
        Assertions.assertTrue(itineraryFile.getItineraryNumber().equals("Itinerary 1"));
    }

    @org.junit.jupiter.api.Test
    @Order(5)
    void addItiObs() throws InvalidObjectException {
        ObservationFile itiObs = new ObservationFile( itineraryFile);
        itiObs.setId("obsName");
        Assertions.assertTrue(itineraryFile.getListObservation().size() == 1);
        Assertions.assertTrue(itiObs.getFolderPath().equals(general1.getFolderPath() + "observation" + File.separator));
        Assertions.assertTrue(itiObs.getFilePath().equals(general1.getFolderPath() + "observation" + File.separator + itiObs.getFileName()+".xlsx"));

        TablePart part = itiObs.createObsData(true);
        SimpleTable st = (SimpleTable) part.getTable();

        TablePart cpart = itiObs.createObsData(false);
        ComplexTable ct = (ComplexTable) cpart.getTable();

        Assertions.assertTrue(itiObs.getContentList().size() == 2);

        Map<KeyWords, ComplexField> line = st.addLine();
        Assertions.assertNotNull(line);
        line.put(SimpleTable.caractK, new ComplexField("temp"));
        line.put(SimpleTable.ObjK, new ComplexField("water::milk"));
        line.put(SimpleTable.valK, new ComplexField("10"));
        line.put(SimpleTable.unitK, new ComplexField("Cel"));
        line.put(SimpleTable.commentK, new ComplexField("this is a comment"));

        ComplexField cTemp = ct.addColumn("temp (unit=Cel)  (comment=this is a comment)  (object=water::milk) ");
        ComplexField cVol = ct.addColumn("volume (unit=m3)  (comment=this is a comment)");

        Map<ComplexField, ComplexField> lineComplexe = ct.addLine();
        Assertions.assertNotNull(lineComplexe);
        lineComplexe.put(cTemp, new ComplexField("10"));
        lineComplexe.put(cVol, new ComplexField("[[15;1]]"));
    }

    @org.junit.jupiter.api.Test
    @Order(6)
    void addStep() throws InvalidObjectException {
        StepFile s1 = new StepFile("step 1", general1);
        Assertions.assertTrue(general1.getListStep().size() == 1);
        StepFile ss1 = new StepFile("subStep 1", general1);
        Assertions.assertTrue(general1.getListStep().size() == 2);
        s1.addSubStep(s1);
        Assertions.assertTrue(s1.getSubStep().size() == 0);
        s1.addSubStep(ss1);
        Assertions.assertTrue(s1.getSubStep().size() == 1);
        Assertions.assertTrue(ss1.getFather().equals(s1));
        StepFile s2 = new StepFile("step 2", general1);
        Assertions.assertTrue(general1.getListStep().size() == 3);
        itineraryFile.addLinkItinerary(s1, s2);
        Assertions.assertTrue(itineraryFile.getItinerary().size() == 1);
    }

    @org.junit.jupiter.api.Test
    @Order(7)
    void addStepObs() throws InvalidObjectException {
        StepFile step = general1.getListStep().stream().findFirst().orElse(null);
        ObservationFile stepObs = new ObservationFile(step);
        stepObs.setId("obsName");
        Assertions.assertTrue(step.getObservationFiles().size() == 1);

        Assertions.assertTrue(stepObs.getFolderPath().equals(step.getFolderPath() + "observation" + File.separator));
        Assertions.assertTrue(stepObs.getFilePath().equals(step.getFolderPath() + "observation" + File.separator + stepObs.getFileName()+".xlsx"));

    }

    @org.junit.jupiter.api.Test
    @Order(8)
    void addCompositions() throws InvalidObjectException {
        StepFile step = general1.getListStep().stream().findFirst().orElse(null);
        CompositionFile cInput = new CompositionFile(step, true);
        Assertions.assertTrue(cInput.getFolderPath().equals(step.getFolderPath() + "composition-conduite" + File.separator));
        Assertions.assertTrue(cInput.getFilePath().equals(step.getFolderPath() + "composition-conduite" + File.separator + cInput.getFileName()+".xlsx"));

        cInput.setCompositionType("compoType");
        Assertions.assertTrue(step.getCompositionFile().size() == 1);

        SimpleTable t = (SimpleTable) cInput.getContentPart().getTable();
        t.addLine(0);
        Map<KeyWords, ComplexField> line = t.getLine(0);
        line.put(SimpleTable.caractK, new ComplexField("volume"));
        line.put(SimpleTable.ObjK, new ComplexField("milk"));
        line.put(SimpleTable.valK, new ComplexField("10"));
        line.put(SimpleTable.unitK, new ComplexField("l"));
        line.put(SimpleTable.commentK, new ComplexField("this is a comment"));

        t.addLine(0);
        Map<KeyWords, ComplexField> line2 = t.getLine(0);
        line.put(SimpleTable.caractK, new ComplexField("mass"));
        line.put(SimpleTable.ObjK, new ComplexField("sugar"));
        line.put(SimpleTable.valK, new ComplexField("100"));
        line.put(SimpleTable.unitK, new ComplexField("g"));
        line.put(SimpleTable.commentK, new ComplexField("this is a comment"));

        Assertions.assertTrue(((SimpleTable) cInput.getContentPart().getTable()).getNbLine() == 2);

    }

    @org.junit.jupiter.api.Test
    @Order(10)
    void removeProject() {
        projectName = "test creation ontol 8";
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        CloudConnector.removeProjectOntology(projectName);
    }
}
