/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

import fr.inrae.po2engine.exception.*;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.externalTools.JenaTools;
import fr.inrae.po2engine.externalTools.RDF4JTools;
import fr.inrae.po2engine.model.*;
import fr.inrae.po2engine.model.dataModel.*;
import fr.inrae.po2engine.model.partModel.*;
import fr.inrae.po2engine.utils.*;
import fr.inrae.po2engine.vocabsearch.Requete;
import javafx.beans.property.SimpleDoubleProperty;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.lang3.tuple.MutablePair;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CloudTest {

    @org.junit.jupiter.api.Test
    @Order(1)
    void listOnto() throws InvalidObjectException {
        CloudConnector.listOntology(new SimpleDoubleProperty(0.0), 0.5);
        for (OntoData o : Ontologies.getOntologies().values()) {
            RDF4JTools.initRepositotyInfos(o);
        }
        Assertions.assertTrue(Ontologies.getOntologies().size() > 0);
    }

    @org.junit.jupiter.api.Test
    @Order(2)
    void listData() throws InvalidObjectException {
        CloudConnector.listData(new SimpleDoubleProperty(0.0), 0.5);
        for (OntoData o : Datas.getDatas().values()) {
            RDF4JTools.initRepositotyInfos(o);
        }
        Assertions.assertTrue(Ontologies.getOntologies().size() > 0);
    }

    @org.junit.jupiter.api.Test
    @Order(3)
    void queryCible() throws InvalidObjectException {
        CloudConnector.refreshCible();
        Assertions.assertTrue(Requete.getDefautCibles().size() > 0);
    }


    @org.junit.jupiter.api.Test
    @Order(4)
    void createNewProject() throws AlreadyExistException {
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        ProjectFile newProject = CloudConnector.createNewProject("unitTest", true);
        Assertions.assertNotNull(newProject);
        Assertions.assertNotNull(Datas.getData("unitTest"));
    }

    @org.junit.jupiter.api.Test
    @Order(5)
    void getZip() {
        ZipFile data = CloudConnector.getZipFile(Datas.getData("unitTest"));
        Assertions.assertNotNull(data);
    }

    @org.junit.jupiter.api.Test
    @Order(6)
    void davData() {
        // user must be initialised
        File data = CloudConnector.getDavData(Datas.getData("unitTest"));
        Assertions.assertNotNull(data);
    }

    @org.junit.jupiter.api.Test
    @Order(7)
    void removeProject() {
        // user must be initialised
        String projectName = "unitTest";
        CloudConnector.removeProjectData(projectName);
        Assertions.assertNull(Datas.getData(projectName));
    }

    @org.junit.jupiter.api.Test
    @Order(8)
    void loadSaveOnto() throws CantWriteException, NotLoginException, AlreayLockException, IOException, URISyntaxException, LocalLockException {
        String ontoName = "test";
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(resOK);
        CloudConnector.listOntology(null, 0.5);
        Ontology onto = Ontologies.getOntology(ontoName);
        onto.load();
        Assert.assertTrue(onto.lockMode());
        Assert.assertTrue(JenaTools.saveModel(onto, false));
        Assert.assertTrue(CloudConnector.update(onto));
        Assert.assertTrue(onto.unlockMode());
    }


    @org.junit.jupiter.api.Test
    @Order(9)
    void davOnto() throws IOException, URISyntaxException {
        if (Ontologies.getOntologies().isEmpty()) {
            CloudConnector.listOntology(null, 0.5);
        }
        MutablePair<String, Date> mu = CloudConnector.getDavVersion(Ontologies.getOntology("test"));
        Assertions.assertNotNull(mu.getLeft());
        Assertions.assertNotNull(mu.getRight());
        Boolean res = CloudConnector.initUser("fail", "test");
        Assertions.assertFalse(res);
        Assertions.assertFalse(CloudConnector.isInitUser());
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("nextCloud.listFile.login"), PO2Properties.getProperty("nextCloud.listFile.password"));
        Assertions.assertTrue(resOK);
        Assertions.assertTrue(CloudConnector.isInitUser());

        File onto = CloudConnector.getDavOntology(Ontologies.getOntology("test"));
        Assertions.assertNotNull(onto);
    }

    @org.junit.jupiter.api.Test
    @Order(10)
    void createEmptyOntology() throws IOException, URISyntaxException, AlreadyExistException, InterruptedException {
        String ontoName = "test creation ontol 8";
        Boolean userInit = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
//        CloudConnector.listOntology(new SimpleDoubleProperty(0.0), 0.5);
//        Ontologies.getOntology("test creation ontol 6").load();
//        JenaTools.loadModel(Ontologies.getOntology("test creation ontol 6"));
        Ontology newOnto = CloudConnector.createNewOntology(ontoName, true); // true pour public, false pour prive

    }


    void fullTest() throws IOException, AlreadyExistException {
        String projectName = "proj Name_test";
        Boolean userInit = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));

        ProjectFile projectFile = CloudConnector.createNewProject(projectName, true); // true pour public, false pour prive
        HashMap<KeyWords, ComplexField> inraeAgent = projectFile.addAgent("INRAE", "", "", "");
        HashMap<KeyWords, ComplexField> contactAgent =  projectFile.addAgent("INRAE", "nom du contact", "prénom du contact", "contact@inrae.fr");
        projectFile.getFundingProperty().set(inraeAgent);
        projectFile.getContactProperty().set(contactAgent);
        projectFile.setDescription("short description of the project");
        projectFile.addExternalLink("http://external link/if/available");
        projectFile.addExternalLink("http://another/external link/if/available");


        // ajout d'un nouveau materiel au projet
        MaterialMethodPart materiel1 = new MaterialMethodPart(projectFile, DataPartType.MATERIAL_RAW);
        materiel1.setId("pompe n1");
        materiel1.setOntoType("pompe"); // -> vocabulaire de l'ontologie prefLabel@EN
        materiel1.addCharacteristic("masse", "", "5", "kg", "");
        materiel1.setComment("test commentaire mat 1");


        // Ajout d'un nouveau process au projet
        GeneralFile process1 = new GeneralFile(projectFile);
        process1.setOntoType("Transformation Process"); // -> vocabulaire de l'ontologie prefLabel@EN
        process1.setStartDate("11/08/2021");
        process1.setEndDate("15/11/2021");
        process1.setTitle("my new Process");
        process1.setDescription("short description for this process");

        // Ajout d'un nouvel itinéraire au process 1
        ItineraryFile itineraryFile = new ItineraryFile(process1);
        itineraryFile.setItineraryName("itinerary 1 name");
        itineraryFile.addProductsOfInterest("milk"); // --> vocabulaire de l'ontologie prefLabel@EN

        // Ajout d'un nouvel itinéraire au process 1
        ItineraryFile itineraryFile2 = new ItineraryFile(process1);
        itineraryFile2.setItineraryName("itinerary 2 name");
        itineraryFile2.addProductsOfInterest("raw milk"); // --> vocabulaire de l'ontologie prefLabel@EN


        // Création d'une observation d'itinéraire de type ICV
        ObservationFile itiObs = new ObservationFile(itineraryFile);
        itiObs.setDate("21/08/2021");
        itiObs.setHour("23:10");
        itiObs.setDuree("24:00");
        itiObs.setScale("scale");
        itiObs.setId("observation name");

        // creation d'un lien entre observation et materiel. (observation faite avec le materiel)
        MaterialMethodLinkPart lPart = new MaterialMethodLinkPart(materiel1);
        lPart.addCharacteristic("volume", "", "1600", "L/h", "");
        itiObs.addMaterialMethod(lPart);

        TablePart simpleObs = itiObs.createObsData(true);
        SimpleTable st = (SimpleTable) simpleObs.getTable();
        st.addObservationLine("temp", "water::milk", "10", "Cel", "this is a comment");
        st.addObservationLine("temp", "cheese", "150", "[degF]", "this is a comment on the second line");

        // new Complexe table obs
        TablePart partComplexe = itiObs.createObsData(false);
        ComplexTable ct = (ComplexTable) partComplexe.getTable();

        // il faut creer les colonnes (contrairement aux simpleTable)
        ComplexField cTemp = ct.addColumn("temp", "water::milk", "Cel", "this is a comment");
        ComplexField cVol = ct.addColumn("volume", "", "m3", "this is a comment");

        Map<ComplexField, ComplexField> lineComplexe = ct.addLine();
        lineComplexe.put(cTemp, new ComplexField("10"));
        lineComplexe.put(cVol, new ComplexField("[[15;1]]"));

        Map<ComplexField, ComplexField> lineComplexe2 = ct.addLine();
        lineComplexe2.put(cTemp, new ComplexField("13"));
        lineComplexe2.put(cVol, new ComplexField("[[17;2]]"));

        // Création d'une étape de type transport
        StepFile step1 = new StepFile("transport", process1);
        step1.setId("transport didier");
        //step1.setType("transport");

        // Création d'une étape de type pompage
        StepFile step2 = new StepFile("pompage", process1);
        step2.setId("pompage 1");
        step2.addMaterialMethod(lPart);

        // ajout des étapes step1 et step2 à l'itinéraire avec enchainement temporel. step1 précéde step2
        itineraryFile.addLinkItinerary(step1, step2);

        // ajout d'une observation sur l'étape step1
        ObservationFile stepObs = new ObservationFile(step1);
        stepObs.setId("observation name");
        stepObs.setFoi("step");


        // ajout d'une composition input sur l'étape step1
        CompositionFile cInput = new CompositionFile(step1, true);
        cInput.addComponent("volume", "milk", "10", "l", "a comment");

        stepObs.setFoi(cInput.getFileName());

        // creatation et ajout d'une composition output sur l'étape step1
        CompositionFile cOutput = new CompositionFile(step1, false);

        // ajout de la composition cOutput (step1) comme input de step2
        step2.addCompositionFile(cOutput, true);

        projectFile.saveData(false); // sauvegarde local uniquement
        CloudConnector.update(projectFile.getData()); // on envoie la sauvegarde local sur le cloud

        Boolean deleteProj = CloudConnector.removeProjectData(projectName);
        Assertions.assertTrue(deleteProj);
    }


    @org.junit.jupiter.api.Test
    @Order(11)
    void shaclTest() throws CantWriteException, NotLoginException, AlreayLockException, IOException, URISyntaxException, LocalLockException {
        String dataName = "data_test";
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(resOK);
//        CloudConnector.listOntology(null, 0.5);
//        CloudConnector.listData(null, 0.5);

//        Ontology onto = Ontologies.getOntology(ontoName);
//        onto.load();

//        Data data = Datas.getData(dataName);
//        Boolean rr = data.load();

//        DataTools.analyseFiles(data);

//        data.lockMode();
//        data.setModified(false);

//        JSONObject res = data.semantize(null, onto);
//        Boolean publishRes = data.publish(res);
//        Assert.assertTrue(publishRes);
//        if (data.publish(res)) {
            RDF4JTools.updateDataShaclGraph(dataName, getClass().getClassLoader().getResourceAsStream("test_shapes_success.ttl"), "Data");
            Report r = RDF4JTools.startShaclValidation(dataName);
            Assert.assertTrue(r.success());

            // update with wrong shacl
            RDF4JTools.updateDataShaclGraph(dataName, getClass().getClassLoader().getResourceAsStream("test_shapes_failed.ttl"), "Data");
            r = RDF4JTools.startShaclValidation(dataName);
        System.out.println("output : ");
            System.out.println(r.prettyPrintError());
            Assert.assertFalse(r.success());

//        }
//        data.unlockMode();

    }
}