/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexFieldTest {

    @Test
    void parseValue() {
        ComplexField f = new ComplexField();
        f.parseValue("type (id=an id)(unit=a unit)(comment=a comment)(object=obj1::obj2)");
        assertEquals("type", f.getValue().get());
        assertEquals("an id", f.getID().get());
        assertEquals("a unit", f.getUnit().get());
        assertEquals("a comment", f.getTooltip().get());
        assertEquals(2, f.getListObject().size());
        assertEquals("obj1", f.getListObject().get(0));
        assertEquals("obj2", f.getListObject().get(1));

        ComplexField f1 = new ComplexField();
        f1.parseValue("ty(-)pe {{id=a:-)n id  }}{{unit=a un        *.?it}}{{comment=a comm:-)ent}}{{object=obj(1 :: ob))j2}}");
        assertEquals("ty(-)pe", f1.getValue().get());
        assertEquals("a:-)n id", f1.getID().get());
        assertEquals("a un        *.?it", f1.getUnit().get());
        assertEquals("a comm:-)ent", f1.getTooltip().get());
        assertEquals("obj(1", f1.getListObject().get(0));
        assertEquals("ob))j2", f1.getListObject().get(1));
    }

    @Test
    void testToString() {
    }

    @Test
    void cloneFrom() {
    }
}
