/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2engine.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.atlas.json.JSON;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

class ToolsTest {


    @Test
    void generateUniqueTimeStamp() {
        Assertions.assertNotEquals(Tools.generateUniqueTimeStamp(), Tools.generateUniqueTimeStamp());
        Assertions.assertNotEquals(Tools.generateUniqueTimeStamp(), Tools.generateUniqueTimeStamp());
        Assertions.assertNotEquals(Tools.generateUniqueTimeStamp(), Tools.generateUniqueTimeStamp());
        // in an older version, generate unique timestamp too quickly resulted in same timeStamp !
    }

    @Test
    void getQuantValueAsMinMax() {
        JSONObject json = new JSONObject();
        json.put("error", new JSONArray());
        json.put("warning", new JSONArray());

        Pair<Double, Double> res;
        res = Tools.getQuantValueAsMinMax("-inf", json);
        Assertions.assertEquals(Double.NEGATIVE_INFINITY, res.getLeft());
        Assertions.assertEquals(Double.NEGATIVE_INFINITY, res.getRight());

        res = Tools.getQuantValueAsMinMax("inf", json);
        Assertions.assertEquals(Double.POSITIVE_INFINITY, res.getLeft());
        Assertions.assertEquals(Double.POSITIVE_INFINITY, res.getRight());

        res = Tools.getQuantValueAsMinMax("+inf", json);
        Assertions.assertEquals(Double.POSITIVE_INFINITY, res.getLeft());
        Assertions.assertEquals(Double.POSITIVE_INFINITY, res.getRight());


        res = Tools.getQuantValueAsMinMax("[-inf;inf]", json);
        Assertions.assertEquals(Double.NEGATIVE_INFINITY, res.getLeft());
        Assertions.assertEquals(Double.POSITIVE_INFINITY, res.getRight());

        res = Tools.getQuantValueAsMinMax("[-inf;+inf]", json);
        Assertions.assertEquals(Double.NEGATIVE_INFINITY, res.getLeft());
        Assertions.assertEquals(Double.POSITIVE_INFINITY, res.getRight());
    }

    @Test
    void getQuantValueAsFuzzy() {
    }

    @Test
    void getQuantValueType() {
    }

    @Test
    void convertTime() {
    }

    @Test
    void convertDate() {
        JSONObject json = new JSONObject();
        json.put("error", new JSONArray());
        json.put("warning", new JSONArray());

        LocalDate ld = Tools.convertDate("21/07/1987", json);
        Assertions.assertNotNull(ld);

        ld = Tools.convertDate("1987-07-21", json);
        Assertions.assertNotNull(ld);

        ld = Tools.convertDate("1987/07", json);
        Assertions.assertNotNull(ld);

        ld = Tools.convertDate("1987", json);
        Assertions.assertNotNull(ld);

        ld = Tools.convertDate(null, json);
        Assertions.assertNull(ld);

        ld = Tools.convertDate("", json);
        Assertions.assertNull(ld);

        LocalDateTime ldt = Tools.convertDate("1987", "01:01:01", json);
        Assertions.assertNotNull(ldt);

        ldt = Tools.convertDate("1987", "badTime", json);
        Assertions.assertNotNull(ldt);

        ldt = Tools.convertDate("1987", "", json);
        Assertions.assertNotNull(ldt);

        ldt = Tools.convertDate("1987", null, json);
        Assertions.assertNotNull(ldt);

        ldt = Tools.convertDate("bad Date and ", "badTime", json);
        Assertions.assertNull(ldt);
    }

    @Test
    void testConvertDate() {
    }

    @Test
    void normalize() {
    }

    @Test
    void isEditable() {
    }

    @Test
    void getConverter() {
    }

    @Test
    void getUnit() {
    }

    @Test
    void getPrettyName() {
    }

    @Test
    void checkDimension() {
    }

    @Test
    void getDimension() {
    }

    @Test
    void getPrettyUnit() {
        Assertions.assertEquals(Tools.getPrettyUnit("m/s2"), "m/s2");
    }

    @Test
    void isBalanced() {
        Assertions.assertTrue(Tools.isBalanced(""));
        Assertions.assertTrue(Tools.isBalanced("{}"));
        Assertions.assertTrue(Tools.isBalanced("()"));
        Assertions.assertTrue(Tools.isBalanced("[]"));
        Assertions.assertTrue(Tools.isBalanced("([])"));
        Assertions.assertFalse(Tools.isBalanced("("));
        Assertions.assertFalse(Tools.isBalanced("]"));
        Assertions.assertFalse(Tools.isBalanced("(]"));
        Assertions.assertFalse(Tools.isBalanced("({}"));
        Assertions.assertFalse(Tools.isBalanced("([)]"));
    }

    @Test
    void clearFile() {
    }

    @Test
    void autoResize() {
    }

    @Test
    void reverseString() {
        Assertions.assertEquals(",nbvcxwmlkj", Tools.reverseString("jklmwxcvbn,"));
        Assertions.assertEquals("{]oaze]é}", Tools.reverseString("}é]ezao]{"));
    }
}
