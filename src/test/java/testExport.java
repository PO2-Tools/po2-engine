/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

 
 import java.io.File;
 import java.io.IOException;
 import java.io.InvalidObjectException;
 import java.net.URISyntaxException;
 import java.security.KeyManagementException;
 import java.security.KeyStoreException;
 import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer;
 import org.junit.jupiter.api.Order;
 import org.junit.jupiter.api.TestMethodOrder;
 
 import org.apache.commons.io.FileUtils;
 
 import fr.inrae.po2engine.externalTools.CloudConnector;
 import fr.inrae.po2engine.importExport.ExportDataNouveauFormat;
 import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.OntoData;
import fr.inrae.po2engine.model.dataModel.ProjectFile;
 import fr.inrae.po2engine.utils.DataTools;
 import fr.inrae.po2engine.utils.PO2Properties;
import javafx.beans.property.SimpleDoubleProperty;
import fr.inrae.po2engine.model.Data;
 
 @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
 public class testExport {
     @org.junit.jupiter.api.Test
     @Order(1)
     void init() throws InvalidObjectException, URISyntaxException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
         //Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
         Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.hg.login"), PO2Properties.getProperty("ldap.hg.password"));
         CloudConnector.listOntology(null, 1.0);
     }
     
     @org.junit.jupiter.api.Test
     @Order(2)
     void export() throws IOException {
 
 
         //String nomProjet = "Planet-Milling test";
         //String nomProjet = "Training guillemin";
         //String nomProjet = "Sorgho vegetal powder";
         //String nomProjet = "TestTransmat";
         //String nomProjet = "SMARTPOP powder";
         //String nomProjet = "WasteWaterTreatment";
         //String nomProjet = "Material biodegradation Glopack";
         //String nomProjet = "Material biodegradation Muniyasamy et al 2019";
         //String nomProjet = "Material biodegradation Weng & Wang & Wang";
         //String nomProjet = "PHA production GLOPACK";
         //String nomProjet = "PHA production ECOBIOCAP";
         //String nomProjet = "Fromages AOP";
         //String nomProjet = "Planet-Milling itineraries for a collection of wood byproducts";
         //String nomProjet = "LCA Cheeses";
         //String nomProjet = "Brdlik et al, 2022";
         //String nomProjet = "David et al, 2020";
         String nomProjet = "IDCOLAIT";

         Boolean ok = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
         Assert.assertTrue(ok);

         CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
         Data data = Datas.getData(nomProjet);
         data.load();
         
         DataTools.analyseFiles(data);
         ProjectFile projectFile = data.getProjectFile();
         
         File exportFile = projectFile.exportProject();
         File destinationFile = File.createTempFile("asupprimer",".xlsx");
         if (destinationFile != null) {
             FileUtils.copyFile(exportFile, destinationFile);
         }
     }
 
     @org.junit.jupiter.api.Test
     @Order(3)
     void removeProject() throws IOException {
         Boolean ok = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
         Assert.assertTrue(ok);
         ok = CloudConnector.removeProjectData("test unittester publie");
         Assert.assertTrue(ok);
         ok = CloudConnector.removeProjectData("test unittester non publie");
         Assert.assertTrue(ok);
     }
}
 