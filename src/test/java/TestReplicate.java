/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

import fr.inrae.po2engine.exception.AlreayLockException;
import fr.inrae.po2engine.exception.CantWriteException;
import fr.inrae.po2engine.exception.LocalLockException;
import fr.inrae.po2engine.exception.NotLoginException;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.ComplexField;
import fr.inrae.po2engine.model.Data;
import fr.inrae.po2engine.model.Datas;
import fr.inrae.po2engine.model.dataModel.ObservationFile;
import fr.inrae.po2engine.utils.DataTools;
import fr.inrae.po2engine.utils.PO2Properties;
import javafx.beans.property.SimpleDoubleProperty;
import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.net.URISyntaxException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestReplicate {

    public static String dataName = "data_test";

    @org.junit.jupiter.api.Test
    @Order(1)
    void addAReplicate() throws IOException, CantWriteException, NotLoginException, AlreayLockException, URISyntaxException, LocalLockException {
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(resOK);

        CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
        Data data = Datas.getData(dataName);
        Assert.assertNotNull(data);
        data.load();
        data.lockMode();
        DataTools.analyseFiles(data);

        Assert.assertTrue(data.getListGeneralFile().size() > 0);
        Assert.assertTrue(data.getListGeneralFile().get(0).constructData());
        Assert.assertTrue(data.getListGeneralFile().get(0).getItinerary().size() > 0);

        data.getListGeneralFile().get(0).getListReplicate().clear();
        Assert.assertTrue(data.getListGeneralFile().get(0).getListReplicate().isEmpty());

        data.getListGeneralFile().get(0).addReplicate("Replicate 1", "description of the replicate");
        data.getProjectFile().saveData(false);
        Assert.assertTrue(CloudConnector.update(data));
        Assert.assertTrue(data.unlockMode());
    }

    @org.junit.jupiter.api.Test
    @Order(2)
    void listReplicate(){
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(resOK);
        CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
        Data data = Datas.getData(dataName);
        Assert.assertNotNull(data);
        data.load();
        DataTools.analyseFiles(data);

        Assert.assertTrue(data.getListGeneralFile().size() > 0);
        Assert.assertTrue(data.getListGeneralFile().get(0).constructData());

        Assert.assertFalse(data.getListGeneralFile().get(0).getListReplicate().isEmpty());
        System.out.println(data.getListGeneralFile().get(0).getReplicateAsString());
    }

    @org.junit.jupiter.api.Test
    @Order(3)
    void toStringWithReplicate() {
        ComplexField cf = new ComplexField("initial value");
        System.out.println(cf.toString());
        Assert.assertTrue("initial value".equalsIgnoreCase(cf.toString()));

        cf.setValueWithReplicate(1,"replicate 1");
        System.out.println(cf.toString());
        Assert.assertTrue("[{\"replicate\":0,\"value\":\"initial value\"},{\"replicate\":1,\"value\":\"replicate 1\"}]".equalsIgnoreCase(cf.toString()));
    }

    @org.junit.jupiter.api.Test
    @Order(4)
    void addReplicateObservation() throws IOException, CantWriteException, NotLoginException, AlreayLockException, URISyntaxException, LocalLockException {
        Boolean resOK = CloudConnector.initUser(PO2Properties.getProperty("ldap.unitTester.login"), PO2Properties.getProperty("ldap.unitTester.password"));
        Assert.assertTrue(resOK);
        CloudConnector.listData(new SimpleDoubleProperty(0.0),0.5);		// Paramètres = ?
        Data data = Datas.getData(dataName);
        Assert.assertNotNull(data);
        data.load();
        data.lockMode();
        DataTools.analyseFiles(data);

        Assert.assertTrue(data.getListGeneralFile().size() > 0);
        Assert.assertTrue(data.getListGeneralFile().get(0).constructData());
        Assert.assertTrue(data.getListGeneralFile().get(0).getItinerary().size() > 0);

        data.getListGeneralFile().get(0).getItinerary().get(0).getListObservation().forEach(o -> o.removeFile());
        ObservationFile o = new ObservationFile(data.getListGeneralFile().get(0).getItinerary().get(0));
        o.getCDuree().setValueWithReplicate(0, "duree replicate 0");
        o.getCDuree().setValueWithReplicate(1, "duree replicate 1");
        o.setHour("hour replicate 0");
        data.getProjectFile().saveData(false);
        CloudConnector.update(data);
        data.unlockMode();
    }
}
